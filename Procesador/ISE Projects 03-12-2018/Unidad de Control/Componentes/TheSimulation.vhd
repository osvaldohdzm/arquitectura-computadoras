LIBRARY ieee;
LIBRARY std;
use std.textio.all;
USE ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_textio.all;

ENTITY TheSimulation IS
END TheSimulation;
 
ARCHITECTURE behavior OF TheSimulation IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Componentes
    PORT(
         codFunc : IN  std_logic_vector(3 downto 0);
         codOper : IN  std_logic_vector(4 downto 0);
         banderas : IN  std_logic_vector(3 downto 0);
         clk : IN  std_logic;
         clr : IN  std_logic;
         lf : IN  std_logic;
         s : OUT  std_logic_vector(19 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal codFunc : std_logic_vector(3 downto 0) := (others => '0');
   signal codOper : std_logic_vector(4 downto 0) := (others => '0');
   signal banderas : std_logic_vector(3 downto 0) := (others => '0');
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';
   signal lf : std_logic := '0';

 	--Outputs
   signal s : std_logic_vector(19 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Componentes PORT MAP (
          codFunc => codFunc,
          codOper => codOper,
          banderas => banderas,
          clk => clk,
          clr => clr,
          lf => lf,
          s => s
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
      -- insert stimulus here 
		file ARCH_RES : TEXT;
		file ARCH_VEC : TEXT;
		
		variable LINEA_RES : line;
		variable LINEA_VEC : line;
		

		VARIABLE VAR_codFunc    :  STD_LOGIC_VECTOR(3 DOWNTO 0);
		VARIABLE VAR_codOper    :  STD_LOGIC_VECTOR(4 DOWNTO 0);
		VARIABLE VAR_banderas  :  STD_LOGIC_VECTOR(3 DOWNTO 0);
		VARIABLE VAR_clr     :  STD_LOGIC;
		VARIABLE VAR_lf     :  STD_LOGIC;
		VARIABLE VAR_s  :  STD_LOGIC_VECTOR(19 DOWNTO 0);
		
		VARIABLE CADENA : STRING(1 TO 10);
		VARIABLE CADENA_I : STRING(1 TO 18);
		VARIABLE CADENA_X : STRING(1 TO 5);

		begin		
			file_open(ARCH_RES, "./SALIDA.TXT", WRITE_MODE); 	
			file_open(ARCH_VEC, "./ENTRADA.TXT", READ_MODE); 	

			CADENA := "OP_CODE   ";
			write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
			CADENA := "FUN_CODE  ";
			write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
			CADENA := "BANDERAS  ";
			write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
			CADENA_x := "CLR  ";
			write(LINEA_RES, CADENA_X, right, CADENA_X'LENGTH+1);
			CADENA_x := "LF   ";
			write(LINEA_RES, CADENA_X, right, CADENA_X'LENGTH+1);
			CADENA_I := "MICROINSTRUCCION  ";
			write(LINEA_RES, CADENA_I, right, CADENA_I'LENGTH+1);
			CADENA_x := "NIVEL";
			write(LINEA_RES, CADENA_X, right, CADENA_X'LENGTH+1);

			writeline(ARCH_RES,LINEA_RES);
			--wait for 10 ns;	
			
			FOR I IN 0 TO 51 LOOP --52 PRUEBAS
				readline(ARCH_VEC,LINEA_VEC);
				
				read(LINEA_VEC, VAR_codOper);
				codOper<=VAR_codOper;

				read(LINEA_VEC, VAR_codFunc);
				codFunc<=VAR_codFunc;
				
				--BANDERAS
				read(LINEA_VEC, VAR_banderas);
				banderas<=VAR_banderas;
				
				read(LINEA_VEC, VAR_clr);
				CLR<=VAR_clr;
				
				read(LINEA_VEC, VAR_lf);
				LF<=VAR_lf;
				
				
				WAIT UNTIL RISING_EDGE(CLK);
				--wait for 4 ns;
				VAR_s := S;
		
				write(LINEA_RES, VAR_codOper, 	right, 5);
				write(LINEA_RES, VAR_codFunc, 	right, 5);
				write(LINEA_RES, VAR_banderas, 	right, 5);
				write (LINEA_RES, VAR_clr , right, 5);
				write (LINEA_RES, VAR_lf , right, 5);
				write (LINEA_RES, "   " , right, 5);
				write (LINEA_RES, VAR_s , right, 5);
				write (LINEA_RES, "ALTO" , right, 5);
				writeline(ARCH_RES,LINEA_RES);
				
				WAIT UNTIL FALLING_EDGE(CLK);
				--wait for 1 ns;
				VAR_s := S;
		
				write(LINEA_RES, VAR_codOper, 	right, 5);
				write(LINEA_RES, VAR_codFunc, 	right, 5);
				write(LINEA_RES, VAR_banderas, 	right, 5);
				write (LINEA_RES, VAR_clr , right, 5);
				write (LINEA_RES, VAR_lf , right, 5);
				write (LINEA_RES, "   " , right, 5);
				write (LINEA_RES, VAR_s , right, 5);
				write (LINEA_RES, "BAJO" , right, 5);
				writeline(ARCH_RES,LINEA_RES);

			end loop;
			
			file_close(ARCH_VEC);
			file_close(ARCH_RES);  

			wait for CLK_period*10;

			wait;
		end process;

END;
