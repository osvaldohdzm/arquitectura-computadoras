/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/leonardo faydella/Desktop/ProyectoFinal/barrel_shifter/Barrel.vhd";



static void work_a_1692363465_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned char t5;
    int t6;
    int t7;
    char *t8;
    char *t9;
    int t10;
    int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    char *t15;
    unsigned char t16;
    unsigned char t17;
    char *t18;
    char *t19;
    int t20;
    int t21;
    char *t22;
    char *t23;
    int t24;
    int t25;
    unsigned char t26;
    char *t27;
    char *t28;
    int t29;
    int t30;
    unsigned int t31;
    unsigned int t32;
    unsigned int t33;
    char *t34;
    int t35;
    int t36;

LAB0:    xsi_set_current_line(18, ng0);
    t1 = (t0 + 1352U);
    t2 = *((char **)t1);
    t1 = (t0 + 1808U);
    t3 = *((char **)t1);
    t1 = (t3 + 0);
    memcpy(t1, t2, 16U);
    xsi_set_current_line(19, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t4 = *((unsigned char *)t2);
    t5 = (t4 == (unsigned char)2);
    if (t5 != 0)
        goto LAB2;

LAB4:    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t4 = *((unsigned char *)t2);
    t5 = (t4 == (unsigned char)3);
    if (t5 != 0)
        goto LAB21;

LAB22:
LAB3:    xsi_set_current_line(45, ng0);
    t1 = (t0 + 1808U);
    t2 = *((char **)t1);
    t1 = (t0 + 3192);
    t3 = (t1 + 56U);
    t8 = *((char **)t3);
    t9 = (t8 + 56U);
    t15 = *((char **)t9);
    memcpy(t15, t2, 16U);
    xsi_driver_first_trans_fast_port(t1);
    t1 = (t0 + 3112);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(20, ng0);
    t1 = (t0 + 5245);
    *((int *)t1) = 0;
    t3 = (t0 + 5249);
    *((int *)t3) = 3;
    t6 = 0;
    t7 = 3;

LAB5:    if (t6 <= t7)
        goto LAB6;

LAB8:    goto LAB3;

LAB6:    xsi_set_current_line(21, ng0);
    t8 = (t0 + 1192U);
    t9 = *((char **)t8);
    t8 = (t0 + 5245);
    t10 = *((int *)t8);
    t11 = (t10 - 3);
    t12 = (t11 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t8));
    t13 = (1U * t12);
    t14 = (0 + t13);
    t15 = (t9 + t14);
    t16 = *((unsigned char *)t15);
    t17 = (t16 == (unsigned char)3);
    if (t17 != 0)
        goto LAB9;

LAB11:
LAB10:
LAB7:    t1 = (t0 + 5245);
    t6 = *((int *)t1);
    t2 = (t0 + 5249);
    t7 = *((int *)t2);
    if (t6 == t7)
        goto LAB8;

LAB20:    t10 = (t6 + 1);
    t6 = t10;
    t3 = (t0 + 5245);
    *((int *)t3) = t6;
    goto LAB5;

LAB9:    xsi_set_current_line(22, ng0);
    t18 = (t0 + 5253);
    *((int *)t18) = 15;
    t19 = (t0 + 5257);
    *((int *)t19) = 0;
    t20 = 15;
    t21 = 0;

LAB12:    if (t20 >= t21)
        goto LAB13;

LAB15:    goto LAB10;

LAB13:    xsi_set_current_line(23, ng0);
    t22 = (t0 + 5253);
    t23 = (t0 + 5245);
    t24 = xsi_vhdl_pow(2, *((int *)t23));
    t25 = *((int *)t22);
    t26 = (t25 < t24);
    if (t26 != 0)
        goto LAB16;

LAB18:    xsi_set_current_line(26, ng0);
    t1 = (t0 + 1808U);
    t2 = *((char **)t1);
    t1 = (t0 + 5253);
    t3 = (t0 + 5245);
    t10 = xsi_vhdl_pow(2, *((int *)t3));
    t11 = *((int *)t1);
    t24 = (t11 - t10);
    t25 = (t24 - 15);
    t12 = (t25 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, t24);
    t13 = (1U * t12);
    t14 = (0 + t13);
    t8 = (t2 + t14);
    t4 = *((unsigned char *)t8);
    t9 = (t0 + 1808U);
    t15 = *((char **)t9);
    t9 = (t0 + 5253);
    t29 = *((int *)t9);
    t30 = (t29 - 15);
    t31 = (t30 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t9));
    t32 = (1U * t31);
    t33 = (0 + t32);
    t18 = (t15 + t33);
    *((unsigned char *)t18) = t4;

LAB17:
LAB14:    t1 = (t0 + 5253);
    t20 = *((int *)t1);
    t2 = (t0 + 5257);
    t21 = *((int *)t2);
    if (t20 == t21)
        goto LAB15;

LAB19:    t10 = (t20 + -1);
    t20 = t10;
    t3 = (t0 + 5253);
    *((int *)t3) = t20;
    goto LAB12;

LAB16:    xsi_set_current_line(24, ng0);
    t27 = (t0 + 1808U);
    t28 = *((char **)t27);
    t27 = (t0 + 5253);
    t29 = *((int *)t27);
    t30 = (t29 - 15);
    t31 = (t30 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t27));
    t32 = (1U * t31);
    t33 = (0 + t32);
    t34 = (t28 + t33);
    *((unsigned char *)t34) = (unsigned char)2;
    goto LAB17;

LAB21:    xsi_set_current_line(32, ng0);
    t1 = (t0 + 5261);
    *((int *)t1) = 0;
    t3 = (t0 + 5265);
    *((int *)t3) = 3;
    t6 = 0;
    t7 = 3;

LAB23:    if (t6 <= t7)
        goto LAB24;

LAB26:    goto LAB3;

LAB24:    xsi_set_current_line(33, ng0);
    t8 = (t0 + 1192U);
    t9 = *((char **)t8);
    t8 = (t0 + 5261);
    t10 = *((int *)t8);
    t11 = (t10 - 3);
    t12 = (t11 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t8));
    t13 = (1U * t12);
    t14 = (0 + t13);
    t15 = (t9 + t14);
    t16 = *((unsigned char *)t15);
    t17 = (t16 == (unsigned char)3);
    if (t17 != 0)
        goto LAB27;

LAB29:
LAB28:
LAB25:    t1 = (t0 + 5261);
    t6 = *((int *)t1);
    t2 = (t0 + 5265);
    t7 = *((int *)t2);
    if (t6 == t7)
        goto LAB26;

LAB38:    t10 = (t6 + 1);
    t6 = t10;
    t3 = (t0 + 5261);
    *((int *)t3) = t6;
    goto LAB23;

LAB27:    xsi_set_current_line(34, ng0);
    t18 = (t0 + 5269);
    *((int *)t18) = 0;
    t19 = (t0 + 5273);
    *((int *)t19) = 15;
    t20 = 0;
    t21 = 15;

LAB30:    if (t20 <= t21)
        goto LAB31;

LAB33:    goto LAB28;

LAB31:    xsi_set_current_line(35, ng0);
    t22 = (t0 + 5269);
    t24 = *((int *)t22);
    t25 = (-(t24));
    t29 = (t25 + 15);
    t23 = (t0 + 5261);
    t30 = xsi_vhdl_pow(2, *((int *)t23));
    t26 = (t29 < t30);
    if (t26 != 0)
        goto LAB34;

LAB36:    xsi_set_current_line(38, ng0);
    t1 = (t0 + 1808U);
    t2 = *((char **)t1);
    t1 = (t0 + 5269);
    t3 = (t0 + 5261);
    t10 = xsi_vhdl_pow(2, *((int *)t3));
    t11 = *((int *)t1);
    t24 = (t11 + t10);
    t25 = (t24 - 15);
    t12 = (t25 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, t24);
    t13 = (1U * t12);
    t14 = (0 + t13);
    t8 = (t2 + t14);
    t4 = *((unsigned char *)t8);
    t9 = (t0 + 1808U);
    t15 = *((char **)t9);
    t9 = (t0 + 5269);
    t29 = *((int *)t9);
    t30 = (t29 - 15);
    t31 = (t30 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t9));
    t32 = (1U * t31);
    t33 = (0 + t32);
    t18 = (t15 + t33);
    *((unsigned char *)t18) = t4;

LAB35:
LAB32:    t1 = (t0 + 5269);
    t20 = *((int *)t1);
    t2 = (t0 + 5273);
    t21 = *((int *)t2);
    if (t20 == t21)
        goto LAB33;

LAB37:    t10 = (t20 + 1);
    t20 = t10;
    t3 = (t0 + 5269);
    *((int *)t3) = t20;
    goto LAB30;

LAB34:    xsi_set_current_line(36, ng0);
    t27 = (t0 + 1808U);
    t28 = *((char **)t27);
    t27 = (t0 + 5269);
    t35 = *((int *)t27);
    t36 = (t35 - 15);
    t31 = (t36 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t27));
    t32 = (1U * t31);
    t33 = (0 + t32);
    t34 = (t28 + t33);
    *((unsigned char *)t34) = (unsigned char)2;
    goto LAB35;

}


extern void work_a_1692363465_3212880686_init()
{
	static char *pe[] = {(void *)work_a_1692363465_3212880686_p_0};
	xsi_register_didat("work_a_1692363465_3212880686", "isim/TESTBENCH_BARREL_isim_beh.exe.sim/work/a_1692363465_3212880686.didat");
	xsi_register_executes(pe);
}
