
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY t_nivel IS
END t_nivel;
 
ARCHITECTURE behavior OF t_nivel IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT nivel
    PORT(
         clk : IN  std_logic;
         clr : IN  std_logic;
         na : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';

 	--Outputs
   signal na : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: nivel PORT MAP (
          clk => clk,
          clr => clr,
          na => na
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      clr <= '1';
		wait for 25 ns;
		clr <= '0';
      wait;
   end process;

END;
