/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x1048c146 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/osvaldohm/Desktop/Mem_datos/TB_DATOS.vhd";
extern char *STD_TEXTIO;
extern char *IEEE_P_3564397177;
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
void ieee_p_3564397177_sub_1496949865_91900896(char *, char *, char *, unsigned char , unsigned char , int );
void ieee_p_3564397177_sub_2743816878_91900896(char *, char *, char *, char *);
void ieee_p_3564397177_sub_3205433008_91900896(char *, char *, char *, char *, char *, unsigned char , int );
void ieee_p_3564397177_sub_3988856810_91900896(char *, char *, char *, char *, char *);


static void work_a_2377627064_2372691052_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    int64 t7;
    int64 t8;

LAB0:    t1 = (t0 + 2252U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(58, ng0);
    t2 = (t0 + 2636);
    t3 = (t2 + 32U);
    t4 = *((char **)t3);
    t5 = (t4 + 40U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(59, ng0);
    t2 = (t0 + 1132U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 2152);
    xsi_process_wait(t2, t8);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(60, ng0);
    t2 = (t0 + 2636);
    t3 = (t2 + 32U);
    t4 = *((char **)t3);
    t5 = (t4 + 40U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(61, ng0);
    t2 = (t0 + 1132U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 2152);
    xsi_process_wait(t2, t8);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    goto LAB2;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

}

static void work_a_2377627064_2372691052_p_1(char *t0)
{
    char t5[16];
    char t10[16];
    char t11[16];
    char t12[16];
    char t13[16];
    char t23[8];
    char t24[8];
    char t25[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    int t8;
    unsigned int t9;
    int64 t14;
    int t15;
    char *t16;
    unsigned char t17;
    char *t18;
    char *t19;
    char *t20;
    unsigned int t21;
    unsigned int t22;
    int t26;

LAB0:    t1 = (t0 + 2396U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(75, ng0);
    t2 = (t0 + 1656U);
    t3 = (t0 + 5219);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 12;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (12 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)0);
    xsi_set_current_line(76, ng0);
    t2 = (t0 + 1592U);
    t3 = (t0 + 5231);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 13;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (13 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)1);
    xsi_set_current_line(78, ng0);
    t2 = (t0 + 5244);
    t4 = (t0 + 1924U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 10U);
    xsi_set_current_line(79, ng0);
    t2 = (t0 + 2296);
    t3 = (t0 + 1760U);
    t4 = (t0 + 1924U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t10, t7, 10U);
    t6 = (t0 + 5040U);
    t8 = (10U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t10, t6, (unsigned char)0, t8);
    xsi_set_current_line(80, ng0);
    t2 = (t0 + 5254);
    t4 = (t0 + 1924U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 10U);
    xsi_set_current_line(81, ng0);
    t2 = (t0 + 2296);
    t3 = (t0 + 1760U);
    t4 = (t0 + 1924U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t11, t7, 10U);
    t6 = (t0 + 5040U);
    t8 = (10U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t11, t6, (unsigned char)0, t8);
    xsi_set_current_line(82, ng0);
    t2 = (t0 + 5264);
    t4 = (t0 + 1924U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 10U);
    xsi_set_current_line(83, ng0);
    t2 = (t0 + 2296);
    t3 = (t0 + 1760U);
    t4 = (t0 + 1924U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t12, t7, 10U);
    t6 = (t0 + 5040U);
    t8 = (10U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t12, t6, (unsigned char)0, t8);
    xsi_set_current_line(84, ng0);
    t2 = (t0 + 5274);
    t4 = (t0 + 1924U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 10U);
    xsi_set_current_line(85, ng0);
    t2 = (t0 + 2296);
    t3 = (t0 + 1760U);
    t4 = (t0 + 1924U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t13, t7, 10U);
    t6 = (t0 + 5040U);
    t8 = (10U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t13, t6, (unsigned char)0, t8);
    xsi_set_current_line(86, ng0);
    t2 = (t0 + 2296);
    t3 = (t0 + 1592U);
    t4 = (t0 + 1760U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    xsi_set_current_line(88, ng0);
    t14 = (100 * 1000LL);
    t2 = (t0 + 2296);
    xsi_process_wait(t2, t14);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(89, ng0);
    t2 = (t0 + 5284);
    *((int *)t2) = 0;
    t3 = (t0 + 5288);
    *((int *)t3) = 5;
    t8 = 0;
    t15 = 5;

LAB8:    if (t8 <= t15)
        goto LAB9;

LAB11:    xsi_set_current_line(114, ng0);
    t2 = (t0 + 1656U);
    std_textio_file_close(t2);
    xsi_set_current_line(115, ng0);
    t2 = (t0 + 1592U);
    std_textio_file_close(t2);
    xsi_set_current_line(116, ng0);

LAB21:    *((char **)t1) = &&LAB22;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB9:    xsi_set_current_line(90, ng0);
    t4 = (t0 + 2296);
    t6 = (t0 + 1656U);
    t7 = (t0 + 1800U);
    std_textio_readline(STD_TEXTIO, t4, t6, t7);
    xsi_set_current_line(92, ng0);
    t2 = (t0 + 2296);
    t3 = (t0 + 1800U);
    t4 = (t0 + 1336U);
    t6 = *((char **)t4);
    t4 = (t0 + 5024U);
    ieee_p_3564397177_sub_3988856810_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(93, ng0);
    t2 = (t0 + 1336U);
    t3 = *((char **)t2);
    t2 = (t0 + 2672);
    t4 = (t2 + 32U);
    t6 = *((char **)t4);
    t7 = (t6 + 40U);
    t16 = *((char **)t7);
    memcpy(t16, t3, 8U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(95, ng0);
    t2 = (t0 + 2296);
    t3 = (t0 + 1800U);
    t4 = (t0 + 1268U);
    t6 = *((char **)t4);
    t4 = (t0 + 5008U);
    ieee_p_3564397177_sub_3988856810_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(96, ng0);
    t2 = (t0 + 1268U);
    t3 = *((char **)t2);
    t4 = ((IEEE_P_2592010699) + 2332);
    t6 = (t0 + 5008U);
    t2 = xsi_base_array_concat(t2, t5, t4, (char)99, (unsigned char)2, (char)97, t3, t6, (char)101);
    t9 = (1U + 8U);
    t17 = (9U != t9);
    if (t17 == 1)
        goto LAB12;

LAB13:    t7 = (t0 + 2708);
    t16 = (t7 + 32U);
    t18 = *((char **)t16);
    t19 = (t18 + 40U);
    t20 = *((char **)t19);
    memcpy(t20, t2, 9U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(98, ng0);
    t2 = (t0 + 2296);
    t3 = (t0 + 1800U);
    t4 = (t0 + 1404U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_2743816878_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(99, ng0);
    t2 = (t0 + 1404U);
    t3 = *((char **)t2);
    t17 = *((unsigned char *)t3);
    t2 = (t0 + 2744);
    t4 = (t2 + 32U);
    t6 = *((char **)t4);
    t7 = (t6 + 40U);
    t16 = *((char **)t7);
    *((unsigned char *)t16) = t17;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(101, ng0);

LAB16:    t2 = (t0 + 2592);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB17;
    goto LAB1;

LAB10:    t2 = (t0 + 5284);
    t8 = *((int *)t2);
    t3 = (t0 + 5288);
    t15 = *((int *)t3);
    if (t8 == t15)
        goto LAB11;

LAB18:    t26 = (t8 + 1);
    t8 = t26;
    t4 = (t0 + 5284);
    *((int *)t4) = t8;
    goto LAB8;

LAB12:    xsi_size_not_matching(9U, t9, 0);
    goto LAB13;

LAB14:    t4 = (t0 + 2592);
    *((int *)t4) = 0;
    xsi_set_current_line(103, ng0);
    t2 = (t0 + 776U);
    t3 = *((char **)t2);
    t2 = (t0 + 1336U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 8U);
    xsi_set_current_line(104, ng0);
    t2 = (t0 + 868U);
    t3 = *((char **)t2);
    t9 = (8 - 7);
    t21 = (t9 * 1U);
    t22 = (0 + t21);
    t2 = (t3 + t22);
    t4 = (t0 + 1268U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    memcpy(t4, t2, 8U);
    xsi_set_current_line(105, ng0);
    t2 = (t0 + 592U);
    t3 = *((char **)t2);
    t17 = *((unsigned char *)t3);
    t2 = (t0 + 1404U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    *((unsigned char *)t2) = t17;
    xsi_set_current_line(106, ng0);
    t2 = (t0 + 960U);
    t3 = *((char **)t2);
    t2 = (t0 + 1200U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 8U);
    xsi_set_current_line(107, ng0);
    t2 = (t0 + 2296);
    t3 = (t0 + 1760U);
    t4 = (t0 + 1336U);
    t6 = *((char **)t4);
    memcpy(t23, t6, 8U);
    t4 = (t0 + 5024U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t23, t4, (unsigned char)0, 8);
    xsi_set_current_line(108, ng0);
    t2 = (t0 + 2296);
    t3 = (t0 + 1760U);
    t4 = (t0 + 1268U);
    t6 = *((char **)t4);
    memcpy(t24, t6, 8U);
    t4 = (t0 + 5008U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t24, t4, (unsigned char)0, 10);
    xsi_set_current_line(109, ng0);
    t2 = (t0 + 2296);
    t3 = (t0 + 1760U);
    t4 = (t0 + 1404U);
    t6 = *((char **)t4);
    t17 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t17, (unsigned char)0, 14);
    xsi_set_current_line(110, ng0);
    t2 = (t0 + 2296);
    t3 = (t0 + 1760U);
    t4 = (t0 + 1200U);
    t6 = *((char **)t4);
    memcpy(t25, t6, 8U);
    t4 = (t0 + 4992U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t25, t4, (unsigned char)0, 8);
    xsi_set_current_line(112, ng0);
    t2 = (t0 + 2296);
    t3 = (t0 + 1592U);
    t4 = (t0 + 1760U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    goto LAB10;

LAB15:    t3 = (t0 + 660U);
    t17 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t3, 0U, 0U);
    if (t17 == 1)
        goto LAB14;
    else
        goto LAB16;

LAB17:    goto LAB15;

LAB19:    goto LAB2;

LAB20:    goto LAB19;

LAB22:    goto LAB20;

}


extern void work_a_2377627064_2372691052_init()
{
	static char *pe[] = {(void *)work_a_2377627064_2372691052_p_0,(void *)work_a_2377627064_2372691052_p_1};
	xsi_register_didat("work_a_2377627064_2372691052", "isim/TB_DATOS_isim_beh.exe.sim/work/a_2377627064_2372691052.didat");
	xsi_register_executes(pe);
}
