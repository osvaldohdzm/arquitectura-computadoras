-- Unidad de control 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity CartaASM_codigo is
    Port ( clr, clk : in  STD_LOGIC;
           ini : in  STD_LOGIC;
           a0, z : in  STD_LOGIC;
           la, ea : out  STD_LOGIC;
           ec, eb, lb : out  STD_LOGIC);
end CartaASM_codigo;

architecture Behavioral of CartaASM_codigo is
	type estados is (estado_0,estado_1,estado_2);
	signal estado_actual, estado_siguiente : estados;
begin

	control: process(clr,clk)
		begin
			if(clr = '1') then
				estado_actual <= estado_0;
			elsif( clk'event and clk = '1' ) then
				estado_actual <= estado_siguiente;
			end if;
	end process control;
	
	estado: process(ini,a0,z,estado_actual)
		begin
			la <= '0';
			ea <= '0';
			ec <= '0';
			eb <= '0';
			lb <= '0';
			case estado_actual is 
				when estado_0 =>
					lb <= '1' ;
					if( ini = '1' ) then
						estado_siguiente <= estado_1;
					else
						la <= '1';
						estado_siguiente <= estado_0;
					end if;
				when estado_1 =>
					ea <= '1';
					if( z = '1' ) then
						estado_siguiente <= estado_2;
					else
						if( a0 = '1' ) then
							eb <= '1';
							estado_siguiente <= estado_1;
						else
							estado_siguiente <= estado_1;
						end if;
					end if;
				when estado_2 =>
					ec <= '1';
					if( ini = '1' ) then
						estado_siguiente <= estado_2;
					else
						estado_siguiente <= estado_0;
					end if;
			end case;
	end process estado;

end Behavioral;

