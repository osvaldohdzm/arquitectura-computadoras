-- ESCOMips | Pr�ctica 5 - Memoria de Datos | Marco Antonio Rubio Cort�s

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use iEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity practica5 is
	generic
	(
		wordSize : integer := 16;											-- Tama�o de palabra
		busSize  : integer := 11												-- Tama�o del bus de direcciones
	);
	port
	(
		dir		: in 	std_logic_vector(busSize-1 downto 0);
		d_in		: in 	std_logic_vector(wordSize-1 downto 0);
		d_out		: out	std_logic_vector(wordSize-1 downto 0);
		
		WD					: in	std_logic;
		clk				: in	std_logic
	);	
end practica5;

architecture A_practica5 of practica5 is
-- Archivo de la memoria de datos
type memoria is array (0 to ((2**busSize)-1)) of std_logic_vector((wordSize-1) downto 0);
signal dataMemory: memoria;

begin
	
	-- Lecturas as�ncronas
	d_out <= dataMemory(conv_integer(dir));							-- Lee de la localidad dada por dir y lo almacena en d_out
	
	-- Memoria de Datos
	memoriaDatos: process(clk)
	begin
		if(rising_edge(clk)) then
			if(WD = '1') then													-- Escritura en la memoria de datos
				dataMemory(conv_integer(dir)) <= d_in;					-- Escribe en la localidad dada por dir lo que est� en d_in
			end if;
		end if;
	end process;

end A_practica5;

