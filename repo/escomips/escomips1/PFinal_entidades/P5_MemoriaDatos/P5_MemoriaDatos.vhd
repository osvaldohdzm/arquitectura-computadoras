library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity P5_MemoriaDatos is
generic(
	dir : integer := 11;
	datos : integer := 16
	);
    Port ( dir_w : in  STD_LOGIC_VECTOR (dir-1 downto 0);
			  data_in : in STD_LOGIC_VECTOR (datos-1 downto 0);
			  clk, wd : in  STD_LOGIC;
           data_out : out  STD_LOGIC_VECTOR (datos-1 downto 0));
end P5_MemoriaDatos;

architecture Behavioral of P5_MemoriaDatos is
type memoriaD is array (0 to (2**dir)-1) of STD_LOGIC_VECTOR (datos-1 downto 0);
signal aux_mem : memoriaD;
begin

	process(clk)
	begin
		if(rising_edge(clk)) then
			if(wd = '1') then
				aux_mem(conv_integer(dir_w)) <= data_in;
			end if;
		end if;
	end process;
	
	data_out <= aux_mem(conv_integer(dir_w));

end Behavioral;

