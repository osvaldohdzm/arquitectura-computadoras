
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity sumbit is
    Port ( a,b,cin : in  STD_LOGIC;
           cout,s : out  STD_LOGIC);
end sumbit;

architecture Behavioral of sumbit is

begin
	s <= a xor b xor cin;
	cout <= (a and b) or (a and cin) or (b and cin);
	

end Behavioral;

