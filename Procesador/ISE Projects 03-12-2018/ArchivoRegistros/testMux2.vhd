
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY testMux2 IS
END testMux2;
 
ARCHITECTURE behavior OF testMux2 IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT mux2
    PORT(
         e1 : IN  std_logic_vector(15 downto 0);
         e2 : IN  std_logic_vector(15 downto 0);
         s : INOUT  std_logic_vector(15 downto 0);
         sel : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal e1 : std_logic_vector(15 downto 0) := (others => '0');
   signal e2 : std_logic_vector(15 downto 0) := (others => '0');
   signal sel : std_logic := '0';

	--BiDirs
   signal s : std_logic_vector(15 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: mux2 PORT MAP (
          e1 => e1,
          e2 => e2,
          s => s,
          sel => sel
        );
 

   -- Stimulus process
   stim_proc: process
   begin		
      e1 <= "1010101010101010";
		e2 <= "1100111011101100";
		sel <= '0';
		wait for 30 ns;
		
      e1 <= "1010101010101010";
		e2 <= "1100111011101100";
		sel <= '1';
		wait;
   end process;

END;
