library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity extensorSigno is
    Port ( d_in : in  STD_LOGIC_VECTOR (11 downto 0);
           d_out : out  STD_LOGIC_VECTOR (15 downto 0));
end extensorSigno;

architecture Behavioral of extensorSigno is

constant ceros : STD_LOGIC_VECTOR (3 downto 0) := "0000";
constant unos : STD_LOGIC_VECTOR (3 downto 0) := "1111";

begin
	process(d_in)
	begin
		if (d_in(11) = '1') then
			d_out <= unos & d_in;
		else
			d_out <= ceros & d_in;
		end if;
	end process;

end Behavioral;

