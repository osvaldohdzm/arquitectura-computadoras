library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity extensorDireccion is
    Port ( d_in : in  STD_LOGIC_VECTOR (11 downto 0);
           d_out : out  STD_LOGIC_VECTOR (15 downto 0));
end extensorDireccion;

architecture Behavioral of extensorDireccion is

begin
	d_out <= "0000" & d_in;

end Behavioral;