
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Mux4_sim IS
END Mux4_sim;
 
ARCHITECTURE behavior OF Mux4_sim IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Mux4
    PORT(
         in0 : IN  std_logic_vector(3 downto 0);
         in1 : IN  std_logic_vector(3 downto 0);
         salida : OUT  std_logic_vector(3 downto 0);
         sel : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal in0 : std_logic_vector(3 downto 0) := (others => '0');
   signal in1 : std_logic_vector(3 downto 0) := (others => '0');
   signal sel : std_logic := '0';

 	--Outputs
   signal salida : std_logic_vector(3 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Mux4 PORT MAP (
          in0 => in0,
          in1 => in1,
          salida => salida,
          sel => sel
        );

 

   -- Stimulus process
   stim_proc: process
   begin		
      -- insert stimulus here 
		in0 <= "1010";
		in1 <= "0101";
		sel <= '0';
      wait for 10 ns;
		
		in0 <= "1010";
		in1 <= "0101";
		sel <= '1';
      wait for 10 ns;
   end process;

END;
