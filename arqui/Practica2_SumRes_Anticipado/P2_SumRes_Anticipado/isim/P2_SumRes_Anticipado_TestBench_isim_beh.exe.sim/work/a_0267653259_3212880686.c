/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/Edgar Roa/Documents/8vo sem/Arquitectura/P2_SumRes_Anticipado/P2_SumRes_Anticipado.vhd";
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1605435078_503743352(char *, unsigned char , unsigned char );
unsigned char ieee_p_2592010699_sub_2507238156_503743352(char *, unsigned char , unsigned char );
unsigned char ieee_p_2592010699_sub_2545490612_503743352(char *, unsigned char , unsigned char );


static void work_a_0267653259_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    int t9;
    char *t10;
    int t11;
    int t12;
    char *t13;
    char *t14;
    char *t15;
    unsigned char t16;
    unsigned char t17;
    char *t18;
    int t19;
    int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    char *t24;
    int t25;
    int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    char *t30;
    char *t31;
    char *t32;
    int t33;
    int t34;
    int t35;
    int t36;
    int t37;
    int t38;
    int t39;
    int t40;
    char *t41;
    char *t42;
    char *t43;
    int t44;
    unsigned char t45;
    unsigned char t46;
    unsigned char t47;
    unsigned char t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;

LAB0:    xsi_set_current_line(25, ng0);
    t1 = (t0 + 1352U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 2328U);
    t4 = *((char **)t1);
    t5 = (0 - 4);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t4 + t8);
    *((unsigned char *)t1) = t3;
    xsi_set_current_line(27, ng0);
    t1 = (t0 + 6925);
    *((int *)t1) = 0;
    t2 = (t0 + 6929);
    *((int *)t2) = 3;
    t5 = 0;
    t9 = 3;

LAB2:    if (t5 <= t9)
        goto LAB3;

LAB5:    xsi_set_current_line(72, ng0);
    t1 = (t0 + 2328U);
    t2 = *((char **)t1);
    t5 = (4 - 4);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t3 = *((unsigned char *)t1);
    t4 = (t0 + 4016);
    t10 = (t4 + 56U);
    t13 = *((char **)t10);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    *((unsigned char *)t15) = t3;
    xsi_driver_first_trans_fast_port(t4);
    t1 = (t0 + 3872);
    *((int *)t1) = 1;

LAB1:    return;
LAB3:    xsi_set_current_line(28, ng0);
    t4 = (t0 + 1192U);
    t10 = *((char **)t4);
    t4 = (t0 + 6925);
    t11 = *((int *)t4);
    t12 = (t11 - 3);
    t6 = (t12 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t4));
    t7 = (1U * t6);
    t8 = (0 + t7);
    t13 = (t10 + t8);
    t3 = *((unsigned char *)t13);
    t14 = (t0 + 1352U);
    t15 = *((char **)t14);
    t16 = *((unsigned char *)t15);
    t17 = ieee_p_2592010699_sub_2507238156_503743352(IEEE_P_2592010699, t3, t16);
    t14 = (t0 + 1968U);
    t18 = *((char **)t14);
    t14 = (t0 + 6925);
    t19 = *((int *)t14);
    t20 = (t19 - 3);
    t21 = (t20 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t14));
    t22 = (1U * t21);
    t23 = (0 + t22);
    t24 = (t18 + t23);
    *((unsigned char *)t24) = t17;
    xsi_set_current_line(29, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 6925);
    t11 = *((int *)t1);
    t12 = (t11 - 3);
    t6 = (t12 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t1));
    t7 = (1U * t6);
    t8 = (0 + t7);
    t4 = (t2 + t8);
    t3 = *((unsigned char *)t4);
    t10 = (t0 + 1968U);
    t13 = *((char **)t10);
    t10 = (t0 + 6925);
    t19 = *((int *)t10);
    t20 = (t19 - 3);
    t21 = (t20 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t10));
    t22 = (1U * t21);
    t23 = (0 + t22);
    t14 = (t13 + t23);
    t16 = *((unsigned char *)t14);
    t17 = ieee_p_2592010699_sub_2507238156_503743352(IEEE_P_2592010699, t3, t16);
    t15 = (t0 + 2088U);
    t18 = *((char **)t15);
    t15 = (t0 + 6925);
    t25 = *((int *)t15);
    t26 = (t25 - 3);
    t27 = (t26 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t15));
    t28 = (1U * t27);
    t29 = (0 + t28);
    t24 = (t18 + t29);
    *((unsigned char *)t24) = t17;
    xsi_set_current_line(30, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 6925);
    t11 = *((int *)t1);
    t12 = (t11 - 3);
    t6 = (t12 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t1));
    t7 = (1U * t6);
    t8 = (0 + t7);
    t4 = (t2 + t8);
    t3 = *((unsigned char *)t4);
    t10 = (t0 + 1968U);
    t13 = *((char **)t10);
    t10 = (t0 + 6925);
    t19 = *((int *)t10);
    t20 = (t19 - 3);
    t21 = (t20 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t10));
    t22 = (1U * t21);
    t23 = (0 + t22);
    t14 = (t13 + t23);
    t16 = *((unsigned char *)t14);
    t17 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t3, t16);
    t15 = (t0 + 2208U);
    t18 = *((char **)t15);
    t15 = (t0 + 6925);
    t25 = *((int *)t15);
    t26 = (t25 - 3);
    t27 = (t26 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t15));
    t28 = (1U * t27);
    t29 = (0 + t28);
    t24 = (t18 + t29);
    *((unsigned char *)t24) = t17;
    xsi_set_current_line(31, ng0);
    t1 = (t0 + 2088U);
    t2 = *((char **)t1);
    t1 = (t0 + 6925);
    t11 = *((int *)t1);
    t12 = (t11 - 3);
    t6 = (t12 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t1));
    t7 = (1U * t6);
    t8 = (0 + t7);
    t4 = (t2 + t8);
    t3 = *((unsigned char *)t4);
    t10 = (t0 + 2328U);
    t13 = *((char **)t10);
    t10 = (t0 + 6925);
    t19 = *((int *)t10);
    t20 = (t19 - 4);
    t21 = (t20 * -1);
    xsi_vhdl_check_range_of_index(4, 0, -1, *((int *)t10));
    t22 = (1U * t21);
    t23 = (0 + t22);
    t14 = (t13 + t23);
    t16 = *((unsigned char *)t14);
    t17 = ieee_p_2592010699_sub_2507238156_503743352(IEEE_P_2592010699, t3, t16);
    t15 = (t0 + 6925);
    t25 = *((int *)t15);
    t26 = (t25 - 3);
    t27 = (t26 * -1);
    t28 = (1 * t27);
    t29 = (0U + t28);
    t18 = (t0 + 3952);
    t24 = (t18 + 56U);
    t30 = *((char **)t24);
    t31 = (t30 + 56U);
    t32 = *((char **)t31);
    *((unsigned char *)t32) = t17;
    xsi_driver_first_trans_delta(t18, t29, 1, 0LL);
    xsi_set_current_line(35, ng0);
    t1 = (t0 + 6925);
    t2 = (t0 + 6933);
    *((int *)t2) = 0;
    t4 = (t0 + 6937);
    *((int *)t4) = *((int *)t1);
    t11 = 0;
    t12 = *((int *)t1);

LAB6:    if (t11 <= t12)
        goto LAB7;

LAB9:    xsi_set_current_line(44, ng0);
    t1 = (t0 + 6925);
    t11 = *((int *)t1);
    t3 = (t11 > 0);
    if (t3 != 0)
        goto LAB14;

LAB16:    xsi_set_current_line(64, ng0);
    t1 = (t0 + 2568U);
    t2 = *((char **)t1);
    t1 = (t2 + 0);
    *((unsigned char *)t1) = (unsigned char)2;

LAB15:    xsi_set_current_line(67, ng0);
    t1 = (t0 + 2328U);
    t2 = *((char **)t1);
    t11 = (0 - 4);
    t6 = (t11 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t3 = *((unsigned char *)t1);
    t4 = (t0 + 2448U);
    t10 = *((char **)t4);
    t12 = (0 - 1);
    t21 = (t12 * -1);
    t22 = (1U * t21);
    t23 = (0 + t22);
    t4 = (t10 + t23);
    t16 = *((unsigned char *)t4);
    t17 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t3, t16);
    t13 = (t0 + 2568U);
    t14 = *((char **)t13);
    t45 = *((unsigned char *)t14);
    t46 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t17, t45);
    t13 = (t0 + 2208U);
    t15 = *((char **)t13);
    t13 = (t0 + 6925);
    t19 = *((int *)t13);
    t20 = (t19 - 3);
    t27 = (t20 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t13));
    t28 = (1U * t27);
    t29 = (0 + t28);
    t18 = (t15 + t29);
    t47 = *((unsigned char *)t18);
    t48 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t46, t47);
    t24 = (t0 + 2328U);
    t30 = *((char **)t24);
    t24 = (t0 + 6925);
    t25 = *((int *)t24);
    t26 = (t25 + 1);
    t33 = (t26 - 4);
    t49 = (t33 * -1);
    xsi_vhdl_check_range_of_index(4, 0, -1, t26);
    t50 = (1U * t49);
    t51 = (0 + t50);
    t31 = (t30 + t51);
    *((unsigned char *)t31) = t48;
    xsi_set_current_line(68, ng0);
    t1 = (t0 + 2568U);
    t2 = *((char **)t1);
    t1 = (t2 + 0);
    *((unsigned char *)t1) = (unsigned char)2;

LAB4:    t1 = (t0 + 6925);
    t5 = *((int *)t1);
    t2 = (t0 + 6929);
    t9 = *((int *)t2);
    if (t5 == t9)
        goto LAB5;

LAB33:    t11 = (t5 + 1);
    t5 = t11;
    t4 = (t0 + 6925);
    *((int *)t4) = t5;
    goto LAB2;

LAB7:    xsi_set_current_line(36, ng0);
    t10 = (t0 + 6933);
    t19 = *((int *)t10);
    t3 = (t19 == 0);
    if (t3 != 0)
        goto LAB10;

LAB12:    xsi_set_current_line(39, ng0);
    t1 = (t0 + 2448U);
    t2 = *((char **)t1);
    t19 = (0 - 1);
    t6 = (t19 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t3 = *((unsigned char *)t1);
    t4 = (t0 + 2088U);
    t10 = *((char **)t4);
    t4 = (t0 + 6933);
    t20 = *((int *)t4);
    t25 = (t20 - 3);
    t21 = (t25 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t4));
    t22 = (1U * t21);
    t23 = (0 + t22);
    t13 = (t10 + t23);
    t16 = *((unsigned char *)t13);
    t17 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t3, t16);
    t14 = (t0 + 2448U);
    t15 = *((char **)t14);
    t26 = (0 - 1);
    t27 = (t26 * -1);
    t28 = (1U * t27);
    t29 = (0 + t28);
    t14 = (t15 + t29);
    *((unsigned char *)t14) = t17;

LAB11:
LAB8:    t1 = (t0 + 6933);
    t11 = *((int *)t1);
    t2 = (t0 + 6937);
    t12 = *((int *)t2);
    if (t11 == t12)
        goto LAB9;

LAB13:    t19 = (t11 + 1);
    t11 = t19;
    t4 = (t0 + 6933);
    *((int *)t4) = t11;
    goto LAB6;

LAB10:    xsi_set_current_line(37, ng0);
    t13 = (t0 + 2088U);
    t14 = *((char **)t13);
    t20 = (0 - 3);
    t6 = (t20 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t13 = (t14 + t8);
    t16 = *((unsigned char *)t13);
    t15 = (t0 + 2448U);
    t18 = *((char **)t15);
    t25 = (0 - 1);
    t21 = (t25 * -1);
    t22 = (1U * t21);
    t23 = (0 + t22);
    t15 = (t18 + t23);
    *((unsigned char *)t15) = t16;
    goto LAB11;

LAB14:    xsi_set_current_line(45, ng0);
    t2 = (t0 + 6925);
    t12 = *((int *)t2);
    t19 = (t12 - 1);
    t4 = (t0 + 6941);
    *((int *)t4) = 0;
    t10 = (t0 + 6945);
    *((int *)t10) = t19;
    t20 = 0;
    t25 = t19;

LAB17:    if (t20 <= t25)
        goto LAB18;

LAB20:    goto LAB15;

LAB18:    xsi_set_current_line(47, ng0);
    t13 = (t0 + 6925);
    t14 = (t0 + 6941);
    t26 = *((int *)t14);
    t33 = (t26 + 1);
    t15 = (t0 + 6949);
    *((int *)t15) = t33;
    t18 = (t0 + 6953);
    *((int *)t18) = *((int *)t13);
    t34 = t33;
    t35 = *((int *)t13);

LAB21:    if (t34 <= t35)
        goto LAB22;

LAB24:    xsi_set_current_line(56, ng0);
    t1 = (t0 + 6941);
    t11 = *((int *)t1);
    t3 = (t11 == 0);
    if (t3 != 0)
        goto LAB29;

LAB31:    xsi_set_current_line(59, ng0);
    t1 = (t0 + 2568U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 2208U);
    t4 = *((char **)t1);
    t1 = (t0 + 6941);
    t11 = *((int *)t1);
    t12 = (t11 - 3);
    t6 = (t12 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t1));
    t7 = (1U * t6);
    t8 = (0 + t7);
    t10 = (t4 + t8);
    t16 = *((unsigned char *)t10);
    t13 = (t0 + 2448U);
    t14 = *((char **)t13);
    t19 = (1 - 1);
    t21 = (t19 * -1);
    t22 = (1U * t21);
    t23 = (0 + t22);
    t13 = (t14 + t23);
    t17 = *((unsigned char *)t13);
    t45 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t16, t17);
    t46 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t3, t45);
    t15 = (t0 + 2568U);
    t18 = *((char **)t15);
    t15 = (t18 + 0);
    *((unsigned char *)t15) = t46;

LAB30:    xsi_set_current_line(61, ng0);
    t1 = (t0 + 2448U);
    t2 = *((char **)t1);
    t11 = (1 - 1);
    t6 = (t11 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    *((unsigned char *)t1) = (unsigned char)2;

LAB19:    t1 = (t0 + 6941);
    t20 = *((int *)t1);
    t2 = (t0 + 6945);
    t25 = *((int *)t2);
    if (t20 == t25)
        goto LAB20;

LAB32:    t11 = (t20 + 1);
    t20 = t11;
    t4 = (t0 + 6941);
    *((int *)t4) = t20;
    goto LAB17;

LAB22:    xsi_set_current_line(48, ng0);
    t24 = (t0 + 6949);
    t30 = (t0 + 6941);
    t36 = *((int *)t30);
    t37 = (t36 + 1);
    t38 = *((int *)t24);
    t16 = (t38 == t37);
    if (t16 != 0)
        goto LAB25;

LAB27:    xsi_set_current_line(51, ng0);
    t1 = (t0 + 2448U);
    t2 = *((char **)t1);
    t11 = (1 - 1);
    t6 = (t11 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t3 = *((unsigned char *)t1);
    t4 = (t0 + 2088U);
    t10 = *((char **)t4);
    t4 = (t0 + 6949);
    t12 = *((int *)t4);
    t19 = (t12 - 3);
    t21 = (t19 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t4));
    t22 = (1U * t21);
    t23 = (0 + t22);
    t13 = (t10 + t23);
    t16 = *((unsigned char *)t13);
    t17 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t3, t16);
    t14 = (t0 + 2448U);
    t15 = *((char **)t14);
    t26 = (1 - 1);
    t27 = (t26 * -1);
    t28 = (1U * t27);
    t29 = (0 + t28);
    t14 = (t15 + t29);
    *((unsigned char *)t14) = t17;

LAB26:
LAB23:    t1 = (t0 + 6949);
    t34 = *((int *)t1);
    t2 = (t0 + 6953);
    t35 = *((int *)t2);
    if (t34 == t35)
        goto LAB24;

LAB28:    t11 = (t34 + 1);
    t34 = t11;
    t4 = (t0 + 6949);
    *((int *)t4) = t34;
    goto LAB21;

LAB25:    xsi_set_current_line(49, ng0);
    t31 = (t0 + 2088U);
    t32 = *((char **)t31);
    t31 = (t0 + 6949);
    t39 = *((int *)t31);
    t40 = (t39 - 3);
    t6 = (t40 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t31));
    t7 = (1U * t6);
    t8 = (0 + t7);
    t41 = (t32 + t8);
    t17 = *((unsigned char *)t41);
    t42 = (t0 + 2448U);
    t43 = *((char **)t42);
    t44 = (1 - 1);
    t21 = (t44 * -1);
    t22 = (1U * t21);
    t23 = (0 + t22);
    t42 = (t43 + t23);
    *((unsigned char *)t42) = t17;
    goto LAB26;

LAB29:    xsi_set_current_line(57, ng0);
    t2 = (t0 + 2208U);
    t4 = *((char **)t2);
    t12 = (0 - 3);
    t6 = (t12 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t2 = (t4 + t8);
    t16 = *((unsigned char *)t2);
    t10 = (t0 + 2448U);
    t13 = *((char **)t10);
    t19 = (1 - 1);
    t21 = (t19 * -1);
    t22 = (1U * t21);
    t23 = (0 + t22);
    t10 = (t13 + t23);
    t17 = *((unsigned char *)t10);
    t45 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t16, t17);
    t14 = (t0 + 2568U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    *((unsigned char *)t14) = t45;
    goto LAB30;

}


extern void work_a_0267653259_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0267653259_3212880686_p_0};
	xsi_register_didat("work_a_0267653259_3212880686", "isim/P2_SumRes_Anticipado_TestBench_isim_beh.exe.sim/work/a_0267653259_3212880686.didat");
	xsi_register_executes(pe);
}
