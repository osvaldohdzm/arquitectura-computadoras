
library IEEE;

use IEEE.STD_LOGIC_1164.ALL;

entity ExtensorDeDireccion is
	port
	(
		DataIn	: in std_logic_vector(11 downto 0);
		DataOut	: out std_logic_vector(15 downto 0)
	);
end ExtensorDeDireccion;

architecture A_ExtensorDeDireccion of ExtensorDeDireccion is
	signal aux: std_logic_vector(11 downto 0);
begin 
	
	aux<=DataIn;
	DataOut<=x"0"&aux;
	
end A_ExtensorDeDireccion;