
library IEEE;

use IEEE.STD_LOGIC_1164.ALL;
use iEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity MemoriaDeDatos is
	generic
	(
		--Tama�o del Bus de Datos
		constant tamWord	: integer:=16;
		
		--Tama�o del Bus de Direcciones
		constant tamBusDir: integer:=11
	);
	Port 
	( 
		--Entradas
		
		BusDir  : in  STD_LOGIC_VECTOR (tamBusDir-1  downto 0); 	--Bus de Direcciones
		DataIn  : in  STD_LOGIC_VECTOR (tamWord-1 downto 0); 		--Bus de Datos de Entrada
		WD 	  : in  STD_LOGIC;  											--Se�al para habilitar escritura
		clk 	  : in  STD_LOGIC;											--Se�al de Reloj
		
		--Salidas
		
		DataOut : out  STD_LOGIC_VECTOR (tamWord-1 downto 0) 		--Bus de Datos de Salida
	);
end MemoriaDeDatos;

architecture A_MemoriaDeDatos of MemoriaDeDatos is
	
	--Tipo de Dato para crear las localidades de memoria
	TYPE Localidades IS ARRAY (0 to ((2**tamBusDir)-1)) OF std_logic_vector(tamWord-1 downto 0);
	
	--Se�al para la RAM
	signal RAM: Localidades;
	
begin
	--Se realizan lecturas
	DataOut<=RAM(conv_integer(BusDir));
	
	--Se realizan escrituras
	process(clk)
	begin
		if(rising_edge(clk)) then
			if(WD='1') then
				RAM(conv_integer(BusDir))<=DataIn;
			end if;
		end if;
	end process;

end A_MemoriaDeDatos;