/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;

char *WORK_P_0191184638;
char *IEEE_P_3620187407;
char *WORK_P_4101039350;
char *IEEE_P_3499444699;
char *IEEE_P_2592010699;
char *STD_STANDARD;


int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    ieee_p_2592010699_init();
    work_p_4101039350_init();
    ieee_p_3499444699_init();
    ieee_p_3620187407_init();
    work_p_0191184638_init();
    work_a_1843038851_2635067010_init();
    work_a_0804651208_0067551032_init();
    work_a_0251975645_2208211959_init();
    work_a_0915008280_0446165581_init();
    work_a_0569573211_1838203611_init();
    work_a_1167299218_4092876664_init();
    work_a_1390970577_2230544366_init();
    work_a_1801828884_0502937172_init();
    work_a_4158304394_1794967234_init();
    work_a_0792250385_4198636371_init();
    work_a_3034346949_2370120645_init();
    work_a_4026509680_1679631861_init();
    work_a_0306024963_0447502664_init();
    work_a_0686054477_1587663614_init();
    work_a_1233029691_0706053088_init();
    work_a_2492482463_2227752171_init();
    work_a_1905801119_1839950302_init();
    work_a_0868587044_1008471584_init();
    work_a_4254811276_2372691052_init();


    xsi_register_tops("work_a_4254811276_2372691052");

    WORK_P_0191184638 = xsi_get_engine_memory("work_p_0191184638");
    IEEE_P_3620187407 = xsi_get_engine_memory("ieee_p_3620187407");
    WORK_P_4101039350 = xsi_get_engine_memory("work_p_4101039350");
    IEEE_P_3499444699 = xsi_get_engine_memory("ieee_p_3499444699");
    IEEE_P_2592010699 = xsi_get_engine_memory("ieee_p_2592010699");
    xsi_register_ieee_std_logic_1164(IEEE_P_2592010699);
    STD_STANDARD = xsi_get_engine_memory("std_standard");

    return xsi_run_simulation(argc, argv);

}
