LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY DECO_test IS
END DECO_test;
 
ARCHITECTURE behavior OF DECO_test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT DECODE
    PORT(
         entrada : IN  std_logic_vector(3 downto 0);
         salida : OUT  std_logic_vector(6 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal entrada : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal salida : std_logic_vector(6 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
  
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: DECODE PORT MAP (
          entrada => entrada,
          salida => salida
        );

   -- Stimulus process
   stim_proc: process
   begin
	entrada <= "0101"; --5
	wait for 50 ns;
	entrada <= "0011"; --3
	wait for 50 ns;
	entrada <= "0010"; --2
	wait for 50 ns;
	entrada <= "1000"; --8
	wait for 50 ns;
	entrada <= "0000"; --0
	wait for 50 ns;
	entrada <= "0100"; --4
	wait for 50 ns;
      wait;
   end process;

END;
