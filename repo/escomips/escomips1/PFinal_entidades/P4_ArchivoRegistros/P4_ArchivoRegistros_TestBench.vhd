LIBRARY ieee;
LIBRARY STD;
USE STD.TEXTIO.ALL;
USE ieee.std_logic_TEXTIO.ALL;	--PERMITE USAR STD_LOGIC 

USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;
 
ENTITY P4_ArchivoRegistros_TestBench IS
END P4_ArchivoRegistros_TestBench;
 
ARCHITECTURE behavior OF P4_ArchivoRegistros_TestBench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT P4_ArchivoRegistros
    PORT(
         write_data : IN  std_logic_vector(15 downto 0);
         write_reg : IN  std_logic_vector(3 downto 0);
         read_reg1 : IN  std_logic_vector(3 downto 0);
         read_reg2 : IN  std_logic_vector(3 downto 0);
         shamt : IN  std_logic_vector(3 downto 0);
         clk : IN  std_logic;
         clr : IN  std_logic;
         wr : IN  std_logic;
         she : IN  std_logic;
         dir : IN  std_logic;
         read_data1 : OUT  std_logic_vector(15 downto 0);
         read_data2 : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal write_data : std_logic_vector(15 downto 0) := (others => '0');
   signal write_reg : std_logic_vector(3 downto 0) := (others => '0');
   signal read_reg1 : std_logic_vector(3 downto 0) := (others => '0');
   signal read_reg2 : std_logic_vector(3 downto 0) := (others => '0');
   signal shamt : std_logic_vector(3 downto 0) := (others => '0');
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';
   signal wr : std_logic := '0';
   signal she : std_logic := '0';
   signal dir : std_logic := '0';

 	--Outputs
   signal read_data1 : std_logic_vector(15 downto 0);
   signal read_data2 : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 50 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: P4_ArchivoRegistros PORT MAP (
          write_data => write_data,
          write_reg => write_reg,
          read_reg1 => read_reg1,
          read_reg2 => read_reg2,
          shamt => shamt,
          clk => clk,
          clr => clr,
          wr => wr,
          she => she,
          dir => dir,
          read_data1 => read_data1,
          read_data2 => read_data2
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
	file ARCH_RES : TEXT;																					
	variable LINEA_RES : line;
	VARIABLE VAR_read_data1 : STD_LOGIC_VECTOR(15 DOWNTO 0);
	VARIABLE VAR_read_data2 : STD_LOGIC_VECTOR(15 DOWNTO 0);
	
	file ARCH_VEC : TEXT;
	variable LINEA_VEC : line;
	VARIABLE VAR_read_reg1 : STD_LOGIC_VECTOR(3 DOWNTO 0);
	VARIABLE VAR_read_reg2 : STD_LOGIC_VECTOR(3 DOWNTO 0);
	VARIABLE VAR_shamt : STD_LOGIC_VECTOR(3 DOWNTO 0);
	VARIABLE VAR_write_reg : STD_LOGIC_VECTOR(3 DOWNTO 0);
	VARIABLE VAR_write_data : STD_LOGIC_VECTOR(15 DOWNTO 0);
	VARIABLE VAR_wr : STD_LOGIC;
	VARIABLE VAR_she : STD_LOGIC;
	VARIABLE VAR_dir : STD_LOGIC;
	VARIABLE VAR_clr : STD_LOGIC;
	VARIABLE CADENA : STRING(1 TO 4);
   begin		
		file_open(ARCH_VEC, "VECTORES.TXT", READ_MODE); 	
		file_open(ARCH_RES, "RESULTADO.TXT", WRITE_MODE); 	

		CADENA := " RR1";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA "READ REGISTER 1"
		CADENA := " RR2";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA "READ REGISTER 2"
		CADENA := "  SH";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA "SHAMT"
		CADENA := "WREG";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA "WRITE REGISTER"
		CADENA := "  WD";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA "WRITE DATA"
		CADENA := "  WR";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA "WR"
		CADENA := " SHE";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA "SHE"
		CADENA := " DIR";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA "DIR"
		CADENA := " RD1";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA "READ DATA 1"
		CADENA := " RD2";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA "READ DATA 2"
		writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo

		wait for 100 NS;
		FOR I IN 0 TO 8 LOOP
			readline(ARCH_VEC,LINEA_VEC); -- lee una linea completa
			
			Hread(LINEA_VEC, VAR_read_reg1);
         read_reg1 <= VAR_read_reg1;
			
			Hread(LINEA_VEC, VAR_read_reg2);
         read_reg2 <= VAR_read_reg2;
			
			Hread(LINEA_VEC, VAR_shamt);
			shamt <= VAR_shamt;
			
			Hread(LINEA_VEC, VAR_write_reg);
         write_reg <= VAR_write_reg;
			
			Hread(LINEA_VEC, VAR_write_data);
			write_data <= VAR_write_data;
			
			read(LINEA_VEC, VAR_wr);
         wr <= VAR_wr;
			
			read(LINEA_VEC, VAR_she);
         she <= VAR_she;
			
			read(LINEA_VEC, VAR_dir);
         dir <= VAR_dir;
			
			read(LINEA_VEC, VAR_clr);
         clr <= VAR_clr;
			
			WAIT UNTIL RISING_EDGE(CLK);	--ESPERO AL FLANCO DE SUBIDA 

			VAR_read_data1 := read_data1;
         VAR_read_data2 := read_data2;
			
			Hwrite(LINEA_RES, VAR_read_reg1, right, 5);	--ESCRIBE EL CAMPO READ REG 1
			Hwrite(LINEA_RES,VAR_read_reg2, 	right, 5);	--ESCRIBE EL CAMPO READ REG 2
			Hwrite(LINEA_RES, VAR_shamt, 	right, 5);	--ESCRIBE EL CAMPO SHAMT
			Hwrite(LINEA_RES, VAR_write_reg, right, 5);	--ESCRIBE EL CAMPO WRITE REG
			Hwrite(LINEA_RES, VAR_write_data, 	right, 5);	--ESCRIBE EL CAMPO WRITE DATA
			write(LINEA_RES, VAR_wr, 	right, 5);	--ESCRIBE EL CAMPO WR
			write(LINEA_RES, VAR_she, right, 5);	--ESCRIBE EL CAMPO SHE
			write(LINEA_RES,VAR_dir, 	right, 5);	--ESCRIBE EL CAMPO DIR
			Hwrite(LINEA_RES, VAR_read_data1, 	right, 5);	--ESCRIBE EL CAMPO READ DATA 1
			Hwrite(LINEA_RES, VAR_read_data2, 	right, 5);	--ESCRIBE EL CAMPO READ DATA 2

			writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo
			
		end loop;
		file_close(ARCH_VEC);  -- cierra el archivo
		file_close(ARCH_RES);  -- cierra el archivo

      wait;
   end process;

END;
