
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY extSign_test IS
END extSign_test;
 
ARCHITECTURE behavior OF extSign_test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ExtensorSigno
    PORT(
         ENTRADA : IN  std_logic_vector(11 downto 0);
         SALIDA : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal ENTRADA : std_logic_vector(11 downto 0) := (others => '0');

 	--Outputs
   signal SALIDA : std_logic_vector(15 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ExtensorSigno PORT MAP (
          ENTRADA => ENTRADA,
          SALIDA => SALIDA
        );


   -- Stimulus process
   stim_proc: process
   begin		
      -- insert stimulus here 
		ENTRADA <= "000111010010";
      wait FOR 10 NS;
		
		ENTRADA <= "111111111111";
      wait FOR 10 NS;
		
		ENTRADA <= "000000000000";
      wait FOR 10 NS;
   end process;

END;
