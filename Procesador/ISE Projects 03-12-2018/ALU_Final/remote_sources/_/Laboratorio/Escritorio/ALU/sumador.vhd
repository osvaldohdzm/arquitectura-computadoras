library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity sumador is
    Port ( a, b, cin : in  STD_LOGIC;
           s, cout : out  STD_LOGIC);
end sumador;

architecture Behavioral of sumador is
	signal eb : std_logic;
begin
	eb <= b xor cin;
	s <= a xor eb;
	cout <= (b and cin) or (a and b) or (a and cin);

end Behavioral;

