--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:54:15 06/14/2017
-- Design Name:   
-- Module Name:   D:/Anthony/Sexto Semestre/Arquitectura/ESCOMips/T_ESCOMips.vhd
-- Project Name:  ESCOMips
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: ESCOMips
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY T_ESCOMips IS
END T_ESCOMips;
 
ARCHITECTURE behavior OF T_ESCOMips IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ESCOMips
    PORT(
         RCLR : IN  std_logic;
         RCLK : IN  std_logic;
         READ1 : OUT  std_logic_vector(15 downto 0);
         READ2 : OUT  std_logic_vector(15 downto 0);
         PCOUNTER : OUT  std_logic_vector(8 downto 0);
         ALUU : OUT  std_logic_vector(3 downto 0);
         salida : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal RCLR : std_logic := '0';
   signal RCLK : std_logic := '0';

 	--Outputs
   signal READ1 : std_logic_vector(15 downto 0);
   signal READ2 : std_logic_vector(15 downto 0);
   signal PCOUNTER : std_logic_vector(8 downto 0);
   signal ALUU : std_logic_vector(3 downto 0);
   signal salida : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant RCLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ESCOMips PORT MAP (
          RCLR => RCLR,
          RCLK => RCLK,
          READ1 => READ1,
          READ2 => READ2,
          PCOUNTER => PCOUNTER,
          ALUU => ALUU,
          salida => salida
        );

   -- Clock process definitions
   RCLK_process :process
   begin
		RCLK <= '0';
		wait for RCLK_period/2;
		RCLK <= '1';
		wait for RCLK_period/2;
   end process;
 
 
   -- Stimulus process
   stim_proc: process
   begin		
      RCLR <= '1';
		wait for 12 ns;
		RCLR <= '0';
      wait;
   end process;
END;
