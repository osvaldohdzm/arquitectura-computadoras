LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY Cont_test IS
END Cont_test;
 
ARCHITECTURE behavior OF Cont_test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Contador
    PORT(
         LB : IN  std_logic;
         EB : IN  std_logic;
         clk : IN  std_logic;
         clr : IN  std_logic;
         B : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal LB : std_logic := '0';
   signal EB : std_logic := '0';
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';

 	--Outputs
   signal B : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Contador PORT MAP (
          LB => LB,
          EB => EB,
          clk => clk,
          clr => clr,
          B => B
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
      stim_proc: process
   begin		
      clr <= '1';
		wait for 20 ns;
		clr <= '0';
		wait;
   end process;
	
	escritura: process
   begin
	   LB <= '0';
		EB <= '1';
		wait for 20 ns;
		LB <= '0';
		EB <= '1';
		wait for 20 ns;
		LB <= '0';
		EB <= '1';
		wait for 20 ns;
		
		LB <= '0';
		EB <= '0';
		wait for 20 ns;
		
		LB <= '0';
		EB <= '1';
		wait for 20 ns;
		LB <= '0';
		EB <= '1';
		wait for 20 ns;
		LB <= '0';
		EB <= '1';
		wait for 20 ns;
		
		LB <= '1';
		EB <= '0';
		wait for 20 ns;
		
		LB <= '0';
		EB <= '1';
		wait for 20 ns;
		LB <= '0';
		EB <= '1';
		wait for 20 ns;
		LB <= '0';
		EB <= '1';
		wait;
	end process;
END;
