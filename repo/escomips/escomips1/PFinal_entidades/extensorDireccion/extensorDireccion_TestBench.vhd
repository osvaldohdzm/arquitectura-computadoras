LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY extensorDireccion_TestBench IS
END extensorDireccion_TestBench;
 
ARCHITECTURE behavior OF extensorDireccion_TestBench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT extensorDireccion
    PORT(
         d_in : IN  std_logic_vector(11 downto 0);
         d_out : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal d_in : std_logic_vector(11 downto 0) := (others => '0');

 	--Outputs
   signal d_out : std_logic_vector(15 downto 0);
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: extensorDireccion PORT MAP (
          d_in => d_in,
          d_out => d_out
        );
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;
		d_in <= "100111111110";
		wait for 100 ns;
		d_in <= "111111111111";
		wait for 100 ns;
		d_in <= "000000000000";
      wait;
   end process;

END;
