library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity Decodificador is
	port(
		b : in std_logic_vector(3 downto 0);
		sal_7_seg : out std_logic_vector(6 downto 0)
	);
end Decodificador;

architecture Behavioral of Decodificador is
begin
	process(b)
	begin
			case b is
				when "0000" =>
					sal_7_seg <= "0000001"; -- 0
				when "0001" =>
					sal_7_seg <= "1001111"; -- 1
				when "0010" =>
					sal_7_seg <= "0010010"; -- 2
				when "0011" =>
					sal_7_seg <= "0000110"; -- 3
				when "0100" =>
					sal_7_seg <= "1001100"; -- 4
				when "0101" =>
					sal_7_seg <= "0100101"; -- 5
				when "0110" =>
					sal_7_seg <= "0100000"; -- 6
				when "0111" =>
					sal_7_seg <= "0001101"; -- 7 
				when "1000" =>
					sal_7_seg <= "0000000"; -- 8
				when "1001" =>
					sal_7_seg <= "0000100"; -- 9
				when others =>
					sal_7_seg <= "1111111";
			end case;
	end process;
end Behavioral;