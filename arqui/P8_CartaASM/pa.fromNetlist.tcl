
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name P8_CartaASM -dir "C:/_PracticasArqui/P8_CartaASM/planAhead_run_1" -part xc7a100tcsg324-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "C:/_PracticasArqui/P8_CartaASM/main.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/_PracticasArqui/P8_CartaASM} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "main.ucf" [current_fileset -constrset]
add_files [list {main.ucf}] -fileset [get_property constrset [current_run]]
link_design
