LIBRARY IEEE;
LIBRARY STD;
USE STD.TEXTIO.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY tb_ram IS
END tb_ram;
 
ARCHITECTURE behavior OF tb_ram IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
	COMPONENT MemoriaDatos
	generic(
		m : integer := 9; --bits de dir -> log2(#palabras)
		n : integer := 8 --tamano de palabra
	);
	PORT(
		dir : IN  std_logic_vector(m - 1 downto 0);
		data_in : IN  std_logic_vector(n - 1 downto 0);
		wd : IN  std_logic;
		clk : IN  std_logic;
		data_out : OUT  std_logic_vector(n - 1 downto 0)
	);
	END COMPONENT;
    

   --Inputs
   signal dir : std_logic_vector(m - 1 downto 0) := (others => '0');
   signal data_in : std_logic_vector(n - 1 downto 0) := (others => '0');
   signal wd : std_logic := '0';
   signal clk : std_logic := '0';

 	--Outputs
   signal data_out : std_logic_vector(n - 1 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: MemoriaDatos PORT MAP (
          dir => dir,
          data_in => data_in,
          wd => wd,
          clk => clk,
          data_out => data_out
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
	file ARCH_RES : TEXT;																					
	variable LINEA_RES : line;
	VARIABLE VAR_DOUT : STD_LOGIC_VECTOR(7 DOWNTO 0);
	
	file ARCH_VEC : TEXT;
	variable LINEA_VEC : line;
	VARIABLE VAR_DIR : STD_LOGIC_VECTOR(8 DOWNTO 0);
	VARIABLE VAR_DIN : STD_LOGIC_VECTOR(7 DOWNTO 0);
	VARIABLE VAR_WD : STD_LOGIC;
	
	VARIABLE CADENA : STRING(1 TO 6);--DIR D_IN WD D_OUT
	begin		
		file_open(ARCH_RES, "./ARCHIVO_SALIDA.txt", WRITE_MODE); 	
		file_open(ARCH_VEC, "./ARCHIVO_ENTRADA.txt", READ_MODE); 
		
		CADENA := "  DIR ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		CADENA := "  D_IN";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		CADENA := " WD   ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		CADENA := "D_OUT ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
	
		writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo
		
		WAIT FOR 100 NS;
		FOR I IN 0 TO 11 LOOP
			readline(ARCH_VEC,LINEA_VEC); -- lee una linea completa

			hread(LINEA_VEC, VAR_DIN);
			dato_in <= VAR_DIN;
			read(LINEA_VEC, VAR_DIR);
			dir <= VAR_DIR;
			read(LINEA_VEC, VAR_WD);
			wd <= VAR_WD;
			
			WAIT UNTIL RISING_EDGE(CLK);	--ESPERO AL FLANCO DE SUBIDA 

			VAR_DOUT := dato_out;
			
			write(LINEA_RES, VAR_DIR, right, 9);
			hwrite(LINEA_RES, VAR_DIN, 	right, 5);	
			write(LINEA_RES, VAR_WD, 	right, 4);
			hwrite(LINEA_RES, VAR_DOUT, 	right, 9);	
			
			writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo
			
		end loop;
		file_close(ARCH_VEC);  -- cierra el archivo
		file_close(ARCH_RES);  -- cierra el archivo

      wait;
   end process;
END;