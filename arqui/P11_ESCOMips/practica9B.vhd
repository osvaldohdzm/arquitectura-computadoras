-- Pr�ctica 9 | Pr�ctica 9B - Verificador Condici�n | Marco Antonio Rubio Cort�s

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity practica9B is
	port
	(
		OV								: in std_logic;
		N								: in std_logic;
		Z								: in std_logic;
		C								: in std_logic;
		
		EQ								: out std_logic;
		NEQ							: out std_logic;
		LT								: out std_logic;
		LE								: out std_logic;
		GT								: out std_logic;
		GET							: out std_logic
	);
end practica9B;

architecture A_practica9B of practica9B is

begin
		EQ <= Z;												-- Condici�n de igualdad
		
		NEQ <= (not Z);									-- Condici�n de desigualdad
		
		LT <= (not C);										-- Condici�n menor que
		
		LE <= (Z or (not C));							-- Condici�n menor o igual
		
		GT <= (C and (not Z));							-- Condici�n mayor que
		
		GET <= C;											-- Condici�n mayor o igual
end A_practica9B;