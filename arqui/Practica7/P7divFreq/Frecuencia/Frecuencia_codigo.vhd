-- Se realiza un contador de 25 bits 
-- Con el cual se planea reducir la frecuencia del clock de la nexis ya que trabaja a 100 MHZ
-- Entonces nosotros necesitamos una frecuencia de 1 MHz
-- Por lo que se tienen que usar el bit 24
-- Asi podremos reducir la frecuencia

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity Frecuencia_codigo is
    Port ( osc_clk : in  STD_LOGIC;
           clr : in  STD_LOGIC;
           clk : out  STD_LOGIC);
end Frecuencia_codigo;

architecture Behavioral of Frecuencia_codigo is
	signal clocky_cont : std_logic_vector(25 downto 0);
begin

	process(clr, osc_clk)
	begin
		if(clr = '1') then
			clocky_cont <= (others => '0');
		elsif(osc_clk'event and osc_clk = '1') then
			clocky_cont <= clocky_cont + 1;
		end if;
	end process;
	
	clk <= clocky_cont(2);
	
end Behavioral;

