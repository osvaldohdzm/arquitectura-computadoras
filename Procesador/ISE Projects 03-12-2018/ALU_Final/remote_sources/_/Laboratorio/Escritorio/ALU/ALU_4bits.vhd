library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ALU is
    Port ( a, b : in  STD_LOGIC_VECTOR (1 downto 0);
           sel_a, sel_b, cin: in  STD_LOGIC;
           op : in  STD_LOGIC_VECTOR (1 downto 0);
           res : inout  STD_LOGIC_VECTOR (3 downto 0);
           c, n, ov, z : out  STD_LOGIC);
end ALU;

architecture Behavioral of ALU is
	
	component ALU_1bit is Port(
		a, b, sel_a, sel_b, cin: in  STD_LOGIC;
      op : in  STD_LOGIC_VECTOR (1 downto 0);
      res, cout : out  STD_LOGIC);
	end component;
	
	signal carry : STD_LOGIC_VECTOR (4 downto 0);
begin
	
	carry(0) <= cin;

	ciclo : for i in 0 to 3 generate
		alu : ALU_1bit
		Port map
		(
			a => a(i),
			b => b(i),
			sel_a => sel_a,
			sel_b => sel_b,
			cin => carry(i),
			op => op,
			res => res(i),
			cout => carry(i+1)
		);
	end generate ciclo;
	
	process(sel_a, sel_b, cin, carry, op)
	begin
		if( (op="11" and sel_a='0' and sel_b='0' and cin='0') or (op="11" and sel_a='0' and sel_b='1' and cin='1')) then
			c <= carry(4);
		else
			c <= '0';
		end if;
	end process;
	
	process(carry, op)
	begin
		if(op = "11") then
			ov <= carry(4) xor carry(3);
		else
			ov <= '0';
		end if;
	end process;
	
	process(res)
	begin
		n <= res(3);
	end process;
	
	process(res)
	begin
		if(res = "0000") then
			z <= '1';
		else
			z <= '0';
		end if;
	end process;

end Behavioral;

