

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package PAQUETILLO is


COMPONENT DEMUX is
    Port ( 
			  WR : in  STD_LOGIC_VECTOR (3 downto 0);
			  L: out std_logic_vector(15 downto 0);
           WRITE_REG : in  STD_LOGIC);
end COMPONENT;
COMPONENT Arreglo is
    Port ( clk,clr : in  STD_LOGIC;
           l : in  STD_LOGIC_VECTOR (15 DOWNTO 0);
           d : in  STD_LOGIC_VECTOR(15 DOWNTO 0);
           read_reg1 : in  STD_LOGIC_VECTOR (3 downto 0);
           read_reg2 : in  STD_LOGIC_VECTOR (3 downto 0);
			  q2 : out  STD_LOGIC_VECTOR (15 downto 0);
           q1 : out  STD_LOGIC_VECTOR (15 downto 0));
end COMPONENT;

COMPONENT Barrel is
    Port ( DIR : in  STD_LOGIC;
           SHAMT : in  STD_LOGIC_VECTOR (3 downto 0);
           READ_DATA2 : in  STD_LOGIC_VECTOR (15 downto 0);
           Q : out  STD_LOGIC_VECTOR (15 downto 0));
end COMPONENT;

end PAQUETILLO;

package body PAQUETILLO is






end PAQUETILLO;
