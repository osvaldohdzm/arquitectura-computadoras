library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity alu4b is
    Port ( a : in  STD_LOGIC_VECTOR (3 downto 0);
           b : in  STD_LOGIC_VECTOR (3 downto 0);
           ALUop : in  STD_LOGIC_VECTOR (3 downto 0);
           resultado : out  STD_LOGIC_VECTOR (3 downto 0);
			  banderas : out  STD_LOGIC_VECTOR (3 downto 0));
end alu4b;

architecture Behavioral of alu4b is
signal amux: STD_LOGIC_VECTOR (3 downto 0);
signal bmux: STD_LOGIC_VECTOR (3 downto 0);
signal res_and: STD_LOGIC_VECTOR (3 downto 0);
signal res_or: STD_LOGIC_VECTOR (3 downto 0);
signal res_xor: STD_LOGIC_VECTOR (3 downto 0);
signal res_suma: STD_LOGIC_VECTOR (3 downto 0):= "0000";

signal carry: STD_LOGIC_VECTOR (4 downto 0) := "00000";
signal Eb: STD_LOGIC_VECTOR (3 downto 0) := "0000";

signal res: STD_LOGIC_VECTOR (3 downto 0):= "0000";

begin
ALU: process (a,b,ALUop,amux,bmux,res_and,res_or,res_xor,res_suma,carry,Eb,res)
  begin
	FOR i IN 0 TO 3 LOOP
		amux(i) <= a(i) XOR ALUop(3);
		bmux(i) <= b(i) XOR ALUop(2);
		res_and(i) <= amux(i) AND bmux(i);
		res_or(i) <= amux(i) OR bmux(i);
		res_xor(i) <= amux(i) XOR bmux(i);
		
		carry(0) <= ALUop(2);
		Eb(i) <= b(i) XOR carry(0);
		res_suma(i)<= a(i) XOR Eb(i) XOR carry(i);
		carry(i+1) <= (a(i) AND carry(i)) OR (Eb(i) AND carry(i)) OR(a(i) AND Eb(i));
		END  LOOP;
		
		CASE ALUop(1 downto 0) IS
			WHEN "00" => res<= res_and;
			WHEN "01" => res<= res_or;
			WHEN "10" => res<= res_xor;
			WHEN OTHERS => res <= res_suma;  banderas(0) <= carry(4); -- C
		END CASE;
end process;
resultado <= res;
banderas(3) <= carry(4) XOR carry(3); -- OV
banderas(2) <= res(3); -- N
banderas(1)<=  NOT (res(0) OR res(1) OR res(2) OR res(3)); -- Z
end Behavioral;
