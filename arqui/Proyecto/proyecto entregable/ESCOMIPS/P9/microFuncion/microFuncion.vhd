library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;


entity microFuncion is

generic (
		bus_dir : integer := 4;
	bus_datos : integer:= 20
);
    Port ( i : in  STD_LOGIC_VECTOR (3 downto 0);
           funSal : out  STD_LOGIC_VECTOR (19 downto 0));
end microFuncion;

architecture Behavioral of microFuncion is

constant add : std_logic_vector (19 downto 0) :=  "00000100010000110010";
constant sub : std_logic_vector (19 downto 0) :=  "00000100010001110010";
constant logAND: std_logic_vector (19 downto 0):= "00000100010000000010";
constant logOR: std_logic_vector (19 downto 0) := "00000100010000010010";
constant logXOR: std_logic_vector (19 downto 0) := "00000100010000100010";
constant logNAND: std_logic_vector (19 downto 0) := "00000100010011010010";
constant logNOR: std_logic_vector (19 downto 0) :=  "00000100010011000010";
constant logXNOR: std_logic_vector (19 downto 0) := "00000100010010100010";
constant logNOT: std_logic_vector (19 downto 0) :=  "00000100010011010010";

constant corrimientoI: std_logic_vector (19 downto 0) := "00000001110000000000";
constant corrimientoD: std_logic_vector (19 downto 0) := "00000001010000000000";


type ejemplo is array (0 to (2**bus_dir)-1) of std_logic_vector(bus_Datos-1 downto 0);
constant aux : ejemplo := (add, sub, logAND, logOR, logXOR, logNAND, logNOR, logXNOR, logNOT, corrimientoI, corrimientoD ,others=>(others=> '0'));

begin

	funSal <= aux(conv_integer(i));

end Behavioral;
