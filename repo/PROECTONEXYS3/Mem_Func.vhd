
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;


entity Mem_Func is
    Port ( A : in  STD_LOGIC_VECTOR (3 downto 0);
           D : out  STD_LOGIC_VECTOR (19 downto 0));
end Mem_Func;
architecture Behavioral of Mem_Func is
TYPE arre is array (0 to  ((2**(4))-1)) of std_logic_vector(19 downto 0);
constant ADD : STD_LOGIC_VECTOR(19 downto 0) := "00000100010000110011";
constant SUB : STD_LOGIC_VECTOR(19 downto 0) := "00000100010001110011";  
constant ANDD : STD_LOGIC_VECTOR(19 downto 0) :="00000100010000000011"; 
constant ORR : STD_LOGIC_VECTOR(19 downto 0)  :="00000100010000010011"; 
constant XORR : STD_LOGIC_VECTOR(19 downto 0) :="00000100010000100011"; 
constant NANDD : STD_LOGIC_VECTOR(19 downto 0):="00000100010011010011"; 
constant NORR : STD_LOGIC_VECTOR(19 downto 0):= "00000100010011000011"; 
constant XNORR :STD_LOGIC_VECTOR(19 downto 0):= "00000100010010100011"; 
constant NOTT :STD_LOGIC_VECTOR(19 downto 0):=  "00000100010011010011";
constant SLLL :STD_LOGIC_VECTOR(19 downto 0):=  "00000001110000000000";
constant SRLL :STD_LOGIC_VECTOR(19 downto 0):=  "00000001010000000000";
constant ROM : ARRE :=(ADD,SUB,ANDD,ORR,XORR,NANDD,NORR,XNORR,NOTT,SLLL,SRLL,OTHERS=>(OTHERS=>'0'));
begin

D	<=	ROM(conv_integer(A));
end Behavioral;

