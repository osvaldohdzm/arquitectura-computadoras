
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Principal is
    Port ( a,b : in  STD_LOGIC_VECTOR (3 downto 0);
			  ALUOP: IN STD_LOGIC_VECTOR (3 DOWNTO 0);
           S : out  STD_LOGIC_VECTOR (3 downto 0);
           C,Z,OV,N : out  STD_LOGIC
			 );
end Principal;

architecture Behavioral of Principal is
component ALU is
    Port ( a,b,sel_a,sel_b,cin : in  STD_LOGIC;
           op : in  STD_LOGIC_VECTOR (1 downto 0);
           res,cout : out  STD_LOGIC);
end component;
signal CARR: std_logic_Vector(4 DOWNTO 0);
signal res: std_logic_Vector(3 DOWNTO 0);
signal ainvert,binvert: std_logic;
begin
-- aluop(0) es op(0)
-- aluop(1) es op(1)
-- aluop(2) es binvert/cin
-- aluop(3) es ainvert
CARR(0) <= ALUOP(2);
ciclo_gen: 
	for i in 0 to 3
		generate
				alubit: ALU port map(a(i),b(i),aluop(3),aluop(2),CARR(i),ALUOP(1 DOWNTO 0),res(i),CARR(i+1));
		end generate ciclo_gen;
-- ASIGNACION DE BANERA CARRY
	C <= CARR(4);
-- FIN CARRY
--RESULTADO
	S <= res;
--FIN RESULTADO
--ASIGNACION DE BANDERA Z
	Z <= '1' WHEN res = ("0000") else '0';
--FIN ASIGNACION BANDERA Z
--ASIGNACION DE BANERDA OVERFLOW, XOR C(3) Y C(4)
	OV <= CARR(3) XOR CARR(4);
--FIN ASIGNACION DE BANERA OVERFLOW
--BANDERA NEGATIVOS
	N <= res(3);
--FIN BANDERA NEGATIVO
end Behavioral;












































-- AUTHOR: FAYDELLA ROJAS LEONARDO

