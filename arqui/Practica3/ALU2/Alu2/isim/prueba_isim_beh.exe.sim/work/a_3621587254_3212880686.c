/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/Caos/Desktop/ESCOM/2017-2/Materias/Arqui/Practicas/Practica3/ALU2/Alu2/ALU.vhd";
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1605435078_503743352(char *, unsigned char , unsigned char );
unsigned char ieee_p_2592010699_sub_1690584930_503743352(char *, unsigned char );
unsigned char ieee_p_2592010699_sub_2507238156_503743352(char *, unsigned char , unsigned char );
unsigned char ieee_p_2592010699_sub_2545490612_503743352(char *, unsigned char , unsigned char );


static void work_a_3621587254_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    int t3;
    int t4;
    char *t5;
    char *t6;
    int t7;
    int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    char *t12;
    unsigned char t13;
    char *t14;
    char *t15;
    int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned char t20;
    unsigned char t21;
    char *t22;
    int t23;
    int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    char *t32;
    int t33;
    unsigned char t34;
    unsigned char t35;
    int t36;
    int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    char *t41;
    char *t42;
    unsigned char t43;
    unsigned char t44;
    int t45;
    int t46;
    unsigned int t47;
    unsigned int t48;
    unsigned int t49;
    unsigned char t50;
    char *t51;
    char *t52;
    int t53;
    int t54;
    unsigned int t55;
    unsigned int t56;
    unsigned int t57;
    char *t58;
    unsigned char t59;
    unsigned char t60;
    unsigned char t61;
    char *t62;
    int t63;
    int t64;
    int t65;
    unsigned int t66;
    unsigned int t67;
    unsigned int t68;
    char *t69;
    char *t70;
    char *t71;
    char *t72;
    char *t73;

LAB0:    xsi_set_current_line(28, ng0);
    t1 = (t0 + 10117);
    *((int *)t1) = 0;
    t2 = (t0 + 10121);
    *((int *)t2) = 3;
    t3 = 0;
    t4 = 3;

LAB2:    if (t3 <= t4)
        goto LAB3;

LAB5:    xsi_set_current_line(41, ng0);
    t1 = (t0 + 1352U);
    t2 = *((char **)t1);
    t9 = (3 - 1);
    t10 = (t9 * 1U);
    t11 = (0 + t10);
    t1 = (t2 + t11);
    t5 = (t0 + 10125);
    t3 = xsi_mem_cmp(t5, t1, 2U);
    if (t3 == 1)
        goto LAB8;

LAB12:    t12 = (t0 + 10127);
    t4 = xsi_mem_cmp(t12, t1, 2U);
    if (t4 == 1)
        goto LAB9;

LAB13:    t15 = (t0 + 10129);
    t7 = xsi_mem_cmp(t15, t1, 2U);
    if (t7 == 1)
        goto LAB10;

LAB14:
LAB11:    xsi_set_current_line(45, ng0);
    t1 = (t0 + 2632U);
    t2 = *((char **)t1);
    t1 = (t0 + 6240);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t12 = (t6 + 56U);
    t14 = *((char **)t12);
    memcpy(t14, t2, 4U);
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(45, ng0);
    t1 = (t0 + 2792U);
    t2 = *((char **)t1);
    t3 = (4 - 4);
    t9 = (t3 * -1);
    t10 = (1U * t9);
    t11 = (0 + t10);
    t1 = (t2 + t11);
    t13 = *((unsigned char *)t1);
    t5 = (t0 + 6304);
    t6 = (t5 + 56U);
    t12 = *((char **)t6);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    *((unsigned char *)t15) = t13;
    xsi_driver_first_trans_delta(t5, 3U, 1, 0LL);

LAB7:    t1 = (t0 + 5584);
    *((int *)t1) = 1;

LAB1:    return;
LAB3:    xsi_set_current_line(29, ng0);
    t5 = (t0 + 1032U);
    t6 = *((char **)t5);
    t5 = (t0 + 10117);
    t7 = *((int *)t5);
    t8 = (t7 - 3);
    t9 = (t8 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t5));
    t10 = (1U * t9);
    t11 = (0 + t10);
    t12 = (t6 + t11);
    t13 = *((unsigned char *)t12);
    t14 = (t0 + 1352U);
    t15 = *((char **)t14);
    t16 = (3 - 3);
    t17 = (t16 * -1);
    t18 = (1U * t17);
    t19 = (0 + t18);
    t14 = (t15 + t19);
    t20 = *((unsigned char *)t14);
    t21 = ieee_p_2592010699_sub_2507238156_503743352(IEEE_P_2592010699, t13, t20);
    t22 = (t0 + 10117);
    t23 = *((int *)t22);
    t24 = (t23 - 3);
    t25 = (t24 * -1);
    t26 = (1 * t25);
    t27 = (0U + t26);
    t28 = (t0 + 5728);
    t29 = (t28 + 56U);
    t30 = *((char **)t29);
    t31 = (t30 + 56U);
    t32 = *((char **)t31);
    *((unsigned char *)t32) = t21;
    xsi_driver_first_trans_delta(t28, t27, 1, 0LL);
    xsi_set_current_line(30, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t1 = (t0 + 10117);
    t7 = *((int *)t1);
    t8 = (t7 - 3);
    t9 = (t8 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t1));
    t10 = (1U * t9);
    t11 = (0 + t10);
    t5 = (t2 + t11);
    t13 = *((unsigned char *)t5);
    t6 = (t0 + 1352U);
    t12 = *((char **)t6);
    t16 = (2 - 3);
    t17 = (t16 * -1);
    t18 = (1U * t17);
    t19 = (0 + t18);
    t6 = (t12 + t19);
    t20 = *((unsigned char *)t6);
    t21 = ieee_p_2592010699_sub_2507238156_503743352(IEEE_P_2592010699, t13, t20);
    t14 = (t0 + 10117);
    t23 = *((int *)t14);
    t24 = (t23 - 3);
    t25 = (t24 * -1);
    t26 = (1 * t25);
    t27 = (0U + t26);
    t15 = (t0 + 5792);
    t22 = (t15 + 56U);
    t28 = *((char **)t22);
    t29 = (t28 + 56U);
    t30 = *((char **)t29);
    *((unsigned char *)t30) = t21;
    xsi_driver_first_trans_delta(t15, t27, 1, 0LL);
    xsi_set_current_line(31, ng0);
    t1 = (t0 + 1832U);
    t2 = *((char **)t1);
    t1 = (t0 + 10117);
    t7 = *((int *)t1);
    t8 = (t7 - 3);
    t9 = (t8 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t1));
    t10 = (1U * t9);
    t11 = (0 + t10);
    t5 = (t2 + t11);
    t13 = *((unsigned char *)t5);
    t6 = (t0 + 1992U);
    t12 = *((char **)t6);
    t6 = (t0 + 10117);
    t16 = *((int *)t6);
    t23 = (t16 - 3);
    t17 = (t23 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t6));
    t18 = (1U * t17);
    t19 = (0 + t18);
    t14 = (t12 + t19);
    t20 = *((unsigned char *)t14);
    t21 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t13, t20);
    t15 = (t0 + 10117);
    t24 = *((int *)t15);
    t33 = (t24 - 3);
    t25 = (t33 * -1);
    t26 = (1 * t25);
    t27 = (0U + t26);
    t22 = (t0 + 5856);
    t28 = (t22 + 56U);
    t29 = *((char **)t28);
    t30 = (t29 + 56U);
    t31 = *((char **)t30);
    *((unsigned char *)t31) = t21;
    xsi_driver_first_trans_delta(t22, t27, 1, 0LL);
    xsi_set_current_line(32, ng0);
    t1 = (t0 + 1832U);
    t2 = *((char **)t1);
    t1 = (t0 + 10117);
    t7 = *((int *)t1);
    t8 = (t7 - 3);
    t9 = (t8 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t1));
    t10 = (1U * t9);
    t11 = (0 + t10);
    t5 = (t2 + t11);
    t13 = *((unsigned char *)t5);
    t6 = (t0 + 1992U);
    t12 = *((char **)t6);
    t6 = (t0 + 10117);
    t16 = *((int *)t6);
    t23 = (t16 - 3);
    t17 = (t23 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t6));
    t18 = (1U * t17);
    t19 = (0 + t18);
    t14 = (t12 + t19);
    t20 = *((unsigned char *)t14);
    t21 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t13, t20);
    t15 = (t0 + 10117);
    t24 = *((int *)t15);
    t33 = (t24 - 3);
    t25 = (t33 * -1);
    t26 = (1 * t25);
    t27 = (0U + t26);
    t22 = (t0 + 5920);
    t28 = (t22 + 56U);
    t29 = *((char **)t28);
    t30 = (t29 + 56U);
    t31 = *((char **)t30);
    *((unsigned char *)t31) = t21;
    xsi_driver_first_trans_delta(t22, t27, 1, 0LL);
    xsi_set_current_line(33, ng0);
    t1 = (t0 + 1832U);
    t2 = *((char **)t1);
    t1 = (t0 + 10117);
    t7 = *((int *)t1);
    t8 = (t7 - 3);
    t9 = (t8 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t1));
    t10 = (1U * t9);
    t11 = (0 + t10);
    t5 = (t2 + t11);
    t13 = *((unsigned char *)t5);
    t6 = (t0 + 1992U);
    t12 = *((char **)t6);
    t6 = (t0 + 10117);
    t16 = *((int *)t6);
    t23 = (t16 - 3);
    t17 = (t23 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t6));
    t18 = (1U * t17);
    t19 = (0 + t18);
    t14 = (t12 + t19);
    t20 = *((unsigned char *)t14);
    t21 = ieee_p_2592010699_sub_2507238156_503743352(IEEE_P_2592010699, t13, t20);
    t15 = (t0 + 10117);
    t24 = *((int *)t15);
    t33 = (t24 - 3);
    t25 = (t33 * -1);
    t26 = (1 * t25);
    t27 = (0U + t26);
    t22 = (t0 + 5984);
    t28 = (t22 + 56U);
    t29 = *((char **)t28);
    t30 = (t29 + 56U);
    t31 = *((char **)t30);
    *((unsigned char *)t31) = t21;
    xsi_driver_first_trans_delta(t22, t27, 1, 0LL);
    xsi_set_current_line(35, ng0);
    t1 = (t0 + 1352U);
    t2 = *((char **)t1);
    t7 = (2 - 3);
    t9 = (t7 * -1);
    t10 = (1U * t9);
    t11 = (0 + t10);
    t1 = (t2 + t11);
    t13 = *((unsigned char *)t1);
    t5 = (t0 + 6048);
    t6 = (t5 + 56U);
    t12 = *((char **)t6);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    *((unsigned char *)t15) = t13;
    xsi_driver_first_trans_delta(t5, 4U, 1, 0LL);
    xsi_set_current_line(36, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t1 = (t0 + 10117);
    t7 = *((int *)t1);
    t8 = (t7 - 3);
    t9 = (t8 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t1));
    t10 = (1U * t9);
    t11 = (0 + t10);
    t5 = (t2 + t11);
    t13 = *((unsigned char *)t5);
    t6 = (t0 + 2792U);
    t12 = *((char **)t6);
    t16 = (0 - 4);
    t17 = (t16 * -1);
    t18 = (1U * t17);
    t19 = (0 + t18);
    t6 = (t12 + t19);
    t20 = *((unsigned char *)t6);
    t21 = ieee_p_2592010699_sub_2507238156_503743352(IEEE_P_2592010699, t13, t20);
    t14 = (t0 + 10117);
    t23 = *((int *)t14);
    t24 = (t23 - 3);
    t25 = (t24 * -1);
    t26 = (1 * t25);
    t27 = (0U + t26);
    t15 = (t0 + 6112);
    t22 = (t15 + 56U);
    t28 = *((char **)t22);
    t29 = (t28 + 56U);
    t30 = *((char **)t29);
    *((unsigned char *)t30) = t21;
    xsi_driver_first_trans_delta(t15, t27, 1, 0LL);
    xsi_set_current_line(37, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 10117);
    t7 = *((int *)t1);
    t8 = (t7 - 3);
    t9 = (t8 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t1));
    t10 = (1U * t9);
    t11 = (0 + t10);
    t5 = (t2 + t11);
    t13 = *((unsigned char *)t5);
    t6 = (t0 + 2952U);
    t12 = *((char **)t6);
    t6 = (t0 + 10117);
    t16 = *((int *)t6);
    t23 = (t16 - 3);
    t17 = (t23 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t6));
    t18 = (1U * t17);
    t19 = (0 + t18);
    t14 = (t12 + t19);
    t20 = *((unsigned char *)t14);
    t21 = ieee_p_2592010699_sub_2507238156_503743352(IEEE_P_2592010699, t13, t20);
    t15 = (t0 + 2792U);
    t22 = *((char **)t15);
    t15 = (t0 + 10117);
    t24 = *((int *)t15);
    t33 = (t24 - 4);
    t25 = (t33 * -1);
    xsi_vhdl_check_range_of_index(4, 0, -1, *((int *)t15));
    t26 = (1U * t25);
    t27 = (0 + t26);
    t28 = (t22 + t27);
    t34 = *((unsigned char *)t28);
    t35 = ieee_p_2592010699_sub_2507238156_503743352(IEEE_P_2592010699, t21, t34);
    t29 = (t0 + 10117);
    t36 = *((int *)t29);
    t37 = (t36 - 3);
    t38 = (t37 * -1);
    t39 = (1 * t38);
    t40 = (0U + t39);
    t30 = (t0 + 6176);
    t31 = (t30 + 56U);
    t32 = *((char **)t31);
    t41 = (t32 + 56U);
    t42 = *((char **)t41);
    *((unsigned char *)t42) = t35;
    xsi_driver_first_trans_delta(t30, t40, 1, 0LL);
    xsi_set_current_line(38, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 10117);
    t7 = *((int *)t1);
    t8 = (t7 - 3);
    t9 = (t8 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t1));
    t10 = (1U * t9);
    t11 = (0 + t10);
    t5 = (t2 + t11);
    t13 = *((unsigned char *)t5);
    t6 = (t0 + 2792U);
    t12 = *((char **)t6);
    t6 = (t0 + 10117);
    t16 = *((int *)t6);
    t23 = (t16 - 4);
    t17 = (t23 * -1);
    xsi_vhdl_check_range_of_index(4, 0, -1, *((int *)t6));
    t18 = (1U * t17);
    t19 = (0 + t18);
    t14 = (t12 + t19);
    t20 = *((unsigned char *)t14);
    t21 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t13, t20);
    t15 = (t0 + 2952U);
    t22 = *((char **)t15);
    t15 = (t0 + 10117);
    t24 = *((int *)t15);
    t33 = (t24 - 3);
    t25 = (t33 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t15));
    t26 = (1U * t25);
    t27 = (0 + t26);
    t28 = (t22 + t27);
    t34 = *((unsigned char *)t28);
    t29 = (t0 + 2792U);
    t30 = *((char **)t29);
    t29 = (t0 + 10117);
    t36 = *((int *)t29);
    t37 = (t36 - 4);
    t38 = (t37 * -1);
    xsi_vhdl_check_range_of_index(4, 0, -1, *((int *)t29));
    t39 = (1U * t38);
    t40 = (0 + t39);
    t31 = (t30 + t40);
    t35 = *((unsigned char *)t31);
    t43 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t34, t35);
    t44 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t21, t43);
    t32 = (t0 + 1032U);
    t41 = *((char **)t32);
    t32 = (t0 + 10117);
    t45 = *((int *)t32);
    t46 = (t45 - 3);
    t47 = (t46 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t32));
    t48 = (1U * t47);
    t49 = (0 + t48);
    t42 = (t41 + t49);
    t50 = *((unsigned char *)t42);
    t51 = (t0 + 2952U);
    t52 = *((char **)t51);
    t51 = (t0 + 10117);
    t53 = *((int *)t51);
    t54 = (t53 - 3);
    t55 = (t54 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t51));
    t56 = (1U * t55);
    t57 = (0 + t56);
    t58 = (t52 + t57);
    t59 = *((unsigned char *)t58);
    t60 = ieee_p_2592010699_sub_1605435078_503743352(IEEE_P_2592010699, t50, t59);
    t61 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t44, t60);
    t62 = (t0 + 10117);
    t63 = *((int *)t62);
    t64 = (t63 + 1);
    t65 = (t64 - 4);
    t66 = (t65 * -1);
    t67 = (1 * t66);
    t68 = (0U + t67);
    t69 = (t0 + 6048);
    t70 = (t69 + 56U);
    t71 = *((char **)t70);
    t72 = (t71 + 56U);
    t73 = *((char **)t72);
    *((unsigned char *)t73) = t61;
    xsi_driver_first_trans_delta(t69, t68, 1, 0LL);

LAB4:    t1 = (t0 + 10117);
    t3 = *((int *)t1);
    t2 = (t0 + 10121);
    t4 = *((int *)t2);
    if (t3 == t4)
        goto LAB5;

LAB6:    t7 = (t3 + 1);
    t3 = t7;
    t5 = (t0 + 10117);
    *((int *)t5) = t3;
    goto LAB2;

LAB8:    xsi_set_current_line(42, ng0);
    t28 = (t0 + 2152U);
    t29 = *((char **)t28);
    t28 = (t0 + 6240);
    t30 = (t28 + 56U);
    t31 = *((char **)t30);
    t32 = (t31 + 56U);
    t41 = *((char **)t32);
    memcpy(t41, t29, 4U);
    xsi_driver_first_trans_fast(t28);
    goto LAB7;

LAB9:    xsi_set_current_line(43, ng0);
    t1 = (t0 + 2312U);
    t2 = *((char **)t1);
    t1 = (t0 + 6240);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t12 = (t6 + 56U);
    t14 = *((char **)t12);
    memcpy(t14, t2, 4U);
    xsi_driver_first_trans_fast(t1);
    goto LAB7;

LAB10:    xsi_set_current_line(44, ng0);
    t1 = (t0 + 2472U);
    t2 = *((char **)t1);
    t1 = (t0 + 6240);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t12 = (t6 + 56U);
    t14 = *((char **)t12);
    memcpy(t14, t2, 4U);
    xsi_driver_first_trans_fast(t1);
    goto LAB7;

LAB15:;
}

static void work_a_3621587254_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(48, ng0);

LAB3:    t1 = (t0 + 3112U);
    t2 = *((char **)t1);
    t1 = (t0 + 6368);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 4U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 5600);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3621587254_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;
    int t3;
    unsigned int t4;
    unsigned int t5;
    unsigned int t6;
    unsigned char t7;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned char t14;
    unsigned char t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;

LAB0:    xsi_set_current_line(49, ng0);

LAB3:    t1 = (t0 + 2792U);
    t2 = *((char **)t1);
    t3 = (4 - 4);
    t4 = (t3 * -1);
    t5 = (1U * t4);
    t6 = (0 + t5);
    t1 = (t2 + t6);
    t7 = *((unsigned char *)t1);
    t8 = (t0 + 2792U);
    t9 = *((char **)t8);
    t10 = (3 - 4);
    t11 = (t10 * -1);
    t12 = (1U * t11);
    t13 = (0 + t12);
    t8 = (t9 + t13);
    t14 = *((unsigned char *)t8);
    t15 = ieee_p_2592010699_sub_2507238156_503743352(IEEE_P_2592010699, t7, t14);
    t16 = (t0 + 6432);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    t19 = (t18 + 56U);
    t20 = *((char **)t19);
    *((unsigned char *)t20) = t15;
    xsi_driver_first_trans_delta(t16, 0U, 1, 0LL);

LAB2:    t21 = (t0 + 5616);
    *((int *)t21) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3621587254_3212880686_p_3(char *t0)
{
    char *t1;
    char *t2;
    int t3;
    unsigned int t4;
    unsigned int t5;
    unsigned int t6;
    unsigned char t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;

LAB0:    xsi_set_current_line(50, ng0);

LAB3:    t1 = (t0 + 3112U);
    t2 = *((char **)t1);
    t3 = (3 - 3);
    t4 = (t3 * -1);
    t5 = (1U * t4);
    t6 = (0 + t5);
    t1 = (t2 + t6);
    t7 = *((unsigned char *)t1);
    t8 = (t0 + 6496);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    *((unsigned char *)t12) = t7;
    xsi_driver_first_trans_delta(t8, 1U, 1, 0LL);

LAB2:    t13 = (t0 + 5632);
    *((int *)t13) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3621587254_3212880686_p_4(char *t0)
{
    char *t1;
    char *t2;
    int t3;
    unsigned int t4;
    unsigned int t5;
    unsigned int t6;
    unsigned char t7;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned char t14;
    unsigned char t15;
    char *t16;
    char *t17;
    int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned char t22;
    unsigned char t23;
    char *t24;
    char *t25;
    int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    unsigned char t30;
    unsigned char t31;
    unsigned char t32;
    char *t33;
    char *t34;
    char *t35;
    char *t36;
    char *t37;
    char *t38;

LAB0:    xsi_set_current_line(51, ng0);

LAB3:    t1 = (t0 + 3112U);
    t2 = *((char **)t1);
    t3 = (0 - 3);
    t4 = (t3 * -1);
    t5 = (1U * t4);
    t6 = (0 + t5);
    t1 = (t2 + t6);
    t7 = *((unsigned char *)t1);
    t8 = (t0 + 3112U);
    t9 = *((char **)t8);
    t10 = (1 - 3);
    t11 = (t10 * -1);
    t12 = (1U * t11);
    t13 = (0 + t12);
    t8 = (t9 + t13);
    t14 = *((unsigned char *)t8);
    t15 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t7, t14);
    t16 = (t0 + 3112U);
    t17 = *((char **)t16);
    t18 = (2 - 3);
    t19 = (t18 * -1);
    t20 = (1U * t19);
    t21 = (0 + t20);
    t16 = (t17 + t21);
    t22 = *((unsigned char *)t16);
    t23 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t15, t22);
    t24 = (t0 + 3112U);
    t25 = *((char **)t24);
    t26 = (3 - 3);
    t27 = (t26 * -1);
    t28 = (1U * t27);
    t29 = (0 + t28);
    t24 = (t25 + t29);
    t30 = *((unsigned char *)t24);
    t31 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t23, t30);
    t32 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t31);
    t33 = (t0 + 6560);
    t34 = (t33 + 56U);
    t35 = *((char **)t34);
    t36 = (t35 + 56U);
    t37 = *((char **)t36);
    *((unsigned char *)t37) = t32;
    xsi_driver_first_trans_delta(t33, 2U, 1, 0LL);

LAB2:    t38 = (t0 + 5648);
    *((int *)t38) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}


extern void work_a_3621587254_3212880686_init()
{
	static char *pe[] = {(void *)work_a_3621587254_3212880686_p_0,(void *)work_a_3621587254_3212880686_p_1,(void *)work_a_3621587254_3212880686_p_2,(void *)work_a_3621587254_3212880686_p_3,(void *)work_a_3621587254_3212880686_p_4};
	xsi_register_didat("work_a_3621587254_3212880686", "isim/prueba_isim_beh.exe.sim/work/a_3621587254_3212880686.didat");
	xsi_register_executes(pe);
}
