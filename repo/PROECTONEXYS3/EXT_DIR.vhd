
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



entity EXT_DIR is
    Port ( DIN : in  STD_LOGIC_VECTOR (11 downto 0);
           DOUT : out  STD_LOGIC_VECTOR (15 downto 0));
end EXT_DIR;

architecture Behavioral of EXT_DIR is
SIGNAL DS: STD_LOGIC_VECTOR(15 DOWNTO 0);
begin

DOUT<= "0000"&DIN;


end Behavioral;

