--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
-- Author : Leonardo Faydella
-- Create Date:   17:29:47 09/29/2018
-- Design Name:   
-- Module Name:   C:/Users/leonardo faydella/Desktop/ProyectoFinal/barrel_shifter/TESTBENCH_BARREL.vhd
-- Project Name:  barrel_shifter
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Barrel
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TESTBENCH_BARREL IS
END TESTBENCH_BARREL;
 
ARCHITECTURE behavior OF TESTBENCH_BARREL IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Barrel
    PORT(
         DIR : IN  std_logic;
         SHAMT : IN  std_logic_vector(3 downto 0);
         READ_DATA2 : IN  std_logic_vector(15 downto 0);
         Q : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal DIR : std_logic := '0';
   signal SHAMT : std_logic_vector(3 downto 0) := (others => '0');
   signal READ_DATA2 : std_logic_vector(15 downto 0) := (others => '0');

 	--Outputs
   signal Q : std_logic_vector(15 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 

 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Barrel PORT MAP (
          DIR => DIR,
          SHAMT => SHAMT,
          READ_DATA2 => READ_DATA2,
          Q => Q
        );

   -- Clock process definitions


   -- Stimulus process
   stim_proc: process
   begin		
     
	  DIR <= '0';
	  SHAMT <= "0000";
	  READ_DATA2 <= "0000000000000001";
     wait for 100 ns;
	  DIR <= '0';
	  SHAMT <= "0001";
	  READ_DATA2 <= "0000000000000001";
     wait for 100 ns;	
	  DIR <= '0';
	  SHAMT <= "0010";
	  READ_DATA2 <= "0000000000000001";
     wait for 100 ns;
		DIR <= '0';
	  SHAMT <= "0011";
	  READ_DATA2 <= "0000000000000001";
     wait for 100 ns;
		DIR <= '0';
	  SHAMT <= "0100";
	  READ_DATA2 <= "0000000000000001";
     wait for 100 ns;	
	  DIR <= '0';
	  SHAMT <= "0101";
	  READ_DATA2 <= "0000000000000001";
     wait for 100 ns;	
	  DIR <= '0';
	  SHAMT <= "0110";
	  READ_DATA2 <= "0000000000000001";
     wait for 100 ns;	
	  DIR <= '0';
	  SHAMT <= "0111";
	  READ_DATA2 <= "0000000000000001";
     wait for 100 ns;	
	  DIR <= '0';
	  SHAMT <= "1000";
	  READ_DATA2 <= "0000000000000001";
     wait for 100 ns;	
	  DIR <= '0';
	  SHAMT <= "1001";
	  READ_DATA2 <= "0000000000000001";
     wait for 100 ns;	
	  DIR <= '0';
	  SHAMT <= "1010";
	  READ_DATA2 <= "0000000000000001";
     wait for 100 ns;
	  DIR <= '0';
	  SHAMT <= "1011";
	  READ_DATA2 <= "0000000000000001";
     wait for 100 ns;
	  DIR <= '0';
	  SHAMT <= "1100";
	  READ_DATA2 <= "0000000000000001";
     wait for 100 ns;
	  DIR <= '0';
	  SHAMT <= "1101";
	  READ_DATA2 <= "0000000000000001";
     wait for 100 ns;
	   DIR <= '0';
	  SHAMT <= "1110";
	  READ_DATA2 <= "0000000000000001";
     wait for 100 ns;
	  DIR <= '0';
	  SHAMT <= "1111";
	  READ_DATA2 <= "0000000000000001";
     wait for 100 ns;
	  DIR <= '0';
	  SHAMT <= "1101";
	  READ_DATA2 <= "0000000000000101";
     wait for 100 ns;
	  
	  --fin corrimiento izquierda
	  DIR <= '1';
	  SHAMT <= "0000";
	  READ_DATA2 <= "1000000000000000";
     wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "0001";
	  READ_DATA2 <= "1000000000000000";
	  wait for 100 ns;	
	  DIR <= '1';
	  SHAMT <= "0010";
	  READ_DATA2 <= "1000000000000000";
	  wait for 100 ns;
		DIR <= '1';
	  SHAMT <= "0011";
	  READ_DATA2 <= "1000000000000000";
	  wait for 100 ns;
		DIR <= '1';
	  SHAMT <= "0100";
	  READ_DATA2 <= "1000000000000000";
	  wait for 100 ns;	
	  DIR <= '1';
	  SHAMT <= "0101";
	  READ_DATA2 <= "1000000000000000";
	  wait for 100 ns;	
	  DIR <= '1';
	  SHAMT <= "0110";
	  READ_DATA2 <= "1000000000000000";
	  wait for 100 ns;	
	  DIR <= '1';
	  SHAMT <= "0111";
	  READ_DATA2 <= "1000000000000000";
	  wait for 100 ns;	
	  DIR <= '1';
	  SHAMT <= "1000";
	  READ_DATA2 <= "1000000000000000";
	  wait for 100 ns;	
	  DIR <= '1';
	  SHAMT <= "1001";
	  READ_DATA2 <= "1000000000000000";
	  wait for 100 ns;	
	  DIR <= '1';
	  SHAMT <= "1010";
	  READ_DATA2 <= "1000000000000000";
	  wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "1011";
	  READ_DATA2 <= "1000000000000000";
	  wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "1100";
	  READ_DATA2 <= "1000000000000000";
     wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "1101";
	  READ_DATA2 <= "1000000000000000";
     wait for 100 ns;
	   DIR <= '1';
	  SHAMT <= "1110";
	  READ_DATA2 <= "1000000000000000";
     wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "1111";
	  READ_DATA2 <= "1000000000000000";
     wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "1100";
	  READ_DATA2 <= "1011000000000000";
     wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "0100";
	  READ_DATA2 <= "1111011111111111";
     wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "1000";
	  READ_DATA2 <= "1111011111111111";
     wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "1000";
	  READ_DATA2 <= "0000000000000010";
     wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "0001";
	  READ_DATA2 <= "0000000000000011";
     wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "0001";
	  READ_DATA2 <= "0000000000000100";
     wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "0100";
	  READ_DATA2 <= "0000000000100000";
     wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "0101";
	  READ_DATA2 <= "0000000000100000";
     wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "0110";
	  READ_DATA2 <= "0000000000100000";
     wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "1000";
	  READ_DATA2 <= "0000011110100000";
     wait for 100 ns;
		DIR <= '1';
	  SHAMT <= "0000";
	  READ_DATA2 <= "1100110011001100";
     wait for 100 ns;
     DIR <= '1';
	  SHAMT <= "0000";
	  READ_DATA2 <= "1100110011001100";
     wait for 100 ns;
		DIR <= '1';
	  SHAMT <= "0001";
	  READ_DATA2 <= "1100110011001100";
     wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "0010";
	  READ_DATA2 <= "1100110011001100";
     wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "0011";
	  READ_DATA2 <= "1100110011001100";
     wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "0100";
	  READ_DATA2 <= "1100110011001100";
     wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "0101";
	  READ_DATA2 <= "1100110011001100";
     wait for 100 ns;
	DIR <= '1';
	  SHAMT <= "0110";
	  READ_DATA2 <= "1100110011001100";
     wait for 100 ns;
DIR <= '1';
	  SHAMT <= "0111";
	  READ_DATA2 <= "1100110011001100";
     wait for 100 ns;
DIR <= '1';
	  SHAMT <= "1000";
	  READ_DATA2 <= "1100110011001100";
     wait for 100 ns;
DIR <= '1';
	  SHAMT <= "1001";
	  READ_DATA2 <= "1100110011001100";
     wait for 100 ns;
DIR <= '1';
	  SHAMT <= "1010";
	  READ_DATA2 <= "1100110011001100";
     wait for 100 ns;
DIR <= '1';
	  SHAMT <= "1011";
	  READ_DATA2 <= "1100110011001100";
     wait for 100 ns;
DIR <= '1';
	  SHAMT <= "1100";
	  READ_DATA2 <= "1100110011001100";
     wait for 100 ns;
DIR <= '1';
	  SHAMT <= "1101";
	  READ_DATA2 <= "1100110011001100";
     wait for 100 ns;
DIR <= '1';
	  SHAMT <= "1110";
	  READ_DATA2 <= "1100110011001100";
     wait for 100 ns;
DIR <= '1';
	  SHAMT <= "1111";
	  READ_DATA2 <= "1100110011001100";
     wait for 100 ns;
	  DIR <= '1';
	  SHAMT <= "1111";
	  READ_DATA2 <= "1111111111111111";
     wait for 100 ns;
DIR <= '0';
	  SHAMT <= "1111";
	  READ_DATA2 <= "1111111111111111";
     wait for 100 ns;



	  
	  
	  
	  
		


      wait;
   end process;

END;
