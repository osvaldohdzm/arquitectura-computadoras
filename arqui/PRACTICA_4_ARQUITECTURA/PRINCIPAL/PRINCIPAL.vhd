
library IEEE;
library work;
use work.PAQUETILLO.all;
use IEEE.STD_LOGIC_1164.ALL;

entity PRINCIPAL is
    Port ( clk,clr,she,dir,write_reg : in  STD_LOGIC;
           SHAMT,WR,read_reg1,read_reg2 : in  STD_LOGIC_VECTOR (3 downto 0);
           write_data : in  STD_LOGIC_VECTOR (15 downto 0);
           read_data1,read_data2 : out  STD_LOGIC_VECTOR (15 downto 0));
end PRINCIPAL;

architecture Behavioral of PRINCIPAL is
SIGNAL L,D,RD2,SHIFT_DATA: STD_LOGIC_VECTOR(15 DOWNTO 0);
begin
	DEM : DEMUX PORT MAP(WR,L,write_reg);
	ARR_REG: Arreglo PORT MAP (clk,clr,L,D,read_reg1,read_reg2,RD2,read_data1); 
	BARR_SHIFT: Barrel PORT MAP (dir, SHAMT, RD2, SHIFT_DATA); 
	read_data2 <= RD2;
	
	
	
	D <= write_DATA WHEN SHE = '0' ELSE
		  SHIFT_DATA WHEN SHE = '1';
		  
end Behavioral;

