
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
 
ENTITY sim_demux IS
END sim_demux;
 
ARCHITECTURE behavior OF sim_demux IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT DemuxCodOpera
    PORT(
         codigo : IN  std_logic_vector(4 downto 0);
         sal : OUT  std_logic_vector(4 downto 0);
         sdopc : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal codigo : std_logic_vector(4 downto 0) := (others => '0');
   signal sdopc : std_logic := '0';

 	--Outputs
   signal sal : std_logic_vector(4 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: DemuxCodOpera PORT MAP (
          codigo => codigo,
          sal => sal,
          sdopc => sdopc
        );

 

   -- Stimulus process
   stim_proc: process
   begin		
      -- insert stimulus here 
		sdopc <= '1';
		codigo <= "10101";
      wait for 20 ns;
		sdopc <= '0';
		codigo <= "10101";
      wait;
   end process;

END;
