-- ESCOMips | Pr�ctica 6B - Pila | Marco Antonio Rubio Cort�s

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

--use ieee.numeric_std.all;

entity practica6B is
	generic
	(
		constant busSize 					: integer := 16;
		constant stackPointerSize		: integer := 3
	);

	port
	(
		pcData				: in std_logic_vector(busSize-1 downto 0);
		programCounter		: out std_logic_vector(busSize-1 downto 0);
		L						: in std_logic;
		UP						: in std_logic;
		DW						: in std_logic;
		clk					: in std_logic;
		clr					: in std_logic
	);
end practica6B;

architecture A_practica6B of practica6B is

type stack is array (0 to (2**stackPointerSize)-1) of std_logic_vector (busSize-1 downto 0);
signal PC: stack;

--55 69 31 68 48

begin
	
	process(clk, clr, PC)
	variable SP: integer range 0 to (2**stackPointerSize)-1;
	
	begin
		if(clr = '1') then
			PC <= (others => (others => '0'));
			SP := 0;
		elsif(rising_edge(clk)) then
			if(L = '1' and UP = '0' and DW = '0') then				-- B
				PC(SP) <= pcData;
			elsif(L = '1' and UP = '1' and DW = '0') then			-- CALL
				SP := SP + 1;
				PC(SP) <= pcData;
			elsif(L = '0' and UP = '0' and DW = '1') then			-- RET
				SP := SP - 1;
				PC(SP) <= PC(SP) + 1;
			else
				PC(SP) <= PC(SP) + 1;
			end if;
		end if;
		
		programCounter <= PC(SP);
	end process;
	
end A_practica6B;

