/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/Caos/Desktop/ESCOM/2017-2/Materias/Arqui/Practicas/Practica6/Lab6/practica6/P6_TB.vhd";
extern char *STD_TEXTIO;
extern char *IEEE_P_3564397177;
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
void ieee_p_3564397177_sub_1281154728_91900896(char *, char *, char *, char *, char *, unsigned char , int );
void ieee_p_3564397177_sub_1496949865_91900896(char *, char *, char *, unsigned char , unsigned char , int );
void ieee_p_3564397177_sub_2743816878_91900896(char *, char *, char *, char *);
void ieee_p_3564397177_sub_3205433008_91900896(char *, char *, char *, char *, char *, unsigned char , int );
void ieee_p_3564397177_sub_3988856810_91900896(char *, char *, char *, char *, char *);


static void work_a_1803361737_2372691052_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    int64 t7;
    int64 t8;

LAB0:    t1 = (t0 + 4888U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(61, ng0);
    t2 = (t0 + 5536);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(62, ng0);
    t2 = (t0 + 2448U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 4696);
    xsi_process_wait(t2, t8);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(63, ng0);
    t2 = (t0 + 5536);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(64, ng0);
    t2 = (t0 + 2448U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 4696);
    xsi_process_wait(t2, t8);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    goto LAB2;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

}

static void work_a_1803361737_2372691052_p_1(char *t0)
{
    char t5[16];
    char t10[8];
    char t11[8];
    char t12[8];
    char t13[8];
    char t14[8];
    char t15[8];
    char t16[8];
    char t17[8];
    char t25[8];
    char t26[32];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    int t8;
    unsigned int t9;
    int64 t18;
    unsigned char t19;
    unsigned char t20;
    unsigned int t21;
    unsigned int t22;
    char *t23;
    char *t24;

LAB0:    t1 = (t0 + 5136U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(94, ng0);
    t2 = (t0 + 3736U);
    t3 = (t0 + 10577);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 12;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (12 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)0);
    xsi_set_current_line(95, ng0);
    t2 = (t0 + 3840U);
    t3 = (t0 + 10589);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 10;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (10 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)1);
    xsi_set_current_line(97, ng0);
    t2 = (t0 + 10599);
    t4 = (t0 + 4304U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(98, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4088U);
    t4 = (t0 + 4304U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t10, t7, 5U);
    t6 = (t0 + 10292U);
    std_textio_write7(STD_TEXTIO, t2, t3, t10, t6, (unsigned char)0, 5);
    xsi_set_current_line(99, ng0);
    t2 = (t0 + 10604);
    t4 = (t0 + 4304U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(100, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4088U);
    t4 = (t0 + 4304U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t11, t7, 5U);
    t6 = (t0 + 10292U);
    std_textio_write7(STD_TEXTIO, t2, t3, t11, t6, (unsigned char)0, 5);
    xsi_set_current_line(101, ng0);
    t2 = (t0 + 10609);
    t4 = (t0 + 4304U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(102, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4088U);
    t4 = (t0 + 4304U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t12, t7, 5U);
    t6 = (t0 + 10292U);
    std_textio_write7(STD_TEXTIO, t2, t3, t12, t6, (unsigned char)0, 5);
    xsi_set_current_line(103, ng0);
    t2 = (t0 + 10614);
    t4 = (t0 + 4304U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(104, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4088U);
    t4 = (t0 + 4304U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t13, t7, 5U);
    t6 = (t0 + 10292U);
    std_textio_write7(STD_TEXTIO, t2, t3, t13, t6, (unsigned char)0, 5);
    xsi_set_current_line(105, ng0);
    t2 = (t0 + 10619);
    t4 = (t0 + 4304U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(106, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4088U);
    t4 = (t0 + 4304U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t14, t7, 5U);
    t6 = (t0 + 10292U);
    std_textio_write7(STD_TEXTIO, t2, t3, t14, t6, (unsigned char)0, 5);
    xsi_set_current_line(107, ng0);
    t2 = (t0 + 10624);
    t4 = (t0 + 4304U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(108, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4088U);
    t4 = (t0 + 4304U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t15, t7, 5U);
    t6 = (t0 + 10292U);
    std_textio_write7(STD_TEXTIO, t2, t3, t15, t6, (unsigned char)0, 5);
    xsi_set_current_line(109, ng0);
    t2 = (t0 + 10629);
    t4 = (t0 + 4304U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(110, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4088U);
    t4 = (t0 + 4304U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t16, t7, 5U);
    t6 = (t0 + 10292U);
    std_textio_write7(STD_TEXTIO, t2, t3, t16, t6, (unsigned char)0, 5);
    xsi_set_current_line(111, ng0);
    t2 = (t0 + 10634);
    t4 = (t0 + 4304U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(112, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4088U);
    t4 = (t0 + 4304U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t17, t7, 5U);
    t6 = (t0 + 10292U);
    std_textio_write7(STD_TEXTIO, t2, t3, t17, t6, (unsigned char)0, 5);
    xsi_set_current_line(113, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 3840U);
    t4 = (t0 + 4088U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    xsi_set_current_line(115, ng0);
    t18 = (20 * 1000LL);
    t2 = (t0 + 4944);
    xsi_process_wait(t2, t18);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(118, ng0);

LAB8:    t2 = (t0 + 3736U);
    t19 = std_textio_endfile(t2);
    t20 = (!(t19));
    if (t20 != 0)
        goto LAB9;

LAB11:    xsi_set_current_line(152, ng0);
    t2 = (t0 + 3736U);
    std_textio_file_close(t2);
    xsi_set_current_line(153, ng0);
    t2 = (t0 + 3840U);
    std_textio_file_close(t2);
    xsi_set_current_line(155, ng0);

LAB18:    *((char **)t1) = &&LAB19;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB9:    xsi_set_current_line(119, ng0);
    t3 = (t0 + 4944);
    t4 = (t0 + 3736U);
    t6 = (t0 + 4016U);
    std_textio_readline(STD_TEXTIO, t3, t4, t6);
    xsi_set_current_line(121, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4016U);
    t4 = (t0 + 2568U);
    t6 = *((char **)t4);
    t4 = (t0 + 10244U);
    ieee_p_3564397177_sub_3988856810_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(122, ng0);
    t2 = (t0 + 2568U);
    t3 = *((char **)t2);
    t9 = (15 - 10);
    t21 = (t9 * 1U);
    t22 = (0 + t21);
    t2 = (t3 + t22);
    t4 = (t0 + 5600);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t23 = (t7 + 56U);
    t24 = *((char **)t23);
    memcpy(t24, t2, 11U);
    xsi_driver_first_trans_fast(t4);
    xsi_set_current_line(123, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4016U);
    t4 = (t0 + 2808U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_2743816878_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(124, ng0);
    t2 = (t0 + 2808U);
    t3 = *((char **)t2);
    t19 = *((unsigned char *)t3);
    t2 = (t0 + 5664);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t23 = *((char **)t7);
    *((unsigned char *)t23) = t19;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(125, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4016U);
    t4 = (t0 + 2928U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_2743816878_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(126, ng0);
    t2 = (t0 + 2928U);
    t3 = *((char **)t2);
    t19 = *((unsigned char *)t3);
    t2 = (t0 + 5728);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t23 = *((char **)t7);
    *((unsigned char *)t23) = t19;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(127, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4016U);
    t4 = (t0 + 2688U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_2743816878_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(128, ng0);
    t2 = (t0 + 2688U);
    t3 = *((char **)t2);
    t19 = *((unsigned char *)t3);
    t2 = (t0 + 5792);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t23 = *((char **)t7);
    *((unsigned char *)t23) = t19;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(129, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4016U);
    t4 = (t0 + 3048U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_2743816878_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(130, ng0);
    t2 = (t0 + 3048U);
    t3 = *((char **)t2);
    t19 = *((unsigned char *)t3);
    t2 = (t0 + 5856);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t23 = *((char **)t7);
    *((unsigned char *)t23) = t19;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(132, ng0);

LAB14:    t2 = (t0 + 5456);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB15;
    goto LAB1;

LAB10:;
LAB12:    t4 = (t0 + 5456);
    *((int *)t4) = 0;
    xsi_set_current_line(134, ng0);
    t2 = (t0 + 1672U);
    t3 = *((char **)t2);
    t19 = *((unsigned char *)t3);
    t2 = (t0 + 3168U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    *((unsigned char *)t2) = t19;
    xsi_set_current_line(135, ng0);
    t2 = (t0 + 1992U);
    t3 = *((char **)t2);
    t2 = (t0 + 3288U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 25U);
    xsi_set_current_line(136, ng0);
    t2 = (t0 + 2152U);
    t3 = *((char **)t2);
    t2 = (t0 + 3408U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 3U);
    xsi_set_current_line(139, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4088U);
    t4 = (t0 + 3168U);
    t6 = *((char **)t4);
    t19 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t19, (unsigned char)0, 5);
    xsi_set_current_line(140, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4088U);
    t4 = (t0 + 3048U);
    t6 = *((char **)t4);
    t19 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t19, (unsigned char)0, 5);
    xsi_set_current_line(141, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4088U);
    t4 = (t0 + 2568U);
    t6 = *((char **)t4);
    memcpy(t5, t6, 16U);
    t4 = (t0 + 10244U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t5, t4, (unsigned char)0, 5);
    xsi_set_current_line(142, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4088U);
    t4 = (t0 + 2688U);
    t6 = *((char **)t4);
    t19 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t19, (unsigned char)0, 5);
    xsi_set_current_line(143, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4088U);
    t4 = (t0 + 2808U);
    t6 = *((char **)t4);
    t19 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t19, (unsigned char)0, 5);
    xsi_set_current_line(144, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4088U);
    t4 = (t0 + 2928U);
    t6 = *((char **)t4);
    t19 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t19, (unsigned char)0, 5);
    xsi_set_current_line(145, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4088U);
    t4 = (t0 + 3408U);
    t6 = *((char **)t4);
    memcpy(t25, t6, 3U);
    t4 = (t0 + 10276U);
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t25, t4, (unsigned char)0, 5);
    xsi_set_current_line(146, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 4088U);
    t4 = (t0 + 3288U);
    t6 = *((char **)t4);
    memcpy(t26, t6, 25U);
    t4 = (t0 + 10260U);
    t8 = (25U + 2);
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t26, t4, (unsigned char)0, t8);
    xsi_set_current_line(148, ng0);
    t2 = (t0 + 4944);
    t3 = (t0 + 3840U);
    t4 = (t0 + 4088U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    goto LAB8;

LAB13:    t3 = (t0 + 1632U);
    t19 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t3, 0U, 0U);
    if (t19 == 1)
        goto LAB12;
    else
        goto LAB14;

LAB15:    goto LAB13;

LAB16:    goto LAB2;

LAB17:    goto LAB16;

LAB19:    goto LAB17;

}


extern void work_a_1803361737_2372691052_init()
{
	static char *pe[] = {(void *)work_a_1803361737_2372691052_p_0,(void *)work_a_1803361737_2372691052_p_1};
	xsi_register_didat("work_a_1803361737_2372691052", "isim/P6_TB_isim_beh.exe.sim/work/a_1803361737_2372691052.didat");
	xsi_register_executes(pe);
}
