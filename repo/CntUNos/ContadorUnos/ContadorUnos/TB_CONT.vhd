--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   00:02:45 06/06/2018
-- Design Name:   
-- Module Name:   /home/ise/Desktop/ContadorUnos/ContadorUnos/TB_CONT.vhd
-- Project Name:  ContadorUnos
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Contador
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TB_CONT IS
END TB_CONT;
 
ARCHITECTURE behavior OF TB_CONT IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Contador
    PORT(
         D : IN  std_logic_vector(4 downto 0);
         L : IN  std_logic;
         E : IN  std_logic;
         Q : INOUT  std_logic_vector(4 downto 0);
         CLK : IN  std_logic;
         CLR : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal D : std_logic_vector(4 downto 0) := (others => '0');
   signal L : std_logic := '0';
   signal E : std_logic := '0';
   signal CLK : std_logic := '0';
   signal CLR : std_logic := '0';

	--BiDirs
   signal Q : std_logic_vector(4 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Contador PORT MAP (
          D => D,
          L => L,
          E => E,
          Q => Q,
          CLK => CLK,
          CLR => CLR
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc1: process
   begin	
      wait for 100 ns;	
		D<="0000";
		CLR<='1';
		L<='0';
		E<='1';
   end process;
		
END;
