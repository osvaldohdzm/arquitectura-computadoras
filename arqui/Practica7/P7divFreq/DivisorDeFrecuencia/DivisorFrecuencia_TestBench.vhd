
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
  
ENTITY DivisorFrecuencia_TestBench IS
END DivisorFrecuencia_TestBench;
 
ARCHITECTURE behavior OF DivisorFrecuencia_TestBench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT DivisorFrecuencia_codigo
    PORT(
         osc_clk : IN  std_logic;
         clr : IN  std_logic;
			cont : OUT std_logic_vector (24 downto 0);
         clk : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal osc_clk : std_logic := '0';
   signal clr : std_logic := '0';

 	--Outputs
	signal cont :std_logic_vector (24 downto 0);
   signal clk : std_logic;

   -- Clock period definitions
   constant osc_clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: DivisorFrecuencia_codigo PORT MAP (
          osc_clk => osc_clk,
          clr => clr,
			 cont => cont,
          clk => clk
        );

   -- Clock process definitions
   osc_clk_process :process
   begin
		osc_clk <= '0';
		wait for osc_clk_period/2;
		osc_clk <= '1';
		wait for osc_clk_period/2;
   end process;

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
      -- insert stimulus here 
		clr <= '1';
		wait for 8 ns;
		clr <= '0';
		wait for 800 ns;
      wait;
   end process;

END;
