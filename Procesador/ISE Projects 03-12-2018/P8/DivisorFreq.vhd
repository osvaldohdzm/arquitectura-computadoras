library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity DivisorFreq is
   port(
 	 Clock: in std_logic;
 	 Reset: in std_logic;
 	 Output: out std_logic );
end DivisorFreq;
 
architecture Behavioral of DivisorFreq is
   signal temp: std_logic_vector(25 downto 0) := "00000000000000000000000000";
begin   process(Clock,Reset)
   begin
      if Reset='1' then
         temp <= "00000000000000000000000000";
      elsif(rising_edge(Clock)) then
         temp <= temp + 1;
      end if;
   end process;
   Output <= temp(25);
end Behavioral;