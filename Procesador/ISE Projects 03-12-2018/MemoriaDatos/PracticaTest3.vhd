LIBRARY ieee;
LIBRARY std;
use std.textio.all;
USE ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_textio.all;

 
ENTITY PracticaTest3 IS
	GENERIC(
		m : integer := 9;
		n : integer := 8
	);
END PracticaTest3;
 
ARCHITECTURE behavior OF PracticaTest3 IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
	COMPONENT MemoriaDatos
	PORT(
		dir : IN  std_logic_vector(m - 1 downto 0);
		data_in : IN  std_logic_vector(n - 1 downto 0);
		wd : IN  std_logic;
		clk : IN  std_logic;
		data_out : OUT  std_logic_vector(n - 1 downto 0)
	);
	END COMPONENT;
    

   --Inputs
   signal dir : std_logic_vector(m - 1 downto 0) := (others => '0');
   signal data_in : std_logic_vector(n - 1 downto 0) := (others => '0');
   signal wd : std_logic := '0';
   signal clk : std_logic := '0';

 	--Outputs
   signal data_out : std_logic_vector(n - 1 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: MemoriaDatos PORT MAP (
          dir => dir,
          data_in => data_in,
          wd => wd,
          clk => clk,
          data_out => data_out
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
	file ARCH_RES : TEXT;
	file ARCH_VEC : TEXT;
	
	variable LINEA_RES : line;
	variable LINEA_VEC : line;

	VARIABLE VAR_DIR : STD_LOGIC_VECTOR(m + 2 DOWNTO 0);
	VARIABLE VAR_DATA_IN : STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
	VARIABLE VAR_DATA_OUT : STD_LOGIC_VECTOR(n - 1 DOWNTO 0);
	VARIABLE VAR_WD : STD_LOGIC;
	
	VARIABLE CADENA : STRING(1 TO 3);
	VARIABLE CADENA_I : STRING(1 TO 6);
	VARIABLE CADENA_X : STRING(1 TO 5);

   begin		
		file_open(ARCH_RES, "./ARCHIVO_SALIDA_2.txt", WRITE_MODE); 	
		file_open(ARCH_VEC, "./ARCHIVO_ENTRADA_2.txt", READ_MODE); 	

		CADENA_X := "  DIR";
		write(LINEA_RES, CADENA_X, right, CADENA_X'LENGTH+1);	
		CADENA_I := "  D_in";
		write(LINEA_RES, CADENA_I, right, CADENA'LENGTH+1);
		CADENA := " WD";
		write(LINEA_RES, CADENA, right, CADENA_I'LENGTH+1);	
		CADENA_I := " D_out";
		write(LINEA_RES, CADENA_I, right, CADENA'LENGTH+1);
		writeline(ARCH_RES,LINEA_RES);
      wait for 100 ns;	
 
		FOR I IN 0 TO 5 LOOP
			readline(ARCH_VEC,LINEA_VEC);
			
			hread(LINEA_VEC, VAR_DIR);
			dir<=VAR_DIR(8 downto 0);
			
			hread(LINEA_VEC, VAR_DATA_IN);
			data_in<=VAR_DATA_IN;

			read(LINEA_VEC, VAR_WD);
			wd<=VAR_WD;
			
			WAIT UNTIL RISING_EDGE(CLK);
			
			VAR_DATA_OUT := data_out;
			VAR_DIR := "000"&dir;
	
			hwrite(LINEA_RES, VAR_DIR, 	right, 5);
			hwrite(LINEA_RES, VAR_DATA_IN, 	right, 5);
			write(LINEA_RES, VAR_WD, 	right, 5);
			hwrite (LINEA_RES, VAR_DATA_OUT , right, 5);		
			writeline(ARCH_RES,LINEA_RES);

		end loop;
		
		file_close(ARCH_VEC);
		file_close(ARCH_RES);  

      wait for CLK_period*10;

      wait;
   end process;

END;
