
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



entity CONDI is
    Port ( RBANDERAS : in  STD_LOGIC_VECTOR (3 downto 0);
           EQ : out  STD_LOGIC;
           NEQ : out  STD_LOGIC;
           LT : out  STD_LOGIC;
           LE : out  STD_LOGIC;
           GTI : out  STD_LOGIC;
           GET : out  STD_LOGIC);
end CONDI;
--RBANDERAS(3) = OV RBANDERAS(2)= N
-- RBANDERAS(1)=Z RBANDERAS(0)=C
architecture Behavioral of CONDI is

begin
EQ <= RBANDERAS(1); -- Q = Z
NEQ<=NOT RBANDERAS(1); -- Q = NOT Z
GTI<=(NOT RBANDERAS(1) AND NOT(RBANDERAS(2) XOR RBANDERAS(3))); -- Q=NOT Z AND NOT(OV XOR N)
GET<=(NOT(RBANDERAS(2) XOR RBANDERAS(3)) OR RBANDERAS(1)); -- Q = NOT(OV XOR N) OR Z
LT <= (RBANDERAS(3) XOR RBANDERAS(2))AND NOT RBANDERAS(1); -- Q = (OV XOR N) AND NOT Z
LE <=(RBANDERAS(3) XOR RBANDERAS(2)) OR RBANDERAS(1); -- Q=(OV XOR N) OR Z
 
end Behavioral;

