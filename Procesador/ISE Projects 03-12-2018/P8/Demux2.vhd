
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Demux2 is
    Port ( deco_sal : in  STD_LOGIC_VECTOR (6 downto 0);
           ec : in  STD_LOGIC;
           sal : out  STD_LOGIC_VECTOR (6 downto 0));
end Demux2;

architecture Behavioral of Demux2 is
begin
	process(ec, deco_sal)
	begin
		if(ec = '1') then
			sal <= deco_sal;
		else
			sal <= "1000000"; -- guion
		end if;
	end process;

end Behavioral;

