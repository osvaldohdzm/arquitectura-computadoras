
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



entity DEMUX is
    Port ( 
			  WR : in  STD_LOGIC_VECTOR (3 downto 0);
			  L: out std_logic_vector(15 downto 0);
           WRITE_REG : in  STD_LOGIC);
end DEMUX;

architecture Behavioral of DEMUX is
begin

DEM: PROCESS(WRITE_REG,WR)
begin
	case WRITE_REG&WR is
		when "10000" => L <= "0000000000000001";
		when "10001" => L <= "0000000000000010";
		when "10010" => L <= "0000000000000100";
		when "10011" => L <= "0000000000001000";
		when "10100" => L <= "0000000000010000";
		when "10101" => L <= "0000000000100000";
		when "10110" => L <= "0000000001000000";
		when "10111" => L <= "0000000010000000";
		when "11000" => L <= "0000000100000000";
		when "11001" => L <= "0000001000000000";
		when "11010" => L <= "0000010000000000";
		when "11011" => L <= "0000100000000000";
		when "11100" => L <= "0001000000000000";
		when "11101" => L <= "0010000000000000";
		when "11110" => L <= "0100000000000000";
		when "11111" => L <= "1000000000000000";
		when others   => L <= "0000000000000000";
	end case;
END PROCESS DEM;

end Behavioral;

