LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY nivel_TestBench IS
END nivel_TestBench;
 
ARCHITECTURE behavior OF nivel_TestBench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT nivel
    PORT(
         clk : IN  std_logic;
         clr : IN  std_logic;
         na : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';

 	--Outputs
   signal na : std_logic;

   -- Clock period definitions
   constant clk_period : time := 50 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: nivel PORT MAP (
          clk => clk,
          clr => clr,
          na => na
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin
		clr <= '1';
      wait for 10 ns;
		clr <= '0';
      wait;
   end process;

END;
