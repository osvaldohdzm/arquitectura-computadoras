library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux_opCode is
    Port ( op_code : in  STD_LOGIC_VECTOR (4 downto 0);
           SDOPC : in  STD_LOGIC;
           a : out  STD_LOGIC_VECTOR (4 downto 0));
end mux_opCode;

architecture Behavioral of mux_opCode is

begin
	with SDOPC select a <=
		"00000" when '0',
		op_code when others;		

end Behavioral;

