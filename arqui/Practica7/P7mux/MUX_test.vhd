LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY MUX_test IS
END MUX_test;
 
ARCHITECTURE behavior OF MUX_test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT MUX
    PORT(
         b : IN  std_logic_vector(6 downto 0);
         Salida : OUT  std_logic_vector(6 downto 0);
         sel : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal b : std_logic_vector(6 downto 0) := (others => '0');
   signal sel : std_logic := '0';

 	--Outputs
   signal Salida : std_logic_vector(6 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
  
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: MUX PORT MAP (
          b => b,
          Salida => Salida,
          sel => sel
        );
   -- Stimulus process
   stim_proc: process
   begin		
      b <= "1010101"; --guion
		sel <= '0';
		wait for 20 ns;
		b <= "1010100"; --3
		sel <= '1';
		wait for 20 ns;
		b <= "1010111"; --guion
		sel <= '0';
		wait for 20 ns;
		b <= "1101101"; --5
		sel <= '1';
		wait for 20 ns;
      wait;
   end process;

END;
