
-- VHDL Instantiation Created from source file clk_divider.vhd -- 01:39:39 09/22/2017
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT clk_divider
	PORT(
		clk : IN std_logic;
		clr : IN std_logic;          
		Q26 : OUT std_logic
		);
	END COMPONENT;

	Inst_clk_divider: clk_divider PORT MAP(
		clk => ,
		clr => ,
		Q26 => 
	);


