
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

 
ENTITY CartaASM_TestBench IS
END CartaASM_TestBench;
 
ARCHITECTURE behavior OF CartaASM_TestBench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT CartaASM_codigo
    PORT(
         clr : IN  std_logic;
         clk : IN  std_logic;
         ini : IN  std_logic;
         a0 : IN  std_logic;
         z : IN  std_logic;
         la : OUT  std_logic;
         ea : OUT  std_logic;
         ec : OUT  std_logic;
         eb : OUT  std_logic;
         lb : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clr : std_logic := '0';
   signal clk : std_logic := '0';
   signal ini : std_logic := '0';
   signal a0 : std_logic := '0';
   signal z : std_logic := '0';

 	--Outputs
   signal la : std_logic;
   signal ea : std_logic;
   signal ec : std_logic;
   signal eb : std_logic;
   signal lb : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: CartaASM_codigo PORT MAP (
          clr => clr,
          clk => clk,
          ini => ini,
          a0 => a0,
          z => z,
          la => la,
          ea => ea,
          ec => ec,
          eb => eb,
          lb => lb
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
      -- insert stimulus here 
		clr <= '1';
		ini <= '0';
		z <= '0';
		wait for 4 ns;
		clr <= '0';
		ini <= '1';
		z <= '0';
		wait for 4 ns;
		a0 <= '1';
		wait for 4 ns;
		z <= '1';
		wait for 4 ns;
		ini <= '0';
      wait;
   end process;

END;
