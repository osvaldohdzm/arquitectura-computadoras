
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY sim_condicion IS
END sim_condicion;
 
ARCHITECTURE behavior OF sim_condicion IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Condicion
    PORT(
         rbanderas : IN  std_logic_vector(3 downto 0);
         eq : OUT  std_logic;
         neq : OUT  std_logic;
         lt : OUT  std_logic;
         le : OUT  std_logic;
         gthan : OUT  std_logic;
         get : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal rbanderas : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal eq : std_logic;
   signal neq : std_logic;
   signal lt : std_logic;
   signal le : std_logic;
   signal gthan : std_logic;
   signal get : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Condicion PORT MAP (
          rbanderas => rbanderas,
          eq => eq,
          neq => neq,
          lt => lt,
          le => le,
          gthan => gthan,
          get => get
        );
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- insert stimulus here 
		rbanderas <= "1010";
      wait for 10 ns;
		rbanderas <= "0000";
      wait for 10 ns;
		rbanderas <= "1100";
      wait for 10 ns;
		rbanderas <= "0010";
      wait for 10 ns;
		rbanderas <= "0101";
      wait for 10 ns;
		rbanderas <= "0001";
      wait for 10 ns;
   end process;

END;
