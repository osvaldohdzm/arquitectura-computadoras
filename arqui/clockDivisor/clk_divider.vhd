----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    23:44:20 09/21/2017 
-- Design Name: 
-- Module Name:    clk_divider - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clk_divider is
    Port ( clk : in  STD_LOGIC;
           clr : in  STD_LOGIC;
           q25 : out  STD_LOGIC);
end clk_divider;

architecture divisor of clk_divider is
signal qaux:std_logic_vector(25 downto 0);
begin
	process(clk,clr)
		begin
			if(clr = '1') then qaux <= (others => '0');
			elsif(clk'event and clk ='1') then
				qaux <= qaux + 1;
			end if;
	end process;
	q25 <= qaux(25);
end divisor;