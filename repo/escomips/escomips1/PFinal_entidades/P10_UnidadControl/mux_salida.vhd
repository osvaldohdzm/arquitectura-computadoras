library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux_salida is
    Port ( d_fun, d_op_code : in  STD_LOGIC_VECTOR (19 downto 0);
           SM : in  STD_LOGIC;
           s : out  STD_LOGIC_VECTOR (19 downto 0));
end mux_salida;

architecture Behavioral of mux_salida is

begin
	with SM select s <=
		d_fun when '0',
		d_op_code when others;	

end Behavioral;

