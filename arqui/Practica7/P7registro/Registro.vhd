library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Registro is
    Port ( DATO : in  STD_LOGIC_VECTOR (8 downto 0);
           LA 	 : in  STD_LOGIC;
           EA   : in  STD_LOGIC;
			  clk  : in  STD_LOGIC;
			  clr  : in  STD_LOGIC;
           A    : out  STD_LOGIC_VECTOR (8 downto 0);
			  Z    : out STD_LOGIC
	 );
end Registro;

architecture Behavioral of Registro is
signal sel: STD_LOGIC_VECTOR (1 downto 0);
signal Reg: STD_LOGIC_VECTOR (8 downto 0);
begin
   sel <= (LA&EA);
   process(clk,clr)
      begin
         if (clr = '1') then
            Reg <= "000000000";
         elsif(clk'event and clk = '1') then
            if(sel = "10") then
				   Reg <= DATO;
				elsif(sel = "01") then
				   Reg <= to_stdlogicvector((to_bitvector(Reg)) SRL 1);
			   end if;
			end if;
	end process;
	Z <= not(Reg(8)or Reg(7)or Reg(6)or Reg(5)or Reg(4)or Reg(3)or Reg(2)or Reg(1)or Reg(0));
   A <= Reg;
end Behavioral;

