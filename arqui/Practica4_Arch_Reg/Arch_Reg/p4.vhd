----------------------------------------------------------------------------------
-- Archivo de Registros - Manuel Arreola 3CV2
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity p4 is
	generic(--16X16
		bus_dir : integer := 4;
		n : integer :=16;
		tam_palabra : integer := 16
	);
    Port ( 
			  clk,clr : in STD_LOGIC;
			  writeData : in  STD_LOGIC_VECTOR (n-1 downto 0); --16
           writeReg : in  STD_LOGIC_VECTOR (3 downto 0); --4
           wr : in  STD_LOGIC; --Escribir o no (1/0)
           readRegister1 : in  STD_LOGIC_VECTOR (3 downto 0); --4
			  readRegister2 : in  STD_LOGIC_VECTOR (3 downto 0); --4
           she : in  STD_LOGIC; --shift enable (1 Barrel /0 Write Data)
           dir : in  STD_LOGIC; -- Direcci�n de corrimiento (1 Izq / 0 Der)
           readData1 : inout  STD_LOGIC_VECTOR (n-1 downto 0);
           readData2 : out  STD_LOGIC_VECTOR (n-1 downto 0);
				shamt: in std_logic_vector(3 downto 0) --2 downto 0 (Cuanto a recorrer en dir)
				--d: in std_logic_vector(15 downto 0)		--7 downto 0
				);
end p4;

architecture Behavioral of p4 is
--se�ales y variables
type registros is array (0 to (2**bus_dir)-1) of std_logic_vector(tam_palabra-1 downto 0); --Para c/registro
signal arreglo : registros; --Para saber cual de los registros
signal q,muxc,e: std_logic_vector(15 downto 0); --La salida de X registro o lo que venga del barrel 

begin

--barrel shifter
process(shamt,dir,readData1)
variable aux : std_logic_vector(15 downto 0);
begin
aux := readData1; --de Reg1
	for i in 0 to 3 loop
		if(dir='0') then
			for j in 0 to n-1 loop --corrimiento der
				if(shamt(i) = '0') then --shamt va: 2 1 0 -> E0 E1 E2
					aux(j):= aux(j);
				else 
					if((j+2**i)<n) then
						aux(j):=aux((j+2**i));
					else
						aux(j) :='0';
					end if;
				end if;
			end loop;
		elsif(dir='1') then
			for j in n-1 downto 0 loop --corrimiento izq
				if(shamt(i) = '0') then --shamt va: 2 1 0 -> E0 E1 E2
					aux(j):= aux(j);
				else
					if((j-2**i)<0)then
						aux(j):='0';
					else
						aux(j):=aux(j-2**i);
					end if;
				end if;
			end loop;
		end if;
	end loop;
q <= aux; --valor que saldr� del Barrel
end process;

--mux corrimiento
process(she,q,writeData)
begin
		if(she='1') then --toma el valor que viene del Barrel Shifter
			muxc<=q;
		else
			muxc<=writeData; --Dice de que registro va a colocar el valor 
		end if;
end process;

--deco
process(wr,writeReg)
begin
	if(wr='1')then --cual de los registros habilita:
		case writeReg is
		when "0000" => e <= "0000000000000001";
		when "0001" => e <= "0000000000000010";
		when "0010" => e <= "0000000000000100";
		when "0011" => e <= "0000000000001000";
		when "0100" => e <= "0000000000010000";
		when "0101" => e <= "0000000000100000";
		when "0110" => e <= "0000000001000000";
		when "0111" => e <= "0000000010000000";
		when "1000" => e <= "0000000100000000";
		when "1001" => e <= "0000001000000000";
		when "1010" => e <= "0000010000000000";
		when "1011" => e <= "0000100000000000";
		when "1100" => e <= "0001000000000000";
		when "1101" => e <= "0010000000000000";
		when "1110" => e <= "0100000000000000";
		when "1111" => e <= "1000000000000000";
		when others => e <= (others=>'0');
		end case;
	else
		e <= (others=>'0');
	end if;
end process;

--banco de registros
process(clk,clr)
begin
	if(clr='1') then --clear
		for i in 0 to (2**bus_dir)-1 loop
				arreglo(i) <= (others => '0');
			end loop;
	elsif(clk'event and clk='1') then
	for h in n-1 downto 0 loop
		if(e(h)='1')then
			arreglo(h)<=muxc; --Toma el ReadData1 del Registro indicado
		end if;
	end loop;
	end if;
end process;

--mux de salida 1
process(readRegister1, arreglo)
begin
		case readRegister1 is
		when "0000" => readData1 <= arreglo(0);
		when "0001" => readData1 <= arreglo(1);
		when "0010" => readData1 <= arreglo(2);
		when "0011" => readData1 <= arreglo(3);
		when "0100" => readData1 <= arreglo(4);
		when "0101" => readData1 <= arreglo(5);
		when "0110" => readData1 <= arreglo(6);
		when "0111" => readData1 <= arreglo(7);
		when "1000" => readData1 <= arreglo(8);
		when "1001" => readData1 <= arreglo(9);
		when "1010" => readData1 <= arreglo(10);
		when "1011" => readData1 <= arreglo(11);
		when "1100" => readData1 <= arreglo(12);
		when "1101" => readData1 <= arreglo(13);
		when "1110" => readData1 <= arreglo(14);
		when "1111" => readData1 <= arreglo(15);
		when others => readData1 <= arreglo(0);
		end case;
end process;
--mux de salida 2:
process(readRegister2, arreglo)
begin
		case readRegister2 is
		when "0000" => readData2 <= arreglo(0);
		when "0001" => readData2 <= arreglo(1);
		when "0010" => readData2 <= arreglo(2);
		when "0011" => readData2 <= arreglo(3);
		when "0100" => readData2 <= arreglo(4);
		when "0101" => readData2 <= arreglo(5);
		when "0110" => readData2 <= arreglo(6);
		when "0111" => readData2 <= arreglo(7);
		when "1000" => readData2 <= arreglo(8);
		when "1001" => readData2 <= arreglo(9);
		when "1010" => readData2 <= arreglo(10);
		when "1011" => readData2 <= arreglo(11);
		when "1100" => readData2 <= arreglo(12);
		when "1101" => readData2 <= arreglo(13);
		when "1110" => readData2 <= arreglo(14);
		when "1111" => readData2 <= arreglo(15);
		when others => readData2 <= arreglo(0);
		end case;
end process;

end Behavioral;
