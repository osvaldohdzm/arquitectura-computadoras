library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity MEMORIA_CODIGO is
    Port ( A : in  STD_LOGIC_VECTOR (4 downto 0);
           D : out  STD_LOGIC_VECTOR (19 downto 0));
end MEMORIA_CODIGO;

architecture Behavioral of MEMORIA_CODIGO is
TYPE arre is array (0 to  ((2**(5))-1)) of std_logic_vector(19 downto 0);

constant BCOND : STD_LOGIC_VECTOR(19 downto 0) := "00001000000001110001"; 
constant LI: STD_LOGIC_VECTOR(19 downto 0)	  := "00000000010000000000";
constant LWI: STD_LOGIC_VECTOR(19 DOWNTO 0)	  := "00000100010000001000";
constant SWI: STD_LOGIC_VECTOR(19 DOWNTO 0)	  := "00001000000000001100";
constant SW: STD_LOGIC_VECTOR(19 DOWNTO 0)	  := "00001010000100110101";


constant ADDI : STD_LOGIC_VECTOR(19 DOWNTO 0)  := "00000100010100110011";
constant SUBI : STD_LOGIC_VECTOR(19 DOWNTO 0)  := "00000100010101110011";
constant ANDI: STD_LOGIC_VECTOR(19 DOWNTO 0)	  := "00000100010100000011";
constant ORI: STD_LOGIC_VECTOR(19 DOWNTO 0)	  := "00000100010100010011";
constant XORI: STD_LOGIC_VECTOR(19 DOWNTO 0)	  := "00000100010100100011";
constant NANDI: STD_LOGIC_VECTOR(19 DOWNTO 0)  := "00000100010111010011";
constant NORI: STD_LOGIC_VECTOR(19 DOWNTO 0)   := "00000100010111000011";
constant XNORI: STD_LOGIC_VECTOR(19 DOWNTO 0)  := "00000100010110100011";

constant BEQI: STD_LOGIC_VECTOR(19 DOWNTO 0)   := "10010000001100110010";
constant BNEI: STD_LOGIC_VECTOR(19 DOWNTO 0)   := "10010000001100110010";
constant BLTI: STD_LOGIC_VECTOR(19 DOWNTO 0)   := "10010000001100110010";
constant BLETI: STD_LOGIC_VECTOR(19 DOWNTO 0)   :="10010000001100110010";
constant BGTI: STD_LOGIC_VECTOR(19 DOWNTO 0)   := "10010000001100110010";
constant BGETI: STD_LOGIC_VECTOR(19 DOWNTO 0)   :="10010000001100110010";
constant B: STD_LOGIC_VECTOR(19 DOWNTO 0)   :=    "00010000000000000000";
constant CALL: STD_LOGIC_VECTOR(19 DOWNTO 0)   := "01010000000000000000";
constant RET: STD_LOGIC_VECTOR(19 DOWNTO 0)   :=  "00100000000000000000";
constant NOP: STD_LOGIC_VECTOR(19 DOWNTO 0)   :=  "00000000000000000000";
constant LW: STD_LOGIC_VECTOR(19 DOWNTO 0)	  := "00000000000000000001"; --MODIFICAR

constant ROM : ARRE :=(BCOND,LI,LWI,SWI,SW,ADDI,SUBI,ANDI,ORI,XORI,NANDI,NORI,XNORI,BEQI,BNEI,BLTI,BLETI,BGTI,BGETI,B,CALL,RET,NOP,LW,OTHERS=>(OTHERS=>'0'));
begin
D	<=	ROM(conv_integer(A));

end Behavioral;

