-- Pr�ctica 9 - Paquete de Unidad de Control | Marco Antonio Rubio Cort�s

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package P_practica9 is
	
	component practica9A is													-- Registro de Banderas
		port
		(
			flags							: in std_logic_vector (3 downto 0);
			LF								: in std_logic;
			
			CLK							: in std_logic;
			CLR							: in std_logic;
			
			OV								: inout std_logic;
			N								: inout std_logic;
			Z								: inout std_logic;
			C								: inout std_logic
		);
	end component;
	
	component practica9B is													-- Verificador de Condici�n
		port
		(
			OV								: in std_logic;
			N								: in std_logic;
			Z								: in std_logic;
			C								: in std_logic;
			
			EQ								: out std_logic;
			NEQ							: out std_logic;
			LT								: out std_logic;
			LE								: out std_logic;
			GT								: out std_logic;
			GET							: out std_logic
		);
	end component;
	
	component practica9C is													-- Detector de nivel
		port
		(
			CLK							: in std_logic;
			CLR							: in std_logic;
			
			NA								: out std_logic
		);
	end component;
	
	component practica9D is													-- Decodificador de Instruccci�n
		port
		(
			op_code				: in std_logic_vector(4 downto 0);
		
			-- Salidas | Decodificador de Instrucci�n
			TIPOR							: out std_logic;
			BEQI							: out std_logic;
			BNEQI							: out std_logic;
			BLTI							: out std_logic;
			BLETI							: out std_logic;
			BGTI							: out std_logic;
			BGETI							: out std_logic
		);
	end component;
	
	component practica9E is													-- Unidad de Control
		port
		(
			-- Entradas | Decodificador de Instrucci�n
			TIPOR							: in std_logic;
			BEQI							: in std_logic;
			BNEQI							: in std_logic;
			BLTI							: in std_logic;
			BLETI							: in std_logic;
			BGTI							: in std_logic;
			BGETI							: in std_logic;		
			
			-- Entradas | Nivel
			NA								: in std_logic;
			
			-- Entradas | Verificador de Condici�n
			EQ								: in std_logic;
			NEQ							: in std_logic;
			LT								: in std_logic;
			LE								: in std_logic;
			GT								: in std_logic;
			GET							: in std_logic;
			
			-- Entradas | Se�ales de control
			CLK							: in std_logic;
			CLR							: in std_logic;
			
			-- Salidas | MUX
			SDOPC							: out std_logic;
			SM								: out std_logic
		);
	end component;
	
	component practica9F is													-- MUX SDOPC
		port
		(
			zeros							: in std_logic_vector (4 downto 0);
			operationCode				: in std_logic_vector (4 downto 0);
			SDOPC							: in std_logic;
		
			selOPC						: out std_logic_vector (4 downto 0)
		);
	end component;
	
	component practica9G is													-- ROM C�digos de Operaci�n
		generic
		(
			wordSize : integer := 20;
			busSize  : integer := 5
		);
		port
		(
			dir		: in 	std_logic_vector(busSize-1 downto 0);
			data		: out	std_logic_vector(wordSize-1 downto 0)
		);
	end component;
	
	component practica9H is													-- ROM C�digos de Funci�n
		generic
		(
			wordSize : integer := 20;
			busSize  : integer := 4
		);
		port
		(
			dir		: in 	std_logic_vector(busSize-1 downto 0);
			data		: out	std_logic_vector(wordSize-1 downto 0)
		);
	end component;
	
	component practica9I is													-- MUX SM
		port
		(
			microCodFun					: in std_logic_vector (19 downto 0);
			microCodOpe					: in std_logic_vector (19 downto 0);
			SM								: in std_logic;
			
			microInstruccion			: out std_logic_vector (19 downto 0)
		);
	end component;
	
end P_practica9;

package body P_practica9 is

 
end P_practica9;
