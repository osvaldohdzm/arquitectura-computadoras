--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   05:26:33 12/04/2018
-- Design Name:   
-- Module Name:   /home/ise/Documents/PROYECTO/ISE Projects 03-12-2018/ExtensorDirec/extDir_test.vhd
-- Project Name:  ExtensorDirec
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: ExtensorDirec
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY extDir_test IS
END extDir_test;
 
ARCHITECTURE behavior OF extDir_test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ExtensorDirec
    PORT(
         ENTRADA : IN  std_logic_vector(11 downto 0);
         SALIDA : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal ENTRADA : std_logic_vector(11 downto 0) := (others => '0');

 	--Outputs
   signal SALIDA : std_logic_vector(15 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ExtensorDirec PORT MAP (
          ENTRADA => ENTRADA,
          SALIDA => SALIDA
        );

   -- Stimulus process
   stim_proc: process
   begin		
      -- insert stimulus here 
		entrada <= "111111111111";
      wait for 10 ns;
		entrada <= "000000000000";
      wait for 10 ns;
		entrada <= "101001010110";
      wait for 10 ns;
   end process;

END;
