
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library work;
use work.packet.all;


entity Union is
    Port ( LFU, clk, clr   : in  STD_LOGIC;
           banderasU : in  STD_LOGIC_VECTOR (3 downto 0);
           CP_CODEU : in  STD_LOGIC_VECTOR (4 downto 0);
           IU : in  STD_LOGIC_VECTOR (3 downto 0);
			  --NIVEL_UC : out  STD_LOGIC;
           SalidaU : out  STD_LOGIC_VECTOR (19 downto 0));
end Union;

architecture Behavioral of Union is

signal tipoR2,beq2,bneq2,blt2,ble2,bgt2,bget2: std_logic;
signal na2: std_logic;
signal eq2,neq2,lt2,le2,gt2,get2: std_logic;
signal microFun2, microOp2: std_logic_vector (19 downto 0);
signal sdopc2, sm2: std_logic;
signal banderas2: std_logic_vector (3 downto 0);
signal mux1 : std_logic_vector (4 downto 0);

begin

		condicion1 : condicion port map(banderas2,eq2,neq2,lt2,le2,gt2,get2);
		
		decoInst1 : decoInst port map(CP_CODEU,tipoR2,beq2,bneq2,blt2,ble2,bgt2,bget2);
		
      microFuncion1 : microFuncion port map(IU,microFun2);
		
		microOpCode1 : microOpCode port map (mux1,microOp2);
		
		nivel1 : nivel port map (clk, clr, na2);
		
		regEdo1 : regEdo2 port map (banderasU, clr, clk, LFU, banderas2 );
		
		unicontrol1 : unicontrol port map(tipoR2,beq2,bneq2,blt2,ble2,bgt2,bget2,clk,clr,eq2,neq2,lt2,le2,gt2,get2,na2,sdopc2,sm2);
		
      mux1 <= "00000" when sdopc2 = '0' else
		        CP_CODEU;
	   
		SalidaU <= microFun2 when sm2 = '0' else 
		           microOp2;
					  
		--NIVEL_UC <= na2;



end Behavioral;

