LIBRARY ieee;
LIBRARY STD;
USE STD.TEXTIO.ALL;
USE ieee.std_logic_TEXTIO.ALL;	--PERMITE USAR STD_LOGIC 

USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;
 
ENTITY P6_MemoriaPrograma_TestBench IS
END P6_MemoriaPrograma_TestBench;
 
ARCHITECTURE behavior OF P6_MemoriaPrograma_TestBench IS 

    COMPONENT P6_MemoriaPrograma
    PORT(
         dir : IN  std_logic_vector(11 downto 0);
         dato : OUT  std_logic_vector(24 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal dir : std_logic_vector(11 downto 0) := (others => '0');

 	--Outputs
   signal dato : std_logic_vector(24 downto 0);
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: P6_MemoriaPrograma PORT MAP (
          dir => dir,
          dato => dato
        ); 

   -- Stimulus process
   stim_proc: process
		file ARCH_RES : TEXT;
		variable LINEA_RES : line;
		VARIABLE VAR_dato : STD_LOGIC_VECTOR(24 DOWNTO 0);

		file ARCH_VEC : TEXT;
		variable LINEA_VEC : line;
		VARIABLE VAR_dir : STD_LOGIC_VECTOR(11 DOWNTO 0);
		VARIABLE CADENA : STRING(1 TO 6);
   begin
		file_open(ARCH_VEC, "vectores.txt", READ_MODE);
		file_open(ARCH_RES, "resultados.txt", WRITE_MODE);
			
			--ESCRIBE LA CABECERA
			CADENA := "     A";
			write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
			CADENA := "OPCODE";
			write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
			CADENA := "19..16";
			write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
			CADENA := "15..12";
			write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
			CADENA := "11...8";
			write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
			CADENA := "7....4";
			write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
			CADENA := "3....0";
			write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
			writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo
			
			wait for 100 ns;
			FOR I IN 0 TO 6 LOOP
				readline(ARCH_VEC, LINEA_VEC);
				
				Hread(LINEA_VEC, VAR_dir);
				dir <= VAR_dir;
				
				wait for 100 ns;
				VAR_dato := dato;
			
				Hwrite(LINEA_RES, VAR_dir, right, 7); --ESCRIBE EN EL CAMPO A (dir_w)
				write(LINEA_RES, VAR_dato(24 downto 20), right, 7);
				write(LINEA_RES, VAR_dato(19 downto 16), right, 7);
				write(LINEA_RES, VAR_dato(15 downto 12), right, 7);
				write(LINEA_RES, VAR_dato(11 downto 8), right, 7);
				write(LINEA_RES, VAR_dato(7 downto 4), right, 7);
				write(LINEA_RES, VAR_dato(3 downto 0), right, 7);
				
				writeline(ARCH_RES, LINEA_RES); --Escribe la linea en el archivo de resultados
			end loop;
		file_close(ARCH_VEC);
		file_close(ARCH_RES);
      wait;
   end process;

END;
