
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY t_decoInst IS
END t_decoInst;
 
ARCHITECTURE behavior OF t_decoInst IS 

 
    COMPONENT decoInst
    PORT(
         CP_CODE : IN  std_logic_vector(4 downto 0);
         SalDeco : INOUT  std_logic_vector(6 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CP_CODE : std_logic_vector(4 downto 0) := (others => '0');

	--BiDirs
   signal SalDeco : std_logic_vector(6 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: decoInst PORT MAP (
          CP_CODE => CP_CODE,
          SalDeco => SalDeco
        );

   -- Stimulus process
   stim_proc: process
   begin		
    
	 CP_CODE <= "00000";
	 
	 WAIT FOR 20 NS;
	 
	 CP_CODE <= "01010";
	 
	 WAIT FOR 20 NS;
	 
	 CP_CODE <= "01101";
	 
	 WAIT FOR 20 NS;
	 
	 CP_CODE <= "10000";
	 
	 WAIT FOR 20 NS;
	 
	 CP_CODE <= "10110";
	
    wait;
   end process;

END;
