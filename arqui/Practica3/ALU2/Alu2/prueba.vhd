--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY prueba IS
END prueba;
 
ARCHITECTURE behavior OF prueba IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT alu4b
    PORT(
         a : IN  std_logic_vector(3 downto 0);
         b : IN  std_logic_vector(3 downto 0);
         ALUop : IN  std_logic_vector(3 downto 0);
         resultado : OUT  std_logic_vector(3 downto 0);
         banderas : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(3 downto 0) := (others => '0');
   signal b : std_logic_vector(3 downto 0) := (others => '0');
   signal ALUop : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal resultado : std_logic_vector(3 downto 0);
   signal banderas : std_logic_vector(3 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: alu4b PORT MAP (
          a => a,
          b => b,
          ALUop => ALUop,
          resultado => resultado,
          banderas => banderas
        );
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- insert stimulus here 
-------------- PARTE 1

--------------- PARTE 2
		a <= "0101";--  5
		b <= "0111";-- 7
		
		ALUop <= "0011";-- A+B

		wait for 300 ns;

      wait;
   end process;

END;
