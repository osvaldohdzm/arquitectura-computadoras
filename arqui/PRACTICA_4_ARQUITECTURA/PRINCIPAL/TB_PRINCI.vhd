LIBRARY ieee;
LIBRARY STD;
USE STD.TEXTIO.ALL;
USE ieee.std_logic_TEXTIO.ALL;	

USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_UNSIGNED.ALL;
USE ieee.std_logic_ARITH.ALL;
 
ENTITY TB_PRINCI IS
END TB_PRINCI;
 
ARCHITECTURE behavior OF TB_PRINCI IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT PRINCIPAL
    PORT(
         clk : IN  std_logic;
         clr : IN  std_logic;
         she : IN  std_logic;
         dir : IN  std_logic;
         write_reg : IN  std_logic;
         SHAMT : IN  std_logic_vector(3 downto 0);
         WR : IN  std_logic_vector(3 downto 0);
         read_reg1 : IN  std_logic_vector(3 downto 0);
         read_reg2 : IN  std_logic_vector(3 downto 0);
         write_data : IN  std_logic_vector(15 downto 0);
         read_data1 : OUT  std_logic_vector(15 downto 0);
         read_data2 : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';
   signal she : std_logic := '0';
   signal dir : std_logic := '0';
   signal write_reg : std_logic := '0';
   signal SHAMT : std_logic_vector(3 downto 0) := (others => '0');
   signal WR : std_logic_vector(3 downto 0) := (others => '0');
   signal read_reg1 : std_logic_vector(3 downto 0) := (others => '0');
   signal read_reg2 : std_logic_vector(3 downto 0) := (others => '0');
   signal write_data : std_logic_vector(15 downto 0) := (others => '0');

 	--Outputs
   signal read_data1 : std_logic_vector(15 downto 0);
   signal read_data2 : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: PRINCIPAL PORT MAP (
          clk => clk,
          clr => clr,
          she => she,
          dir => dir,
          write_reg => write_reg,
          SHAMT => SHAMT,
          WR => WR,
          read_reg1 => read_reg1,
          read_reg2 => read_reg2,
          write_data => write_data,
          read_data1 => read_data1,
          read_data2 => read_data2
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
stim_proc: process
	file ARCH_RES : TEXT;																					
	variable LINEA_RES : line;
	VARIABLE VAR_READ_DATA1 : STD_LOGIC_VECTOR(15 DOWNTO 0);
	VARIABLE VAR_READ_DATA2 : STD_LOGIC_VECTOR(15 DOWNTO 0);
	
	file ARCH_VEC : TEXT;
	variable LINEA_VEC : line;
	VARIABLE READ_REGISTER1: STD_LOGIC_VECTOR(3 DOWNTO 0); 
	VARIABLE READ_REGISTER2: STD_LOGIC_VECTOR(3 DOWNTO 0);
	VARIABLE VAR_SHAMT: STD_LOGIC_VECTOR(3 DOWNTO 0);
	VARIABLE VAR_WR: STD_LOGIC_VECTOR(3 DOWNTO 0);
	VARIABLE VAR_WRITE_DATA: STD_LOGIC_VECTOR(15 DOWNTO 0);
	VARIABLE VAR_write_reg: std_logic;
	VARIABLE VAR_SHE,VAR_DIR,VAR_CLR: STD_LOGIC;
	VARIABLE CADENA : STRING(1 TO 10);
   begin		
		file_open(ARCH_VEC, "VECTORES2.TXT", READ_MODE); 	
		file_open(ARCH_RES, "RESULTADO2.TXT", WRITE_MODE); 	

		CADENA := " read_reg1";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := " read_reg2";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := "     SHAMT";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := "     WR   ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := "write_data";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := " write_reg";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := "       she";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := "       dir";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := "       clr";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
			CADENA := "       RD1";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
			CADENA := "       RD2";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		writeline(ARCH_RES,LINEA_RES);

		WAIT FOR 100 NS;
		FOR I IN 0 TO 6 LOOP
			readline(ARCH_VEC,LINEA_VEC); 

			hread(LINEA_VEC, READ_REGISTER1);
			read_reg1 <= READ_REGISTER1;
			
			hread(LINEA_VEC, READ_REGISTER2);
			read_reg2 <= READ_REGISTER2;
			
			hread(LINEA_VEC, VAR_SHAMT);
			SHAMT <= VAR_SHAMT;
			
			hread(LINEA_VEC, VAR_WR);
			wr <= VAR_WR;
			
			Hread(LINEA_VEC, VAR_WRITE_DATA);
			write_data <= VAR_WRITE_DATA;
			
			read(LINEA_VEC, VAR_write_reg);
			write_reg <= VAR_write_reg;
			
			read(LINEA_VEC, VAR_SHE);
			she <= VAR_SHE;
			
			read(LINEA_VEC, VAR_dir);
			dir <= VAR_dir;
			
			
			read(LINEA_VEC, VAR_clr);
			clr <= VAR_clr;
			
			
			WAIT UNTIL RISING_EDGE(clk);	--ESPERO AL FLANCO DE SUBIDA 

			VAR_READ_DATA1 := read_data1;
			VAR_READ_DATA2 := read_data2;			
			hwrite(LINEA_RES, READ_REGISTER1, right, 8);	
		   hwrite(LINEA_RES, READ_REGISTER2, right, 10);	
			hwrite(LINEA_RES, VAR_SHAMT, right, 14);	
			hwrite(LINEA_RES, VAR_WR, right, 8);	
			hwrite(LINEA_RES, VAR_WRITE_DATA, right, 13);	
			write(LINEA_RES, VAR_write_reg,right,10);
			write(LINEA_RES, VAR_SHE,right,12);
			write(LINEA_RES, VAR_dir,right,13);
			write(LINEA_RES, VAR_clr,right,12);
			hwrite(LINEA_RES,VAR_READ_DATA1,RIGHT,10);
			hwrite(LINEA_RES,VAR_READ_DATA2,RIGHT,11);
			writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo
		end loop;
		file_close(ARCH_VEC);  -- cierra el archivo
		file_close(ARCH_RES);  -- cierra el archivo

      wait;
   end process;

END;
