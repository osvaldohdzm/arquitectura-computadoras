--------------------------------------------------------------------------------



--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY T_practica3 IS
END T_practica3;

ARCHITECTURE behavior OF T_practica3 IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT practica3
    PORT(
         a : IN  std_logic_vector(3 downto 0);
         b : IN  std_logic_vector(3 downto 0);
         aluop : IN  std_logic_vector(3 downto 0);
         result : OUT  std_logic_vector(3 downto 0);
         flags : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(3 downto 0) := (others => '0');
   signal b : std_logic_vector(3 downto 0) := (others => '0');
   signal aluop : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal result : std_logic_vector(3 downto 0);
   signal flags : std_logic_vector(3 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: practica3 PORT MAP (
          a => a,
          b => b,
          aluop => aluop,
          result => result,
          flags => flags
        );

   -- Stimulus process
   stim_proc: process
   begin
		-- Suma
      a <= "0101";
		b <= "1110";
		aluop <= "0011";
		wait for 20 ns;
		
		-- Resta
      a <= "0101";
		b <= "1110";
		aluop <= "0111";
		wait for 20 ns;
		
		-- And
      a <= "0101";
		b <= "1110";
		aluop <= "0000";
		wait for 20 ns;
		
		-- Nand
      a <= "0101";
		b <= "1110";
		aluop <= "1101";
		wait for 20 ns;
		
		-- Or
      a <= "0101";
		b <= "1110";
		aluop <= "0001";
		wait for 20 ns;
		
		-- Nor
      a <= "0101";
		b <= "1110";
		aluop <= "1100";
		wait for 20 ns;
      
		-- Xor
      a <= "0101";
		b <= "1110";
      aluop <= "0010";
		wait for 20 ns;		
		
		-- Xnor
      a <= "0101";
		b <= "1110";
	   aluop <= "1010";
		wait for 20 ns;
		
		-----------------------------------
		-- Suma
		a <= "0101";
		b <= "0111";
		aluop <= "0011";
		wait for 20 ns;
		
		-----------------------------------
		-- Resta
		a <= "0101";
		b <= "0101";
		aluop <= "0111";
		wait for 20 ns;
		
		-- Not
		a <= "0101";
		b <= "0101";
		aluop <= "1100";
      wait for 20 ns;
   end process;

END;
