LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY DECODE IS
    PORT ( 
         entrada : IN STD_LOGIC_VECTOR(3 downto 0);
			salida: OUT STD_LOGIC_VECTOR (6 DOWNTO 0)
			  );
END DECODE;

ARCHITECTURE BEHAVIORAL OF DECODE IS 

 -- N�meros (0-9)9 bit el registro
 --CONSTANT GUION: STD_LOGIC_VECTOR (6 DOWNTO 0):= "1111110"; -- Guion
 CONSTANT num0: STD_LOGIC_VECTOR (6 DOWNTO 0):= "0000001"; -- N�mero 0. 
 CONSTANT num1: STD_LOGIC_VECTOR (6 DOWNTO 0):= "1001111"; -- N�mero 1.
 CONSTANT num2: STD_LOGIC_VECTOR (6 DOWNTO 0):= "0010010"; -- N�mero 2. 
 CONSTANT num3: STD_LOGIC_VECTOR (6 DOWNTO 0):= "0000110"; -- N�mero 3. 
 CONSTANT num4: STD_LOGIC_VECTOR (6 DOWNTO 0):= "1001100"; -- N�mero 4. 
 CONSTANT num5: STD_LOGIC_VECTOR (6 DOWNTO 0):= "0100100"; -- N�mero 5. 
 CONSTANT num6: STD_LOGIC_VECTOR (6 DOWNTO 0):= "0100000"; -- N�mero 6.
 CONSTANT num7: STD_LOGIC_VECTOR (6 DOWNTO 0):= "0001110"; -- N�mero 7. 
 CONSTANT num8: STD_LOGIC_VECTOR (6 DOWNTO 0):= "0000000"; -- N�mero 8.
 CONSTANT num9: STD_LOGIC_VECTOR (6 DOWNTO 0):= "0000100"; -- N�mero 9.
 CONSTANT nada: STD_LOGIC_VECTOR (6 DOWNTO 0):= "1111111"; -- Nada.

 BEGIN
	
	conv:PROCESS (entrada)
	BEGIN
			CASE entrada IS
				WHEN "0000"=>
					salida<=num0;
				WHEN "0001"=>
					salida<=num1;
				WHEN "0010"=>
					salida<=num2;
				WHEN "0011"=>
					salida<=num3;
				WHEN "0100"=>
					salida<=num4;
				WHEN "0101"=>
					salida<=num5;
				WHEN "0110"=>
					salida<=num6;
				WHEN "0111"=>
					salida<=num7;
				WHEN "1000"=>
					salida<=num8;
				WHEN "1001"=>
					salida<=num9;
				WHEN OTHERS=>
					salida<=nada;
			END CASE;
	END PROCESS conv;

END BEHAVIORAL ;