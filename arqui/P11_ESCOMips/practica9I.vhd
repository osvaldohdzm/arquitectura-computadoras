library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity practica9I is
	port
	(
		microCodFun					: in std_logic_vector (19 downto 0);
		microCodOpe					: in std_logic_vector (19 downto 0);
		SM								: in std_logic;
		
		microInstruccion			: out std_logic_vector (19 downto 0)
	);
end practica9I;

architecture A_practica9I of practica9I is

begin

	microInstruccion <= microCodFun when SM = '0' else microCodOpe;
	
end A_practica9I;

