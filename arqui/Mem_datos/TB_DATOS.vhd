LIBRARY ieee;
LIBRARY STD;
USE STD.TEXTIO.ALL;
USE ieee.std_logic_TEXTIO.ALL;	

USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_UNSIGNED.ALL;
USE ieee.std_logic_ARITH.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TB_DATOS IS
END TB_DATOS;
 
ARCHITECTURE behavior OF TB_DATOS IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Memoria_datos
    PORT(
         wd : IN  std_logic;
         clk : IN  std_logic;
         d_in : IN  std_logic_vector(7 downto 0);
         dir : IN  std_logic_vector(8 downto 0);
         d_out : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT; 
    

   --Inputs
   signal wd : std_logic := '0';
   signal clk : std_logic := '0';
   signal d_in : std_logic_vector(7 downto 0) := (others => '0');
   signal dir : std_logic_vector(8 downto 0) := (others => '0');

 	--Outputs
   signal d_out : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Memoria_datos PORT MAP (
          wd => wd,
          clk => clk,
          d_in => d_in,
          dir => dir,
          d_out => d_out
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
stim_proc: process
		file ARCH_RES : TEXT;																					
		variable LINEA_RES : line;
		VARIABLE var_d_out : STD_LOGIC_VECTOR(7 DOWNTO 0);
		file ARCH_VEC : TEXT;
		variable LINEA_VEC : line;
		VARIABLE var_dir: STD_LOGIC_VECTOR(7 DOWNTO 0);
		VARIABLE var_d_in: STD_LOGIC_VECTOR(7 DOWNTO 0);
		VARIABLE var_wd: std_logic;
		VARIABLE CADENA : STRING(1 TO 10);
	   begin		
			file_open(ARCH_VEC, "VECTORES.TXT", READ_MODE); 	
		file_open(ARCH_RES, "RESULTADO.TXT", WRITE_MODE); 	

		CADENA := "  d_in    ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := "    dir   ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := "   wd     ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := "   d_out  ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		writeline(ARCH_RES,LINEA_RES);

		WAIT FOR 100 NS;
		FOR I IN 0 TO 5 LOOP
			readline(ARCH_VEC,LINEA_VEC); 

			hread(LINEA_VEC, var_d_in);
			d_in <= var_d_in;
			
			hread(LINEA_VEC, var_dir);
			dir <= '0'&var_dir;
			
			read(LINEA_VEC, VAR_wd);
			wd <= var_wd;
			
			WAIT UNTIL RISING_EDGE(clk);	--ESPERO AL FLANCO DE SUBIDA 

			var_d_in := d_in;			
			var_dir := dir(7 downto 0);
			var_wd := wd;
			var_d_out:=d_out;
			hwrite(LINEA_RES, var_d_in, right, 8);
		   hwrite(LINEA_RES, var_dir, right, 10);	
			write(LINEA_RES, var_wd, right, 14);	
			hwrite(LINEA_RES, var_d_out, right, 8);	
		
			writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo
		end loop;
		file_close(ARCH_VEC);  -- cierra el archivo
		file_close(ARCH_RES);  -- cierra el archivo   end process;
		  wait;
   end process;

END;