
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity REGISTRO_ESTADO is
    Port ( LF : in  STD_LOGIC;
           CLK,CLR : in  STD_LOGIC;
           RBANDERAS : out  STD_LOGIC_VECTOR (3 downto 0);
           BANDERAS : in  STD_LOGIC_VECTOR (3 downto 0));
end REGISTRO_ESTADO;

architecture Behavioral of REGISTRO_ESTADO is

begin
EDO_REG:PROCESS (CLK,CLR)
BEGIN
	IF(CLR='1')THEN
		RBANDERAS<=(others=>'0');
	elsif(FALLING_EDGE(CLK))THEN
		IF(LF='1')THEN
			RBANDERAS<=BANDERAS;
		END IF;
	END IF;
END PROCESS EDO_REG;

end Behavioral;

