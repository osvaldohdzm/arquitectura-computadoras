-- Pr�ctica 9 | Pr�ctica 9F - MUX SDOPC | Marco Antonio Rubio Cort�s

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity practica9F is
	port
	(
		zeros							: in std_logic_vector (4 downto 0);
		operationCode				: in std_logic_vector (4 downto 0);
		SDOPC							: in std_logic;
	
		selOPC						: out std_logic_vector (4 downto 0)
	);
end practica9F;

architecture A_practica9F of practica9F is

begin
	
	selOPC <= zeros when SDOPC = '0' else operationCode;
	
end A_practica9F;