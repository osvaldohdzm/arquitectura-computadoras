library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity P3_ALU_1bit is
    Port ( a,b,sel_a,sel_b,cin : in  STD_LOGIC;
           op : in  STD_LOGIC_VECTOR (1 downto 0);
           s,cout : out  STD_LOGIC);
end P3_ALU_1bit;

architecture Behavioral of P3_ALU_1bit is

begin
	process(a,b,op,sel_a,sel_b,cin)
		variable a_aux, b_aux: STD_LOGIC;
	begin
		--SELECTOR
		-- Para a
		if (sel_a = '0') then
			a_aux := a;
		else
			a_aux := not(a);
		end if;
		
		-- Para b
		if (sel_b = '0') then
			b_aux := b;
		else
			b_aux := not(b);
		end if;
		
		--MUX
		case op is
			--AND
			WHEN "00" => s <= a_aux AND b_aux;
							 cout <= '0';
			--OR
			WHEN "01" => s <= a_aux OR b_aux;
							 cout <= '0';
			--XOR
			WHEN "10" => s <= a_aux XOR b_aux;
							 cout <= '0';
			--SUMADOR
			WHEN OTHERS => s <= a_aux XOR b_aux XOR cin;
							   cout <= (a_aux AND cin) OR (b_aux AND cin) OR (a_aux AND b_aux); 
		end case;
	end process;

end Behavioral;