
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name ESCOMIPS -dir "C:/Users/Edgar Roa/Desktop/1_pe/ESCOMIPS/ESCOMIPS/planAhead_run_4" -part xc7a100tcsg324-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "C:/Users/Edgar Roa/Desktop/1_pe/ESCOMIPS/ESCOMIPS/ESCOMIPS.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/Users/Edgar Roa/Desktop/1_pe/ESCOMIPS/ESCOMIPS} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "ESCOMIPS.ucf" [current_fileset -constrset]
add_files [list {ESCOMIPS.ucf}] -fileset [get_property constrset [current_run]]
link_design
