-- Proyecto - ESCOMips | Marco Antonio Rubio Cortés

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.P_ESCOMips.all;

entity ESCOMips is
	generic
	(
			N						: integer := 16
	);
	port
	(
		RCLR						: in std_logic;
		RCLK						: in std_logic;
		
		READ1						: out std_logic_vector(N - 1 downto 0);
		READ2						: out std_logic_vector(N - 1 downto 0);
		PCOUNTER					: out std_logic_vector(8 downto 0);
		ALUU						: out std_logic_vector(3 downto 0);
		
		salida					: out std_logic_vector(N - 1 downto 0)
	);
end ESCOMips;

architecture A_ESCOMips of ESCOMips is

signal CLR							: std_logic;
signal CLK							: std_logic;

signal programCounter			: std_logic_vector (N - 1 downto 0);

signal instruccion				: std_logic_vector (24 downto 0);

signal microInstruccion			: std_logic_vector (19 downto 0);

signal read_data1					: std_logic_vector (N - 1 downto 0);
signal read_data2					: std_logic_vector (N - 1 downto 0);
signal flags						: std_logic_vector (3 downto 0);
signal result						: std_logic_vector (N - 1 downto 0);

signal SIGNO						: std_logic_vector (N - 1 downto 0);
signal DIRECCION					: std_logic_vector (N - 1 downto 0);

signal dataMem						: std_logic_vector (N - 1 downto 0);


-- MUX
signal SSDMP						: std_logic_vector (N - 1 downto 0);

signal SSR2							: std_logic_vector (3 downto 0);
signal SSWD							: std_logic_vector (N - 1 downto 0);

signal SSEXT						: std_logic_vector (N - 1 downto 0);

signal SSOP1						: std_logic_vector (N - 1 downto 0);
signal SSOP2						: std_logic_vector (N - 1 downto 0);

signal SSDMD						: std_logic_vector (N - 1 downto 0);

signal SSR							: std_logic_vector (N - 1 downto 0);

-- Microinstrucción
signal SDMP							: std_logic;
signal UP 							: std_logic;
signal DW							: std_logic;
signal WPC							: std_logic;
signal SR2							: std_logic;
signal SWD							: std_logic;
signal SEXT							: std_logic;
signal SHE							: std_logic;
signal DIR							: std_logic;
signal WR							: std_logic;
signal SOP1							: std_logic;
signal SOP2							: std_logic;
signal ALUOP						: std_logic_vector (3 downto 0);
signal SDMD 						: std_logic;
signal WD							: std_logic;
signal SR							: std_logic;
signal LF							: std_logic;


begin
	
	SDMP							<= microInstruccion (19);
	UP 							<= microInstruccion (18);
	DW								<= microInstruccion (17);
	WPC							<= microInstruccion (16);
	SR2							<= microInstruccion (15);
	SWD							<= microInstruccion (14);
	SEXT							<= microInstruccion (13);
	SHE							<= microInstruccion (12);
	DIR							<= microInstruccion (11);
	WR								<= microInstruccion (10);
	SOP1							<= microInstruccion (9);
	SOP2							<= microInstruccion (8);
	ALUOP							<= microInstruccion (7 downto 4);
	SDMD 							<= microInstruccion (3);
	WD								<= microInstruccion (2);
	SR								<= microInstruccion (1);
	LF								<= microInstruccion (0);
	
--	divisorFrec : practica7G												-- Divisor de Frecuencias
--		port map
--		(
--			Osc_clk => RCLK,
--			clr => CLR,
--			clk => CLK
--		);

	CLK <= RCLK;
	
	process(CLK)																-- Clear
	begin
		if (falling_edge(CLK)) then
			CLR <= RCLR;
		end if;
	end process;
	
	SSDMP	<= instruccion (15 downto 0) when SDMP = '0' else SSR;
	
	pila : practica6B															-- Pila
		port map
		(
			pcData => SSDMP,
			programCounter => programCounter,
			UP => UP,
			DW => DW,
			L => WPC,
			clk => CLK,
			clr => CLR
		);
	
	memoriaPrograma : practica6A											-- Memoria de Programa
		port map
		(
			dir		=> programCounter (8 downto 0),
			data		=> instruccion
		);	
	
	UC : practica9																-- Unidad de Control
		port map
		(
			op_function => instruccion (3 downto 0),
			op_code => instruccion (24 downto 20),
			flags => flags,
			LF => LF,

			CLR => CLR,
			CLK => CLK,

			microInstruccion => microInstruccion
		);
	
	SSR2	<= instruccion (11 downto 8) when SR2 = '0' else instruccion (19 downto 16);
	SSWD	<= instruccion (15 downto 0) when SWD = '0' else SSR;
	
	archivoRegistros : practica4											-- Archivo de Registros
		port map
		(
			readRegister1	=> instruccion (15 downto 12),
			readRegister2	=> SSR2,
			writeRegister	=> instruccion (19 downto 16),
			SHAMT				=> instruccion (7 downto 4),
			writeData		=> SSWD,
			readData1		=> read_data1,
			readData2		=> read_data2,
			SHE				=> SHE,
			DIR				=> DIR,
			WR					=> WR,
			clk				=> CLK,
			clr				=> CLR
		);
		
	extensorSigno : EXTSIG													-- Extensor de Signo
		port map
		(
				entrada	=> instruccion(11 downto 0),
				salida	=> SIGNO
		);
	
	extensorDireccion : EXTDIR												-- Extensor de Dirección
		port map
		(
				entrada	=> instruccion(11 downto 0),
				salida	=> DIRECCION
		);
	
	SSEXT	<= SIGNO when SEXT = '0' else DIRECCION;
	
	SSOP1	<= read_data1 when SOP1 = '0' else programCounter;
	SSOP2	<= read_data2 when SOP2 = '0' else SSEXT;
	
	ALU : practica3															-- ALU
		port map
		(
			a => SSOP1,
			b => SSOP2,
			aluop => ALUOP,
			result => result,
			flags => flags
		);
	
	SSDMD	<= result when SDMD = '0' else instruccion (15 downto 0);
	
	memoriaDatos : practica5												-- Memoria de Datos
		port map
		(
			dir => SSDMD (10 downto 0),
			d_in => read_data2,
			d_out => dataMem,
			
			WD => WD,
			clk => CLK
		);	

	SSR <= dataMem when SR = '0' else result;
	
	READ1			<= SSOP1;
	READ2			<= SSOP2;
	PCOUNTER		<= programCounter(8 downto 0);
	ALUU			<= ALUOP;
	
	salida <= result;
	
end A_ESCOMips;

