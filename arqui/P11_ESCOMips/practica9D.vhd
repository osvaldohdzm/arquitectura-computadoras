-- Pr�ctica 9 | Pr�ctica 9D - Decodificador de Instrucci�n | Marco Antonio Rubio Cort�s

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity practica9D is
	port
	(
		op_code				: in std_logic_vector(4 downto 0);
	
		-- Salidas | Decodificador de Instrucci�n
		TIPOR							: out std_logic;
		BEQI							: out std_logic;
		BNEQI							: out std_logic;
		BLTI							: out std_logic;
		BLETI							: out std_logic;
		BGTI							: out std_logic;
		BGETI							: out std_logic
	);
end practica9D;

architecture A_practica9D of practica9D is
begin
	
	decodificador : process(op_code)
	begin	
		TIPOR	<= '0';
		BEQI	<= '0';
		BNEQI	<= '0';
		BLTI	<= '0';
		BLETI	<= '0';
		BGTI	<= '0';
		BGETI	<= '0';
		
		case op_code is
			when "00000" => TIPOR	<= '1';					-- 0
			when "01101" => BEQI		<= '1';					-- 13
			when "01110" => BNEQI	<= '1';					-- 14
			when "01111" => BLTI		<= '1';					-- 15
			when "10000" => BLETI	<= '1';					-- 16
			when "10001" => BGTI		<= '1';					-- 17
			when "10010" => BGETI	<= '1';					-- 18
			when others =>
				TIPOR	<= '0';
				BEQI	<= '0';
				BNEQI	<= '0';
				BLTI	<= '0';
				BLETI	<= '0';
				BGTI	<= '0';
				BGETI	<= '0';
		end case;
	end process decodificador;

end A_practica9D;