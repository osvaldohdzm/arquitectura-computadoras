LIBRARY ieee;
LIBRARY std;
use std.textio.all;
USE ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_textio.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY simArchivo_pila IS
END simArchivo_pila;
 
ARCHITECTURE behavior OF simArchivo_pila IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Pila
    PORT(
         d : IN  std_logic_vector(15 downto 0);
         up : IN  std_logic;
         down : IN  std_logic;
         wpc : IN  std_logic;
         clk : IN  std_logic;
         clr : IN  std_logic;
         pc_sal : OUT  std_logic_vector(15 downto 0);
         sp_aux : OUT  std_logic_vector(2 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal d : std_logic_vector(15 downto 0) := (others => '0');
   signal up : std_logic := '0';
   signal down : std_logic := '0';
   signal wpc : std_logic := '0';
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';

 	--Outputs
   signal pc_sal : std_logic_vector(15 downto 0);
   signal sp_aux : std_logic_vector(2 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Pila PORT MAP (
          d => d,
          up => up,
          down => down,
          wpc => wpc,
          clk => clk,
          clr => clr,
          pc_sal => pc_sal,
          sp_aux => sp_aux
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

  -- Stimulus process
   stim_proc: process
	file ARCH_RES : TEXT;
	file ARCH_VEC : TEXT;
	
	variable LINEA_RES : line;
	variable LINEA_VEC : line;
	

	VARIABLE VAR_D    :  STD_LOGIC_VECTOR(15 DOWNTO 0);
	VARIABLE VAR_PC_SAL    :  STD_LOGIC_VECTOR(15 DOWNTO 0);
	VARIABLE VAR_SP_AUX  :  STD_LOGIC_VECTOR(2 DOWNTO 0);
	VARIABLE VAR_UP     :  STD_LOGIC;
	VARIABLE VAR_DOWN     :  STD_LOGIC;
	VARIABLE VAR_WPC     :  STD_LOGIC;
	VARIABLE VAR_CLR     :  STD_LOGIC;
	
	VARIABLE CADENA : STRING(1 TO 4);
	VARIABLE CADENA_I : STRING(1 TO 6);
	VARIABLE CADENA_X : STRING(1 TO 5);

   begin		
		file_open(ARCH_RES, "./SALIDA.TXT", WRITE_MODE); 	
		file_open(ARCH_VEC, "./ENTRADA.TXT", READ_MODE); 	

		CADENA := "   D";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := "  UP";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		CADENA := "  DW";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := " WPC";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		CADENA := " CLR";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		CADENA := "  SP";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		CADENA := "   Q";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		writeline(ARCH_RES,LINEA_RES);
      wait for 100 ns;	
 
		FOR I IN 0 TO 29 LOOP
			readline(ARCH_VEC,LINEA_VEC);
			
			read(LINEA_VEC, VAR_CLR);
			CLR<=VAR_CLR;

			read(LINEA_VEC, VAR_UP);
			UP<=VAR_UP;

			read(LINEA_VEC, VAR_DOWN);
			DOWN<=VAR_DOWN;
			
			read(LINEA_VEC, VAR_WPC);
			WPC<=VAR_WPC;
			
			hread(LINEA_VEC, VAR_D);
			D<=VAR_D;
			
			
			WAIT UNTIL RISING_EDGE(CLK);
			VAR_PC_SAL := pc_sal;
			VAR_SP_AUX := sp_aux;
	
			Hwrite(LINEA_RES, VAR_D, 	right, 5);
			write(LINEA_RES, VAR_UP, 	right, 5);
			write(LINEA_RES, VAR_DOWN, 	right, 5);
			write (LINEA_RES, VAR_WPC , right, 5);
			write (LINEA_RES, VAR_CLR , right, 5);
			write (LINEA_RES, VAR_SP_AUX , right, 5);
			hwrite (LINEA_RES, VAR_PC_SAL , right, 5);
			writeline(ARCH_RES,LINEA_RES);

		end loop;
		
		file_close(ARCH_VEC);
		file_close(ARCH_RES);  

      wait for CLK_period*10;

      wait;
   end process;

END;
