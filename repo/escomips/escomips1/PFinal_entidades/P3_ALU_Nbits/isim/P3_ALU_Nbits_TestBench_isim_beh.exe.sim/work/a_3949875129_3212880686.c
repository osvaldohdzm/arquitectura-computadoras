/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/PracticasArqui/P3_ALU_Nbits/P3_ALU_Nbits.vhd";
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1690584930_503743352(char *, unsigned char );
unsigned char ieee_p_2592010699_sub_2507238156_503743352(char *, unsigned char , unsigned char );
unsigned char ieee_p_2592010699_sub_2545490612_503743352(char *, unsigned char , unsigned char );


static void work_a_3949875129_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;

LAB0:    xsi_set_current_line(23, ng0);

LAB3:    t1 = (t0 + 2568U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 5752);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = t3;
    xsi_driver_first_trans_delta(t1, 4U, 1, 0LL);

LAB2:    t8 = (t0 + 5656);
    *((int *)t8) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3949875129_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    int t3;
    int t4;
    int t5;
    char *t6;
    char *t7;
    unsigned char t8;
    char *t9;
    int t10;
    int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    char *t15;
    unsigned char t16;
    unsigned char t17;
    char *t18;
    char *t19;
    unsigned int t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    unsigned int t25;
    unsigned int t26;

LAB0:    xsi_set_current_line(49, ng0);
    t1 = (t0 + 4104U);
    t2 = *((char **)t1);
    t1 = (t2 + 0);
    *((unsigned char *)t1) = (unsigned char)2;
    xsi_set_current_line(50, ng0);
    t3 = (4 - 1);
    t1 = (t0 + 8933);
    *((int *)t1) = 0;
    t2 = (t0 + 8937);
    *((int *)t2) = t3;
    t4 = 0;
    t5 = t3;

LAB2:    if (t4 <= t5)
        goto LAB3;

LAB5:    xsi_set_current_line(53, ng0);
    t1 = (t0 + 4104U);
    t2 = *((char **)t1);
    t8 = *((unsigned char *)t2);
    t16 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t8);
    t1 = (t0 + 5816);
    t6 = (t1 + 56U);
    t7 = *((char **)t6);
    t9 = (t7 + 56U);
    t15 = *((char **)t9);
    *((unsigned char *)t15) = t16;
    xsi_driver_first_trans_delta(t1, 2U, 1, 0LL);
    xsi_set_current_line(56, ng0);
    t1 = (t0 + 2888U);
    t2 = *((char **)t1);
    t3 = (4 - 1);
    t4 = (t3 - 3);
    t12 = (t4 * -1);
    t13 = (1U * t12);
    t14 = (0 + t13);
    t1 = (t2 + t14);
    t8 = *((unsigned char *)t1);
    t6 = (t0 + 5816);
    t7 = (t6 + 56U);
    t9 = *((char **)t7);
    t15 = (t9 + 56U);
    t18 = *((char **)t15);
    *((unsigned char *)t18) = t8;
    xsi_driver_first_trans_delta(t6, 1U, 1, 0LL);
    xsi_set_current_line(58, ng0);
    t1 = (t0 + 2728U);
    t2 = *((char **)t1);
    t1 = (t0 + 8941);
    t8 = 1;
    if (2U == 2U)
        goto LAB10;

LAB11:    t8 = 0;

LAB12:    if (t8 != 0)
        goto LAB7;

LAB9:    xsi_set_current_line(66, ng0);
    t1 = (t0 + 5816);
    t2 = (t1 + 56U);
    t6 = *((char **)t2);
    t7 = (t6 + 56U);
    t9 = *((char **)t7);
    *((unsigned char *)t9) = (unsigned char)2;
    xsi_driver_first_trans_delta(t1, 3U, 1, 0LL);
    xsi_set_current_line(68, ng0);
    t1 = (t0 + 5816);
    t2 = (t1 + 56U);
    t6 = *((char **)t2);
    t7 = (t6 + 56U);
    t9 = *((char **)t7);
    *((unsigned char *)t9) = (unsigned char)2;
    xsi_driver_first_trans_delta(t1, 0U, 1, 0LL);

LAB8:    t1 = (t0 + 5672);
    *((int *)t1) = 1;

LAB1:    return;
LAB3:    xsi_set_current_line(51, ng0);
    t6 = (t0 + 4104U);
    t7 = *((char **)t6);
    t8 = *((unsigned char *)t7);
    t6 = (t0 + 2888U);
    t9 = *((char **)t6);
    t6 = (t0 + 8933);
    t10 = *((int *)t6);
    t11 = (t10 - 3);
    t12 = (t11 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t6));
    t13 = (1U * t12);
    t14 = (0 + t13);
    t15 = (t9 + t14);
    t16 = *((unsigned char *)t15);
    t17 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t8, t16);
    t18 = (t0 + 4104U);
    t19 = *((char **)t18);
    t18 = (t19 + 0);
    *((unsigned char *)t18) = t17;

LAB4:    t1 = (t0 + 8933);
    t4 = *((int *)t1);
    t2 = (t0 + 8937);
    t5 = *((int *)t2);
    if (t4 == t5)
        goto LAB5;

LAB6:    t3 = (t4 + 1);
    t4 = t3;
    t6 = (t0 + 8933);
    *((int *)t6) = t4;
    goto LAB2;

LAB7:    xsi_set_current_line(61, ng0);
    t15 = (t0 + 3208U);
    t18 = *((char **)t15);
    t3 = (4 - 4);
    t13 = (t3 * -1);
    t14 = (1U * t13);
    t20 = (0 + t14);
    t15 = (t18 + t20);
    t16 = *((unsigned char *)t15);
    t19 = (t0 + 5816);
    t21 = (t19 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t16;
    xsi_driver_first_trans_delta(t19, 3U, 1, 0LL);
    xsi_set_current_line(63, ng0);
    t1 = (t0 + 3208U);
    t2 = *((char **)t1);
    t3 = (4 - 4);
    t12 = (t3 * -1);
    t13 = (1U * t12);
    t14 = (0 + t13);
    t1 = (t2 + t14);
    t8 = *((unsigned char *)t1);
    t6 = (t0 + 3208U);
    t7 = *((char **)t6);
    t4 = (4 - 1);
    t5 = (t4 - 4);
    t20 = (t5 * -1);
    t25 = (1U * t20);
    t26 = (0 + t25);
    t6 = (t7 + t26);
    t16 = *((unsigned char *)t6);
    t17 = ieee_p_2592010699_sub_2507238156_503743352(IEEE_P_2592010699, t8, t16);
    t9 = (t0 + 5816);
    t15 = (t9 + 56U);
    t18 = *((char **)t15);
    t19 = (t18 + 56U);
    t21 = *((char **)t19);
    *((unsigned char *)t21) = t17;
    xsi_driver_first_trans_delta(t9, 0U, 1, 0LL);
    goto LAB8;

LAB10:    t12 = 0;

LAB13:    if (t12 < 2U)
        goto LAB14;
    else
        goto LAB12;

LAB14:    t7 = (t2 + t12);
    t9 = (t1 + t12);
    if (*((unsigned char *)t7) != *((unsigned char *)t9))
        goto LAB11;

LAB15:    t12 = (t12 + 1);
    goto LAB13;

}


extern void work_a_3949875129_3212880686_init()
{
	static char *pe[] = {(void *)work_a_3949875129_3212880686_p_0,(void *)work_a_3949875129_3212880686_p_1};
	xsi_register_didat("work_a_3949875129_3212880686", "isim/P3_ALU_Nbits_TestBench_isim_beh.exe.sim/work/a_3949875129_3212880686.didat");
	xsi_register_executes(pe);
}
