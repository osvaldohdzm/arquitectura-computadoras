library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity unicontrol is
    Port ( tipor: in STD_LOGIC;
	        beq: IN STD_LOGIC;
			  bneq: IN STD_LOGIC;
			  blt: IN STD_LOGIC;
			  ble: IN STD_LOGIC;
			  bgt: IN STD_LOGIC;
			  bget: IN STD_LOGIC; 
			  clk: IN STD_LOGIC;
			  clr: IN STD_LOGIC; 
			  eq: IN STD_LOGIC;
			  neq: IN STD_LOGIC;
			  lt: IN STD_LOGIC;
			  le: IN STD_LOGIC;
			  gtt: IN STD_LOGIC;
			  get: IN STD_LOGIC;
			  na : in  STD_LOGIC;
           sdopc: OUT STD_LOGIC;
			  sm : out  STD_LOGIC
			  );
end unicontrol;

architecture Behavioral of unicontrol is
type estado is(a);
signal est_act,est_sig:estado;
begin
process(clk,clr)
begin
	if(clr='1') then
		est_act<=a;
	elsif(clk'event and clk='1')then
		est_act<=est_sig;
	end if;
end process;


process(est_act,tipor,beq,bneq,blt,ble,bgt,bget,eq,neq,lt,le,gtt,get,na)
begin
	sm<='0';
	sdopc<='0';
	case est_act is
	when a=>
		if(tipor='1')then
			sm<='0';
			sdopc<='0';
			est_sig<=a;
		else
			if(beq='1')then
				if(na='1')then
					--est_sig<=a;
					sdopc<='0';
					sm<='1';
					est_sig<=a;
				else
					if(eq='0')then
						--est_sig<=a;
						sdopc<='0';
						sm<='1';
						est_sig<=a;
					else
						--est_sig<=a;
						sdopc<='1';
						sm<='1';
						est_sig<=a;
					end if;
				end if;
			else
				if(bneq='1') then
					if(na='1') then
						--est_sig<=a;
						sdopc<='0';
						sm<='1';
						est_sig<=a;
					else
						if(neq='0') then
							--est_sig<=a;
							sdopc<='0';
							sm<='1';
							est_sig<=a;
						else
							--est_sig<=a;
							sdopc<='1';
							sm<='1';
							est_sig<=a;
						end if;
					end if;
				else
					if(blt='1')then
						if(na='1')then
							sdopc<='0';
							sm<='1';
							est_sig<=a;
						else
							if(lt='0')then
								sdopc<='0';
								sm<='1';
								est_sig<=a;
							else
								sdopc<='1';
								sm<='1';
								est_sig<=a;
							end if;
						end if;
					else
						if(ble='1')then
							if(na='1')then
								sdopc<='0';
								sm<='1';
								est_sig<=a;
							else
								if(le='0')then
	
									sdopc<='0';
									sm<='1';
									est_sig<=a;
								else
									
									sdopc<='1';
									sm<='1';
									est_sig<=a;
								end if;
							end if;
						else
							if(bgt='1')then
								if(na='1')then
									
									sdopc<='0';
									sm<='1';
									est_sig<=a;
								else
									if(gtt='0') then
										
										sdopc<='0';
										sm<='1';
										est_sig<=a;
									else
										
										sdopc<='1';
										sm<='1';
										est_sig<=a;
									end if;
								end if;
							else
								if(bget='1')then
									if(na='1')then
										sdopc<='0';
										sm<='1';
										est_sig<=a;
									else
										if(get='0')then
											sdopc<='0';
											sm<='1';
											est_sig<=a;
										else
											sdopc<='1';
											sm<='1';
											est_sig<=a;
										end if;
									end if;
								else
									sdopc<='1';
									sm<='1';
									est_sig<=a;
								end if;
							end if;
						end if;
					end if;
				end if;
			end if;
		end if;
	end case;
end process;

end Behavioral;