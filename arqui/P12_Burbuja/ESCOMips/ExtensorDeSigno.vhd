
library IEEE;

use IEEE.STD_LOGIC_1164.ALL;

entity ExtensorDeSigno is
	port
	(
		DataIn	: in std_logic_vector(11 downto 0);
		DataOut	: out std_logic_vector(15 downto 0)
	);
end ExtensorDeSigno;

architecture A_ExtensorDeSigno of ExtensorDeSigno is
	signal aux: std_logic_vector(11 downto 0);
begin
	
	aux<=DataIn;
	DataOut<=aux(11)&aux(11)&aux(11)&aux(11)&aux;
	
end A_ExtensorDeSigno;