/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "D:/Anthony/Sexto Semestre/Arquitectura/ESCOMips/practica4.vhd";
extern char *IEEE_P_3620187407;
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
int ieee_p_3620187407_sub_514432868_3965413181(char *, char *, char *);


static void work_a_0306024963_0447502664_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    xsi_set_current_line(51, ng0);

LAB3:    t1 = (t0 + 2952U);
    t2 = *((char **)t1);
    t1 = (t0 + 1032U);
    t3 = *((char **)t1);
    t1 = (t0 + 11280U);
    t4 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t3, t1);
    t5 = (t4 - 0);
    t6 = (t5 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t4);
    t7 = (16U * t6);
    t8 = (0 + t7);
    t9 = (t2 + t8);
    t10 = (t0 + 7096);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t9, 16U);
    xsi_driver_first_trans_fast(t10);

LAB2:    t15 = (t0 + 6920);
    *((int *)t15) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0306024963_0447502664_p_1(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(52, ng0);

LAB3:    t1 = (t0 + 3592U);
    t2 = *((char **)t1);
    t1 = (t0 + 7160);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 16U);
    xsi_driver_first_trans_fast(t1);

LAB2:    t7 = (t0 + 6936);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0306024963_0447502664_p_2(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(53, ng0);

LAB3:    t1 = (t0 + 3592U);
    t2 = *((char **)t1);
    t1 = (t0 + 7224);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 16U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 6952);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0306024963_0447502664_p_3(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    xsi_set_current_line(54, ng0);

LAB3:    t1 = (t0 + 2952U);
    t2 = *((char **)t1);
    t1 = (t0 + 1192U);
    t3 = *((char **)t1);
    t1 = (t0 + 11296U);
    t4 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t3, t1);
    t5 = (t4 - 0);
    t6 = (t5 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t4);
    t7 = (16U * t6);
    t8 = (0 + t7);
    t9 = (t2 + t8);
    t10 = (t0 + 7288);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t9, 16U);
    xsi_driver_first_trans_fast_port(t10);

LAB2:    t15 = (t0 + 6968);
    *((int *)t15) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0306024963_0447502664_p_4(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;

LAB0:    xsi_set_current_line(57, ng0);
    t1 = (t0 + 2152U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)2);
    if (t4 != 0)
        goto LAB3;

LAB4:
LAB5:    t10 = (t0 + 3272U);
    t11 = *((char **)t10);
    t10 = (t0 + 7352);
    t12 = (t10 + 56U);
    t13 = *((char **)t12);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    memcpy(t15, t11, 16U);
    xsi_driver_first_trans_fast(t10);

LAB2:    t16 = (t0 + 6984);
    *((int *)t16) = 1;

LAB1:    return;
LAB3:    t1 = (t0 + 1672U);
    t5 = *((char **)t1);
    t1 = (t0 + 7352);
    t6 = (t1 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t5, 16U);
    xsi_driver_first_trans_fast(t1);
    goto LAB2;

LAB6:    goto LAB2;

}

static void work_a_0306024963_0447502664_p_5(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    int t5;
    int t6;
    char *t7;
    int t8;
    int t9;
    char *t10;
    char *t11;
    unsigned char t12;
    char *t13;
    int t14;
    int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;

LAB0:    xsi_set_current_line(63, ng0);
    t1 = (t0 + 2792U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    t1 = (t0 + 2592U);
    t3 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t3 != 0)
        goto LAB12;

LAB13:
LAB3:    t1 = (t0 + 7000);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(64, ng0);
    t5 = xsi_vhdl_pow(2, 4);
    t6 = (t5 - 1);
    t1 = (t0 + 11893);
    *((int *)t1) = 0;
    t7 = (t0 + 11897);
    *((int *)t7) = t6;
    t8 = 0;
    t9 = t6;

LAB5:    if (t8 <= t9)
        goto LAB6;

LAB8:    goto LAB3;

LAB6:    xsi_set_current_line(65, ng0);
    t10 = (t0 + 11901);
    t12 = (16U != 16U);
    if (t12 == 1)
        goto LAB9;

LAB10:    t13 = (t0 + 11893);
    t14 = *((int *)t13);
    t15 = (t14 - 0);
    t16 = (t15 * 1);
    t17 = (16U * t16);
    t18 = (0U + t17);
    t19 = (t0 + 7416);
    t20 = (t19 + 56U);
    t21 = *((char **)t20);
    t22 = (t21 + 56U);
    t23 = *((char **)t22);
    memcpy(t23, t10, 16U);
    xsi_driver_first_trans_delta(t19, t18, 16U, 0LL);

LAB7:    t1 = (t0 + 11893);
    t8 = *((int *)t1);
    t2 = (t0 + 11897);
    t9 = *((int *)t2);
    if (t8 == t9)
        goto LAB8;

LAB11:    t5 = (t8 + 1);
    t8 = t5;
    t7 = (t0 + 11893);
    *((int *)t7) = t8;
    goto LAB5;

LAB9:    xsi_size_not_matching(16U, 16U, 0);
    goto LAB10;

LAB12:    xsi_set_current_line(68, ng0);
    t2 = (t0 + 2472U);
    t7 = *((char **)t2);
    t4 = *((unsigned char *)t7);
    t12 = (t4 == (unsigned char)3);
    if (t12 != 0)
        goto LAB14;

LAB16:
LAB15:    goto LAB3;

LAB14:    xsi_set_current_line(69, ng0);
    t2 = (t0 + 3432U);
    t10 = *((char **)t2);
    t2 = (t0 + 1352U);
    t11 = *((char **)t2);
    t2 = (t0 + 11312U);
    t5 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t11, t2);
    t6 = (t5 - 0);
    t16 = (t6 * 1);
    t17 = (16U * t16);
    t18 = (0U + t17);
    t13 = (t0 + 7416);
    t19 = (t13 + 56U);
    t20 = *((char **)t19);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t10, 16U);
    xsi_driver_first_trans_delta(t13, t18, 16U, 0LL);
    goto LAB15;

}

static void work_a_0306024963_0447502664_p_6(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned char t5;
    int t6;
    int t7;
    int t8;
    int t9;
    char *t10;
    char *t11;
    int t12;
    int t13;
    char *t14;
    char *t15;
    int t16;
    int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    char *t21;
    unsigned char t22;
    unsigned char t23;
    char *t24;
    char *t25;
    char *t26;
    int t27;
    int t28;
    unsigned int t29;
    unsigned int t30;
    unsigned int t31;
    int t32;
    int t33;
    int t34;

LAB0:    xsi_set_current_line(80, ng0);
    t1 = (t0 + 3112U);
    t2 = *((char **)t1);
    t1 = (t0 + 4128U);
    t3 = *((char **)t1);
    t1 = (t3 + 0);
    memcpy(t1, t2, 16U);
    xsi_set_current_line(83, ng0);
    t1 = (t0 + 2312U);
    t2 = *((char **)t1);
    t4 = *((unsigned char *)t2);
    t5 = (t4 == (unsigned char)3);
    if (t5 != 0)
        goto LAB2;

LAB4:    xsi_set_current_line(98, ng0);
    t6 = (4 - 1);
    t1 = (t0 + 11933);
    *((int *)t1) = 0;
    t2 = (t0 + 11937);
    *((int *)t2) = t6;
    t7 = 0;
    t8 = t6;

LAB21:    if (t7 <= t8)
        goto LAB22;

LAB24:
LAB3:    xsi_set_current_line(113, ng0);
    t1 = (t0 + 4128U);
    t2 = *((char **)t1);
    t1 = (t0 + 7480);
    t3 = (t1 + 56U);
    t10 = *((char **)t3);
    t11 = (t10 + 56U);
    t14 = *((char **)t11);
    memcpy(t14, t2, 16U);
    xsi_driver_first_trans_fast(t1);
    t1 = (t0 + 7016);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(84, ng0);
    t6 = (4 - 1);
    t1 = (t0 + 11917);
    *((int *)t1) = 0;
    t3 = (t0 + 11921);
    *((int *)t3) = t6;
    t7 = 0;
    t8 = t6;

LAB5:    if (t7 <= t8)
        goto LAB6;

LAB8:    goto LAB3;

LAB6:    xsi_set_current_line(85, ng0);
    t9 = (16 - 1);
    t10 = (t0 + 11925);
    *((int *)t10) = t9;
    t11 = (t0 + 11929);
    *((int *)t11) = 0;
    t12 = t9;
    t13 = 0;

LAB9:    if (t12 >= t13)
        goto LAB10;

LAB12:
LAB7:    t1 = (t0 + 11917);
    t7 = *((int *)t1);
    t2 = (t0 + 11921);
    t8 = *((int *)t2);
    if (t7 == t8)
        goto LAB8;

LAB20:    t6 = (t7 + 1);
    t7 = t6;
    t3 = (t0 + 11917);
    *((int *)t3) = t7;
    goto LAB5;

LAB10:    xsi_set_current_line(86, ng0);
    t14 = (t0 + 1512U);
    t15 = *((char **)t14);
    t14 = (t0 + 11917);
    t16 = *((int *)t14);
    t17 = (t16 - 3);
    t18 = (t17 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t14));
    t19 = (1U * t18);
    t20 = (0 + t19);
    t21 = (t15 + t20);
    t22 = *((unsigned char *)t21);
    t23 = (t22 == (unsigned char)2);
    if (t23 != 0)
        goto LAB13;

LAB15:    xsi_set_current_line(89, ng0);
    t1 = (t0 + 11925);
    t2 = (t0 + 11917);
    t6 = xsi_vhdl_pow(2, *((int *)t2));
    t9 = *((int *)t1);
    t16 = (t9 - t6);
    t4 = (t16 < 0);
    if (t4 != 0)
        goto LAB16;

LAB18:    xsi_set_current_line(92, ng0);
    t1 = (t0 + 4128U);
    t2 = *((char **)t1);
    t1 = (t0 + 11925);
    t3 = (t0 + 11917);
    t6 = xsi_vhdl_pow(2, *((int *)t3));
    t9 = *((int *)t1);
    t16 = (t9 - t6);
    t17 = (t16 - 15);
    t18 = (t17 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, t16);
    t19 = (1U * t18);
    t20 = (0 + t19);
    t10 = (t2 + t20);
    t4 = *((unsigned char *)t10);
    t11 = (t0 + 4128U);
    t14 = *((char **)t11);
    t11 = (t0 + 11925);
    t27 = *((int *)t11);
    t28 = (t27 - 15);
    t29 = (t28 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t11));
    t30 = (1U * t29);
    t31 = (0 + t30);
    t15 = (t14 + t31);
    *((unsigned char *)t15) = t4;

LAB17:
LAB14:
LAB11:    t1 = (t0 + 11925);
    t12 = *((int *)t1);
    t2 = (t0 + 11929);
    t13 = *((int *)t2);
    if (t12 == t13)
        goto LAB12;

LAB19:    t6 = (t12 + -1);
    t12 = t6;
    t3 = (t0 + 11925);
    *((int *)t3) = t12;
    goto LAB9;

LAB13:    xsi_set_current_line(87, ng0);
    t24 = (t0 + 4128U);
    t25 = *((char **)t24);
    t24 = (t0 + 4128U);
    t26 = *((char **)t24);
    t24 = (t26 + 0);
    memcpy(t24, t25, 16U);
    goto LAB14;

LAB16:    xsi_set_current_line(90, ng0);
    t3 = (t0 + 4128U);
    t10 = *((char **)t3);
    t3 = (t0 + 11925);
    t17 = *((int *)t3);
    t27 = (t17 - 15);
    t18 = (t27 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t3));
    t19 = (1U * t18);
    t20 = (0 + t19);
    t11 = (t10 + t20);
    *((unsigned char *)t11) = (unsigned char)2;
    goto LAB17;

LAB22:    xsi_set_current_line(99, ng0);
    t9 = (16 - 1);
    t3 = (t0 + 11941);
    *((int *)t3) = 0;
    t10 = (t0 + 11945);
    *((int *)t10) = t9;
    t12 = 0;
    t13 = t9;

LAB25:    if (t12 <= t13)
        goto LAB26;

LAB28:
LAB23:    t1 = (t0 + 11933);
    t7 = *((int *)t1);
    t2 = (t0 + 11937);
    t8 = *((int *)t2);
    if (t7 == t8)
        goto LAB24;

LAB36:    t6 = (t7 + 1);
    t7 = t6;
    t3 = (t0 + 11933);
    *((int *)t3) = t7;
    goto LAB21;

LAB26:    xsi_set_current_line(100, ng0);
    t11 = (t0 + 1512U);
    t14 = *((char **)t11);
    t11 = (t0 + 11933);
    t16 = *((int *)t11);
    t17 = (t16 - 3);
    t18 = (t17 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t11));
    t19 = (1U * t18);
    t20 = (0 + t19);
    t15 = (t14 + t20);
    t4 = *((unsigned char *)t15);
    t5 = (t4 == (unsigned char)2);
    if (t5 != 0)
        goto LAB29;

LAB31:    xsi_set_current_line(103, ng0);
    t1 = (t0 + 11941);
    t2 = (t0 + 11933);
    t6 = xsi_vhdl_pow(2, *((int *)t2));
    t9 = *((int *)t1);
    t16 = (t9 + t6);
    t4 = (t16 < 16);
    if (t4 != 0)
        goto LAB32;

LAB34:    xsi_set_current_line(106, ng0);
    t1 = (t0 + 4128U);
    t2 = *((char **)t1);
    t1 = (t0 + 11941);
    t6 = *((int *)t1);
    t9 = (t6 - 15);
    t18 = (t9 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t1));
    t19 = (1U * t18);
    t20 = (0 + t19);
    t3 = (t2 + t20);
    *((unsigned char *)t3) = (unsigned char)2;

LAB33:
LAB30:
LAB27:    t1 = (t0 + 11941);
    t12 = *((int *)t1);
    t2 = (t0 + 11945);
    t13 = *((int *)t2);
    if (t12 == t13)
        goto LAB28;

LAB35:    t6 = (t12 + 1);
    t12 = t6;
    t3 = (t0 + 11941);
    *((int *)t3) = t12;
    goto LAB25;

LAB29:    xsi_set_current_line(101, ng0);
    t21 = (t0 + 4128U);
    t24 = *((char **)t21);
    t21 = (t0 + 4128U);
    t25 = *((char **)t21);
    t21 = (t25 + 0);
    memcpy(t21, t24, 16U);
    goto LAB30;

LAB32:    xsi_set_current_line(104, ng0);
    t3 = (t0 + 4128U);
    t10 = *((char **)t3);
    t3 = (t0 + 11941);
    t11 = (t0 + 11933);
    t17 = xsi_vhdl_pow(2, *((int *)t11));
    t27 = *((int *)t3);
    t28 = (t27 + t17);
    t32 = (t28 - 15);
    t18 = (t32 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, t28);
    t19 = (1U * t18);
    t20 = (0 + t19);
    t14 = (t10 + t20);
    t5 = *((unsigned char *)t14);
    t15 = (t0 + 4128U);
    t21 = *((char **)t15);
    t15 = (t0 + 11941);
    t33 = *((int *)t15);
    t34 = (t33 - 15);
    t29 = (t34 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t15));
    t30 = (1U * t29);
    t31 = (0 + t30);
    t24 = (t21 + t31);
    *((unsigned char *)t24) = t5;
    goto LAB33;

}


extern void work_a_0306024963_0447502664_init()
{
	static char *pe[] = {(void *)work_a_0306024963_0447502664_p_0,(void *)work_a_0306024963_0447502664_p_1,(void *)work_a_0306024963_0447502664_p_2,(void *)work_a_0306024963_0447502664_p_3,(void *)work_a_0306024963_0447502664_p_4,(void *)work_a_0306024963_0447502664_p_5,(void *)work_a_0306024963_0447502664_p_6};
	xsi_register_didat("work_a_0306024963_0447502664", "isim/T_ESCOMips_isim_beh.exe.sim/work/a_0306024963_0447502664.didat");
	xsi_register_executes(pe);
}
