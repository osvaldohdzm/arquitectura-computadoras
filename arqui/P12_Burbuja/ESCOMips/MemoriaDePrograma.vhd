
library IEEE;

use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity MemoriaDePrograma is
	generic
	(
		constant tamInstruccion: integer:=25; --Tama�o del Bus de Instrucciones
		constant tamBusDir	  : integer:=9   --Tama�o del Bus de Direcciones
	);
	port
	(
		BusDir		: in std_logic_vector(tamBusDir-1 downto 0); 	  --Bus de Direcciones
		Instruccion	: out std_logic_vector(tamInstruccion-1 downto 0) --Bus de Instrucciones
	);
end MemoriaDePrograma;

architecture A_MemoriaDePrograma of MemoriaDePrograma is
	
	constant R0	: std_logic_vector(3 downto 0):=x"0";
	constant R1	: std_logic_vector(3 downto 0):=x"1";
	constant R2	: std_logic_vector(3 downto 0):=x"2";
	constant R3	: std_logic_vector(3 downto 0):=x"3";
	constant R4	: std_logic_vector(3 downto 0):=x"4";
	constant R5	: std_logic_vector(3 downto 0):=x"5";
	constant R6	: std_logic_vector(3 downto 0):=x"6";
	constant R7	: std_logic_vector(3 downto 0):=x"7";
	constant R8	: std_logic_vector(3 downto 0):=x"8";
	constant R9	: std_logic_vector(3 downto 0):=x"9";
	constant R10: std_logic_vector(3 downto 0):=x"A";
	constant R11: std_logic_vector(3 downto 0):=x"B";
	constant R12: std_logic_vector(3 downto 0):=x"C";
	constant R13: std_logic_vector(3 downto 0):=x"D";
	constant R14: std_logic_vector(3 downto 0):=x"E";
	constant R15: std_logic_vector(3 downto 0):=x"F";
	
	--Codigos de Operaci�n
	constant codOP_TipoR		: std_logic_vector:="00000";
	constant codOP_LI			: std_logic_vector:="00001";
	constant codOP_LWI		: std_logic_vector:="00010";
	constant codOP_SWI		: std_logic_vector:="00011";
	constant codOP_SW			: std_logic_vector:="00100";
	constant codOP_ADDI		: std_logic_vector:="00101";
	constant codOP_SUBI		: std_logic_vector:="00110";
	constant codOP_ANDI		: std_logic_vector:="00111";
	constant codOP_ORI		: std_logic_vector:="01000";
	constant codOP_XORI		: std_logic_vector:="01001";
	constant codOP_NANDI		: std_logic_vector:="01010";
	constant codOP_NORI		: std_logic_vector:="01011";
	constant codOP_XNORI		: std_logic_vector:="01100";
	constant codOP_BEQI		: std_logic_vector:="01101";
	constant codOP_BNEI		: std_logic_vector:="01110";
	constant codOP_BLTI		: std_logic_vector:="01111";
	constant codOP_BLETI		: std_logic_vector:="10000";
	constant codOP_BGTI		: std_logic_vector:="10001";
	constant codOP_BGETI		: std_logic_vector:="10010";
	constant codOP_B			: std_logic_vector:="10011";
	constant codOP_CALL		: std_logic_vector:="10100";
	constant codOP_RET		: std_logic_vector:="10101";
	constant codOP_NOP		: std_logic_vector:="10110";
	constant codOP_LW			: std_logic_vector:="10111";
	
	--C�digos de Funci�n
	constant codFun_ADD		: std_logic_vector:=x"0";
	constant codFun_SUB		: std_logic_vector:=x"1";
	constant codFun_AND		: std_logic_vector:=x"2";
	constant codFun_OR		: std_logic_vector:=x"3";
	constant codFun_XOR		: std_logic_vector:=x"4";
	constant codFun_NAND 	: std_logic_vector:=x"5";
	constant codFun_NOR		: std_logic_vector:=x"6";
	constant codFun_XNOR 	: std_logic_vector:=x"7";
	constant codFun_NOT		: std_logic_vector:=x"8";
	constant codFun_SLL		: std_logic_vector:=x"9";
	constant codFun_SRL  	: std_logic_vector:=x"A";
	
	--Sin Usar
	constant SU: std_logic_vector:=x"0";
	
	--Etiquetas llamadas a subrutinas
	constant initialize	: std_logic_vector:=x"0005";
	constant sorting		: std_logic_vector:=x"000D";
	constant show	: std_logic_vector:=x"001B";
	
	--Etiquetas para saltos incondicionales
	constant filling		: std_logic_vector:=x"0007";
	constant loop1		: std_logic_vector:=x"000E";
	constant loop2		: std_logic_vector:=x"0010";
	constant reading			: std_logic_vector:=x"001C";
	constant again	: std_logic_vector:=x"0003";
	
	--Etiquetas para saltos condicionales
	constant end_initialize	: std_logic_vector:=x"005";
	constant end_show		: std_logic_vector:=x"004";
	constant end_sorting			: std_logic_vector:=x"00C";
	constant loopi				: std_logic_vector:=x"008";
	constant loopj				: std_logic_vector:=x"003";
	
	TYPE Localidades IS ARRAY (0 to ((2**tamBusDir)-1)) OF std_logic_vector(tamInstruccion-1 downto 0);
	constant Memoria: Localidades:=(
												codOP_LI&R0&x"000A",
												codOP_CALL&SU&initialize,
												codOP_CALL&SU&sorting,
												codOP_CALL&SU&show,
												codOP_B&SU&again,
												codOP_LI&R1&x"0000",
												codOP_LI&R2&x"0384",
												codOP_BEQI&R1&R0&end_initialize,
												codOP_SW&R2&R1&x"000",
												codOP_SUBI&R2&R2&x"00A",
												codOP_ADDI&R1&R1&x"001",
												codOP_B&SU&filling,
												codOP_RET&SU&SU&SU&SU&SU,
												codOP_LI&R1&x"0001",
												codOP_BEQI&R1&R0&end_sorting,
												codOP_ORI&R2&R1&x"000",
												codOP_BEQI&R2&R0&loopi,
												codOP_LW&R3&R1&x"FFF",
												codOP_LW&R4&R2&x"000",
												codOP_BLETI&R4&R3&loopj,
												codOP_SW&R4&R1&x"FFF",
												codOP_SW&R3&R2&x"000",
												codOP_ADDI&R2&R2&x"001",
												codOP_B&SU&loop2,
												codOP_ADDI&R1&R1&x"001",
												codOP_B&SU&loop1,
												codOP_RET&SU&SU&SU&SU&SU,
												codOP_LI&R1&x"0000",
												codOP_BEQI&R1&R0&end_show,
												codOP_LW&R2&R1&x"000",
												codOP_ADDI&R1&R1&x"001",
												codOP_B&SU&reading,
												codOP_RET&SU&SU&SU&SU&SU,
												others=>(others=>'0')				
												);
begin

	Instruccion<=Memoria(CONV_INTEGER(BusDir));

end A_MemoriaDePrograma;