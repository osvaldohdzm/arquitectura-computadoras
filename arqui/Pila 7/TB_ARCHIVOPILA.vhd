LIBRARY ieee;
LIBRARY STD;
USE STD.TEXTIO.ALL;
USE ieee.std_logic_TEXTIO.ALL;	

USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_UNSIGNED.ALL;
USE ieee.std_logic_ARITH.ALL;

 
ENTITY TB_ARCHIVOPILA IS
END TB_ARCHIVOPILA;
 
ARCHITECTURE behavior OF TB_ARCHIVOPILA IS 
 
 
    COMPONENT pila
    PORT(
         UP : IN  std_logic;
         DW : IN  std_logic;
         WPC : IN  std_logic;
         CLK : IN  std_logic;
         CLR : IN  std_logic;
         D : IN  std_logic_vector(15 downto 0);
         PC : OUT  std_logic_vector(15 downto 0);
         SP_AUX : OUT  std_logic_vector(2 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal UP : std_logic := '0';
   signal DW : std_logic := '0';
   signal WPC : std_logic := '0';
   signal CLK : std_logic := '0';
   signal CLR : std_logic := '0';
   signal D : std_logic_vector(15 downto 0) := (others => '0');

 	--Outputs
   signal PC : std_logic_vector(15 downto 0);
   signal SP_AUX : std_logic_vector(2 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
   uut: pila PORT MAP (
          UP => UP,
          DW => DW,
          WPC => WPC,
          CLK => CLK,
          CLR => CLR,
          D => D,
          PC => PC,
          SP_AUX => SP_AUX
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
 stim_proc: process
		file ARCH_RES : TEXT;																					
		variable LINEA_RES : line;
		VARIABLE var_PC : STD_LOGIC_VECTOR(15 DOWNTO 0);
		VARIABLE var_SP_AUX : STD_LOGIC_VECTOR(2 DOWNTO 0);
		file ARCH_VEC : TEXT;
		variable LINEA_VEC : line;
		VARIABLE var_d_in: STD_LOGIC_VECTOR(15 DOWNTO 0);
		VARIABLE var_wpc,var_up,var_dw,var_clr: std_logic;
		VARIABLE CADENA : STRING(1 TO 10);
	   begin		
			file_open(ARCH_VEC, "VECTORES.TXT", READ_MODE); 	
		file_open(ARCH_RES, "RESULTADO.TXT", WRITE_MODE); 	

		CADENA := "     D    ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := "    UP    ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := "   DW     ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := "   WPC    ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		CADENA := "   CLR    ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		CADENA := "   sp     ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		CADENA := "   PC     ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		writeline(ARCH_RES,LINEA_RES);
		WAIT FOR 100 NS;
		FOR I IN 0 TO 28 LOOP
			readline(ARCH_VEC,LINEA_VEC); 

			hread(LINEA_VEC, var_d_in);
			D <= var_d_in;
			
			read(LINEA_VEC, VAR_up);
			up <= var_up;
			read(LINEA_VEC, VAR_dw);
			dw <= var_dw;
			read(LINEA_VEC, VAR_wpc);
			wpc <= var_wpc;
			
			read(LINEA_VEC, VAR_clr);
			clr <= var_clr;
			
			WAIT UNTIL RISING_EDGE(clk);	--ESPERO AL FLANCO DE SUBIDA 

			var_d_in := d;			
			var_up := up;
			var_dw :=dw;
			var_clr :=clr;
			var_wpc := wpc;
			
			
			var_sp_aux:=SP_AUX;
			var_PC:=PC;

			hwrite(LINEA_RES, var_d_in, right, 8);
			write(LINEA_RES, var_up, right, 10);
			write(LINEA_RES, var_dw, right, 10);
			write(LINEA_RES, wpc, right, 10);
			write(LINEA_RES, var_clr, right, 10);
		   write(LINEA_RES, var_SP_AUX, right, 14);
			hwrite(LINEA_RES, var_PC, right, 14);
						
	
		
			writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo
		end loop;
		file_close(ARCH_VEC);  -- cierra el archivo
		file_close(ARCH_RES);  -- cierra el archivo   end process;
		  wait;
   end process;
END;
