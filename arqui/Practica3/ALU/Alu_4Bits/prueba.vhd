--------------------------------------------------------------------------------
--Simulacion
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY prueba IS
END prueba;
 
ARCHITECTURE behavior OF prueba IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ALU
    PORT(
         a : IN  std_logic_vector(3 downto 0);
         b : IN  std_logic_vector(3 downto 0);
         sel_a : IN  std_logic;
         sel_b : IN  std_logic;
         op : IN  std_logic_vector(1 downto 0);
         res : OUT  std_logic_vector(3 downto 0);
         c : OUT  std_logic;
         z : OUT  std_logic;
         n : OUT  std_logic;
         Ov : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(3 downto 0) := (others => '0');
   signal b : std_logic_vector(3 downto 0) := (others => '0');
   signal sel_a : std_logic := '0';
   signal sel_b : std_logic := '0';
   signal op : std_logic_vector(1 downto 0) := (others => '0');

 	--Outputs
   signal res : std_logic_vector(3 downto 0);
   signal c : std_logic;
   signal z : std_logic;
   signal n : std_logic;
   signal Ov : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   --constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ALU PORT MAP (
          a => a,
          b => b,
          sel_a => sel_a,
          sel_b => sel_b,
          op => op,
          res => res,
          c => c,
          z => z,
          n => n,
          Ov => Ov
        );

   -- Clock process definitions
   --<clock>_process :process
   --begin
	--	<clock> <= '0';
	--	wait for <clock>_period/2;
	--	<clock> <= '1';
	--	wait for <clock>_period/2;
   --end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      --wait for 100 ns;	
      --wait for <clock>_period*10;

      -- insert stimulus here 
		

--------------- PARTE 3
		a <= "0101";--  5
		b <= "0101";--  5
		
		sel_a <= '0';--A-B
		sel_b <= '1';
		op(1) <= '1';
		op(0) <= '1';
		
		wait for 100 ns;
		
		a <= "0101";--  5
		b <= "0101";--  5
		
		sel_a <= '1';--NAND 
		sel_b <= '1';
		op(1) <= '0';
		op(0) <= '1';
		
		wait for 100 ns;

		
		
      wait;
   end process;

END;
