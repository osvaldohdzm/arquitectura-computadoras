LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY prueba IS
END prueba;
 
ARCHITECTURE behavior OF prueba IS 
 
    -- Component Declaration for the Unit Under Test (UUT) 
    COMPONENT cascada
    PORT(
         cin : IN  std_logic;--suma/resta
         a : IN  std_logic_vector(3 downto 0);
         b : IN  std_logic_vector(3 downto 0);
         s : OUT  std_logic_vector(3 downto 0);
         cout : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal cin : std_logic := '0';
   signal a : std_logic_vector(3 downto 0) := (others => '0');
   signal b : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal s : std_logic_vector(3 downto 0);
   signal cout : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
  
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: cascada PORT MAP (
          cin => cin,
          a => a,
          b => b,
          s => s,
          cout => cout
        );

   -- Clock process definitions


   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		cin <= '0';
		a <= "0101";
		b <= "0101";
		wait for 10 ns; -- 5 + 5
		
		cin <= '0';
		a <= "1100";		
		b <= "1000";
		wait for 10 ns; -- 12 + 8
      
		cin <= '0';
		a <= "1001";
		b <= "0101";
		wait for 10 ns; -- 9 + 5
		
		cin <= '1';		
		a <= "1010";
		b <= "1001";		
		wait for 10 ns; -- 10  - 9
      
		cin <= '0';
		a <= "0100";
		b <= "0010";			
		wait for 10 ns; -- 4 + 2
		
		cin <= '1';		
		a <= "0111";
		b <= "1001";		
		wait for 10 ns;-- 7 - 9
      
		cin <= '1';		
		a <= "1111";
		b <= "1111";		
		wait for 10 ns; -- 15 - 15
		
		cin <= '1';		
		a <= "1011";
		b <= "1000";		
		wait for 10 ns; -- 11 - 9
		
		cin <= '1';		
		a <= "0001";
		b <= "0100";		
		wait for 10 ns; -- 1 - 4

		wait for 20 ns;

      -- insert stimulus here 

      wait;
   end process;

END;