
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY TB_DEMUX IS
END TB_DEMUX;
 
ARCHITECTURE behavior OF TB_DEMUX IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT DEMUX
    PORT(
         WR : IN  std_logic_vector(3 downto 0);
         L : OUT  std_logic_vector(15 downto 0);
         WRITE_REG : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal WR : std_logic_vector(3 downto 0) := (others => '0');
   signal WRITE_REG : std_logic := '0';

 	--Outputs
   signal L : std_logic_vector(15 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 

BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: DEMUX PORT MAP (
          WR => WR,
          L => L,
          WRITE_REG => WRITE_REG
        );
 

   -- Stimulus process
   stim_proc: process
   begin		
     
	  WRITE_REG <= '1';
	  WR <= "0000";
	  WAIT FOR 30ns;
	  
	  WRITE_REG <= '1';
	  WR <= "0001";
	  WAIT FOR 30ns;
	  WRITE_REG <= '1';
	  WR <= "0010";
	  WAIT FOR 30ns;
	  WRITE_REG <= '1';
	  WR <= "0011";
	  WAIT FOR 30ns;
	  WRITE_REG <= '1';
	  WR <= "0100";
	  WAIT FOR 30ns;
	  WRITE_REG <= '1';
	  WR <= "0101";
	  WAIT FOR 30ns;
	  WRITE_REG <= '1';
	  WR <= "0110";
	  WAIT FOR 30ns;
	  WRITE_REG <= '1';
	  WR <= "0111";
	  WAIT FOR 30ns;
	  WRITE_REG <= '1';
	  WR <= "1000";
	  WAIT FOR 30ns;
	  WRITE_REG <= '1';
	  WR <= "1001";
	  WAIT FOR 30ns;
	  WRITE_REG <= '1';
	  WR <= "1010";
	  WAIT FOR 30ns;
	  WRITE_REG <= '1';
	  WR <= "1011";
	  WAIT FOR 30ns;
	  WRITE_REG <= '1';
	  WR <= "1100";
	  WAIT FOR 30ns;
	  WRITE_REG <= '1';
	  WR <= "1101";
	  WAIT FOR 30ns;
	  WRITE_REG <= '1';
	  WR <= "1110";
	  WAIT FOR 30ns;
	  WRITE_REG <= '1';
	  WR <= "1111";
	  WAIT FOR 30ns;
	  
	  
	
	  WAIT FOR 30ns;
	  WRITE_REG <= '0';
	  WR <= "0010";
	  WAIT FOR 30ns;
	  WRITE_REG <= '0';
	  WR <= "0011";
	  WAIT FOR 30ns;
	  WRITE_REG <= '0';
	  WR <= "0100";
	  WAIT FOR 30ns;
	  WRITE_REG <= '0';
	  WR <= "0101";
	  WAIT FOR 30ns;
	  WRITE_REG <= '0';
	  WR <= "0110";
	  WAIT FOR 30ns;
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  

      wait;
   end process;

END;
