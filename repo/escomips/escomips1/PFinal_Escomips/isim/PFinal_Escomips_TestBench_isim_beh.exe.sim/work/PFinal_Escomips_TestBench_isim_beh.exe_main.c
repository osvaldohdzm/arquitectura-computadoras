/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;

char *STD_STANDARD;
char *IEEE_P_2592010699;
char *IEEE_P_3620187407;
char *WORK_P_2050238965;
char *WORK_P_1440495363;
char *IEEE_P_3499444699;


int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    ieee_p_2592010699_init();
    work_p_1440495363_init();
    ieee_p_3499444699_init();
    ieee_p_3620187407_init();
    work_p_2050238965_init();
    work_a_2565927484_3212880686_init();
    work_a_4165333956_3212880686_init();
    work_a_1222720534_3212880686_init();
    work_a_1481163779_3212880686_init();
    work_a_0283337487_3212880686_init();
    work_a_2168375424_3212880686_init();
    work_a_2467476299_3212880686_init();
    work_a_0164854686_3212880686_init();
    work_a_1148267228_3212880686_init();
    work_a_2178422333_3212880686_init();
    work_a_2253722718_3212880686_init();
    work_a_1452136513_3212880686_init();
    work_a_3034856961_3212880686_init();
    work_a_4229584874_3212880686_init();
    work_a_0435739729_3212880686_init();
    work_a_1878588406_3212880686_init();
    work_a_1193459281_3212880686_init();
    work_a_4225960561_3212880686_init();
    work_a_1774854872_3212880686_init();
    work_a_2793666179_3212880686_init();
    work_a_2975858531_2372691052_init();


    xsi_register_tops("work_a_2975858531_2372691052");

    STD_STANDARD = xsi_get_engine_memory("std_standard");
    IEEE_P_2592010699 = xsi_get_engine_memory("ieee_p_2592010699");
    xsi_register_ieee_std_logic_1164(IEEE_P_2592010699);
    IEEE_P_3620187407 = xsi_get_engine_memory("ieee_p_3620187407");
    WORK_P_2050238965 = xsi_get_engine_memory("work_p_2050238965");
    WORK_P_1440495363 = xsi_get_engine_memory("work_p_1440495363");
    IEEE_P_3499444699 = xsi_get_engine_memory("ieee_p_3499444699");

    return xsi_run_simulation(argc, argv);

}
