
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
 
ENTITY sim_completa IS
END sim_completa;
 
ARCHITECTURE behavior OF sim_completa IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT P8
    PORT(
         clk : IN  std_logic;
         clr : IN  std_logic;
         ini : IN  std_logic;
         d : IN  std_logic_vector(8 downto 0);
         a : INOUT  std_logic_vector(8 downto 0);
         display : OUT  std_logic_vector(6 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';
   signal ini : std_logic := '0';
   signal d : std_logic_vector(8 downto 0) := (others => '0');

	--BiDirs
   signal a : std_logic_vector(8 downto 0);

 	--Outputs
   signal display : std_logic_vector(6 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: P8 PORT MAP (
          clk => clk,
          clr => clr,
          ini => ini,
          d => d,
          a => a,
          display => display
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- insert stimulus here 
		clr <= '1';
		ini <= '0';
		d <= "000000000";
		wait for clk_period;
		
		clr <= '0';
		ini <= '0';
		d <= "101100010";
		wait for clk_period;
		
		clr <= '0';
		ini <= '1';
		d <= "111111111";
		wait for clk_period;
		
		
      wait;
   end process;

END;
