LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY P2_SumRes_Anticipado_TestBench IS
END P2_SumRes_Anticipado_TestBench;
 
ARCHITECTURE behavior OF P2_SumRes_Anticipado_TestBench IS 
 
    COMPONENT P2_SumRes_Anticipado
    PORT(
         a : IN  std_logic_vector(3 downto 0);
         b : IN  std_logic_vector(3 downto 0);
         cin : IN  std_logic;
         s : OUT  std_logic_vector(3 downto 0);
         cout : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(3 downto 0) := (others => '0');
   signal b : std_logic_vector(3 downto 0) := (others => '0');
   signal cin : std_logic := '0';

 	--Outputs
   signal s : std_logic_vector(3 downto 0);
   signal cout : std_logic;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: P2_SumRes_Anticipado PORT MAP (
          a => a,
          b => b,
          cin => cin,
          s => s,
          cout => cout
        );

   -- Stimulus process
   stim_proc: process
   begin
		-- 5 + 5
      wait for 100 ns;
			a <= "1111";
			b <= "0010";
			cin <= '0';
		-- 15 + 2
		wait for 100 ns;
			a <= "1100";
			b <= "0111";
			cin <= '0';
		-- 9 + 5
		wait for 100 ns;
			a <= "1001";
			b <= "0101";
			cin <= '0';
		-- 14 - 9
		wait for 100 ns;
			a <= "1110";
			b <= "1001";
			cin <= '1';
		-- 4 + 2
		wait for 100 ns;
			a <= "0100";
			b <= "0010";
			cin <= '0';
		-- 7 - 7
		wait for 100 ns;
			a <= "0111";
			b <= "0111";
			cin <= '1';
		-- 15 - 5
		wait for 100 ns;
			a <= "1111";
			b <= "0101";
			cin <= '1';
		-- 11 - 8
		wait for 100 ns;
			a <= "1011";
			b <= "1000";
			cin <= '1';
		-- 1 - 4
		wait for 100 ns;
			a <= "0001";
			b <= "0100";
			cin <= '1';
      wait;
   end process;

END;
