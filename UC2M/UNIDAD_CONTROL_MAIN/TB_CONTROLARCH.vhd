LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
use ieee.numeric_std.all;
USE IEEE.std_logic_unsigned.ALL;

LIBRARY STD;
USE STD.TEXTIO.ALL;
USE ieee.std_logic_TEXTIO.ALL;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_UNSIGNED.ALL;
USE ieee.std_logic_ARITH.ALL;
 
ENTITY TB_CONTROLARCH IS
END TB_CONTROLARCH;
 
ARCHITECTURE behavior OF TB_CONTROLARCH IS  
    COMPONENT PRINCIPAL
    PORT(
         CLK : IN  std_logic;
         CLR : IN  std_logic;
         LF : IN  std_logic;
         COD_FUNC : IN  std_logic_vector(3 downto 0);
         BANDERAS : IN  std_logic_vector(3 downto 0);
         S : OUT  std_logic_vector(19 downto 0);
         COD_OP : IN  std_logic_vector(4 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal CLR : std_logic := '0';
   signal LF : std_logic := '0';
   signal COD_FUNC : std_logic_vector(3 downto 0) := (others => '0');
   signal BANDERAS : std_logic_vector(3 downto 0) := (others => '0');
   signal COD_OP : std_logic_vector(4 downto 0) := (others => '0');
   signal S : std_logic_vector(19 downto 0);
   constant CLK_period : time := 10 ns;
 
BEGIN
   uut: PRINCIPAL PORT MAP (
          CLK => CLK,
          CLR => CLR,
          LF => LF,
          COD_FUNC => COD_FUNC,
          BANDERAS => BANDERAS,
          S => S,
          COD_OP => COD_OP
        );
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 
 stim_proc: process
		file ARCH_RES : TEXT;																					
		variable LINEA_RES : line;
		VARIABLE var_S : STD_LOGIC_VECTOR(19 DOWNTO 0);
		file ARCH_VEC : TEXT;
		variable LINEA_VEC : line;
		VARIABLE var_cod_func,var_banderas: STD_LOGIC_VECTOR(3 DOWNTO 0);
		VARIABLE var_cod_op: STD_LOGIC_VECTOR(4 DOWNTO 0);
		VARIABLE var_clr,var_lf: std_logic;
		VARIABLE CADENA : STRING(1 TO 10);
		VARIABLE CAD: STRING (1 TO 3);
	   begin		
			file_open(ARCH_VEC, "vectores.txt", READ_MODE); 	
		file_open(ARCH_RES, "resultado.txt", WRITE_MODE); 	

		CADENA := " OP_CODE  ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := " FUN_CODE ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := " BANDERAS ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := "   CLR    ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		CADENA := "   LF     ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		CADENA := "MICROINSTR";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+13);
		CADENA := "   NIVEL  ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		writeline(ARCH_RES,LINEA_RES);
		
		wait for 100 ns;
		FOR I IN 0 TO 1 LOOP
		   readline(ARCH_VEC,LINEA_VEC); 
			read(LINEA_VEC, var_cod_op);
			COD_OP <= var_cod_op;
			read(LINEA_VEC, var_cod_func);
			COD_FUNC <= var_cod_func;
			read(LINEA_VEC, var_banderas);
			BANDERAS <= var_banderas;
			read(LINEA_VEC, var_clr);
			CLR <= var_clr;
			read(LINEA_VEC, var_lf);
			LF <= var_lf;
			WAIT UNTIL RISING_EDGE(CLK);	--ESPERO AL FLANCO DE SUBIDA 
			var_cod_op:= COD_OP;			
			var_cod_func := COD_FUNC;
			var_banderas :=BANDERAS;
			var_clr :=CLR;
			var_LF := LF;
			var_S:=S;
			write(LINEA_RES, var_cod_op, right, 10);
			write(LINEA_RES, var_cod_func, right, 10);
			write(LINEA_RES, var_banderas, right, 10);
			write(LINEA_RES, var_clr, right, 10);
			write(LINEA_RES, var_lf, right, 9);
			CAD := "   ";
			write(LINEA_RES, CAD, right, CADENA'LENGTH+1);
		   write(LINEA_RES, var_S, right, 7);
			CADENA := "    ALT   ";
			write(LINEA_RES, CADENA, right, CAD'LENGTH+1);
			writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo		
			WAIT UNTIL FALLING_EDGE(CLK);	--ESPERO AL FLANCO DE BAJADA
			var_cod_op:= COD_OP;			
			var_cod_func := COD_FUNC;
			var_banderas :=BANDERAS;
			var_clr :=CLR;
			var_LF := LF;
			var_S:=S;
			write(LINEA_RES, var_cod_op, right, 10);
			write(LINEA_RES, var_cod_func, right, 10);
			write(LINEA_RES, var_banderas, right, 10);
			write(LINEA_RES, var_clr, right, 10);
			write(LINEA_RES, var_lf, right, 9);
			CAD := "   ";
			write(LINEA_RES, CAD, right, CADENA'LENGTH+1);
			write(LINEA_RES, var_S, right, 8);
			CADENA := "   BAJO   ";
			write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
			writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo
			writeline(ARCH_RES, LINEA_RES);
		  end loop;
		 clr <= '0';
		wait for 4 ns;
		FOR I IN 0 TO 50 LOOP
			WAIT UNTIL RISING_EDGE(CLK);	--ESPERO AL FLANCO DE SUBIDA 
			readline(ARCH_VEC,LINEA_VEC); 
			read(LINEA_VEC, var_cod_op);
			COD_OP <= var_cod_op;
			read(LINEA_VEC, var_cod_func);
			COD_FUNC <= var_cod_func;
			read(LINEA_VEC, var_banderas);
			BANDERAS <= var_banderas;
			read(LINEA_VEC, var_clr);
			CLR <= var_clr;
			read(LINEA_VEC, var_lf);
			LF <= var_lf;
			var_cod_op:= COD_OP;			
			var_cod_func := COD_FUNC;
			var_banderas :=BANDERAS;
			var_clr :=CLR;
			var_LF := LF;
			var_S:=S;
			
			write(LINEA_RES, var_cod_op, right, 10);
			write(LINEA_RES, var_cod_func, right, 10);
			write(LINEA_RES, var_banderas, right, 10);
			write(LINEA_RES, var_clr, right, 10);
			write(LINEA_RES, var_lf, right, 9);
			CAD := "   ";
			write(LINEA_RES, CAD, right, CADENA'LENGTH+1);
		   write(LINEA_RES, var_S, right, 7);
			CADENA := "    ALT   ";
			write(LINEA_RES, CADENA, right, CAD'LENGTH+1);
			writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo		
			WAIT UNTIL FALLING_EDGE(CLK);	--ESPERO AL FLANCO DE BAJADA
			var_cod_op:= COD_OP;			
			var_cod_func := COD_FUNC;
			var_banderas :=BANDERAS;
			var_clr :=CLR;
			var_LF := LF;
			var_S:=S;
			write(LINEA_RES, var_cod_op, right, 10);
			write(LINEA_RES, var_cod_func, right, 10);
			write(LINEA_RES, var_banderas, right, 10);
			write(LINEA_RES, var_clr, right, 10);
			write(LINEA_RES, var_lf, right, 9);
			CAD := "   ";
			write(LINEA_RES, CAD, right, CADENA'LENGTH+1);
			write(LINEA_RES, var_S, right, 8);
			CADENA := "   BAJO   ";
			write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
			writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo
			writeline(ARCH_RES, LINEA_RES);
		  end loop;
		file_close(ARCH_VEC);  -- cierra el archivo
		file_close(ARCH_RES);  -- cierra el archivo   end process;
		  wait;
   end process;
END;