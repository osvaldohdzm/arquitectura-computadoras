/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/Emilio/Documents/ISE Projects/MemoriaDatos/PracticaTest3.vhd";
extern char *STD_TEXTIO;
extern char *IEEE_P_3564397177;
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
void ieee_p_3564397177_sub_1496949865_91900896(char *, char *, char *, unsigned char , unsigned char , int );
void ieee_p_3564397177_sub_2743816878_91900896(char *, char *, char *, char *);
void ieee_p_3564397177_sub_3205433008_91900896(char *, char *, char *, char *, char *, unsigned char , int );
void ieee_p_3564397177_sub_3988856810_91900896(char *, char *, char *, char *, char *);


static void work_a_0664804359_2372691052_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    int64 t7;
    int64 t8;

LAB0:    t1 = (t0 + 4456U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(58, ng0);
    t2 = (t0 + 5104);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(59, ng0);
    t2 = (t0 + 2208U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 4264);
    xsi_process_wait(t2, t8);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(60, ng0);
    t2 = (t0 + 5104);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(61, ng0);
    t2 = (t0 + 2208U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 4264);
    xsi_process_wait(t2, t8);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    goto LAB2;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

}

static void work_a_0664804359_2372691052_p_1(char *t0)
{
    char t5[16];
    char t10[8];
    char t11[8];
    char t12[8];
    char t13[8];
    char t21[16];
    char t25[16];
    char t26[8];
    char t27[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    int t8;
    unsigned int t9;
    int64 t14;
    int t15;
    unsigned int t16;
    unsigned int t17;
    char *t18;
    char *t19;
    unsigned char t20;
    int t22;
    char *t23;
    char *t24;
    int64 t28;

LAB0:    t1 = (t0 + 4704U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(83, ng0);
    t2 = (t0 + 3016U);
    t3 = (t0 + 9067);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 22;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (22 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)1);
    xsi_set_current_line(84, ng0);
    t2 = (t0 + 3120U);
    t3 = (t0 + 9089);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 23;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (23 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)0);
    xsi_set_current_line(86, ng0);
    t2 = (t0 + 9112);
    t4 = (t0 + 3872U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(87, ng0);
    t2 = (t0 + 4512);
    t3 = (t0 + 3296U);
    t4 = (t0 + 3872U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t10, t7, 5U);
    t6 = (t0 + 8808U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t10, t6, (unsigned char)0, t8);
    xsi_set_current_line(88, ng0);
    t2 = (t0 + 9117);
    t4 = (t0 + 3728U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 6U);
    xsi_set_current_line(89, ng0);
    t2 = (t0 + 4512);
    t3 = (t0 + 3296U);
    t4 = (t0 + 3728U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t11, t7, 6U);
    t6 = (t0 + 8792U);
    t8 = (3U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t11, t6, (unsigned char)0, t8);
    xsi_set_current_line(90, ng0);
    t2 = (t0 + 9123);
    t4 = (t0 + 3584U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 3U);
    xsi_set_current_line(91, ng0);
    t2 = (t0 + 4512);
    t3 = (t0 + 3296U);
    t4 = (t0 + 3584U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t12, t7, 3U);
    t6 = (t0 + 8776U);
    t8 = (6U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t12, t6, (unsigned char)0, t8);
    xsi_set_current_line(92, ng0);
    t2 = (t0 + 9126);
    t4 = (t0 + 3728U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 6U);
    xsi_set_current_line(93, ng0);
    t2 = (t0 + 4512);
    t3 = (t0 + 3296U);
    t4 = (t0 + 3728U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t13, t7, 6U);
    t6 = (t0 + 8792U);
    t8 = (3U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t13, t6, (unsigned char)0, t8);
    xsi_set_current_line(94, ng0);
    t2 = (t0 + 4512);
    t3 = (t0 + 3016U);
    t4 = (t0 + 3296U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    xsi_set_current_line(95, ng0);
    t14 = (100 * 1000LL);
    t2 = (t0 + 4512);
    xsi_process_wait(t2, t14);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(97, ng0);
    t2 = (t0 + 9132);
    *((int *)t2) = 0;
    t3 = (t0 + 9136);
    *((int *)t3) = 5;
    t8 = 0;
    t15 = 5;

LAB8:    if (t8 <= t15)
        goto LAB9;

LAB11:    xsi_set_current_line(122, ng0);
    t2 = (t0 + 3120U);
    std_textio_file_close(t2);
    xsi_set_current_line(123, ng0);
    t2 = (t0 + 3016U);
    std_textio_file_close(t2);
    xsi_set_current_line(125, ng0);
    t2 = (t0 + 2208U);
    t3 = *((char **)t2);
    t14 = *((int64 *)t3);
    t28 = (t14 * 10);
    t2 = (t0 + 4512);
    xsi_process_wait(t2, t28);

LAB19:    *((char **)t1) = &&LAB20;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB9:    xsi_set_current_line(98, ng0);
    t4 = (t0 + 4512);
    t6 = (t0 + 3120U);
    t7 = (t0 + 3368U);
    std_textio_readline(STD_TEXTIO, t4, t6, t7);
    xsi_set_current_line(100, ng0);
    t2 = (t0 + 4512);
    t3 = (t0 + 3368U);
    t4 = (t0 + 2328U);
    t6 = *((char **)t4);
    t4 = (t0 + 8728U);
    ieee_p_3564397177_sub_3988856810_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(101, ng0);
    t2 = (t0 + 2328U);
    t3 = *((char **)t2);
    t9 = (11 - 8);
    t16 = (t9 * 1U);
    t17 = (0 + t16);
    t2 = (t3 + t17);
    t4 = (t0 + 5168);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t18 = (t7 + 56U);
    t19 = *((char **)t18);
    memcpy(t19, t2, 9U);
    xsi_driver_first_trans_fast(t4);
    xsi_set_current_line(103, ng0);
    t2 = (t0 + 4512);
    t3 = (t0 + 3368U);
    t4 = (t0 + 2448U);
    t6 = *((char **)t4);
    t4 = (t0 + 8744U);
    ieee_p_3564397177_sub_3988856810_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(104, ng0);
    t2 = (t0 + 2448U);
    t3 = *((char **)t2);
    t2 = (t0 + 5232);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t18 = *((char **)t7);
    memcpy(t18, t3, 8U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(106, ng0);
    t2 = (t0 + 4512);
    t3 = (t0 + 3368U);
    t4 = (t0 + 2688U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_2743816878_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(107, ng0);
    t2 = (t0 + 2688U);
    t3 = *((char **)t2);
    t20 = *((unsigned char *)t3);
    t2 = (t0 + 5296);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t18 = *((char **)t7);
    *((unsigned char *)t18) = t20;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(109, ng0);

LAB14:    t2 = (t0 + 5024);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB15;
    goto LAB1;

LAB10:    t2 = (t0 + 9132);
    t8 = *((int *)t2);
    t3 = (t0 + 9136);
    t15 = *((int *)t3);
    if (t8 == t15)
        goto LAB11;

LAB16:    t22 = (t8 + 1);
    t8 = t22;
    t4 = (t0 + 9132);
    *((int *)t4) = t8;
    goto LAB8;

LAB12:    t4 = (t0 + 5024);
    *((int *)t4) = 0;
    xsi_set_current_line(111, ng0);
    t2 = (t0 + 1672U);
    t3 = *((char **)t2);
    t2 = (t0 + 2568U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 8U);
    xsi_set_current_line(112, ng0);
    t2 = (t0 + 9140);
    t4 = (t0 + 1032U);
    t6 = *((char **)t4);
    t7 = ((IEEE_P_2592010699) + 4024);
    t18 = (t21 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = 0;
    t19 = (t18 + 4U);
    *((int *)t19) = 2;
    t19 = (t18 + 8U);
    *((int *)t19) = 1;
    t22 = (2 - 0);
    t9 = (t22 * 1);
    t9 = (t9 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t9;
    t19 = (t0 + 8680U);
    t4 = xsi_base_array_concat(t4, t5, t7, (char)97, t2, t21, (char)97, t6, t19, (char)101);
    t23 = (t0 + 2328U);
    t24 = *((char **)t23);
    t23 = (t24 + 0);
    t9 = (3U + 9U);
    memcpy(t23, t4, t9);
    xsi_set_current_line(114, ng0);
    t2 = (t0 + 4512);
    t3 = (t0 + 3296U);
    t4 = (t0 + 2328U);
    t6 = *((char **)t4);
    memcpy(t25, t6, 12U);
    t4 = (t0 + 8728U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t25, t4, (unsigned char)0, 5);
    xsi_set_current_line(115, ng0);
    t2 = (t0 + 4512);
    t3 = (t0 + 3296U);
    t4 = (t0 + 2448U);
    t6 = *((char **)t4);
    memcpy(t26, t6, 8U);
    t4 = (t0 + 8744U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t26, t4, (unsigned char)0, 5);
    xsi_set_current_line(116, ng0);
    t2 = (t0 + 4512);
    t3 = (t0 + 3296U);
    t4 = (t0 + 2688U);
    t6 = *((char **)t4);
    t20 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t20, (unsigned char)0, 5);
    xsi_set_current_line(117, ng0);
    t2 = (t0 + 4512);
    t3 = (t0 + 3296U);
    t4 = (t0 + 2568U);
    t6 = *((char **)t4);
    memcpy(t27, t6, 8U);
    t4 = (t0 + 8760U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t27, t4, (unsigned char)0, 5);
    xsi_set_current_line(118, ng0);
    t2 = (t0 + 4512);
    t3 = (t0 + 3016U);
    t4 = (t0 + 3296U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    goto LAB10;

LAB13:    t3 = (t0 + 1472U);
    t20 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t3, 0U, 0U);
    if (t20 == 1)
        goto LAB12;
    else
        goto LAB14;

LAB15:    goto LAB13;

LAB17:    xsi_set_current_line(127, ng0);

LAB23:    *((char **)t1) = &&LAB24;
    goto LAB1;

LAB18:    goto LAB17;

LAB20:    goto LAB18;

LAB21:    goto LAB2;

LAB22:    goto LAB21;

LAB24:    goto LAB22;

}


extern void work_a_0664804359_2372691052_init()
{
	static char *pe[] = {(void *)work_a_0664804359_2372691052_p_0,(void *)work_a_0664804359_2372691052_p_1};
	xsi_register_didat("work_a_0664804359_2372691052", "isim/PracticaTest3_isim_beh.exe.sim/work/a_0664804359_2372691052.didat");
	xsi_register_executes(pe);
}
