/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/Edgar Roa/Documents/8vo sem/Arquitectura/Burbuja 2/ESCOMips/ArchivoDeRegistros.vhd";
extern char *IEEE_P_3620187407;
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
int ieee_p_3620187407_sub_514432868_3965413181(char *, char *, char *);


static void work_a_2140513534_1756004201_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned char t5;
    int t6;
    int t7;
    int t8;
    int t9;
    char *t10;
    char *t11;
    int t12;
    int t13;
    char *t14;
    char *t15;
    int t16;
    int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    char *t21;
    unsigned char t22;
    unsigned char t23;
    char *t24;
    char *t25;
    char *t26;
    int t27;
    int t28;
    unsigned int t29;
    unsigned int t30;
    unsigned int t31;
    int t32;
    int t33;
    int t34;

LAB0:    xsi_set_current_line(84, ng0);
    t1 = (t0 + 3432U);
    t2 = *((char **)t1);
    t1 = (t0 + 3968U);
    t3 = *((char **)t1);
    t1 = (t3 + 0);
    memcpy(t1, t2, 16U);
    xsi_set_current_line(86, ng0);
    t1 = (t0 + 2152U);
    t2 = *((char **)t1);
    t4 = *((unsigned char *)t2);
    t5 = (t4 == (unsigned char)3);
    if (t5 != 0)
        goto LAB2;

LAB4:    xsi_set_current_line(103, ng0);
    t6 = (4 - 1);
    t1 = (t0 + 12049);
    *((int *)t1) = 0;
    t2 = (t0 + 12053);
    *((int *)t2) = t6;
    t7 = 0;
    t8 = t6;

LAB21:    if (t7 <= t8)
        goto LAB22;

LAB24:
LAB3:    xsi_set_current_line(118, ng0);
    t1 = (t0 + 3968U);
    t2 = *((char **)t1);
    t1 = (t0 + 6672);
    t3 = (t1 + 56U);
    t10 = *((char **)t3);
    t11 = (t10 + 56U);
    t14 = *((char **)t11);
    memcpy(t14, t2, 16U);
    xsi_driver_first_trans_fast(t1);
    t1 = (t0 + 6512);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(88, ng0);
    t6 = (4 - 1);
    t1 = (t0 + 12033);
    *((int *)t1) = 0;
    t3 = (t0 + 12037);
    *((int *)t3) = t6;
    t7 = 0;
    t8 = t6;

LAB5:    if (t7 <= t8)
        goto LAB6;

LAB8:    goto LAB3;

LAB6:    xsi_set_current_line(89, ng0);
    t9 = (16 - 1);
    t10 = (t0 + 12041);
    *((int *)t10) = t9;
    t11 = (t0 + 12045);
    *((int *)t11) = 0;
    t12 = t9;
    t13 = 0;

LAB9:    if (t12 >= t13)
        goto LAB10;

LAB12:
LAB7:    t1 = (t0 + 12033);
    t7 = *((int *)t1);
    t2 = (t0 + 12037);
    t8 = *((int *)t2);
    if (t7 == t8)
        goto LAB8;

LAB20:    t6 = (t7 + 1);
    t7 = t6;
    t3 = (t0 + 12033);
    *((int *)t3) = t7;
    goto LAB5;

LAB10:    xsi_set_current_line(90, ng0);
    t14 = (t0 + 1672U);
    t15 = *((char **)t14);
    t14 = (t0 + 12033);
    t16 = *((int *)t14);
    t17 = (t16 - 3);
    t18 = (t17 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t14));
    t19 = (1U * t18);
    t20 = (0 + t19);
    t21 = (t15 + t20);
    t22 = *((unsigned char *)t21);
    t23 = (t22 == (unsigned char)2);
    if (t23 != 0)
        goto LAB13;

LAB15:    xsi_set_current_line(93, ng0);
    t1 = (t0 + 12041);
    t2 = (t0 + 12033);
    t6 = xsi_vhdl_pow(2, *((int *)t2));
    t9 = *((int *)t1);
    t16 = (t9 - t6);
    t4 = (t16 < 0);
    if (t4 != 0)
        goto LAB16;

LAB18:    xsi_set_current_line(96, ng0);
    t1 = (t0 + 3968U);
    t2 = *((char **)t1);
    t1 = (t0 + 12041);
    t3 = (t0 + 12033);
    t6 = xsi_vhdl_pow(2, *((int *)t3));
    t9 = *((int *)t1);
    t16 = (t9 - t6);
    t17 = (t16 - 15);
    t18 = (t17 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, t16);
    t19 = (1U * t18);
    t20 = (0 + t19);
    t10 = (t2 + t20);
    t4 = *((unsigned char *)t10);
    t11 = (t0 + 3968U);
    t14 = *((char **)t11);
    t11 = (t0 + 12041);
    t27 = *((int *)t11);
    t28 = (t27 - 15);
    t29 = (t28 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t11));
    t30 = (1U * t29);
    t31 = (0 + t30);
    t15 = (t14 + t31);
    *((unsigned char *)t15) = t4;

LAB17:
LAB14:
LAB11:    t1 = (t0 + 12041);
    t12 = *((int *)t1);
    t2 = (t0 + 12045);
    t13 = *((int *)t2);
    if (t12 == t13)
        goto LAB12;

LAB19:    t6 = (t12 + -1);
    t12 = t6;
    t3 = (t0 + 12041);
    *((int *)t3) = t12;
    goto LAB9;

LAB13:    xsi_set_current_line(91, ng0);
    t24 = (t0 + 3968U);
    t25 = *((char **)t24);
    t24 = (t0 + 3968U);
    t26 = *((char **)t24);
    t24 = (t26 + 0);
    memcpy(t24, t25, 16U);
    goto LAB14;

LAB16:    xsi_set_current_line(94, ng0);
    t3 = (t0 + 3968U);
    t10 = *((char **)t3);
    t3 = (t0 + 12041);
    t17 = *((int *)t3);
    t27 = (t17 - 15);
    t18 = (t27 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t3));
    t19 = (1U * t18);
    t20 = (0 + t19);
    t11 = (t10 + t20);
    *((unsigned char *)t11) = (unsigned char)2;
    goto LAB17;

LAB22:    xsi_set_current_line(104, ng0);
    t9 = (16 - 1);
    t3 = (t0 + 12057);
    *((int *)t3) = 0;
    t10 = (t0 + 12061);
    *((int *)t10) = t9;
    t12 = 0;
    t13 = t9;

LAB25:    if (t12 <= t13)
        goto LAB26;

LAB28:
LAB23:    t1 = (t0 + 12049);
    t7 = *((int *)t1);
    t2 = (t0 + 12053);
    t8 = *((int *)t2);
    if (t7 == t8)
        goto LAB24;

LAB36:    t6 = (t7 + 1);
    t7 = t6;
    t3 = (t0 + 12049);
    *((int *)t3) = t7;
    goto LAB21;

LAB26:    xsi_set_current_line(105, ng0);
    t11 = (t0 + 1672U);
    t14 = *((char **)t11);
    t11 = (t0 + 12049);
    t16 = *((int *)t11);
    t17 = (t16 - 3);
    t18 = (t17 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t11));
    t19 = (1U * t18);
    t20 = (0 + t19);
    t15 = (t14 + t20);
    t4 = *((unsigned char *)t15);
    t5 = (t4 == (unsigned char)2);
    if (t5 != 0)
        goto LAB29;

LAB31:    xsi_set_current_line(108, ng0);
    t1 = (t0 + 12057);
    t2 = (t0 + 12049);
    t6 = xsi_vhdl_pow(2, *((int *)t2));
    t9 = *((int *)t1);
    t16 = (t9 + t6);
    t4 = (t16 < 16);
    if (t4 != 0)
        goto LAB32;

LAB34:    xsi_set_current_line(111, ng0);
    t1 = (t0 + 3968U);
    t2 = *((char **)t1);
    t1 = (t0 + 12057);
    t6 = *((int *)t1);
    t9 = (t6 - 15);
    t18 = (t9 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t1));
    t19 = (1U * t18);
    t20 = (0 + t19);
    t3 = (t2 + t20);
    *((unsigned char *)t3) = (unsigned char)2;

LAB33:
LAB30:
LAB27:    t1 = (t0 + 12057);
    t12 = *((int *)t1);
    t2 = (t0 + 12061);
    t13 = *((int *)t2);
    if (t12 == t13)
        goto LAB28;

LAB35:    t6 = (t12 + 1);
    t12 = t6;
    t3 = (t0 + 12057);
    *((int *)t3) = t12;
    goto LAB25;

LAB29:    xsi_set_current_line(106, ng0);
    t21 = (t0 + 3968U);
    t24 = *((char **)t21);
    t21 = (t0 + 3968U);
    t25 = *((char **)t21);
    t21 = (t25 + 0);
    memcpy(t21, t24, 16U);
    goto LAB30;

LAB32:    xsi_set_current_line(109, ng0);
    t3 = (t0 + 3968U);
    t10 = *((char **)t3);
    t3 = (t0 + 12057);
    t11 = (t0 + 12049);
    t17 = xsi_vhdl_pow(2, *((int *)t11));
    t27 = *((int *)t3);
    t28 = (t27 + t17);
    t32 = (t28 - 15);
    t18 = (t32 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, t28);
    t19 = (1U * t18);
    t20 = (0 + t19);
    t14 = (t10 + t20);
    t5 = *((unsigned char *)t14);
    t15 = (t0 + 3968U);
    t21 = *((char **)t15);
    t15 = (t0 + 12057);
    t33 = *((int *)t15);
    t34 = (t33 - 15);
    t29 = (t34 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t15));
    t30 = (1U * t29);
    t31 = (0 + t30);
    t24 = (t21 + t31);
    *((unsigned char *)t24) = t5;
    goto LAB33;

}

static void work_a_2140513534_1756004201_p_1(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    xsi_set_current_line(123, ng0);

LAB3:    t1 = (t0 + 2952U);
    t2 = *((char **)t1);
    t1 = (t0 + 1352U);
    t3 = *((char **)t1);
    t1 = (t0 + 11484U);
    t4 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t3, t1);
    t5 = (t4 - 0);
    t6 = (t5 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t4);
    t7 = (16U * t6);
    t8 = (0 + t7);
    t9 = (t2 + t8);
    t10 = (t0 + 6736);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t9, 16U);
    xsi_driver_first_trans_fast(t10);

LAB2:    t15 = (t0 + 6528);
    *((int *)t15) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_2140513534_1756004201_p_2(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(124, ng0);

LAB3:    t1 = (t0 + 3432U);
    t2 = *((char **)t1);
    t1 = (t0 + 6800);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 16U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 6544);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_2140513534_1756004201_p_3(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    xsi_set_current_line(125, ng0);

LAB3:    t1 = (t0 + 2952U);
    t2 = *((char **)t1);
    t1 = (t0 + 1512U);
    t3 = *((char **)t1);
    t1 = (t0 + 11500U);
    t4 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t3, t1);
    t5 = (t4 - 0);
    t6 = (t5 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t4);
    t7 = (16U * t6);
    t8 = (0 + t7);
    t9 = (t2 + t8);
    t10 = (t0 + 6864);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t9, 16U);
    xsi_driver_first_trans_fast_port(t10);

LAB2:    t15 = (t0 + 6560);
    *((int *)t15) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_2140513534_1756004201_p_4(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    unsigned int t6;
    char *t7;
    unsigned char t8;
    unsigned int t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    int t15;
    int t16;
    unsigned int t17;
    char *t18;

LAB0:    xsi_set_current_line(130, ng0);
    t1 = (t0 + 2472U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    t1 = (t0 + 2272U);
    t3 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t3 != 0)
        goto LAB7;

LAB8:
LAB3:    t1 = (t0 + 6576);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(131, ng0);
    t1 = xsi_get_transient_memory(256U);
    memset(t1, 0, 256U);
    t5 = t1;
    t6 = (16U * 1U);
    t7 = t5;
    memset(t7, (unsigned char)2, t6);
    t8 = (t6 != 0);
    if (t8 == 1)
        goto LAB5;

LAB6:    t10 = (t0 + 6928);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t1, 256U);
    xsi_driver_first_trans_fast(t10);
    goto LAB3;

LAB5:    t9 = (256U / t6);
    xsi_mem_set_data(t5, t5, t6, t9);
    goto LAB6;

LAB7:    xsi_set_current_line(133, ng0);
    t2 = (t0 + 1832U);
    t5 = *((char **)t2);
    t4 = *((unsigned char *)t5);
    t8 = (t4 == (unsigned char)3);
    if (t8 != 0)
        goto LAB9;

LAB11:
LAB10:    goto LAB3;

LAB9:    xsi_set_current_line(134, ng0);
    t2 = (t0 + 3272U);
    t7 = *((char **)t2);
    t2 = (t0 + 1192U);
    t10 = *((char **)t2);
    t2 = (t0 + 11468U);
    t15 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t10, t2);
    t16 = (t15 - 0);
    t6 = (t16 * 1);
    t9 = (16U * t6);
    t17 = (0U + t9);
    t11 = (t0 + 6928);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    t14 = (t13 + 56U);
    t18 = *((char **)t14);
    memcpy(t18, t7, 16U);
    xsi_driver_first_trans_delta(t11, t17, 16U, 0LL);
    goto LAB10;

}

static void work_a_2140513534_1756004201_p_5(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;

LAB0:    xsi_set_current_line(140, ng0);
    t1 = (t0 + 1992U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB3;

LAB4:
LAB5:    t10 = (t0 + 1032U);
    t11 = *((char **)t10);
    t10 = (t0 + 6992);
    t12 = (t10 + 56U);
    t13 = *((char **)t12);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    memcpy(t15, t11, 16U);
    xsi_driver_first_trans_fast(t10);

LAB2:    t16 = (t0 + 6592);
    *((int *)t16) = 1;

LAB1:    return;
LAB3:    t1 = (t0 + 3112U);
    t5 = *((char **)t1);
    t1 = (t0 + 6992);
    t6 = (t1 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t5, 16U);
    xsi_driver_first_trans_fast(t1);
    goto LAB2;

LAB6:    goto LAB2;

}


extern void work_a_2140513534_1756004201_init()
{
	static char *pe[] = {(void *)work_a_2140513534_1756004201_p_0,(void *)work_a_2140513534_1756004201_p_1,(void *)work_a_2140513534_1756004201_p_2,(void *)work_a_2140513534_1756004201_p_3,(void *)work_a_2140513534_1756004201_p_4,(void *)work_a_2140513534_1756004201_p_5};
	xsi_register_didat("work_a_2140513534_1756004201", "isim/Escompis_tb_isim_beh.exe.sim/work/a_2140513534_1756004201.didat");
	xsi_register_executes(pe);
}
