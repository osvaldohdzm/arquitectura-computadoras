
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decoInst is
    Port ( CP_CODE : in  STD_LOGIC_VECTOR (4 downto 0);
           tipoR,BEQ,BNEQ,BLT,BLE,BGT,BGET: OUT std_logic);
end decoInst;

architecture Behavioral of decoInst is

constant zero : std_logic_vector (4 downto 0) := "00000";
constant dreizenh : std_logic_vector (4 downto 0) := "01101";
constant vierzenh : std_logic_vector (4 downto 0) := "01110";
constant funfzenh : std_logic_vector (4 downto 0) := "01111";
constant sechzenh : std_logic_vector (4 downto 0) := "10000";
constant siebzenh : std_logic_vector (4 downto 0) := "10001";
constant achtzenh : std_logic_vector (4 downto 0) := "10010";

begin

tipoR <= '1' when CP_CODE = zero else
         '0' when CP_CODE = dreizenh else 
			'0' when CP_CODE = vierzenh else
			'0' when CP_CODE = funfzenh else
			'0' when CP_CODE = sechzenh else
			'0' when CP_CODE = siebzenh else
			'0' when CP_CODE = achtzenh else
			'0';
			
			
BEQ <=   '0' when CP_CODE = zero else
         '1' when CP_CODE = dreizenh else 
			'0' when CP_CODE = vierzenh else
			'0' when CP_CODE = funfzenh else
			'0' when CP_CODE = sechzenh else
			'0' when CP_CODE = siebzenh else
			'0' when CP_CODE = achtzenh else
			'0';
			
BNEQ <=  '0' when CP_CODE = zero else
         '0' when CP_CODE = dreizenh else 
			'1' when CP_CODE = vierzenh else
			'0' when CP_CODE = funfzenh else
			'0' when CP_CODE = sechzenh else
			'0' when CP_CODE = siebzenh else
			'0' when CP_CODE = achtzenh else
			'0';
			
BLT <=   '0' when CP_CODE = zero else
         '0' when CP_CODE = dreizenh else 
			'0' when CP_CODE = vierzenh else
			'1' when CP_CODE = funfzenh else
			'0' when CP_CODE = sechzenh else
			'0' when CP_CODE = siebzenh else
			'0' when CP_CODE = achtzenh else
			'0';
			
BLE <=   '0' when CP_CODE = zero else
         '0' when CP_CODE = dreizenh else 
			'0' when CP_CODE = vierzenh else
			'0' when CP_CODE = funfzenh else
			'1' when CP_CODE = sechzenh else
			'0' when CP_CODE = siebzenh else
			'0' when CP_CODE = achtzenh else
			'0';


BGT <=   '0' when CP_CODE = zero else
         '0' when CP_CODE = dreizenh else 
			'0' when CP_CODE = vierzenh else
			'0' when CP_CODE = funfzenh else
			'0' when CP_CODE = sechzenh else
			'1' when CP_CODE = siebzenh else
			'0' when CP_CODE = achtzenh else
			'0';
			
			
BGET <=  '0' when CP_CODE = zero else
         '0' when CP_CODE = dreizenh else 
			'0' when CP_CODE = vierzenh else
			'0' when CP_CODE = funfzenh else
			'0' when CP_CODE = sechzenh else
			'0' when CP_CODE = siebzenh else
			'1' when CP_CODE = achtzenh else
			'0';
			  
end Behavioral;

