LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY extensorSigno_TestBench IS
END extensorSigno_TestBench;
 
ARCHITECTURE behavior OF extensorSigno_TestBench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT extensorSigno
    PORT(
         d_in : IN  std_logic_vector(11 downto 0);
         d_out : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal d_in : std_logic_vector(11 downto 0) := (others => '0');

 	--Outputs
   signal d_out : std_logic_vector(15 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: extensorSigno PORT MAP (
          d_in => d_in,
          d_out => d_out
        );

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;
		d_in <= "000000000001";
		
		wait for 100 ns;
		d_in <= "100000000001";

      wait;
   end process;

END;
