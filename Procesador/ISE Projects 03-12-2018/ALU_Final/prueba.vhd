library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
 
ENTITY prueba IS
END prueba;
 
ARCHITECTURE behavior OF prueba IS 
 
    COMPONENT ALU
    PORT(
         a : IN  STD_LOGIC_VECTOR(3 downto 0);
         b : IN  STD_LOGIC_VECTOR(3 downto 0);
         sel_a : IN  STD_LOGIC;
         sel_b : IN  STD_LOGIC;
         op : IN  STD_LOGIC_VECTOR(1 downto 0);
         res : OUT  STD_LOGIC_VECTOR(3 downto 0);
         c : OUT  STD_LOGIC;
         z : OUT  STD_LOGIC;
         n : OUT  STD_LOGIC;
         Ov : OUT  STD_LOGIC
        );
    END COMPONENT;
    
   signal a : STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
   signal b : STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
   signal sel_a : STD_LOGIC := '0';
   signal sel_b : STD_LOGIC := '0';
   signal op : STD_LOGIC_VECTOR(1 downto 0) := (others => '0');
	
   signal res : STD_LOGIC_VECTOR(3 downto 0);
   signal c : STD_LOGIC;
   signal z : STD_LOGIC;
   signal n : STD_LOGIC;
   signal Ov : STD_LOGIC;

begin
 
   uut: ALU PORT MAP (
          a => a,
          b => b,
          sel_a => sel_a,
          sel_b => sel_b,
          op => op,
          res => res,
          c => c,
          z => z,
          n => n,
          Ov => Ov
        );

   stim_proc: process
   begin		
		a <= "0101";
		b <= "1110";
		sel_a <= '0';
		sel_b <= '0';
		op(1) <= '1';
		op(0) <= '1';
		wait for 100 ns;
		
		a <= "0101";
		b <= "1110";
		sel_a <= '0';
		sel_b <= '1';
		op(1) <= '1';
		op(0) <= '1';
		wait for 100 ns;
		
		a <= "0101";
		b <= "1110";
		sel_a <= '0';
		sel_b <= '0';
		op(1) <= '0';
		op(0) <= '0';
		wait for 100 ns;
		
		a <= "0101";
		b <= "1110";
		sel_a <= '1';
		sel_b <= '1';
		op(1) <= '0';
		op(0) <= '1';
		wait for 100 ns;
		
		a <= "0101";
		b <= "1110";
		sel_a <= '0';
		sel_b <= '0';
		op(1) <= '0';
		op(0) <= '1';
		wait for 100 ns;
		
		
		a <= "0101";
		b <= "1110";
		sel_a <= '1';
		sel_b <= '1';
		op(1) <= '0';
		op(0) <= '0';
		wait for 100 ns;
		
		a <= "0101";
		b <= "1110";
		sel_a <= '0';
		sel_b <= '0';
		op(1) <= '1';
		op(0) <= '0';
		wait for 100 ns;
		
		a <= "0101";
		b <= "1110";
		sel_a <= '1';
		sel_b <= '0';
		op(1) <= '1';
		op(0) <= '0';
		wait for 100 ns;
		
		a <= "0101";
		b <= "0111";
		sel_a <= '0';
		sel_b <= '0';
		op(1) <= '1';
		op(0) <= '1';
		wait for 100 ns;
		
		a <= "0101";
		b <= "0101";
		sel_a <= '0';
		sel_b <= '1';
		op(1) <= '1';
		op(0) <= '1';
		wait for 100 ns;
		
		a <= "0101";
		b <= "0101";
		sel_a <= '1';
		sel_b <= '1';
		op(1) <= '0';
		op(0) <= '1';
      wait;
		
   end process;

END;
