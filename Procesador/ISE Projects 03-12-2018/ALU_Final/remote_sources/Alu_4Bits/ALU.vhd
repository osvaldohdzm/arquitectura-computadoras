library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ALU is
    Port ( a: in STD_LOGIC_VECTOR (3 downto 0);
			b: in STD_LOGIC_VECTOR (3 downto 0);
			sel_a: in STD_LOGIC;
			sel_b: in STD_LOGIC;
			op: in STD_LOGIC_VECTOR (1 downto 0);
			res: out STD_LOGIC_VECTOR (3 downto 0);
			c: out STD_LOGIC;
			z: out STD_LOGIC;
			n: out STD_LOGIC;
			Ov: out STD_LOGIC);
end ALU;

architecture Behavioral of ALU is
signal aux_a, aux_b, aux_res: STD_LOGIC_VECTOR (3 downto 0);
signal ci: STD_LOGIC_VECTOR(4 downto 0);
begin
	
	ciclo:for i in 0 to 3 generate
		aux_a(i) <= a(i) xor sel_a;
		aux_b(i) <= b(i) xor sel_b;
	end generate;
	
	process(aux_a, aux_b,aux_res, op, ci, sel_b, b)
		variable sumaresta: STD_LOGIC;
	begin
	ci <= "00000";
		case (op) is
			when "00"=> 
				aux_res <= aux_a and aux_b;
			when "01"=>
				aux_res <= aux_a or aux_b;
			when "10"=>
				aux_res<= aux_a xor aux_b;
			when others=>
				ci(0)<= sel_b;
				for i in 0 to 3 loop
					sumaresta:=ci(0) xor b(i);
					aux_res(i)<= aux_a(i) xor sumaresta xor ci(i);
					ci(i+1) <= (aux_a(i) and ci(i)) or (sumaresta and ci(i)) or (aux_a(i) and sumaresta);
				end loop;
			end case;
			res<= aux_res;
			
			n <=aux_res(3);
			c <=ci(4);
			ov <= ci(4) xor ci(3);
			z <= not(aux_res(3) or aux_res(2) or aux_res(1) or aux_res(0));
		end process;

end Behavioral;

