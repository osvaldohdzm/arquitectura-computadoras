
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity Memoria_datos is
GENERIC(NUM_DIR: INTEGER:=10;
			TAM_PALABRA: INTEGER:=16);

    Port (
			  wd,clk: in std_logic;
			  d_in : in  STD_LOGIC_VECTOR (TAM_PALABRA-1 downto 0);
           dir : in  STD_LOGIC_VECTOR (NUM_DIR-1 downto 0);
           d_out : out  STD_LOGIC_VECTOR (TAM_PALABRA-1 downto 0));
end Memoria_datos;

architecture Behavioral of Memoria_datos is
TYPE arre is array (0 TO 1023) of std_logic_vector(TAM_PALABRA-1 DOWNTO 0);
signal RAM: arre; 
begin
RAME : Process(clk)
begin
	if(rising_edge(clk))then
		if(wd = '1') then
			RAM(conv_integer(dir)) <= d_in;
		end if;
	end if;
end process RAME;
d_out <= RAM(conv_integer(dir));
end Behavioral;

