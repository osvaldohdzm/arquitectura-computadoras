
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;


entity Memoria_programa is
GENERIC(N_PALABRAS: INTEGER:=12);
    Port ( PC : in  STD_LOGIC_VECTOR (N_PALABRAS-1 downto 0);
           Inst : out  STD_LOGIC_VECTOR (24 downto 0));
end Memoria_programa;

architecture Behavioral of Memoria_programa is
TYPE arre is array (0 to  ((2**(N_PALABRAS))-1)) of std_logic_vector(24 downto 0);
--OP 5 BITS 
constant LI : STD_LOGIC_VECTOR(4 downto 0) := "00001"; --R
constant LWI : STD_LOGIC_VECTOR(4 downto 0) :="00010";--R
constant LW : STD_LOGIC_VECTOR(4 downto 0) :="10111";--R
constant SWI : STD_LOGIC_VECTOR(4 downto 0) :="00011";--R
constant SW : STD_LOGIC_VECTOR(4 downto 0) :="00100";--R
constant OP_R: STD_LOGIC_VECTOR(4 downto 0) := "00000"; --ADD SUB AND ETC ETC
constant ADDI: STD_LOGIC_VECTOR(4 downto 0):="00101";--R
constant SUBI: STD_LOGIC_VECTOR(4 downto 0):="00110";--R
constant ANDI: STD_LOGIC_VECTOR(4 downto 0):="00111";--R
constant ORI: STD_LOGIC_VECTOR(4 downto 0):="01000";--R
constant XORI: STD_LOGIC_VECTOR(4 downto 0):="01001";--R
constant NANDI: STD_LOGIC_VECTOR(4 downto 0):="01010";--R
constant NORI: STD_LOGIC_VECTOR(4 downto 0):="01011";--R
constant XNORI: STD_LOGIC_VECTOR(4 downto 0):="01100";--R
constant BEQI: STD_LOGIC_VECTOR(4 downto 0):="01101";--R
constant BNEI: STD_LOGIC_VECTOR(4 downto 0):="01110";--R
constant BLTI: STD_LOGIC_VECTOR(4 downto 0):="01111";--R
constant BLETI: STD_LOGIC_VECTOR(4 downto 0):="10000";--R
constant BGTI: STD_LOGIC_VECTOR(4 downto 0):="10001";--R
constant BGETI: STD_LOGIC_VECTOR(4 downto 0):="10010";--R
constant B: STD_LOGIC_VECTOR(4 downto 0):="10011";--R
constant CALL: STD_LOGIC_VECTOR(4 downto 0):="10100";--R
constant RET: STD_LOGIC_VECTOR(4 downto 0):="10101";--R
constant NOP: STD_LOGIC_VECTOR(4 downto 0):="10110";--R
--REGISTROS 4 BITS
constant R0: STD_LOGIC_VECTOR(3 downto 0):="0000";
constant R1: STD_LOGIC_VECTOR(3 downto 0):="0001";
constant R2: STD_LOGIC_VECTOR(3 downto 0):="0010";
constant R3: STD_LOGIC_VECTOR(3 downto 0):="0011";
constant R4: STD_LOGIC_VECTOR(3 downto 0):="0100";
constant R5: STD_LOGIC_VECTOR(3 downto 0):="0101";
constant R6: STD_LOGIC_VECTOR(3 downto 0):="0110";
constant R7: STD_LOGIC_VECTOR(3 downto 0):="0111";
constant R8: STD_LOGIC_VECTOR(3 downto 0):="1000";
constant R9: STD_LOGIC_VECTOR(3 downto 0):="1001";
constant R10: STD_LOGIC_VECTOR(3 downto 0):="1010";
constant R11: STD_LOGIC_VECTOR(3 downto 0):="1011";
constant R12: STD_LOGIC_VECTOR(3 downto 0):="1100";
constant R13: STD_LOGIC_VECTOR(3 downto 0):="1101";
constant R14: STD_LOGIC_VECTOR(3 downto 0):="1110";
constant R15: STD_LOGIC_VECTOR(3 downto 0):="1111";
--SIN USAR
constant SU: STD_LOGIC_VECTOR(3 downto 0):="0000";
--CODIGO DE FUNCION
constant F_ADD: STD_LOGIC_VECTOR(3 downto 0):="0000";
constant F_SUB: STD_LOGIC_VECTOR(3 downto 0):="0001";
constant F_AND: STD_LOGIC_VECTOR(3 downto 0):="0010";
constant F_OR: STD_LOGIC_VECTOR(3 downto 0):="0011";
constant F_XOR: STD_LOGIC_VECTOR(3 downto 0):="0100";
constant F_NAND: STD_LOGIC_VECTOR(3 downto 0):="0101";
constant F_NOR: STD_LOGIC_VECTOR(3 downto 0):="0110";
constant F_XNOR: STD_LOGIC_VECTOR(3 downto 0):="0111";
constant F_NOT: STD_LOGIC_VECTOR(3 downto 0):="1000";


constant ROM : ARRE :=(LI&R0&"0000000000000101",
							  LI&R1&"0000000000001010",
							  OP_R&R1&R1&R0&SU&F_ADD,
							  SWI&R1&"0000000000000101",
							  B&SU&"0000000000000010",
							  OTHERS=>(OTHERS=>'0'));
begin

INST<= ROM(conv_integer(PC));

end Behavioral;

