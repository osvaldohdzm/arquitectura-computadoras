
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name Alu2 -dir "C:/Users/Caos/Desktop/ESCOM/2017-2/Materias/Arqui/Practicas/Practica3/ALU2/Alu2/planAhead_run_1" -part xc7a100tcsg324-3
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "alu4b.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {ALU.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set_property top alu4b $srcset
add_files [list {alu4b.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc7a100tcsg324-3
