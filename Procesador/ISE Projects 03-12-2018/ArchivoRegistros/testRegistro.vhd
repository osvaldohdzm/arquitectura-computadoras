
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
 
ENTITY testRegistro IS
END testRegistro;
 
ARCHITECTURE behavior OF testRegistro IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT registro
    PORT(
         d : IN  std_logic_vector(15 downto 0);
         clk : IN  std_logic;
         clr : IN  std_logic;
         l : IN  std_logic;
         q : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal d : std_logic_vector(15 downto 0) := (others => '0');
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';
   signal l : std_logic := '0';

 	--Outputs
   signal q : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: registro PORT MAP (
          d => d,
          clk => clk,
          clr => clr,
          l => l,
          q => q
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      clr <= '1';
		wait for 30 ns;
		
		clr <= '0';
		l <= '1';
		d <= "1010101010101010";
		wait for 30 ns;
		
		clr <= '0';
		l <= '0';
		d <= "1111110111011100";
		wait for 30 ns;
		
		clr <= '0';
		l <= '1';
		d <= "1010101010101010";
      wait;
   end process;

END;
