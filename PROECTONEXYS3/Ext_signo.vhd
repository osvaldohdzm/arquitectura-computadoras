
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Ext_signo is
    Port ( din : in  STD_LOGIC_VECTOR (11 downto 0);
           dout : out  STD_LOGIC_VECTOR (15 downto 0));
end Ext_signo;

architecture Behavioral of Ext_signo is
CONSTANT CERO: STD_LOGIC_VECTOR(3 DOWNTO 0):="0000";
CONSTANT UNO : STD_LOGIC_VECTOR(3 DOWNTO 0):="1111";
begin
P: PROCESS(din)
begin
IF(din(11)='1') then
	dout<=UNO&din;
else
	dout<=CERO&din;
end if;
end process P;

end Behavioral;

