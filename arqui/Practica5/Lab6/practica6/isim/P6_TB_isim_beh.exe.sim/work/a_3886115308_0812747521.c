/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/Caos/Desktop/ESCOM/2017-2/Materias/Arqui/Practicas/Practica6/Lab6/practica6/P6_Pila.vhd";
extern char *IEEE_P_2592010699;
extern char *IEEE_P_3620187407;
extern char *IEEE_P_3499444699;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
char *ieee_p_3499444699_sub_2213602152_3536714472(char *, char *, int , int );
char *ieee_p_3620187407_sub_436279890_3965413181(char *, char *, char *, char *, int );


static void work_a_3886115308_0812747521_p_0(char *t0)
{
    char t26[16];
    char t27[16];
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    unsigned int t6;
    char *t7;
    unsigned char t8;
    unsigned int t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    unsigned char t15;
    unsigned char t16;
    unsigned char t17;
    unsigned char t18;
    unsigned char t19;
    unsigned char t20;
    int t21;
    int t22;
    unsigned int t23;
    char *t24;
    char *t25;
    int t28;
    unsigned int t29;
    unsigned int t30;
    int t31;
    int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    char *t36;

LAB0:    xsi_set_current_line(36, ng0);
    t1 = (t0 + 2152U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    t1 = (t0 + 1952U);
    t3 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t3 != 0)
        goto LAB7;

LAB8:
LAB3:    xsi_set_current_line(53, ng0);
    t1 = (t0 + 2312U);
    t2 = *((char **)t1);
    t1 = (t0 + 2848U);
    t5 = *((char **)t1);
    t21 = *((int *)t5);
    t22 = (t21 - 0);
    t6 = (t22 * 1);
    xsi_vhdl_check_range_of_index(0, 7, 1, t21);
    t9 = (11U * t6);
    t23 = (0 + t9);
    t1 = (t2 + t23);
    t7 = (t0 + 4296);
    t10 = (t7 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t1, 11U);
    xsi_driver_first_trans_fast_port(t7);
    xsi_set_current_line(54, ng0);
    t1 = (t0 + 2848U);
    t2 = *((char **)t1);
    t21 = *((int *)t2);
    t1 = ieee_p_3499444699_sub_2213602152_3536714472(IEEE_P_3499444699, t26, t21, 3);
    t5 = (t26 + 12U);
    t6 = *((unsigned int *)t5);
    t6 = (t6 * 1U);
    t3 = (3U != t6);
    if (t3 == 1)
        goto LAB38;

LAB39:    t7 = (t0 + 4360);
    t10 = (t7 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t1, 3U);
    xsi_driver_first_trans_fast_port(t7);
    t1 = (t0 + 4152);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(37, ng0);
    t1 = xsi_get_transient_memory(88U);
    memset(t1, 0, 88U);
    t5 = t1;
    t6 = (11U * 1U);
    t7 = t5;
    memset(t7, (unsigned char)2, t6);
    t8 = (t6 != 0);
    if (t8 == 1)
        goto LAB5;

LAB6:    t10 = (t0 + 4232);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t1, 88U);
    xsi_driver_first_trans_fast(t10);
    xsi_set_current_line(38, ng0);
    t1 = (t0 + 2848U);
    t2 = *((char **)t1);
    t1 = (t2 + 0);
    *((int *)t1) = 0;
    goto LAB3;

LAB5:    t9 = (88U / t6);
    xsi_mem_set_data(t5, t5, t6, t9);
    goto LAB6;

LAB7:    xsi_set_current_line(40, ng0);
    t2 = (t0 + 1512U);
    t5 = *((char **)t2);
    t15 = *((unsigned char *)t5);
    t16 = (t15 == (unsigned char)3);
    if (t16 == 1)
        goto LAB15;

LAB16:    t8 = (unsigned char)0;

LAB17:    if (t8 == 1)
        goto LAB12;

LAB13:    t4 = (unsigned char)0;

LAB14:    if (t4 != 0)
        goto LAB9;

LAB11:    t1 = (t0 + 1512U);
    t2 = *((char **)t1);
    t8 = *((unsigned char *)t2);
    t15 = (t8 == (unsigned char)3);
    if (t15 == 1)
        goto LAB23;

LAB24:    t4 = (unsigned char)0;

LAB25:    if (t4 == 1)
        goto LAB20;

LAB21:    t3 = (unsigned char)0;

LAB22:    if (t3 != 0)
        goto LAB18;

LAB19:    t1 = (t0 + 1512U);
    t2 = *((char **)t1);
    t8 = *((unsigned char *)t2);
    t15 = (t8 == (unsigned char)2);
    if (t15 == 1)
        goto LAB31;

LAB32:    t4 = (unsigned char)0;

LAB33:    if (t4 == 1)
        goto LAB28;

LAB29:    t3 = (unsigned char)0;

LAB30:    if (t3 != 0)
        goto LAB26;

LAB27:    xsi_set_current_line(49, ng0);
    t1 = (t0 + 2312U);
    t2 = *((char **)t1);
    t1 = (t0 + 2848U);
    t5 = *((char **)t1);
    t21 = *((int *)t5);
    t22 = (t21 - 0);
    t6 = (t22 * 1);
    xsi_vhdl_check_range_of_index(0, 7, 1, t21);
    t9 = (11U * t6);
    t23 = (0 + t9);
    t1 = (t2 + t23);
    t7 = (t27 + 0U);
    t10 = (t7 + 0U);
    *((int *)t10) = 10;
    t10 = (t7 + 4U);
    *((int *)t10) = 0;
    t10 = (t7 + 8U);
    *((int *)t10) = -1;
    t28 = (0 - 10);
    t29 = (t28 * -1);
    t29 = (t29 + 1);
    t10 = (t7 + 12U);
    *((unsigned int *)t10) = t29;
    t10 = ieee_p_3620187407_sub_436279890_3965413181(IEEE_P_3620187407, t26, t1, t27, 1);
    t11 = (t26 + 12U);
    t29 = *((unsigned int *)t11);
    t30 = (1U * t29);
    t3 = (11U != t30);
    if (t3 == 1)
        goto LAB36;

LAB37:    t12 = (t0 + 2848U);
    t13 = *((char **)t12);
    t31 = *((int *)t13);
    t32 = (t31 - 0);
    t33 = (t32 * 1);
    t34 = (11U * t33);
    t35 = (0U + t34);
    t12 = (t0 + 4232);
    t14 = (t12 + 56U);
    t24 = *((char **)t14);
    t25 = (t24 + 56U);
    t36 = *((char **)t25);
    memcpy(t36, t10, 11U);
    xsi_driver_first_trans_delta(t12, t35, 11U, 0LL);

LAB10:    goto LAB3;

LAB9:    xsi_set_current_line(41, ng0);
    t2 = (t0 + 1032U);
    t11 = *((char **)t2);
    t2 = (t0 + 2848U);
    t12 = *((char **)t2);
    t21 = *((int *)t12);
    t22 = (t21 - 0);
    t6 = (t22 * 1);
    t9 = (11U * t6);
    t23 = (0U + t9);
    t2 = (t0 + 4232);
    t13 = (t2 + 56U);
    t14 = *((char **)t13);
    t24 = (t14 + 56U);
    t25 = *((char **)t24);
    memcpy(t25, t11, 11U);
    xsi_driver_first_trans_delta(t2, t23, 11U, 0LL);
    goto LAB10;

LAB12:    t2 = (t0 + 1832U);
    t10 = *((char **)t2);
    t19 = *((unsigned char *)t10);
    t20 = (t19 == (unsigned char)2);
    t4 = t20;
    goto LAB14;

LAB15:    t2 = (t0 + 1672U);
    t7 = *((char **)t2);
    t17 = *((unsigned char *)t7);
    t18 = (t17 == (unsigned char)2);
    t8 = t18;
    goto LAB17;

LAB18:    xsi_set_current_line(43, ng0);
    t1 = (t0 + 2848U);
    t10 = *((char **)t1);
    t21 = *((int *)t10);
    t22 = (t21 + 1);
    t1 = (t0 + 2848U);
    t11 = *((char **)t1);
    t1 = (t11 + 0);
    *((int *)t1) = t22;
    xsi_set_current_line(44, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 2848U);
    t5 = *((char **)t1);
    t21 = *((int *)t5);
    t22 = (t21 - 0);
    t6 = (t22 * 1);
    t9 = (11U * t6);
    t23 = (0U + t9);
    t1 = (t0 + 4232);
    t7 = (t1 + 56U);
    t10 = *((char **)t7);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    memcpy(t12, t2, 11U);
    xsi_driver_first_trans_delta(t1, t23, 11U, 0LL);
    goto LAB10;

LAB20:    t1 = (t0 + 1832U);
    t7 = *((char **)t1);
    t18 = *((unsigned char *)t7);
    t19 = (t18 == (unsigned char)2);
    t3 = t19;
    goto LAB22;

LAB23:    t1 = (t0 + 1672U);
    t5 = *((char **)t1);
    t16 = *((unsigned char *)t5);
    t17 = (t16 == (unsigned char)3);
    t4 = t17;
    goto LAB25;

LAB26:    xsi_set_current_line(46, ng0);
    t1 = (t0 + 2848U);
    t10 = *((char **)t1);
    t21 = *((int *)t10);
    t22 = (t21 - 1);
    t1 = (t0 + 2848U);
    t11 = *((char **)t1);
    t1 = (t11 + 0);
    *((int *)t1) = t22;
    xsi_set_current_line(47, ng0);
    t1 = (t0 + 2312U);
    t2 = *((char **)t1);
    t1 = (t0 + 2848U);
    t5 = *((char **)t1);
    t21 = *((int *)t5);
    t22 = (t21 - 0);
    t6 = (t22 * 1);
    xsi_vhdl_check_range_of_index(0, 7, 1, t21);
    t9 = (11U * t6);
    t23 = (0 + t9);
    t1 = (t2 + t23);
    t7 = (t27 + 0U);
    t10 = (t7 + 0U);
    *((int *)t10) = 10;
    t10 = (t7 + 4U);
    *((int *)t10) = 0;
    t10 = (t7 + 8U);
    *((int *)t10) = -1;
    t28 = (0 - 10);
    t29 = (t28 * -1);
    t29 = (t29 + 1);
    t10 = (t7 + 12U);
    *((unsigned int *)t10) = t29;
    t10 = ieee_p_3620187407_sub_436279890_3965413181(IEEE_P_3620187407, t26, t1, t27, 1);
    t11 = (t26 + 12U);
    t29 = *((unsigned int *)t11);
    t30 = (1U * t29);
    t3 = (11U != t30);
    if (t3 == 1)
        goto LAB34;

LAB35:    t12 = (t0 + 2848U);
    t13 = *((char **)t12);
    t31 = *((int *)t13);
    t32 = (t31 - 0);
    t33 = (t32 * 1);
    t34 = (11U * t33);
    t35 = (0U + t34);
    t12 = (t0 + 4232);
    t14 = (t12 + 56U);
    t24 = *((char **)t14);
    t25 = (t24 + 56U);
    t36 = *((char **)t25);
    memcpy(t36, t10, 11U);
    xsi_driver_first_trans_delta(t12, t35, 11U, 0LL);
    goto LAB10;

LAB28:    t1 = (t0 + 1832U);
    t7 = *((char **)t1);
    t18 = *((unsigned char *)t7);
    t19 = (t18 == (unsigned char)3);
    t3 = t19;
    goto LAB30;

LAB31:    t1 = (t0 + 1672U);
    t5 = *((char **)t1);
    t16 = *((unsigned char *)t5);
    t17 = (t16 == (unsigned char)2);
    t4 = t17;
    goto LAB33;

LAB34:    xsi_size_not_matching(11U, t30, 0);
    goto LAB35;

LAB36:    xsi_size_not_matching(11U, t30, 0);
    goto LAB37;

LAB38:    xsi_size_not_matching(3U, t6, 0);
    goto LAB39;

}


extern void work_a_3886115308_0812747521_init()
{
	static char *pe[] = {(void *)work_a_3886115308_0812747521_p_0};
	xsi_register_didat("work_a_3886115308_0812747521", "isim/P6_TB_isim_beh.exe.sim/work/a_3886115308_0812747521.didat");
	xsi_register_executes(pe);
}
