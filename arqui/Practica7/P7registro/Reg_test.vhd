LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY Reg_test IS
END Reg_test;
 
ARCHITECTURE behavior OF Reg_test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Registro
    PORT(
         DATO : IN  std_logic_vector(8 downto 0);
         LA : IN  std_logic;
         EA : IN  std_logic;
         clk : IN  std_logic;
         clr : IN  std_logic;
         A : OUT  std_logic_vector(8 downto 0);
         Z : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal DATO : std_logic_vector(8 downto 0) := (others => '0');
   signal LA : std_logic := '0';
   signal EA : std_logic := '0';
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';

 	--Outputs
   signal A : std_logic_vector(8 downto 0);
   signal Z : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Registro PORT MAP (
          DATO => DATO,
          LA => LA,
          EA => EA,
          clk => clk,
          clr => clr,
          A => A,
          Z => Z
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin				
      clr <= '1';
		wait for 20 ns;
		clr <= '0';
		wait;
   end process;
	
	escritura: process
   begin
	   DATO <= "001100100";
		LA <= '1';
		EA <= '0';
		wait for 50 ns;
		DATO <= "000000000";
		LA <= '0';
		EA <= '1';
		wait for 20 ns;
		DATO <= "010011001";
		LA <= '0';
		EA <= '1';
		wait for 20 ns;
		DATO <= "011101010";
		LA <= '1';
		EA <= '0';
		wait for 20 ns;
		DATO <= "011100010";
		LA <= '0';
		EA <= '1';
		wait for 20 ns;
	end process;
END;
