
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY testBarrelShifter IS
END testBarrelShifter;
 
ARCHITECTURE behavior OF testBarrelShifter IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 component barrelShifter is
    Port ( shamt : in  STD_LOGIC_VECTOR (3 downto 0);
           dir : in  STD_LOGIC;
           d : in  STD_LOGIC_VECTOR (15 downto 0);
           q : out  STD_LOGIC_VECTOR (15 downto 0));
end component;
    

   --Inputs
   signal shamt : std_logic_vector(3 downto 0) := (others => '0');
   signal dir : std_logic := '0';
   signal d : std_logic_vector(15 downto 0) := (others => '0');

 	--Outputs
   signal q : std_logic_vector(15 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: barrelShifter PORT MAP (
          shamt => shamt,
          dir => dir,
          d => d,
          q => q
        );

   -- Stimulus process
   stim_proc: process
   begin		
	
		dir <= '1';
		shamt <= "1111";
		d <= "1100101011110011";
		wait for 30 ns;
		
      dir <= '0';
		shamt <= "0011";
		d <= "1100101011110011";
      wait;
   end process;

END;
