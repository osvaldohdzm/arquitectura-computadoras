LIBRARY ieee;
LIBRARY std;
use std.textio.all;
USE ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_textio.all;

ENTITY Sim_MemProg IS
END Sim_MemProg;
 
ARCHITECTURE behavior OF Sim_MemProg IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
   	COMPONENT MemProg
	PORT(
		pc : IN std_logic_vector(11 downto 0);          
		inst : OUT std_logic_vector(24 downto 0)
		);
	END COMPONENT;
    

   --Inputs
   signal pc : std_logic_vector(11 downto 0) := (others => '0');

 	--Outputs
   signal inst : std_logic_vector(24 downto 0);
	
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: MemProg PORT MAP (
          pc => pc,
          inst => inst
        );
		  
   -- Stimulus process
   stim_proc: process
   file ARCH_RES : TEXT;
	file ARCH_VEC : TEXT;
	
	variable LINEA_RES : line;
	variable LINEA_VEC : line;
	

	VARIABLE VAR_PC    :  STD_LOGIC_VECTOR(11 DOWNTO 0);
	VARIABLE VAR_INST    :  STD_LOGIC_VECTOR(24 DOWNTO 0);
	
	VARIABLE CADENA : STRING(1 TO 3);
	VARIABLE CADENA_I : STRING(1 TO 6);
	VARIABLE CADENA_X : STRING(1 TO 10);
	VARIABLE CADENA_Y : STRING(1 TO 7);

   begin		
		file_open(ARCH_RES, "./ARCHIVO_SALIDA.TXT", WRITE_MODE); 	
		file_open(ARCH_VEC, "./ARCHIVO_ENTRADA.TXT", READ_MODE); 	

		CADENA_X := "     A    ";
		write(LINEA_RES, CADENA_X, right, CADENA_X'LENGTH+1);	
		CADENA_I := "OPCODE";
		write(LINEA_RES, CADENA_I, right, CADENA_I'LENGTH+1);
		CADENA_Y := " 19..16";
		write(LINEA_RES, CADENA_Y, right, CADENA_Y'LENGTH+1);
		CADENA_Y := " 15..12";
		write(LINEA_RES, CADENA_Y, right, CADENA_Y'LENGTH+1);	
		CADENA_I := " 11..8";
		write(LINEA_RES, CADENA_I, right, CADENA_I'LENGTH+1);	
		CADENA_Y := "   7..4";
		write(LINEA_RES, CADENA_Y, right, CADENA_Y'LENGTH+1);	
		CADENA_Y := "   3..0";
		write(LINEA_RES, CADENA_Y, right, CADENA_Y'LENGTH+1);	
		writeline(ARCH_RES,LINEA_RES);
      wait for 100 ns;	
 
		FOR I IN 1 TO 7 LOOP
		
			readline(ARCH_VEC,LINEA_VEC);
			
			hread(LINEA_VEC, VAR_PC);
			pc<=VAR_PC;
			
			wait for 40 ns;
			
			VAR_INST := inst;
	
			hwrite(LINEA_RES, VAR_PC, 	right, 9);
			write(LINEA_RES, VAR_INST(24 downto 20), 	right, 8);
			write(LINEA_RES, VAR_INST(19 downto 16), 	right, 8);
			write (LINEA_RES, VAR_INST(15 downto 12), right, 8);		
			write (LINEA_RES, VAR_INST(11 downto 8), right, 8);		
			write (LINEA_RES, VAR_INST(7 downto 4), right, 8);		
			write (LINEA_RES, VAR_INST(3 downto 0), right, 8);		
			writeline(ARCH_RES,LINEA_RES);

		end loop;
		
		file_close(ARCH_VEC);
		file_close(ARCH_RES);  

      wait;
   end process;

END;