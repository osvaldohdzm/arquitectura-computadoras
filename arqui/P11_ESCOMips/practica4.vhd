-- ESCOMips | Pr�ctica 4 - Archivo de Registros | Marco Antonio Rubio Cort�s

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity practica4 is
	generic
	(
		wordSize : integer := 16;												-- Tama�o de palabra
		busSize  : integer := 4													-- Tama�o del bus de direcciones
	);
	port
	(
		readRegister1	: in 	std_logic_vector(busSize-1 downto 0);
		readRegister2	: in 	std_logic_vector(busSize-1 downto 0);
		writeRegister	: in 	std_logic_vector(busSize-1 downto 0);
		SHAMT				: in 	std_logic_vector(busSize-1 downto 0);
		writeData		: in 	std_logic_vector(wordSize-1 downto 0);
		readData1		: out	std_logic_vector(wordSize-1 downto 0);
		readData2		: out	std_logic_vector(wordSize-1 downto 0);
		SHE				: in	std_logic;
		DIR				: in	std_logic;
		WR					: in	std_logic;
		clk				: in	std_logic;
		clr				: in	std_logic
	);
end practica4;

architecture A_practica4 of practica4 is

-- Archivo de registros
type archivo is array (0 to ((2**busSize)-1)) of std_logic_vector((wordSize-1) downto 0);
signal fileRegister: archivo;

-- Se�ales para Barrel
signal dataInBarrel	: std_logic_vector(wordSize-1 downto 0);
signal dataOutBarrel	: std_logic_vector(wordSize-1 downto 0);

-- Se�al para escritura al archivo de registros
signal auxWriteData	: std_logic_vector(wordSize-1 downto 0);

-- Se�al para lectura del archivo de registros
signal auxReadData1	: std_logic_vector(wordSize-1 downto 0);


begin
	
	-- Lecturas as�ncronas
	auxReadData1 <= fileRegister(conv_integer(readRegister1));					-- Lee de la localidad dada por readRegister1
	dataInBarrel <= auxReadData1;
	readData1 <= auxReadData1;
	readData2 <= fileRegister(conv_integer(readRegister2));						-- Lee de la localidad dada por readRegister1
	
	-- Mux SHE | 1 => dataOutBarrel | 0 => writeData
	auxWriteData <= writeData when (SHE = '0') else
								dataOutBarrel;
	
	-- Archivo de Registros
	archivoRegistro: process(clr, clk)
	begin
		if(clr = '1') then 																	-- Reset
			for i in 0 to ((2**busSize)-1) loop
				fileRegister(i) <= x"0000";
			end loop;
		elsif(rising_edge(clk)) then
			if(WR = '1') then																	-- Escritura en el archivo de registros
				fileRegister(conv_integer(writeRegister)) <= auxWriteData;		-- Escribe en la localidad dada por writeRegister
			end if;
		end if;
	end process;
	
	-- Barrel Shifter
	barrelShifter: process(SHAMT, dataInBarrel, DIR)	
	
	variable auxShift : std_logic_vector(wordSize-1 downto 0);
	
	begin		
		auxShift := dataInBarrel;
		
		--- DIR para seleccionar la direcci�n del corrimiento
		if (DIR = '1') then																	-- Corrimiento a la Izquierda
			for i in 0 to (busSize-1) loop
				for j in (wordSize-1) downto 0 loop
					if (SHAMT(i) = '0') then
						auxShift := auxShift;
					else
						if ((j-(2**i)) < 0) then
							auxShift(j) := '0';
						else
							auxShift(j) := auxShift(j-(2**i));
						end if;
					end if;
				end loop;
			end loop;
		else																						-- Corrimiento a la Derecha
			for i in 0 to (busSize-1) loop
				for j in 0 to (wordSize-1) loop
					if (SHAMT(i) = '0') then
						auxShift := auxShift;
					else
						if ((j+(2**i)) < wordSize) then
							auxShift(j) := auxShift(j+(2**i));
						else
							auxShift(j) := '0';
						end if;
					end if;
				end loop;
			end loop;
		end if;
		
		dataOutBarrel <= auxShift;															-- Resultado del corrimiento
	end process;

end A_practica4;