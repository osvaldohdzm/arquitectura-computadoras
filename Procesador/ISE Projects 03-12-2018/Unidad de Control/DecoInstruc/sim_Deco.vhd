
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
 
ENTITY sim_Deco IS
END sim_Deco;
 
ARCHITECTURE behavior OF sim_Deco IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT DecoInstruc
    PORT(
         codOper : IN  std_logic_vector(4 downto 0);
         tipor : OUT  std_logic;
         beqi : OUT  std_logic;
         bneqi : OUT  std_logic;
         blti : OUT  std_logic;
         bleti : OUT  std_logic;
         bgti : OUT  std_logic;
         bgeti : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal codOper : std_logic_vector(4 downto 0) := (others => '0');

 	--Outputs
   signal tipor : std_logic;
   signal beqi : std_logic;
   signal bneqi : std_logic;
   signal blti : std_logic;
   signal bleti : std_logic;
   signal bgti : std_logic;
   signal bgeti : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: DecoInstruc PORT MAP (
          codOper => codOper,
          tipor => tipor,
          beqi => beqi,
          bneqi => bneqi,
          blti => blti,
          bleti => bleti,
          bgti => bgti,
          bgeti => bgeti
        );

 

   -- Stimulus process
   stim_proc: process
   begin		
      -- insert stimulus here 
		codOper <= "00000";
      wait for 10 ns;
		
		codOper <= "00001";
      wait for 10 ns;
		
		codOper <= "10000";
      wait for 10 ns;
		
		codOper <= "10001";
      wait for 10 ns;
   end process;

END;
