library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity P4_ArchivoRegistros is
    Port ( write_data : in  STD_LOGIC_VECTOR (15 downto 0);
           write_reg, read_reg1, read_reg2, shamt : in  STD_LOGIC_VECTOR (3 downto 0);
           clk, clr, wr, she, dir : in  STD_LOGIC;
           read_data1, read_data2 : out  STD_LOGIC_VECTOR (15 downto 0));
end P4_ArchivoRegistros;

architecture Behavioral of P4_ArchivoRegistros is

type archivo is array (0 to 15) of STD_LOGIC_VECTOR (15 downto 0);
signal banco: archivo;

begin

	Proceso : process(clr, clk, banco, write_reg, shamt, read_reg2)
		variable corrimiento, aux_corrimiento : bit_vector (15 downto 0);
		variable n : integer;
	begin
		aux_corrimiento := to_bitvector(banco(conv_integer(read_reg2)));
		n := conv_integer(shamt);
		if(clr = '1') then
			for i in 0 to 15 loop
				banco(i) <= X"0000";
			end loop;
		elsif(rising_edge(clk)) then
			-- CARGA
			if((wr = '1') AND (she = '0')) then
				banco(conv_integer(write_reg)) <= write_data;
			end if;
			--CORRIMIENTO DERECCHA >>
			if((wr = '1') AND (she = '1') AND (dir = '0')) then
				corrimiento := aux_corrimiento srl n;
				banco(conv_integer(write_reg)) <= to_stdlogicvector(corrimiento);
			end if;
			--CORRIMIENTO IZQUIERDA <<
			if((wr = '1') AND (she = '1') AND (dir = '1')) then
				corrimiento := aux_corrimiento sll n;
				banco(conv_integer(write_reg)) <= to_stdlogicvector(corrimiento);
			end if;
		end if;
	end process Proceso;
	
	read_data1 <= banco(conv_integer(read_reg1));
	read_data2 <= banco(conv_integer(read_reg2));

end Behavioral;

