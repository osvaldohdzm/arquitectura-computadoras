library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity P6_MemoriaPrograma is
generic(
	n_dir : integer := 10
);
    Port ( dir : in  STD_LOGIC_VECTOR (n_dir-1 downto 0);
           dato : out  STD_LOGIC_VECTOR (24 downto 0));
end P6_MemoriaPrograma;

architecture Behavioral of P6_MemoriaPrograma is
type memoriaP is array (0 to (2**n_dir)-1) of STD_LOGIC_VECTOR (24 downto 0);
--PROGRAMA 1
signal aux_mem : memoriaP:=(0 => "0000100000000000000000001", --LI, R0 = 1
									 1 => "0000100010000000000000111", --LI, R1 = 7

									 2 => "0000000010001000000000000", --ADD, R1 = R1+R0

									 3 => "0001100010000000000000101", --SWI, MEM[5] = R1

									 4 => "1001100000000000000000010", --B, salto 2
       								others=>(others=>'0'));

--PROGRAMA 2									
--signal aux_mem : memoriaP:=(0 => "0000100000000000000000000", --LI, R0 = 0
--									 1 => "0000100010000000000000001", --LI, R1 = 1
--									 2 => "0000100100000000000000000", --LI, R2 = 0
--									 3 => "0000100110000000000001100", --LI, R3 = 12
									 
--									 4 => "0000001000000000100000000", --ADD, R4 = R0+R1
--									 5 => "0001101000000000001001000", --SWI, MEM[72] = R4
									 
--									 6 => "0100000000001000000000000", --ORI, R0 = R1
--									 7 => "0100000010100000000000000", --ORI, R1 = R4
									 
--									 8 => "0010100100010000000000001", --ADDI, R2 = R2+1
									 
--									 9 => "0111000110010111111111011", --BNEI, R2!=R3, salto 4
--									 10 => "1011000000000000000000000", --NOP
--									 11 => "1001100000000000000001010", --B, salto 10
--									others=>(others=>'0'));
begin

	dato <= aux_mem(conv_integer(dir));

end Behavioral;

