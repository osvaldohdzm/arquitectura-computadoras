-- Práctica 9 | Práctica 9E - Unidad de Control | Marco Antonio Rubio Cortés

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity practica9E is
	port
	(
		-- Entradas | Decodificador de Instrucción
		TIPOR							: in std_logic;
		BEQI							: in std_logic;
		BNEQI							: in std_logic;
		BLTI							: in std_logic;
		BLETI							: in std_logic;
		BGTI							: in std_logic;
		BGETI							: in std_logic;		
		
		-- Entradas | Nivel
		NA								: in std_logic;
		
		-- Entradas | Verificador de Condición
		EQ								: in std_logic;
		NEQ							: in std_logic;
		LT								: in std_logic;
		LE								: in std_logic;
		GT								: in std_logic;
		GET							: in std_logic;
		
		-- Entradas | Señales de control
		CLK							: in std_logic;
		CLR							: in std_logic;
		
		-- Salidas | MUX
		SDOPC							: out std_logic;
		SM								: out std_logic
	);
end practica9E;

architecture A_practica9E of practica9E is

type ESTADOS is (A);
signal EDOA, EDOS : ESTADOS;

begin
	transicion : process(CLK, CLR)
	begin
		if (CLR = '1') then
			EDOA <= A;
		elsif (rising_edge(CLK)) then
			EDOA <= EDOS;
		end if;
	end process transicion;
	
	FSM: process (EDOA, TIPOR, BEQI, BNEQI, BLTI, BLETI, BGTI, BGETI, NA, EQ, NEQ, LT, LE, GT, GET)
	begin
		SDOPC		<= '0';
		SM			<= '0';
		
		case EDOA is
			when A =>
				if TIPOR = '1' then							--	Es tipo R, entonces S = mem_opFunc[opFunc]
					SM		<= '0';
					EDOS	<= A;
				elsif BEQI = '1' then						-- Rt == Rd
					if NA = '1' then							--	Verificación S = mem_opCode[0]
						SDOPC <= '0';
						SM <= '1';
						EDOS <= A;
					else
						if EQ = '1' then						--	Se cumple el salto S = mem_opCode[opCode]
							SDOPC <= '1';
							SM <='1';
							EDOS <= A;
						else
							SDOPC <= '0';
							SM <= '1';
							EDOS <= A;
						end if;
					end if;
				elsif BNEQI = '1' then						-- Rt != Rd
					if NA = '1' then							--	Verificación S = mem_opCode[0]
						SDOPC <= '0';
						SM <= '1';
						EDOS <= A;
					else
						if NEQ = '1' then						--	Se cumple el salto S = mem_opCode[opCode]
							SDOPC <= '1';
							SM <='1';
							EDOS <= A;
						else
							SDOPC <= '0';
							SM <= '1';
							EDOS <= A;
						end if;
					end if;
				elsif BLTI = '1' then						-- Rt < Rd
					if NA = '1' then							--	Verificación S = mem_opCode[0]
						SDOPC <= '0';
						SM <= '1';
						EDOS <= A;
					else
						if LT = '1' then						--	Se cumple el salto S = mem_opCode[opCode]
							SDOPC <= '1';
							SM <='1';
							EDOS <= A;
						else
							SDOPC <= '0';
							SM <= '1';
							EDOS <= A;
						end if;
					end if;
				elsif BLETI = '1' then						-- Rt <= Rd
					if NA = '1' then							--	Verificación S = mem_opCode[0]
						SDOPC <= '0';
						SM <= '1';
						EDOS <= A;
					else
						if LE = '1' then						--	Se cumple el salto S = mem_opCode[opCode]
							SDOPC <= '1';
							SM <='1';
							EDOS <= A;
						else
							SDOPC <= '0';
							SM <= '1';
							EDOS <= A;
						end if;
					end if;
				elsif BGTI = '1' then						-- Rt > Rd
					if NA = '1' then							--	Verificación S = mem_opCode[0]
						SDOPC <= '0';
						SM <= '1';
						EDOS <= A;
					else
						if GT = '1' then						--	Se cumple el salto S = mem_opCode[opCode]
							SDOPC <= '1';
							SM <='1';
							EDOS <= A;
						else
							SDOPC <= '0';
							SM <= '1';
							EDOS <= A;
						end if;
					end if;
				elsif BGETI = '1' then						-- Rt >= Rd
					if NA = '1' then							--	Verificación S = mem_opCode[0]
						SDOPC <= '0';
						SM <= '1';
						EDOS <= A;
					else
						if GET = '1' then						--	Se cumple el salto S = mem_opCode[opCode]
							SDOPC <= '1';
							SM <='1';
							EDOS <= A;
						else
							SDOPC <= '0';
							SM <= '1';
							EDOS <= A;
						end if;
					end if;
				else
					SDOPC <= '1';
					SM <= '1';
					EDOS <= A;
				end if;
		end case;
	end process FSM;

end A_practica9E;