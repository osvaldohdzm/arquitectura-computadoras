library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use iEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity memProg is
	generic
	(
		palabra : integer := 25;
		busT  : integer := 10
	);
	port
	(
		dir		: in 	std_logic_vector(busT-1 downto 0);
		data		: out	std_logic_vector(palabra-1 downto 0)
	);
end memProg;

architecture Behavioral of memProg is

type arr is array (0 to ((2**busT)-1)) of std_logic_vector((palabra-1) downto 0);

constant banco: arr :=	(
                      "0000100000000000000000001",
								"0000100010000000000000110",
								"0000000010001000000000000",
								"0001100010000000001101000",
								"1001100000000000000000010",
								others => (others=>'0') );

begin
	data <= banco(conv_integer(dir));
end Behavioral;
