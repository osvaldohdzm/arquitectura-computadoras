
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 

 
ENTITY tb_SIMULADORPILA IS
END tb_SIMULADORPILA;
 
ARCHITECTURE behavior OF tb_SIMULADORPILA IS 
 

 
    COMPONENT pila
    PORT(
         UP : IN  std_logic;
         DW : IN  std_logic;
         WPC : IN  std_logic;
         CLK : IN  std_logic;
         CLR : IN  std_logic;
         D : IN  std_logic_vector(15 downto 0);
         PC : OUT  std_logic_vector(15 downto 0);
         SP_AUX : OUT  std_logic_vector(2 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal UP : std_logic := '0';
   signal DW : std_logic := '0';
   signal WPC : std_logic := '0';
   signal CLK : std_logic := '0';
   signal CLR : std_logic := '0';
   signal D : std_logic_vector(15 downto 0) := (others => '0');

 	--Outputs
   signal PC : std_logic_vector(15 downto 0);
   signal SP_AUX : std_logic_vector(2 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: pila PORT MAP (
          UP => UP,
          DW => DW,
          WPC => WPC,
          CLK => CLK,
          CLR => CLR,
          D => D,
          PC => PC,
          SP_AUX => SP_AUX
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
	WAIT FOR 100 ns;
      CLR <= '1';
      wait for 10 ns;	
		CLR <= '0';
		wait for 40 ns;
		WPC <= '1';
		UP <= '1';
		D<= X"0045";
		wait for 10 ns;
		wpc <= '0';
		UP <='0';
		wait for 20 ns;
		D<=X"0123";
		WPC <= '1';
		wait for 10 ns;
		WPC <= '0';
		wait for 30 ns;
		D<=x"4545";
		WPC <= '1';
		up <='1';
		WAIT FOR 10 ns;
		WPC <= '0';
		up <= '0';
		wait for 30 ns;
		DW <= '1';
		wait for 10 ns;
		dw <= '0';
		wait for 40 ns;
		dw <= '1';
		wait for 10 ns;
		dw <= '0';
		wait for 30 ns;
		dw <= '1';
		wait for 10 ns;
		dw <= '0';
		wait for 30 ns;
		
      -- insert stimulus here 

      wait;
   end process;

END;
