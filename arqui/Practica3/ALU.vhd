----------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ALU is
	generic( N: integer:= 4);
    Port ( a : in  STD_LOGIC_VECTOR (N-1 downto 0);
           b : in  STD_LOGIC_VECTOR (N-1 downto 0);
           aluop : in  STD_LOGIC_VECTOR (3 downto 0);
           res : out  STD_LOGIC_VECTOR (N-1 downto 0);
           flags : out  STD_LOGIC_VECTOR(3 downto 0)
			  );
end ALU;

architecture Behavioral of ALU is
signal amux, bmux, res_and, res_or, res_xor : std_logic_vector(3 downto 0);
begin


amux (3) <= a(3) xor aluop(3);
bmux (3) <= b(3) xor aluop(2);

res_and(3) <= amux(3) and bmux(3);
res_or(3) <= amux(3) or bmux(3);
res_xor(3) <= amux(3) xor bmux(3);


res(3) <= res_and(3) when aluop(1 downto 0) = "00" else
			res_or(3) when aluop(1 downto 0) = "01" else
			res_xor(3) when aluop(1 downto 0) = "10" else
			'0';

	


end Behavioral;

