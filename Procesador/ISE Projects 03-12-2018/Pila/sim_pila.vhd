
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY sim_pila IS
END sim_pila;
 
ARCHITECTURE behavior OF sim_pila IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Pila
    PORT(
         d : IN  std_logic_vector(15 downto 0);
         up : IN  std_logic;
         down : IN  std_logic;
         wpc : IN  std_logic;
         clk : IN  std_logic;
         clr : IN  std_logic;
         pc_sal : OUT  std_logic_vector(15 downto 0);
         sp_aux : OUT  std_logic_vector(2 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal d : std_logic_vector(15 downto 0) := (others => '0');
   signal up : std_logic := '0';
   signal down : std_logic := '0';
   signal wpc : std_logic := '0';
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';

 	--Outputs
   signal pc_sal : std_logic_vector(15 downto 0);
   signal sp_aux : std_logic_vector(2 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Pila PORT MAP (
          d => d,
          up => up,
          down => down,
          wpc => wpc,
          clk => clk,
          clr => clr,
          pc_sal => pc_sal,
          sp_aux => sp_aux
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
		--1
		clr <= '1';
		wait for 10 ns;
		
		--2
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		wait for 10 ns;
		
		
		--3
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		wait for 10 ns;
		
		
		--4
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		wait for 10 ns;
		
		
		--5
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		wait for 10 ns;
		
		--6
		clr <= '0';
		up <= '1';
		down <= '0';
		wpc <= '1';
		d <= x"0045";
		wait for 10 ns;
		
		--7
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--8
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--9
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '1';
		d <= x"0123";
		wait for 10 ns;
		
		--10
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--11
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--12
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--13
		clr <= '0';
		up <= '1';
		down <= '0';
		wpc <= '1';
		d <= x"4545";
		wait for 10 ns;
		
		--14
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--15
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--16
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--17
		clr <= '0';
		up <= '0';
		down <= '1';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--18
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--19
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--20
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--21
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--22
		clr <= '0';
		up <= '0';
		down <= '1';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--23
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--24
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--25
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--26
		clr <= '0';
		up <= '0';
		down <= '1';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--27
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--28
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		d <= x"0000";
		wait for 10 ns;
		
		--29
		clr <= '0';
		up <= '0';
		down <= '0';
		wpc <= '0';
		d <= x"0000";
      wait;
   end process;

END;
