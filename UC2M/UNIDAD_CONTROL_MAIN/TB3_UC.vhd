--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   09:16:47 11/28/2018
-- Design Name:   
-- Module Name:   C:/Users/leonardo faydella/Desktop/ProyectoFinal/UNIDAD_CONTROL_MAIN/TB3_UC.vhd
-- Project Name:  UNIDAD_CONTROL_MAIN
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: PRINCIPAL
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TB3_UC IS
END TB3_UC;
 
ARCHITECTURE behavior OF TB3_UC IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT PRINCIPAL
    PORT(
         CLK : IN  std_logic;
         CLR : IN  std_logic;
         LF : IN  std_logic;
         COD_FUNC : IN  std_logic_vector(3 downto 0);
         BANDERAS : IN  std_logic_vector(3 downto 0);
         S : OUT  std_logic_vector(19 downto 0);
         COD_OP : IN  std_logic_vector(4 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal CLR : std_logic := '0';
   signal LF : std_logic := '0';
   signal COD_FUNC : std_logic_vector(3 downto 0) := (others => '0');
   signal BANDERAS : std_logic_vector(3 downto 0) := (others => '0');
   signal COD_OP : std_logic_vector(4 downto 0) := (others => '0');

 	--Outputs
   signal S : std_logic_vector(19 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: PRINCIPAL PORT MAP (
          CLK => CLK,
          CLR => CLR,
          LF => LF,
          COD_FUNC => COD_FUNC,
          BANDERAS => BANDERAS,
          S => S,
          COD_OP => COD_OP
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for CLK_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
