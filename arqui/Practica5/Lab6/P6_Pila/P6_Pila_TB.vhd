LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY P6_Pila_TB IS
END P6_Pila_TB;
 
ARCHITECTURE behavior OF P6_Pila_TB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT P6_Pila
    PORT(
         pcData : IN  std_logic_vector(10 downto 0);
         contadorPrograma : OUT  std_logic_vector(10 downto 0);
         stackPointer : OUT  std_logic_vector(2 downto 0);
         L : IN  std_logic;
         UP : IN  std_logic;
         DW : IN  std_logic;
         clk : IN  std_logic;
         clr : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal pcData : std_logic_vector(10 downto 0) := (others => '0');
   signal L : std_logic := '0';
   signal UP : std_logic := '0';
   signal DW : std_logic := '0';
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';

 	--Outputs
	signal contadorPrograma : std_logic_vector(10 downto 0);
   signal stackPointer : std_logic_vector(2 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: P6_Pila PORT MAP (
          pcData => pcData,
          contadorPrograma => contadorPrograma,
          stackPointer => stackPointer,
          L => L,
          UP => UP,
          DW => DW,
          clk => clk,
          clr => clr
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

	-- Stimulus process
	stim_proc: process
	variable aux: std_logic_vector(15 downto 0); --Auxiliar de Vectores de 4 Hexa
	begin
		clr <= '1';
		wait for 30 ns;
		
		clr <= '0';
		wait for 100 ns;
		
		L <= '1';
		aux := x"00ff";
		pcData <= aux(10 downto 0);
		wait for 10 ns;
		
		L <= '0';
		wait for 100 ns;
		
		L <= '1';
		UP <= '1';
		aux := x"0678";
		pcData <= aux(10 downto 0);
		wait for 10 ns;
		
		L <= '0';
		UP <= '0';
		wait for 100 ns;
		
		DW <= '1';
		wait for 10 ns;
		
		DW <= '0';
		wait for 400 ns;
		
	end process;
	
END;