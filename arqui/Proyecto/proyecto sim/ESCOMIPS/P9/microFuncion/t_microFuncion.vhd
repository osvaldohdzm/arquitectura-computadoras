
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY t_microFuncion IS
END t_microFuncion;
 
ARCHITECTURE behavior OF t_microFuncion IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT microFuncion
    PORT(
         i : IN  std_logic_vector(3 downto 0);
         funSal : OUT  std_logic_vector(19 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal i : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal funSal : std_logic_vector(19 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: microFuncion PORT MAP (
          i => i,
          funSal => funSal
        );

   -- Stimulus process
   stim_proc: process
   begin		
 
      i <= "0000";
		wait for 10 ns;
		i <= "0001";
		wait for 10 ns;
		i <= "0010";
		wait for 10 ns;
		i <= "0011";
		wait for 10 ns;
		i <= "0100";
		wait for 10 ns;
		i <= "0101";
		wait for 10 ns;
		i <= "0110";
		wait for 10 ns;
		i <= "0111";
		wait for 10 ns;
		i <= "1000";
		wait for 10 ns;
		i <= "1001";
		wait for 10 ns;
		i <= "1010";
		wait for 10 ns;
		i <= "1111";
      wait;
   end process;

END;
