--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.28xd
--  \   \         Application: netgen
--  /   /         Filename: Principal_synthesis.vhd
-- /___/   /\     Timestamp: Wed Sep 19 16:45:49 2018
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm Principal -w -dir netgen/synthesis -ofmt vhdl -sim Principal.ngc Principal_synthesis.vhd 
-- Device	: xc7a100t-2-csg324
-- Input file	: Principal.ngc
-- Output file	: C:\Users\osvaldohm\Desktop\ALUNBITS\netgen\synthesis\Principal_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: Principal
-- Xilinx	: C:\Xilinx\14.2\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity Principal is
  port (
    C : out STD_LOGIC; 
    Z : out STD_LOGIC; 
    OV : out STD_LOGIC; 
    N : out STD_LOGIC; 
    a : in STD_LOGIC_VECTOR ( 3 downto 0 ); 
    b : in STD_LOGIC_VECTOR ( 3 downto 0 ); 
    ALUOP : in STD_LOGIC_VECTOR ( 3 downto 0 ); 
    S : out STD_LOGIC_VECTOR ( 3 downto 0 ) 
  );
end Principal;

architecture Structure of Principal is
  signal a_0_IBUF_0 : STD_LOGIC; 
  signal a_1_IBUF_1 : STD_LOGIC; 
  signal a_2_IBUF_2 : STD_LOGIC; 
  signal a_3_IBUF_3 : STD_LOGIC; 
  signal b_0_IBUF_4 : STD_LOGIC; 
  signal b_1_IBUF_5 : STD_LOGIC; 
  signal b_2_IBUF_6 : STD_LOGIC; 
  signal b_3_IBUF_7 : STD_LOGIC; 
  signal ALUOP_2_IBUF_8 : STD_LOGIC; 
  signal ALUOP_1_IBUF_9 : STD_LOGIC; 
  signal ALUOP_0_IBUF_10 : STD_LOGIC; 
  signal ALUOP_3_IBUF_11 : STD_LOGIC; 
  signal S_0_OBUF_12 : STD_LOGIC; 
  signal S_1_OBUF_14 : STD_LOGIC; 
  signal S_2_OBUF_16 : STD_LOGIC; 
  signal N_OBUF_18 : STD_LOGIC; 
  signal C_OBUF_19 : STD_LOGIC; 
  signal OV_OBUF_20 : STD_LOGIC; 
  signal Z_OBUF_21 : STD_LOGIC; 
  signal ciclo_gen_3_alubit_res_b : STD_LOGIC; 
  signal ciclo_gen_2_alubit_res_b : STD_LOGIC; 
  signal ciclo_gen_1_alubit_res_b : STD_LOGIC; 
  signal CARR : STD_LOGIC_VECTOR ( 3 downto 1 ); 
begin
  Z_3_1 : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => N_OBUF_18,
      I1 => S_2_OBUF_16,
      I2 => S_1_OBUF_14,
      I3 => S_0_OBUF_12,
      O => Z_OBUF_21
    );
  ciclo_gen_3_alubit_Mxor_res_b_xo_0_1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => b_3_IBUF_7,
      I1 => ALUOP_2_IBUF_8,
      O => ciclo_gen_3_alubit_res_b
    );
  ciclo_gen_2_alubit_Mxor_res_b_xo_0_1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => b_2_IBUF_6,
      I1 => ALUOP_2_IBUF_8,
      O => ciclo_gen_2_alubit_res_b
    );
  ciclo_gen_1_alubit_Mxor_res_b_xo_0_1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => b_1_IBUF_5,
      I1 => ALUOP_2_IBUF_8,
      O => ciclo_gen_1_alubit_res_b
    );
  ciclo_gen_0_alubit_Mmux_cout11 : LUT6
    generic map(
      INIT => X"0888880800808000"
    )
    port map (
      I0 => ALUOP_0_IBUF_10,
      I1 => ALUOP_1_IBUF_9,
      I2 => b_0_IBUF_4,
      I3 => a_0_IBUF_0,
      I4 => ALUOP_3_IBUF_11,
      I5 => ALUOP_2_IBUF_8,
      O => CARR(1)
    );
  ciclo_gen_0_alubit_Mmux_res11 : LUT6
    generic map(
      INIT => X"92C86B3E6B3E92C8"
    )
    port map (
      I0 => ALUOP_0_IBUF_10,
      I1 => b_0_IBUF_4,
      I2 => ALUOP_1_IBUF_9,
      I3 => ALUOP_2_IBUF_8,
      I4 => a_0_IBUF_0,
      I5 => ALUOP_3_IBUF_11,
      O => S_0_OBUF_12
    );
  ciclo_gen_1_alubit_Mmux_cout11 : LUT6
    generic map(
      INIT => X"8000888088808000"
    )
    port map (
      I0 => ALUOP_0_IBUF_10,
      I1 => ALUOP_1_IBUF_9,
      I2 => CARR(1),
      I3 => ciclo_gen_1_alubit_res_b,
      I4 => a_1_IBUF_1,
      I5 => ALUOP_3_IBUF_11,
      O => CARR(2)
    );
  ciclo_gen_1_alubit_Mmux_res11 : LUT6
    generic map(
      INIT => X"68E0B63EB63E68E0"
    )
    port map (
      I0 => ALUOP_0_IBUF_10,
      I1 => ALUOP_1_IBUF_9,
      I2 => ciclo_gen_1_alubit_res_b,
      I3 => CARR(1),
      I4 => a_1_IBUF_1,
      I5 => ALUOP_3_IBUF_11,
      O => S_1_OBUF_14
    );
  ciclo_gen_2_alubit_Mmux_cout11 : LUT6
    generic map(
      INIT => X"8000888088808000"
    )
    port map (
      I0 => ALUOP_0_IBUF_10,
      I1 => ALUOP_1_IBUF_9,
      I2 => CARR(2),
      I3 => ciclo_gen_2_alubit_res_b,
      I4 => a_2_IBUF_2,
      I5 => ALUOP_3_IBUF_11,
      O => CARR(3)
    );
  ciclo_gen_2_alubit_Mmux_res11 : LUT6
    generic map(
      INIT => X"68E0B63EB63E68E0"
    )
    port map (
      I0 => ALUOP_0_IBUF_10,
      I1 => ALUOP_1_IBUF_9,
      I2 => ciclo_gen_2_alubit_res_b,
      I3 => CARR(2),
      I4 => a_2_IBUF_2,
      I5 => ALUOP_3_IBUF_11,
      O => S_2_OBUF_16
    );
  ciclo_gen_3_alubit_Mmux_cout11 : LUT6
    generic map(
      INIT => X"8000888088808000"
    )
    port map (
      I0 => ALUOP_0_IBUF_10,
      I1 => ALUOP_1_IBUF_9,
      I2 => CARR(3),
      I3 => ciclo_gen_3_alubit_res_b,
      I4 => a_3_IBUF_3,
      I5 => ALUOP_3_IBUF_11,
      O => C_OBUF_19
    );
  ciclo_gen_3_alubit_Mmux_res11 : LUT6
    generic map(
      INIT => X"68E0B63EB63E68E0"
    )
    port map (
      I0 => ALUOP_0_IBUF_10,
      I1 => ALUOP_1_IBUF_9,
      I2 => ciclo_gen_3_alubit_res_b,
      I3 => CARR(3),
      I4 => a_3_IBUF_3,
      I5 => ALUOP_3_IBUF_11,
      O => N_OBUF_18
    );
  a_3_IBUF : IBUF
    port map (
      I => a(3),
      O => a_3_IBUF_3
    );
  a_2_IBUF : IBUF
    port map (
      I => a(2),
      O => a_2_IBUF_2
    );
  a_1_IBUF : IBUF
    port map (
      I => a(1),
      O => a_1_IBUF_1
    );
  a_0_IBUF : IBUF
    port map (
      I => a(0),
      O => a_0_IBUF_0
    );
  b_3_IBUF : IBUF
    port map (
      I => b(3),
      O => b_3_IBUF_7
    );
  b_2_IBUF : IBUF
    port map (
      I => b(2),
      O => b_2_IBUF_6
    );
  b_1_IBUF : IBUF
    port map (
      I => b(1),
      O => b_1_IBUF_5
    );
  b_0_IBUF : IBUF
    port map (
      I => b(0),
      O => b_0_IBUF_4
    );
  ALUOP_3_IBUF : IBUF
    port map (
      I => ALUOP(3),
      O => ALUOP_3_IBUF_11
    );
  ALUOP_2_IBUF : IBUF
    port map (
      I => ALUOP(2),
      O => ALUOP_2_IBUF_8
    );
  ALUOP_1_IBUF : IBUF
    port map (
      I => ALUOP(1),
      O => ALUOP_1_IBUF_9
    );
  ALUOP_0_IBUF : IBUF
    port map (
      I => ALUOP(0),
      O => ALUOP_0_IBUF_10
    );
  S_3_OBUF : OBUF
    port map (
      I => N_OBUF_18,
      O => S(3)
    );
  S_2_OBUF : OBUF
    port map (
      I => S_2_OBUF_16,
      O => S(2)
    );
  S_1_OBUF : OBUF
    port map (
      I => S_1_OBUF_14,
      O => S(1)
    );
  S_0_OBUF : OBUF
    port map (
      I => S_0_OBUF_12,
      O => S(0)
    );
  C_OBUF : OBUF
    port map (
      I => C_OBUF_19,
      O => C
    );
  Z_OBUF : OBUF
    port map (
      I => Z_OBUF_21,
      O => Z
    );
  OV_OBUF : OBUF
    port map (
      I => OV_OBUF_20,
      O => OV
    );
  N_OBUF : OBUF
    port map (
      I => N_OBUF_18,
      O => N
    );
  Mxor_OV_xo_0_1 : LUT6
    generic map(
      INIT => X"2442AAAAAAAAAAAA"
    )
    port map (
      I0 => CARR(3),
      I1 => ciclo_gen_3_alubit_res_b,
      I2 => a_3_IBUF_3,
      I3 => ALUOP_3_IBUF_11,
      I4 => ALUOP_0_IBUF_10,
      I5 => ALUOP_1_IBUF_9,
      O => OV_OBUF_20
    );

end Structure;

