LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
use ieee.numeric_std.all;
USE IEEE.std_logic_unsigned.ALL;

LIBRARY STD;
USE STD.TEXTIO.ALL;
USE ieee.std_logic_TEXTIO.ALL;

ENTITY P10_UnidadControl_TestBench IS
END P10_UnidadControl_TestBench;
 
ARCHITECTURE behavior OF P10_UnidadControl_TestBench IS 

    COMPONENT main
    PORT(
         clk : IN  std_logic;
         clr : IN  std_logic;
         lf : IN  std_logic;
         a_fun : IN  std_logic_vector(3 downto 0);
         banderas : IN  std_logic_vector(3 downto 0);
         op_code : IN  std_logic_vector(4 downto 0);
         s : OUT  std_logic_vector(19 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';
   signal lf : std_logic := '0';
   signal a_fun : std_logic_vector(3 downto 0) := (others => '0');
   signal banderas : std_logic_vector(3 downto 0) := (others => '0');
   signal op_code : std_logic_vector(4 downto 0) := (others => '0');

 	--Outputs
   signal s : std_logic_vector(19 downto 0);

   -- Clock period definitions
   constant clk_period : time := 15 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: main PORT MAP (
          clk => clk,
          clr => clr,
          lf => lf,
          a_fun => a_fun,
          banderas => banderas,
          op_code => op_code,
          s => s
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
	file ARCH_RES : TEXT;
	variable LINEA_RES : line;
	VARIABLE VAR_s : STD_LOGIC_VECTOR(19 DOWNTO 0);
	
	file ARCH_VEC : TEXT;
	variable LINEA_VEC : line;
	VARIABLE NIVEL : STRING(1 TO 4);
	VARIABLE VAR_clr : STD_LOGIC;
	VARIABLE VAR_lf : STD_LOGIC;
	VARIABLE VAR_a_fun : STD_LOGIC_VECTOR(3 downto 0);
	VARIABLE VAR_banderas : STD_LOGIC_VECTOR(3 downto 0);
	VARIABLE VAR_op_code : STD_LOGIC_VECTOR(4 downto 0);
	VARIABLE CADENA : STRING(1 TO 8);
	VARIABLE CADENA_INST : STRING(1 TO 22);
	begin
		file_open(ARCH_VEC, "_vectores.txt", READ_MODE);
		file_open(ARCH_RES, "_resultados.txt", WRITE_MODE);
		
		--ESCRIBE LA CABECERA
		CADENA := " OP_CODE";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE
		CADENA := "FUN_CODE";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE
		CADENA := "BANDERAS";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE
		CADENA := "     CLR";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE
		CADENA := "      LF";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE
		CADENA_INST := "      MICROINSTRUCCION";
		write(LINEA_RES, CADENA_INST, right, CADENA'LENGTH+1);	--ESCRIBE
		CADENA := "   NIVEL";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE
		writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo
		
		clr <= '1';
		wait for 5 ns;
		clr <= '0';
		
		WHILE NOT ENDFILE (ARCH_VEC) LOOP
			WAIT UNTIL RISING_EDGE(CLK);
			readline(ARCH_VEC, LINEA_VEC);				
			
			read(LINEA_VEC, VAR_op_code);
			op_code <= VAR_op_code;
			
			read(LINEA_VEC, VAR_a_fun);
			a_fun <= VAR_a_fun;
			
			read(LINEA_VEC, VAR_banderas);
			banderas <= VAR_banderas;
			
			read(LINEA_VEC, VAR_clr);
			clr <= VAR_clr;
				
			read(LINEA_VEC, VAR_lf);
			lf <= VAR_lf;
			
			wait for 5 ns;
			VAR_s := s;
			NIVEL := "ALTO";
			
			write(LINEA_RES, VAR_op_code, right, 9);
			write(LINEA_RES, VAR_a_fun, right, 9);
			write(LINEA_RES, VAR_banderas, right, 9);
			write(LINEA_RES, VAR_clr, right, 9);
			write(LINEA_RES, VAR_lf, right, 9);
			write(LINEA_RES, VAR_s, right, 22);
			write(LINEA_RES, NIVEL, right, 9);
			WRITELINE(ARCH_RES, LINEA_RES);
			
			WAIT UNTIL FALLING_EDGE(CLK);
			wait for 5 ns;
			VAR_s := s;
			NIVEL := "BAJO";
			
			write(LINEA_RES, VAR_op_code, right, 9);
			write(LINEA_RES, VAR_a_fun, right, 9);
			write(LINEA_RES, VAR_banderas, right, 9);
			write(LINEA_RES, VAR_clr, right, 9);
			write(LINEA_RES, VAR_lf, right, 9);
			write(LINEA_RES, VAR_s, right, 22);
			write(LINEA_RES, NIVEL, right, 9);
			WRITELINE(ARCH_RES, LINEA_RES);
			
			writeline(ARCH_RES, LINEA_RES); --Escribe la linea en el archivo de resultados
		end loop;
		file_close(ARCH_VEC);
		file_close(ARCH_RES);
      wait;
   end process;

END;
