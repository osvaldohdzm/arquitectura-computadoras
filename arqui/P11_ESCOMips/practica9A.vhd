-- Pr�ctica 9 | Pr�ctica 9A - Registro de Banderas | Marco Antonio Rubio Cort�s

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity practica9A is
	port
	(
		flags							: in std_logic_vector (3 downto 0);
		LF								: in std_logic;
		
		CLK							: in std_logic;
		CLR							: in std_logic;
		
		OV								: inout std_logic;
		N								: inout std_logic;
		Z								: inout std_logic;
		C								: inout std_logic
	);
end practica9A;

architecture A_practica9A of practica9A is

begin
	
	registroBanderas : process (CLK, CLR)
	begin
		if (CLR = '1') then
			OV <= '0';
			N <= '0';
			Z <= '0';
			C <= '0';
		elsif (falling_edge(CLK)) then					-- Se carga en cada flanco de descenso
			if LF = '1' then									-- Cuando LF = 1
				OV <= flags(3);
				N <= flags(2);
				Z <= flags(1);
				C <= flags(0);
			else													-- Retenci�n
				OV <= OV;
				N <= N;
				Z <= Z;
				C <= C;
			end if;
		end if;
	end process registroBanderas;

end A_practica9A;

