/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/_PracticasArqui/P6_MemoriaPrograma/P6_MemoriaPrograma_TestBench.vhd";
extern char *STD_TEXTIO;
extern char *IEEE_P_3564397177;

void ieee_p_3564397177_sub_1281154728_91900896(char *, char *, char *, char *, char *, unsigned char , int );
void ieee_p_3564397177_sub_3205433008_91900896(char *, char *, char *, char *, char *, unsigned char , int );
void ieee_p_3564397177_sub_3988856810_91900896(char *, char *, char *, char *, char *);


static void work_a_2030162914_2372691052_p_0(char *t0)
{
    char t5[16];
    char t10[8];
    char t11[8];
    char t12[8];
    char t13[8];
    char t14[8];
    char t15[8];
    char t16[8];
    char t20[16];
    char t23[8];
    char t26[8];
    char t27[8];
    char t28[8];
    char t29[8];
    char t30[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    int t8;
    unsigned int t9;
    int64 t17;
    int t18;
    char *t19;
    unsigned int t21;
    unsigned int t22;
    int t24;
    unsigned int t25;

LAB0:    t1 = (t0 + 3088U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(48, ng0);
    t2 = (t0 + 2040U);
    t3 = (t0 + 6326);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 12;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (12 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)0);
    xsi_set_current_line(49, ng0);
    t2 = (t0 + 1936U);
    t3 = (t0 + 6338);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 14;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (14 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)1);
    xsi_set_current_line(52, ng0);
    t2 = (t0 + 6352);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 6U);
    xsi_set_current_line(53, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2216U);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t10, t7, 6U);
    t6 = (t0 + 6068U);
    t8 = (6U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t10, t6, (unsigned char)0, t8);
    xsi_set_current_line(54, ng0);
    t2 = (t0 + 6358);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 6U);
    xsi_set_current_line(55, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2216U);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t11, t7, 6U);
    t6 = (t0 + 6068U);
    t8 = (6U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t11, t6, (unsigned char)0, t8);
    xsi_set_current_line(56, ng0);
    t2 = (t0 + 6364);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 6U);
    xsi_set_current_line(57, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2216U);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t12, t7, 6U);
    t6 = (t0 + 6068U);
    t8 = (6U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t12, t6, (unsigned char)0, t8);
    xsi_set_current_line(58, ng0);
    t2 = (t0 + 6370);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 6U);
    xsi_set_current_line(59, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2216U);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t13, t7, 6U);
    t6 = (t0 + 6068U);
    t8 = (6U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t13, t6, (unsigned char)0, t8);
    xsi_set_current_line(60, ng0);
    t2 = (t0 + 6376);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 6U);
    xsi_set_current_line(61, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2216U);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t14, t7, 6U);
    t6 = (t0 + 6068U);
    t8 = (6U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t14, t6, (unsigned char)0, t8);
    xsi_set_current_line(62, ng0);
    t2 = (t0 + 6382);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 6U);
    xsi_set_current_line(63, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2216U);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t15, t7, 6U);
    t6 = (t0 + 6068U);
    t8 = (6U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t15, t6, (unsigned char)0, t8);
    xsi_set_current_line(64, ng0);
    t2 = (t0 + 6388);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 6U);
    xsi_set_current_line(65, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2216U);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t16, t7, 6U);
    t6 = (t0 + 6068U);
    t8 = (6U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t16, t6, (unsigned char)0, t8);
    xsi_set_current_line(66, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 1936U);
    t4 = (t0 + 2216U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    xsi_set_current_line(68, ng0);
    t17 = (100 * 1000LL);
    t2 = (t0 + 2896);
    xsi_process_wait(t2, t17);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(69, ng0);
    t2 = (t0 + 6394);
    *((int *)t2) = 0;
    t3 = (t0 + 6398);
    *((int *)t3) = 6;
    t8 = 0;
    t18 = 6;

LAB8:    if (t8 <= t18)
        goto LAB9;

LAB11:    xsi_set_current_line(88, ng0);
    t2 = (t0 + 2040U);
    std_textio_file_close(t2);
    xsi_set_current_line(89, ng0);
    t2 = (t0 + 1936U);
    std_textio_file_close(t2);
    xsi_set_current_line(90, ng0);

LAB19:    *((char **)t1) = &&LAB20;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB9:    xsi_set_current_line(70, ng0);
    t4 = (t0 + 2896);
    t6 = (t0 + 2040U);
    t7 = (t0 + 2288U);
    std_textio_readline(STD_TEXTIO, t4, t6, t7);
    xsi_set_current_line(72, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2288U);
    t4 = (t0 + 1608U);
    t6 = *((char **)t4);
    t4 = (t0 + 6052U);
    ieee_p_3564397177_sub_3988856810_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(73, ng0);
    t2 = (t0 + 1608U);
    t3 = *((char **)t2);
    t2 = (t0 + 3472);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t19 = *((char **)t7);
    memcpy(t19, t3, 12U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(75, ng0);
    t17 = (100 * 1000LL);
    t2 = (t0 + 2896);
    xsi_process_wait(t2, t17);

LAB14:    *((char **)t1) = &&LAB15;
    goto LAB1;

LAB10:    t2 = (t0 + 6394);
    t8 = *((int *)t2);
    t3 = (t0 + 6398);
    t18 = *((int *)t3);
    if (t8 == t18)
        goto LAB11;

LAB16:    t24 = (t8 + 1);
    t8 = t24;
    t4 = (t0 + 6394);
    *((int *)t4) = t8;
    goto LAB8;

LAB12:    xsi_set_current_line(76, ng0);
    t2 = (t0 + 1192U);
    t3 = *((char **)t2);
    t2 = (t0 + 1488U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 25U);
    xsi_set_current_line(78, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2216U);
    t4 = (t0 + 1608U);
    t6 = *((char **)t4);
    memcpy(t20, t6, 12U);
    t4 = (t0 + 6052U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t20, t4, (unsigned char)0, 7);
    xsi_set_current_line(79, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2216U);
    t4 = (t0 + 1488U);
    t6 = *((char **)t4);
    t9 = (24 - 24);
    t21 = (t9 * 1U);
    t22 = (0 + t21);
    t4 = (t6 + t22);
    memcpy(t23, t4, 5U);
    t7 = (t5 + 0U);
    t19 = (t7 + 0U);
    *((int *)t19) = 24;
    t19 = (t7 + 4U);
    *((int *)t19) = 20;
    t19 = (t7 + 8U);
    *((int *)t19) = -1;
    t24 = (20 - 24);
    t25 = (t24 * -1);
    t25 = (t25 + 1);
    t19 = (t7 + 12U);
    *((unsigned int *)t19) = t25;
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t23, t5, (unsigned char)0, 7);
    xsi_set_current_line(80, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2216U);
    t4 = (t0 + 1488U);
    t6 = *((char **)t4);
    t9 = (24 - 19);
    t21 = (t9 * 1U);
    t22 = (0 + t21);
    t4 = (t6 + t22);
    memcpy(t26, t4, 4U);
    t7 = (t5 + 0U);
    t19 = (t7 + 0U);
    *((int *)t19) = 19;
    t19 = (t7 + 4U);
    *((int *)t19) = 16;
    t19 = (t7 + 8U);
    *((int *)t19) = -1;
    t24 = (16 - 19);
    t25 = (t24 * -1);
    t25 = (t25 + 1);
    t19 = (t7 + 12U);
    *((unsigned int *)t19) = t25;
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t26, t5, (unsigned char)0, 7);
    xsi_set_current_line(81, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2216U);
    t4 = (t0 + 1488U);
    t6 = *((char **)t4);
    t9 = (24 - 15);
    t21 = (t9 * 1U);
    t22 = (0 + t21);
    t4 = (t6 + t22);
    memcpy(t27, t4, 4U);
    t7 = (t5 + 0U);
    t19 = (t7 + 0U);
    *((int *)t19) = 15;
    t19 = (t7 + 4U);
    *((int *)t19) = 12;
    t19 = (t7 + 8U);
    *((int *)t19) = -1;
    t24 = (12 - 15);
    t25 = (t24 * -1);
    t25 = (t25 + 1);
    t19 = (t7 + 12U);
    *((unsigned int *)t19) = t25;
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t27, t5, (unsigned char)0, 7);
    xsi_set_current_line(82, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2216U);
    t4 = (t0 + 1488U);
    t6 = *((char **)t4);
    t9 = (24 - 11);
    t21 = (t9 * 1U);
    t22 = (0 + t21);
    t4 = (t6 + t22);
    memcpy(t28, t4, 4U);
    t7 = (t5 + 0U);
    t19 = (t7 + 0U);
    *((int *)t19) = 11;
    t19 = (t7 + 4U);
    *((int *)t19) = 8;
    t19 = (t7 + 8U);
    *((int *)t19) = -1;
    t24 = (8 - 11);
    t25 = (t24 * -1);
    t25 = (t25 + 1);
    t19 = (t7 + 12U);
    *((unsigned int *)t19) = t25;
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t28, t5, (unsigned char)0, 7);
    xsi_set_current_line(83, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2216U);
    t4 = (t0 + 1488U);
    t6 = *((char **)t4);
    t9 = (24 - 7);
    t21 = (t9 * 1U);
    t22 = (0 + t21);
    t4 = (t6 + t22);
    memcpy(t29, t4, 4U);
    t7 = (t5 + 0U);
    t19 = (t7 + 0U);
    *((int *)t19) = 7;
    t19 = (t7 + 4U);
    *((int *)t19) = 4;
    t19 = (t7 + 8U);
    *((int *)t19) = -1;
    t24 = (4 - 7);
    t25 = (t24 * -1);
    t25 = (t25 + 1);
    t19 = (t7 + 12U);
    *((unsigned int *)t19) = t25;
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t29, t5, (unsigned char)0, 7);
    xsi_set_current_line(84, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2216U);
    t4 = (t0 + 1488U);
    t6 = *((char **)t4);
    t9 = (24 - 3);
    t21 = (t9 * 1U);
    t22 = (0 + t21);
    t4 = (t6 + t22);
    memcpy(t30, t4, 4U);
    t7 = (t5 + 0U);
    t19 = (t7 + 0U);
    *((int *)t19) = 3;
    t19 = (t7 + 4U);
    *((int *)t19) = 0;
    t19 = (t7 + 8U);
    *((int *)t19) = -1;
    t24 = (0 - 3);
    t25 = (t24 * -1);
    t25 = (t25 + 1);
    t19 = (t7 + 12U);
    *((unsigned int *)t19) = t25;
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t30, t5, (unsigned char)0, 7);
    xsi_set_current_line(86, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 1936U);
    t4 = (t0 + 2216U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    goto LAB10;

LAB13:    goto LAB12;

LAB15:    goto LAB13;

LAB17:    goto LAB2;

LAB18:    goto LAB17;

LAB20:    goto LAB18;

}


extern void work_a_2030162914_2372691052_init()
{
	static char *pe[] = {(void *)work_a_2030162914_2372691052_p_0};
	xsi_register_didat("work_a_2030162914_2372691052", "isim/P6_MemoriaPrograma_TestBench_isim_beh.exe.sim/work/a_2030162914_2372691052.didat");
	xsi_register_executes(pe);
}
