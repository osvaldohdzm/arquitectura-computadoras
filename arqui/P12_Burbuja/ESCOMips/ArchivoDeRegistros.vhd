library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity ArchivoDeRegistros is
	generic
	(
		constant tamWord	: INTEGER:=16; 	--Tama�o de palabra
		constant tamDir	: INTEGER:=4 		--Longitud del bus de direcciones
	);
	port
	(
		--Entradas
		
		--Bus de Entrada de Datos
		WriteData: in std_logic_vector(tamWord-1 downto 0);
		
		--Direcci�n del Registro donde se almacenan los datos
		WriteRegister: in std_logic_vector(tamDir-1 downto 0);
		
		--Direcci�n del Primer Registro donde se leen los datos
		ReadRegister1: in std_logic_vector(tamDir-1 downto 0);	
		
		--Direcci�n del Segundo Registro donde se leen los datos
		ReadRegister2: in std_logic_vector(tamDir-1 downto 0);
		
		--Cantidad de bits a desplazar en las instrucciones de corrimiento
		--Toma los valores en el intervalo [0,log2(tamWord)-1]
		SHAMT: in std_logic_vector(tamDir-1 downto 0);
		
		--Habilitador de Escritura
		WR: in std_logic;
		
		--Habilitador de Corrimiento
		SHE: in std_logic;
		
		--Indicador para la direcci�n del corrimiento
		DIR: in std_logic;
		
		--Pulso de Reloj
		clk: in std_logic;
		
		--Se�al de Reset
		clr: in std_logic;
		
		--Salidas
		
		--Primer Bus de Datos de Salida correspondiente a la direcci�n indicada en ReadRegister1
		ReadData1: out std_logic_vector(tamWord-1 downto 0);
		
		--Segundo Bus de Datos de Salida correspondiente a la direcci�n indicada en ReadRegister2
		ReadData2: out std_logic_vector(tamWord-1 downto 0)
	);
end ArchivoDeRegistros;

architecture A_ArchivoDeRegistros of ArchivoDeRegistros is
	
	--Tipo de Dato para crear el Banco de registros
	TYPE BancoRegistros IS ARRAY (0 to ((2**tamDir)-1)) OF std_logic_vector(tamWord-1 downto 0);
	
	--Se�al para el banco de registros
	signal Banco: BancoRegistros;
	
	--Se�al donde se almacena el resultado del corrimiento
	signal corrimiento: std_logic_vector(tamWord-1 downto 0);
	
	--Se�al para almacenar el Resultado del MUX de SHE
	signal DatoEscrito: std_logic_vector(tamWord-1 downto 0);
	
	--Se�al para almacenar el contenido de ReadData1 y que entra a BarrelShifter
	signal DatoSalida: std_logic_vector(tamWord-1 downto 0);

begin
	
	--Bloque para BarrelShifter (Circuito combinatorio para realizar corrimientos)
	process(SHAMT,DatoSalida,DIR)
	
		variable aux: std_logic_vector(tamWord-1 downto 0);
	
	begin
		
		aux:=DatoSalida;
		
		if (DIR='1') then
			--Corrimiento hacia la Izquierda
			for i in 0 to tamDir-1 loop
				for j in tamWord-1 downto 0 loop
					if SHAMT(i)='0' then
						aux:=aux;
					else
						if ((j-2**i)<0) then
							aux(j):='0';
						else
							aux(j):=aux(j-2**i);
						end if;
					end if;
				end loop;
			end loop;
		else	
			--Corrimiento hacia la Derecha
			for i in 0 to tamDir-1 loop
				for j in 0 to tamWord-1 loop
					if SHAMT(i)='0' then
						aux:=aux;
					else
						if ((j+2**i)<tamWord) then
							aux(j):=aux(j+2**i);
						else
							aux(j):='0';
						end if;
					end if;
				end loop;
			end loop;
		end if;
		
		corrimiento<=aux;
		
	end process;
	
	--Bloque para Realizar Lecturas
	DatoSalida 	<= Banco(CONV_INTEGER(ReadRegister1));
	ReadData1 	<= DatoSalida;
	ReadData2 	<= Banco(CONV_INTEGER(ReadRegister2));
	
	--Bloque para Realizar Escrituras
	process(clk, clr)
	begin
		if (clr = '1') then
			Banco<=(others=>(others=>'0'));
		elsif(rising_edge(clk)) then
			if (WR = '1') then
				Banco(CONV_INTEGER(WriteRegister)) <= DatoEscrito;			
			end if;
		end if;
	end process;
	
	--Mux para verificar si se toma el valor de WriteData o se toma el valor del Corrimiento
	DatoEscrito <= Corrimiento when SHE='1' else WriteData;

end A_ArchivoDeRegistros;