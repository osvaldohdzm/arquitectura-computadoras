
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY t_condicion IS
END t_condicion;
 
ARCHITECTURE behavior OF t_condicion IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT condicion
    PORT(
         banderas : IN  std_logic_vector(3 downto 0);
         EQ : OUT  std_logic;
         NE : OUT  std_logic;
         L : OUT  std_logic;
         LE : OUT  std_logic;
         G : OUT  std_logic;
         GE : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal banderas : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal EQ : std_logic;
   signal NE : std_logic;
   signal L : std_logic;
   signal LE : std_logic;
   signal G : std_logic;
   signal GE : std_logic;
   -- No clocks detected in port list. Replace <clock> below wit
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: condicion PORT MAP (
          banderas => banderas,
          EQ => EQ,
          NE => NE,
          L => L,
          LE => LE,
          G => G,
          GE => GE
        );

   -- Clock process definitions


   -- Stimulus process
   stim_proc: process
   begin		
      
		banderas <= "0000";
		wait for 20 ns;
		banderas <= "0001";
		wait for 20 ns;
		banderas <= "0010";
		wait for 20 ns;
		banderas <= "0011";
      wait;
   end process;

END;
