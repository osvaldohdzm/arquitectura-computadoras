
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY t_union IS
END t_union;
 
ARCHITECTURE behavior OF t_union IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT unicontrol
    PORT(
         tipor : IN  std_logic;
         beq : IN  std_logic;
         bneq : IN  std_logic;
         blt : IN  std_logic;
         ble : IN  std_logic;
         bgt : IN  std_logic;
         bget : IN  std_logic;
         clk : IN  std_logic;
         clr : IN  std_logic;
         eq : IN  std_logic;
         neq : IN  std_logic;
         lt : IN  std_logic;
         le : IN  std_logic;
         gtt : IN  std_logic;
         get : IN  std_logic;
         na : IN  std_logic;
         sdopc : OUT  std_logic;
         sm : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal tipor : std_logic := '0';
   signal beq : std_logic := '0';
   signal bneq : std_logic := '0';
   signal blt : std_logic := '0';
   signal ble : std_logic := '0';
   signal bgt : std_logic := '0';
   signal bget : std_logic := '0';
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';
   signal eq : std_logic := '0';
   signal neq : std_logic := '0';
   signal lt : std_logic := '0';
   signal le : std_logic := '0';
   signal gtt : std_logic := '0';
   signal get : std_logic := '0';
   signal na : std_logic := '0';

 	--Outputs
   signal sdopc : std_logic;
   signal sm : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: unicontrol PORT MAP (
          tipor => tipor,
          beq => beq,
          bneq => bneq,
          blt => blt,
          ble => ble,
          bgt => bgt,
          bget => bget,
          clk => clk,
          clr => clr,
          eq => eq,
          neq => neq,
          lt => lt,
          le => le,
          gtt => gtt,
          get => get,
          na => na,
          sdopc => sdopc,
          sm => sm
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      
		wait for 20 ns;
		tipor <= '1';
		
      wait;
   end process;

END;
