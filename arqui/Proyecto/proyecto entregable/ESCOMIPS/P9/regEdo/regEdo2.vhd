
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity regEdo2 is
    Port ( banderas : in  STD_LOGIC_VECTOR (3 downto 0);
           clr, clk, LF : in  STD_LOGIC;
			  salReg : out std_logic_vector (3 downto 0)
			  );
end regEdo2;

architecture Behavioral of regEdo2 is
--signal banderas2 : std_logic_vector (3 downto 0) := "0000";
begin

process(clr, clk)
begin 
   if(clr ='1') then 
	  salReg <= "0000";
	elsif (falling_edge(clk))then 
	   if(LF='1')then 
		  -- banderas2 <= banderas;
		   salReg <= banderas;
			end if;
	end if;
end process;

end Behavioral;
