LIBRARY ieee;
LIBRARY STD;
USE STD.TEXTIO.ALL;
USE ieee.std_logic_TEXTIO.ALL;	--PERMITE USAR STD_LOGIC 

USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;
 
ENTITY P5_MemoriaDatos_TestBench IS
END P5_MemoriaDatos_TestBench;
 
ARCHITECTURE behavior OF P5_MemoriaDatos_TestBench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT P5_MemoriaDatos
    PORT(
         dir_w : IN  std_logic_vector(11 downto 0);
         data_in : IN  std_logic_vector(15 downto 0);
         clk : IN  std_logic;
         wd : IN  std_logic;
         data_out : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal dir_w : std_logic_vector(11 downto 0) := (others => '0');
   signal data_in : std_logic_vector(15 downto 0) := (others => '0');
   signal clk : std_logic := '0';
   signal wd : std_logic := '0';

 	--Outputs
   signal data_out : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 50 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: P5_MemoriaDatos PORT MAP (
          dir_w => dir_w,
          data_in => data_in,
          clk => clk,
          wd => wd,
          data_out => data_out
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
		file ARCH_RES : TEXT;
		variable LINEA_RES : line;
		VARIABLE VAR_data_out : STD_LOGIC_VECTOR(15 DOWNTO 0);

		file ARCH_VEC : TEXT;
		variable LINEA_VEC : line;
		VARIABLE VAR_data_in : STD_LOGIC_VECTOR(15 DOWNTO 0);
		VARIABLE VAR_dir_w : STD_LOGIC_VECTOR(11 DOWNTO 0);
		VARIABLE VAR_wd : STD_LOGIC;
		VARIABLE CADENA : STRING(1 TO 4);
		begin
			file_open(ARCH_VEC, "vectores.txt", READ_MODE);
			file_open(ARCH_RES, "resultados.txt", WRITE_MODE);
			
			--ESCRIBE LA CABECERA
			CADENA := "   A";
			write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE Direccion
			CADENA := "  Di";
			write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE Dato de entrada
			CADENA := "  WD";
			write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE Write Data
			CADENA := "  Do";
			write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE Dato de salida
			writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo
			
			wait for 100 ns;
			FOR I IN 0 TO 11 LOOP
				readline(ARCH_VEC, LINEA_VEC);
				
				Hread(LINEA_VEC, VAR_data_in);
				data_in <= VAR_data_in;
				
				Hread(LINEA_VEC, VAR_dir_w);
				dir_w <= "0" & VAR_dir_w(10 downto 0);
				
				read(LINEA_VEC, VAR_wd);
				wd <= VAR_wd;
				
			WAIT UNTIL RISING_EDGE(CLK);
			
			VAR_data_out := data_out;
			
			Hwrite(LINEA_RES, VAR_dir_w, right, 5); --ESCRIBE EN EL CAMPO A (dir_w)
			Hwrite(LINEA_RES, VAR_data_in, right, 5); --ESCRIBE EN EL CAMPO Di (data_in)
			write(LINEA_RES, VAR_wd, right, 5); --ESCRIBE EN EL CAMPO wd
			Hwrite(LINEA_RES, VAR_data_out, right, 5); --ESCRIBE EN EL CAMPO Do (data_out)
			
			writeline(ARCH_RES, LINEA_RES); --Escribe la linea en el archivo de resultados
		end loop;
		file_close(ARCH_VEC);
		file_close(ARCH_RES);
      wait;
   end process;

END;
