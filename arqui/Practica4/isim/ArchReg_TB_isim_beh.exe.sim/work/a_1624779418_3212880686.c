/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/Caos/Desktop/ESCOM/2017-2/Materias/Arqui/Practicas/Practica4/Lab4/Practica4/ArchReg.vhd";
extern char *IEEE_P_2592010699;
extern char *IEEE_P_3620187407;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
int ieee_p_3620187407_sub_514432868_3965413181(char *, char *, char *);


static void work_a_1624779418_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    char *t7;
    unsigned char t8;
    unsigned int t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    unsigned char t15;
    unsigned char t16;
    unsigned char t17;
    unsigned char t18;
    unsigned char t19;
    unsigned char t20;
    int t21;
    int t22;
    unsigned int t23;
    unsigned int t24;
    char *t25;
    char *t26;
    int t27;
    int t28;
    int t29;
    int t30;
    int t31;
    int t32;
    int t33;
    int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;

LAB0:    xsi_set_current_line(37, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    t1 = (t0 + 992U);
    t3 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t3 != 0)
        goto LAB7;

LAB8:
LAB3:    t1 = (t0 + 5168);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(38, ng0);
    t1 = xsi_get_transient_memory(256U);
    memset(t1, 0, 256U);
    t5 = t1;
    t6 = (t0 + 9689);
    t8 = (16U != 0);
    if (t8 == 1)
        goto LAB5;

LAB6:    t10 = (t0 + 5280);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t1, 256U);
    xsi_driver_first_trans_fast(t10);
    goto LAB3;

LAB5:    t9 = (256U / 16U);
    xsi_mem_set_data(t5, t6, 16U, t9);
    goto LAB6;

LAB7:    xsi_set_current_line(40, ng0);
    t2 = (t0 + 1512U);
    t5 = *((char **)t2);
    t15 = *((unsigned char *)t5);
    t16 = (t15 == (unsigned char)3);
    if (t16 == 1)
        goto LAB15;

LAB16:    t8 = (unsigned char)0;

LAB17:    if (t8 == 1)
        goto LAB12;

LAB13:    t4 = (unsigned char)0;

LAB14:    if (t4 != 0)
        goto LAB9;

LAB11:
LAB10:    xsi_set_current_line(45, ng0);
    t1 = (t0 + 1512U);
    t2 = *((char **)t1);
    t8 = *((unsigned char *)t2);
    t15 = (t8 == (unsigned char)3);
    if (t15 == 1)
        goto LAB24;

LAB25:    t4 = (unsigned char)0;

LAB26:    if (t4 == 1)
        goto LAB21;

LAB22:    t3 = (unsigned char)0;

LAB23:    if (t3 != 0)
        goto LAB18;

LAB20:
LAB19:    xsi_set_current_line(65, ng0);
    t1 = (t0 + 1512U);
    t2 = *((char **)t1);
    t8 = *((unsigned char *)t2);
    t15 = (t8 == (unsigned char)3);
    if (t15 == 1)
        goto LAB49;

LAB50:    t4 = (unsigned char)0;

LAB51:    if (t4 == 1)
        goto LAB46;

LAB47:    t3 = (unsigned char)0;

LAB48:    if (t3 != 0)
        goto LAB43;

LAB45:
LAB44:    goto LAB3;

LAB9:    xsi_set_current_line(43, ng0);
    t2 = (t0 + 1992U);
    t10 = *((char **)t2);
    t2 = (t0 + 1832U);
    t11 = *((char **)t2);
    t2 = (t0 + 9208U);
    t21 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t11, t2);
    t22 = (t21 - 0);
    t9 = (t22 * 1);
    t23 = (16U * t9);
    t24 = (0U + t23);
    t12 = (t0 + 5280);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    t25 = (t14 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t10, 16U);
    xsi_driver_first_trans_delta(t12, t24, 16U, 0LL);
    goto LAB10;

LAB12:    t2 = (t0 + 1352U);
    t7 = *((char **)t2);
    t19 = *((unsigned char *)t7);
    t20 = (t19 == (unsigned char)2);
    t4 = t20;
    goto LAB14;

LAB15:    t2 = (t0 + 1672U);
    t6 = *((char **)t2);
    t17 = *((unsigned char *)t6);
    t18 = (t17 == (unsigned char)2);
    t8 = t18;
    goto LAB17;

LAB18:    xsi_set_current_line(46, ng0);
    t1 = (t0 + 2952U);
    t7 = *((char **)t1);
    t1 = (t0 + 2152U);
    t10 = *((char **)t1);
    t1 = (t0 + 9240U);
    t21 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t10, t1);
    t22 = (t21 - 0);
    t9 = (t22 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t21);
    t23 = (16U * t9);
    t24 = (0 + t23);
    t11 = (t7 + t24);
    t12 = (t0 + 3368U);
    t13 = *((char **)t12);
    t12 = (t13 + 0);
    memcpy(t12, t11, 16U);
    xsi_set_current_line(49, ng0);
    t1 = (t0 + 9705);
    *((int *)t1) = 0;
    t2 = (t0 + 9709);
    *((int *)t2) = 3;
    t21 = 0;
    t22 = 3;

LAB27:    if (t21 <= t22)
        goto LAB28;

LAB30:    xsi_set_current_line(62, ng0);
    t1 = (t0 + 3368U);
    t2 = *((char **)t1);
    t1 = (t0 + 1832U);
    t5 = *((char **)t1);
    t1 = (t0 + 9208U);
    t21 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t5, t1);
    t22 = (t21 - 0);
    t9 = (t22 * 1);
    t23 = (16U * t9);
    t24 = (0U + t23);
    t6 = (t0 + 5280);
    t7 = (t6 + 56U);
    t10 = *((char **)t7);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    memcpy(t12, t2, 16U);
    xsi_driver_first_trans_delta(t6, t24, 16U, 0LL);
    goto LAB19;

LAB21:    t1 = (t0 + 1352U);
    t6 = *((char **)t1);
    t18 = *((unsigned char *)t6);
    t19 = (t18 == (unsigned char)2);
    t3 = t19;
    goto LAB23;

LAB24:    t1 = (t0 + 1672U);
    t5 = *((char **)t1);
    t16 = *((unsigned char *)t5);
    t17 = (t16 == (unsigned char)3);
    t4 = t17;
    goto LAB26;

LAB28:    xsi_set_current_line(50, ng0);
    t27 = (16 - 1);
    t5 = (t0 + 9713);
    *((int *)t5) = 0;
    t6 = (t0 + 9717);
    *((int *)t6) = t27;
    t28 = 0;
    t29 = t27;

LAB31:    if (t28 <= t29)
        goto LAB32;

LAB34:
LAB29:    t1 = (t0 + 9705);
    t21 = *((int *)t1);
    t2 = (t0 + 9709);
    t22 = *((int *)t2);
    if (t21 == t22)
        goto LAB30;

LAB42:    t27 = (t21 + 1);
    t21 = t27;
    t5 = (t0 + 9705);
    *((int *)t5) = t21;
    goto LAB27;

LAB32:    xsi_set_current_line(51, ng0);
    t7 = (t0 + 2472U);
    t10 = *((char **)t7);
    t7 = (t0 + 9705);
    t30 = *((int *)t7);
    t31 = (t30 - 3);
    t9 = (t31 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t7));
    t23 = (1U * t9);
    t24 = (0 + t23);
    t11 = (t10 + t24);
    t3 = *((unsigned char *)t11);
    t4 = (t3 == (unsigned char)2);
    if (t4 != 0)
        goto LAB35;

LAB37:    xsi_set_current_line(54, ng0);
    t1 = (t0 + 9713);
    t2 = (t0 + 9705);
    t27 = xsi_vhdl_pow(2, *((int *)t2));
    t30 = *((int *)t1);
    t31 = (t30 + t27);
    t32 = (16 - 1);
    t3 = (t31 > t32);
    if (t3 != 0)
        goto LAB38;

LAB40:    xsi_set_current_line(57, ng0);
    t1 = (t0 + 3368U);
    t2 = *((char **)t1);
    t1 = (t0 + 9713);
    t5 = (t0 + 9705);
    t27 = xsi_vhdl_pow(2, *((int *)t5));
    t30 = *((int *)t1);
    t31 = (t30 + t27);
    t32 = (t31 - 15);
    t9 = (t32 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, t31);
    t23 = (1U * t9);
    t24 = (0 + t23);
    t6 = (t2 + t24);
    t3 = *((unsigned char *)t6);
    t7 = (t0 + 3368U);
    t10 = *((char **)t7);
    t7 = (t0 + 9713);
    t33 = *((int *)t7);
    t34 = (t33 - 15);
    t35 = (t34 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t7));
    t36 = (1U * t35);
    t37 = (0 + t36);
    t11 = (t10 + t37);
    *((unsigned char *)t11) = t3;

LAB39:
LAB36:
LAB33:    t1 = (t0 + 9713);
    t28 = *((int *)t1);
    t2 = (t0 + 9717);
    t29 = *((int *)t2);
    if (t28 == t29)
        goto LAB34;

LAB41:    t27 = (t28 + 1);
    t28 = t27;
    t5 = (t0 + 9713);
    *((int *)t5) = t28;
    goto LAB31;

LAB35:    xsi_set_current_line(52, ng0);
    t12 = (t0 + 3368U);
    t13 = *((char **)t12);
    t12 = (t0 + 3368U);
    t14 = *((char **)t12);
    t12 = (t14 + 0);
    memcpy(t12, t13, 16U);
    goto LAB36;

LAB38:    xsi_set_current_line(55, ng0);
    t5 = (t0 + 3368U);
    t6 = *((char **)t5);
    t5 = (t0 + 9713);
    t33 = *((int *)t5);
    t34 = (t33 - 15);
    t9 = (t34 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t5));
    t23 = (1U * t9);
    t24 = (0 + t23);
    t7 = (t6 + t24);
    *((unsigned char *)t7) = (unsigned char)2;
    goto LAB39;

LAB43:    xsi_set_current_line(66, ng0);
    t1 = (t0 + 2952U);
    t7 = *((char **)t1);
    t1 = (t0 + 2152U);
    t10 = *((char **)t1);
    t1 = (t0 + 9240U);
    t21 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t10, t1);
    t22 = (t21 - 0);
    t9 = (t22 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t21);
    t23 = (16U * t9);
    t24 = (0 + t23);
    t11 = (t7 + t24);
    t12 = (t0 + 3368U);
    t13 = *((char **)t12);
    t12 = (t13 + 0);
    memcpy(t12, t11, 16U);
    xsi_set_current_line(69, ng0);
    t1 = (t0 + 9721);
    *((int *)t1) = 0;
    t2 = (t0 + 9725);
    *((int *)t2) = 3;
    t21 = 0;
    t22 = 3;

LAB52:    if (t21 <= t22)
        goto LAB53;

LAB55:    xsi_set_current_line(82, ng0);
    t1 = (t0 + 3368U);
    t2 = *((char **)t1);
    t1 = (t0 + 1832U);
    t5 = *((char **)t1);
    t1 = (t0 + 9208U);
    t21 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t5, t1);
    t22 = (t21 - 0);
    t9 = (t22 * 1);
    t23 = (16U * t9);
    t24 = (0U + t23);
    t6 = (t0 + 5280);
    t7 = (t6 + 56U);
    t10 = *((char **)t7);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    memcpy(t12, t2, 16U);
    xsi_driver_first_trans_delta(t6, t24, 16U, 0LL);
    goto LAB44;

LAB46:    t1 = (t0 + 1352U);
    t6 = *((char **)t1);
    t18 = *((unsigned char *)t6);
    t19 = (t18 == (unsigned char)3);
    t3 = t19;
    goto LAB48;

LAB49:    t1 = (t0 + 1672U);
    t5 = *((char **)t1);
    t16 = *((unsigned char *)t5);
    t17 = (t16 == (unsigned char)3);
    t4 = t17;
    goto LAB51;

LAB53:    xsi_set_current_line(70, ng0);
    t27 = (16 - 1);
    t5 = (t0 + 9729);
    *((int *)t5) = t27;
    t6 = (t0 + 9733);
    *((int *)t6) = 0;
    t28 = t27;
    t29 = 0;

LAB56:    if (t28 >= t29)
        goto LAB57;

LAB59:
LAB54:    t1 = (t0 + 9721);
    t21 = *((int *)t1);
    t2 = (t0 + 9725);
    t22 = *((int *)t2);
    if (t21 == t22)
        goto LAB55;

LAB67:    t27 = (t21 + 1);
    t21 = t27;
    t5 = (t0 + 9721);
    *((int *)t5) = t21;
    goto LAB52;

LAB57:    xsi_set_current_line(71, ng0);
    t7 = (t0 + 2472U);
    t10 = *((char **)t7);
    t7 = (t0 + 9721);
    t30 = *((int *)t7);
    t31 = (t30 - 3);
    t9 = (t31 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t7));
    t23 = (1U * t9);
    t24 = (0 + t23);
    t11 = (t10 + t24);
    t3 = *((unsigned char *)t11);
    t4 = (t3 == (unsigned char)2);
    if (t4 != 0)
        goto LAB60;

LAB62:    xsi_set_current_line(74, ng0);
    t1 = (t0 + 9729);
    t2 = (t0 + 9721);
    t27 = xsi_vhdl_pow(2, *((int *)t2));
    t30 = *((int *)t1);
    t31 = (t30 - t27);
    t3 = (t31 < 0);
    if (t3 != 0)
        goto LAB63;

LAB65:    xsi_set_current_line(77, ng0);
    t1 = (t0 + 3368U);
    t2 = *((char **)t1);
    t1 = (t0 + 9729);
    t5 = (t0 + 9721);
    t27 = xsi_vhdl_pow(2, *((int *)t5));
    t30 = *((int *)t1);
    t31 = (t30 - t27);
    t32 = (t31 - 15);
    t9 = (t32 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, t31);
    t23 = (1U * t9);
    t24 = (0 + t23);
    t6 = (t2 + t24);
    t3 = *((unsigned char *)t6);
    t7 = (t0 + 3368U);
    t10 = *((char **)t7);
    t7 = (t0 + 9729);
    t33 = *((int *)t7);
    t34 = (t33 - 15);
    t35 = (t34 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t7));
    t36 = (1U * t35);
    t37 = (0 + t36);
    t11 = (t10 + t37);
    *((unsigned char *)t11) = t3;

LAB64:
LAB61:
LAB58:    t1 = (t0 + 9729);
    t28 = *((int *)t1);
    t2 = (t0 + 9733);
    t29 = *((int *)t2);
    if (t28 == t29)
        goto LAB59;

LAB66:    t27 = (t28 + -1);
    t28 = t27;
    t5 = (t0 + 9729);
    *((int *)t5) = t28;
    goto LAB56;

LAB60:    xsi_set_current_line(72, ng0);
    t12 = (t0 + 3368U);
    t13 = *((char **)t12);
    t12 = (t0 + 3368U);
    t14 = *((char **)t12);
    t12 = (t14 + 0);
    memcpy(t12, t13, 16U);
    goto LAB61;

LAB63:    xsi_set_current_line(75, ng0);
    t5 = (t0 + 3368U);
    t6 = *((char **)t5);
    t5 = (t0 + 9729);
    t32 = *((int *)t5);
    t33 = (t32 - 15);
    t9 = (t33 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t5));
    t23 = (1U * t9);
    t24 = (0 + t23);
    t7 = (t6 + t24);
    *((unsigned char *)t7) = (unsigned char)2;
    goto LAB64;

}

static void work_a_1624779418_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    xsi_set_current_line(88, ng0);

LAB3:    t1 = (t0 + 2952U);
    t2 = *((char **)t1);
    t1 = (t0 + 2152U);
    t3 = *((char **)t1);
    t1 = (t0 + 9240U);
    t4 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t3, t1);
    t5 = (t4 - 0);
    t6 = (t5 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t4);
    t7 = (16U * t6);
    t8 = (0 + t7);
    t9 = (t2 + t8);
    t10 = (t0 + 5344);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t9, 16U);
    xsi_driver_first_trans_fast_port(t10);

LAB2:    t15 = (t0 + 5184);
    *((int *)t15) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_1624779418_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    xsi_set_current_line(90, ng0);

LAB3:    t1 = (t0 + 2952U);
    t2 = *((char **)t1);
    t1 = (t0 + 2312U);
    t3 = *((char **)t1);
    t1 = (t0 + 9256U);
    t4 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t3, t1);
    t5 = (t4 - 0);
    t6 = (t5 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t4);
    t7 = (16U * t6);
    t8 = (0 + t7);
    t9 = (t2 + t8);
    t10 = (t0 + 5408);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t9, 16U);
    xsi_driver_first_trans_fast_port(t10);

LAB2:    t15 = (t0 + 5200);
    *((int *)t15) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}


extern void work_a_1624779418_3212880686_init()
{
	static char *pe[] = {(void *)work_a_1624779418_3212880686_p_0,(void *)work_a_1624779418_3212880686_p_1,(void *)work_a_1624779418_3212880686_p_2};
	xsi_register_didat("work_a_1624779418_3212880686", "isim/ArchReg_TB_isim_beh.exe.sim/work/a_1624779418_3212880686.didat");
	xsi_register_executes(pe);
}
