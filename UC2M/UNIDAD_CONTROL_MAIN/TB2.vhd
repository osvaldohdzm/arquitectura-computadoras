LIBRARY ieee;
LIBRARY STD;
USE STD.TEXTIO.ALL;
USE ieee.std_logic_TEXTIO.ALL;	

USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_UNSIGNED.ALL;
USE ieee.std_logic_ARITH.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TB2 IS
END TB2;
 
ARCHITECTURE behavior OF TB2 IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT PRINCIPAL
    PORT(
         CLK : IN  std_logic;
         CLR : IN  std_logic;
         LF : IN  std_logic;
         COD_FUNC : IN  std_logic_vector(3 downto 0);
         BANDERAS : IN  std_logic_vector(3 downto 0);
         S : OUT  std_logic_vector(19 downto 0);
         COD_OP : IN  std_logic_vector(4 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal CLR : std_logic := '0';
   signal LF : std_logic := '0';
   signal COD_FUNC : std_logic_vector(3 downto 0) := (others => '0');
   signal BANDERAS : std_logic_vector(3 downto 0) := (others => '0');
   signal COD_OP : std_logic_vector(4 downto 0) := (others => '0');

 	--Outputs
   signal S : std_logic_vector(19 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: PRINCIPAL PORT MAP (
          CLK => CLK,
          CLR => CLR,
          LF => LF,
          COD_FUNC => COD_FUNC,
          BANDERAS => BANDERAS,
          S => S,
          COD_OP => COD_OP
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 
   stim_proc: process
	file ARCH_RES : TEXT;
	variable LINEA_RES : line;
	VARIABLE VAR_s : STD_LOGIC_VECTOR(19 DOWNTO 0);
	
	file ARCH_VEC : TEXT;
	variable LINEA_VEC : line;
	VARIABLE NIVEL : STRING(1 TO 4);
	VARIABLE VAR_clr : STD_LOGIC;
	VARIABLE VAR_lf : STD_LOGIC;
	VARIABLE VAR_COD_FUNC : STD_LOGIC_VECTOR(3 downto 0);
	VARIABLE VAR_banderas : STD_LOGIC_VECTOR(3 downto 0);
	VARIABLE VAR_COD_OP : STD_LOGIC_VECTOR(4 downto 0);
	VARIABLE CADENA : STRING(1 TO 8);
	VARIABLE CADENA_INST : STRING(1 TO 22);
	begin
		file_open(ARCH_VEC, "vectores.txt", READ_MODE);
		file_open(ARCH_RES, "resultados.txt", WRITE_MODE);
		
		--ESCRIBE LA CABECERA
		CADENA := " COD_OP";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE
		CADENA := "FUN_CODE";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE
		CADENA := "BANDERAS";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE
		CADENA := "     CLR";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE
		CADENA := "      LF";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE
		CADENA_INST := "      MICROINSTRUCCION";
		write(LINEA_RES, CADENA_INST, right, CADENA'LENGTH+1);	--ESCRIBE
		CADENA := "   NIVEL";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE
		writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo
		
		clr <= '1';
		wait for 5 ns;
		clr <= '0';
		
		WHILE NOT ENDFILE (ARCH_VEC) LOOP
			WAIT UNTIL RISING_EDGE(CLK);
			readline(ARCH_VEC, LINEA_VEC);				
			
			read(LINEA_VEC, VAR_COD_OP);
			COD_OP <= VAR_COD_OP;
			
			read(LINEA_VEC, VAR_COD_FUNC);
			COD_FUNC <= VAR_COD_FUNC;
			
			read(LINEA_VEC, VAR_banderas);
			banderas <= VAR_banderas;
			
			read(LINEA_VEC, VAR_clr);
			clr <= VAR_clr;
				
			read(LINEA_VEC, VAR_lf);
			lf <= VAR_lf;
			
			wait for 5 ns;
			VAR_s := s;
			NIVEL := "ALTO";
			
			write(LINEA_RES, VAR_COD_OP, right, 9);
			write(LINEA_RES, VAR_COD_FUNC, right, 9);
			write(LINEA_RES, VAR_banderas, right, 9);
			write(LINEA_RES, VAR_clr, right, 9);
			write(LINEA_RES, VAR_lf, right, 9);
			write(LINEA_RES, VAR_s, right, 22);
			write(LINEA_RES, NIVEL, right, 9);
			WRITELINE(ARCH_RES, LINEA_RES);
			
			WAIT UNTIL FALLING_EDGE(CLK);
			wait for 5 ns;
			VAR_s := s;
			NIVEL := "BAJO";
			
			write(LINEA_RES, VAR_COD_OP, right, 9);
			write(LINEA_RES, VAR_COD_FUNC, right, 9);
			write(LINEA_RES, VAR_banderas, right, 9);
			write(LINEA_RES, VAR_clr, right, 9);
			write(LINEA_RES, VAR_lf, right, 9);
			write(LINEA_RES, VAR_s, right, 22);
			write(LINEA_RES, NIVEL, right, 9);
			WRITELINE(ARCH_RES, LINEA_RES);
			
			writeline(ARCH_RES, LINEA_RES); --Escribe la linea en el archivo de resultados
		end loop;
		file_close(ARCH_VEC);
		file_close(ARCH_RES);
      wait;
   end process;

END;
   -- Stimulus process
    -- Stimulus proces