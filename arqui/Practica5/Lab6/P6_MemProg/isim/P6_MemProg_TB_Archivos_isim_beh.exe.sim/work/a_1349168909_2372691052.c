/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/Caos/Desktop/ESCOM/2017-2/Materias/Arqui/Practicas/Practica6/Lab6/P6_MemProg/P6_MemProg_TB_Archivos.vhd";
extern char *STD_TEXTIO;
extern char *IEEE_P_3564397177;

void ieee_p_3564397177_sub_1281154728_91900896(char *, char *, char *, char *, char *, unsigned char , int );
void ieee_p_3564397177_sub_3205433008_91900896(char *, char *, char *, char *, char *, unsigned char , int );
void ieee_p_3564397177_sub_3988856810_91900896(char *, char *, char *, char *, char *);


static void work_a_1349168909_2372691052_p_0(char *t0)
{
    char t5[16];
    char t10[8];
    char t11[8];
    char t12[8];
    char t13[8];
    char t14[8];
    char t15[8];
    char t16[8];
    char t24[8];
    char t25[16];
    char t27[8];
    char t28[8];
    char t29[8];
    char t30[8];
    char t31[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    int t8;
    unsigned int t9;
    int64 t17;
    unsigned char t18;
    unsigned char t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    unsigned int t26;

LAB0:    t1 = (t0 + 3088U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(60, ng0);
    t2 = (t0 + 1936U);
    t3 = (t0 + 6869);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 12;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (12 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)0);
    xsi_set_current_line(61, ng0);
    t2 = (t0 + 2040U);
    t3 = (t0 + 6881);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 10;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (10 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)1);
    xsi_set_current_line(63, ng0);
    t2 = (t0 + 6891);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(64, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2288U);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t10, t7, 5U);
    t6 = (t0 + 6608U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t10, t6, (unsigned char)0, t8);
    xsi_set_current_line(65, ng0);
    t2 = (t0 + 6896);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(66, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2288U);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t11, t7, 5U);
    t6 = (t0 + 6608U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t11, t6, (unsigned char)0, t8);
    xsi_set_current_line(67, ng0);
    t2 = (t0 + 6901);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(68, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2288U);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t12, t7, 5U);
    t6 = (t0 + 6608U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t12, t6, (unsigned char)0, t8);
    xsi_set_current_line(69, ng0);
    t2 = (t0 + 6906);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(70, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2288U);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t13, t7, 5U);
    t6 = (t0 + 6608U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t13, t6, (unsigned char)0, t8);
    xsi_set_current_line(71, ng0);
    t2 = (t0 + 6911);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(72, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2288U);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t14, t7, 5U);
    t6 = (t0 + 6608U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t14, t6, (unsigned char)0, t8);
    xsi_set_current_line(73, ng0);
    t2 = (t0 + 6916);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(74, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2288U);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t15, t7, 5U);
    t6 = (t0 + 6608U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t15, t6, (unsigned char)0, t8);
    xsi_set_current_line(75, ng0);
    t2 = (t0 + 6921);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(76, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2288U);
    t4 = (t0 + 2504U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t16, t7, 5U);
    t6 = (t0 + 6608U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t16, t6, (unsigned char)0, t8);
    xsi_set_current_line(78, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2040U);
    t4 = (t0 + 2288U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    xsi_set_current_line(80, ng0);
    t17 = (20 * 1000LL);
    t2 = (t0 + 2896);
    xsi_process_wait(t2, t17);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(83, ng0);

LAB8:    t2 = (t0 + 1936U);
    t18 = std_textio_endfile(t2);
    t19 = (!(t18));
    if (t19 != 0)
        goto LAB9;

LAB11:    xsi_set_current_line(106, ng0);
    t2 = (t0 + 1936U);
    std_textio_file_close(t2);
    xsi_set_current_line(107, ng0);
    t2 = (t0 + 2040U);
    std_textio_file_close(t2);
    xsi_set_current_line(109, ng0);

LAB18:    *((char **)t1) = &&LAB19;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB9:    xsi_set_current_line(84, ng0);
    t3 = (t0 + 2896);
    t4 = (t0 + 1936U);
    t6 = (t0 + 2216U);
    std_textio_readline(STD_TEXTIO, t3, t4, t6);
    xsi_set_current_line(86, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2216U);
    t4 = (t0 + 1488U);
    t6 = *((char **)t4);
    t4 = (t0 + 6576U);
    ieee_p_3564397177_sub_3988856810_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(87, ng0);
    t2 = (t0 + 1488U);
    t3 = *((char **)t2);
    t9 = (15 - 10);
    t20 = (t9 * 1U);
    t21 = (0 + t20);
    t2 = (t3 + t21);
    t4 = (t0 + 3472);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t22 = (t7 + 56U);
    t23 = *((char **)t22);
    memcpy(t23, t2, 11U);
    xsi_driver_first_trans_fast(t4);
    xsi_set_current_line(89, ng0);
    t17 = (20 * 1000LL);
    t2 = (t0 + 2896);
    xsi_process_wait(t2, t17);

LAB14:    *((char **)t1) = &&LAB15;
    goto LAB1;

LAB10:;
LAB12:    xsi_set_current_line(91, ng0);
    t2 = (t0 + 1192U);
    t3 = *((char **)t2);
    t2 = (t0 + 1608U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 25U);
    xsi_set_current_line(94, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2288U);
    t4 = (t0 + 1488U);
    t6 = *((char **)t4);
    memcpy(t5, t6, 16U);
    t4 = (t0 + 6576U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t5, t4, (unsigned char)0, 5);
    xsi_set_current_line(95, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2288U);
    t4 = (t0 + 1608U);
    t6 = *((char **)t4);
    t9 = (24 - 24);
    t20 = (t9 * 1U);
    t21 = (0 + t20);
    t4 = (t6 + t21);
    memcpy(t24, t4, 5U);
    t7 = (t25 + 0U);
    t22 = (t7 + 0U);
    *((int *)t22) = 24;
    t22 = (t7 + 4U);
    *((int *)t22) = 20;
    t22 = (t7 + 8U);
    *((int *)t22) = -1;
    t8 = (20 - 24);
    t26 = (t8 * -1);
    t26 = (t26 + 1);
    t22 = (t7 + 12U);
    *((unsigned int *)t22) = t26;
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t24, t25, (unsigned char)0, 6);
    xsi_set_current_line(96, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2288U);
    t4 = (t0 + 1608U);
    t6 = *((char **)t4);
    t9 = (24 - 19);
    t20 = (t9 * 1U);
    t21 = (0 + t20);
    t4 = (t6 + t21);
    memcpy(t27, t4, 4U);
    t7 = (t25 + 0U);
    t22 = (t7 + 0U);
    *((int *)t22) = 19;
    t22 = (t7 + 4U);
    *((int *)t22) = 16;
    t22 = (t7 + 8U);
    *((int *)t22) = -1;
    t8 = (16 - 19);
    t26 = (t8 * -1);
    t26 = (t26 + 1);
    t22 = (t7 + 12U);
    *((unsigned int *)t22) = t26;
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t27, t25, (unsigned char)0, 6);
    xsi_set_current_line(97, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2288U);
    t4 = (t0 + 1608U);
    t6 = *((char **)t4);
    t9 = (24 - 15);
    t20 = (t9 * 1U);
    t21 = (0 + t20);
    t4 = (t6 + t21);
    memcpy(t28, t4, 4U);
    t7 = (t25 + 0U);
    t22 = (t7 + 0U);
    *((int *)t22) = 15;
    t22 = (t7 + 4U);
    *((int *)t22) = 12;
    t22 = (t7 + 8U);
    *((int *)t22) = -1;
    t8 = (12 - 15);
    t26 = (t8 * -1);
    t26 = (t26 + 1);
    t22 = (t7 + 12U);
    *((unsigned int *)t22) = t26;
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t28, t25, (unsigned char)0, 6);
    xsi_set_current_line(98, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2288U);
    t4 = (t0 + 1608U);
    t6 = *((char **)t4);
    t9 = (24 - 11);
    t20 = (t9 * 1U);
    t21 = (0 + t20);
    t4 = (t6 + t21);
    memcpy(t29, t4, 4U);
    t7 = (t25 + 0U);
    t22 = (t7 + 0U);
    *((int *)t22) = 11;
    t22 = (t7 + 4U);
    *((int *)t22) = 8;
    t22 = (t7 + 8U);
    *((int *)t22) = -1;
    t8 = (8 - 11);
    t26 = (t8 * -1);
    t26 = (t26 + 1);
    t22 = (t7 + 12U);
    *((unsigned int *)t22) = t26;
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t29, t25, (unsigned char)0, 6);
    xsi_set_current_line(99, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2288U);
    t4 = (t0 + 1608U);
    t6 = *((char **)t4);
    t9 = (24 - 7);
    t20 = (t9 * 1U);
    t21 = (0 + t20);
    t4 = (t6 + t21);
    memcpy(t30, t4, 4U);
    t7 = (t25 + 0U);
    t22 = (t7 + 0U);
    *((int *)t22) = 7;
    t22 = (t7 + 4U);
    *((int *)t22) = 4;
    t22 = (t7 + 8U);
    *((int *)t22) = -1;
    t8 = (4 - 7);
    t26 = (t8 * -1);
    t26 = (t26 + 1);
    t22 = (t7 + 12U);
    *((unsigned int *)t22) = t26;
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t30, t25, (unsigned char)0, 6);
    xsi_set_current_line(100, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2288U);
    t4 = (t0 + 1608U);
    t6 = *((char **)t4);
    t9 = (24 - 3);
    t20 = (t9 * 1U);
    t21 = (0 + t20);
    t4 = (t6 + t21);
    memcpy(t31, t4, 4U);
    t7 = (t25 + 0U);
    t22 = (t7 + 0U);
    *((int *)t22) = 3;
    t22 = (t7 + 4U);
    *((int *)t22) = 0;
    t22 = (t7 + 8U);
    *((int *)t22) = -1;
    t8 = (0 - 3);
    t26 = (t8 * -1);
    t26 = (t26 + 1);
    t22 = (t7 + 12U);
    *((unsigned int *)t22) = t26;
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t31, t25, (unsigned char)0, 6);
    xsi_set_current_line(102, ng0);
    t2 = (t0 + 2896);
    t3 = (t0 + 2040U);
    t4 = (t0 + 2288U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    goto LAB8;

LAB13:    goto LAB12;

LAB15:    goto LAB13;

LAB16:    goto LAB2;

LAB17:    goto LAB16;

LAB19:    goto LAB17;

}


extern void work_a_1349168909_2372691052_init()
{
	static char *pe[] = {(void *)work_a_1349168909_2372691052_p_0};
	xsi_register_didat("work_a_1349168909_2372691052", "isim/P6_MemProg_TB_Archivos_isim_beh.exe.sim/work/a_1349168909_2372691052.didat");
	xsi_register_executes(pe);
}
