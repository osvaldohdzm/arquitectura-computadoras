--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:33:55 03/08/2018
-- Design Name:   
-- Module Name:   C:/Users/Edgar Roa/Documents/8vo sem/Arquitectura/Practica1/Lab_02032017/Lab1_Arqui/pruebasT.vhd
-- Project Name:  Lab1_Arqui
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: cascada
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY pruebasT IS
END pruebasT;
 
ARCHITECTURE behavior OF pruebasT IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT cascada
    PORT(
         cin : IN  std_logic;
         a : IN  std_logic_vector(3 downto 0);
         b : IN  std_logic_vector(3 downto 0);
         s : OUT  std_logic_vector(3 downto 0);
         cout : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal cin : std_logic := '0';
   signal a : std_logic_vector(3 downto 0) := (others => '0');
   signal b : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal s : std_logic_vector(3 downto 0);
   signal cout : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 

 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: cascada PORT MAP (
          cin => cin,
          a => a,
          b => b,
          s => s,
          cout => cout
        ); 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		--con 1 restamos
			cin <= '0';
			a <= "0101";
			b <= "0101";
		wait for 100 ns;--5+5=10
			
			cin <= '0';
			a <= "1100";
			b <= "1000";
		wait for 100 ns;--12+8=4
		
			cin <= '0';
			a <= "1001";
			b <= "0101";--9+5=14
		wait for 100 ns;
		
			cin <= '1';
			a <= "1010";
			b <= "1001";
		wait for 100 ns;--10-9=1
		
			cin <= '0';
			a <= "0100";
			b <= "0010";--4+2=6
		wait for 100 ns;
		
			cin <= '1';
			a <= "0111";
			b <= "1001";
		wait for 100 ns;--7-9=14

			cin <= '1';
			a <= "1111";
			b <= "1111";
		wait for 100 ns;--15-15=0

			cin <= '1';
			a <= "1011";
			b <= "1000";
		wait for 100 ns;--11-8=3
		
			cin <= '1';
			a <= "0001";
			b <= "0100";
		wait for 100 ns;--1-4=13

      -- insert stimulus here 

      wait;
   end process;

END;
