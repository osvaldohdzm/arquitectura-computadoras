library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity nivel is
    Port ( clk, clr : in  STD_LOGIC;
           na : out  STD_LOGIC);
end nivel;

architecture Behavioral of nivel is
signal p_clk, n_clk : STD_LOGIC;
begin
	nivel_alto : process(clk, clr)
	begin
		if (clr='1') then
			p_clk <= '0';
		else
			if (rising_edge(clk)) then
				p_clk <= not p_clk;
			end if;
		end if;
	end process;
	
	nivel_bajo : process(clk, clr)
	begin
		if (clr='1') then
			n_clk <= '0';
		else
			if (falling_edge(clk)) then
				n_clk <= not n_clk;
			end if;
		end if;
	end process;
	
	na <= p_clk xor n_clk;

end Behavioral;

