LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

LIBRARY STD;
USE STD.TEXTIO.ALL;
USE ieee.std_logic_TEXTIO.ALL;

ENTITY P6_TB IS
END P6_TB;
 
ARCHITECTURE behavior OF P6_TB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT practica6
    PORT(
         pcData : IN  std_logic_vector(10 downto 0);
         L : IN  std_logic;
         UP : IN  std_logic;
         DW : IN  std_logic;
         clk : IN  std_logic;
         clr : IN  std_logic;
         instrucc : OUT  std_logic_vector(24 downto 0);
         stackPointer : OUT  std_logic_vector(2 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal pcData : std_logic_vector(10 downto 0) := (others => '0');
   signal L : std_logic := '0';
   signal UP : std_logic := '0';
   signal DW : std_logic := '0';
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';

 	--Outputs
   signal instrucc : std_logic_vector(24 downto 0);
   signal stackPointer : std_logic_vector(2 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: practica6 PORT MAP (
          pcData => pcData,
          L => L,
          UP => UP,
          DW => DW,
          clk => clk,
          clr => clr,
          instrucc => instrucc,
          stackPointer => stackPointer
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

	-- Stimulus process
	stim_proc: process

		-- Archivo entradas.txt
		FILE			inputFile	: TEXT;
		VARIABLE		inputLine	: LINE;
		
		-- Variables proceso
		VARIABLE var_pcData					: STD_LOGIC_VECTOR (15 DOWNTO 0);
		VARIABLE var_L							: STD_LOGIC;
		VARIABLE var_UP						: STD_LOGIC;
		VARIABLE var_DW						: STD_LOGIC;
		VARIABLE var_clr						: STD_LOGIC;
		VARIABLE var_clk						: STD_LOGIC;
		
		VARIABLE var_instrucc				: STD_LOGIC_VECTOR (24 DOWNTO 0);
		VARIABLE var_stackPointer			: STD_LOGIC_VECTOR (2 DOWNTO 0);
		
		-- Archivo salida.txt
		FILE			outputFile	: TEXT;
		VARIABLE		outputLine	: LINE;

		VARIABLE line				: STRING (1 TO 5);
		
		begin
			-- Archivos
			FILE_OPEN (inputFile, "entradas.txt", READ_MODE);
			FILE_OPEN (outputFile, "salida.txt", WRITE_MODE);
			
			line := " CLK ";
			WRITE(outputLine, line, RIGHT, line'LENGTH);
			line := " CLR ";
			WRITE(outputLine, line, RIGHT, line'LENGTH);
			line := "  D  ";
			WRITE(outputLine, line, RIGHT, line'LENGTH);
			line := "   L ";
			WRITE(outputLine, line, RIGHT, line'LENGTH);
			line := "  UP ";
			WRITE(outputLine, line, RIGHT, line'LENGTH);
			line := "  DW ";
			WRITE(outputLine, line, RIGHT, line'LENGTH);
			line := "  SP ";
			WRITE(outputLine, line, RIGHT, line'LENGTH);
			line := " INS ";
			WRITE(outputLine, line, RIGHT, line'LENGTH);
			WRITELINE(outputFile, outputLine);
			
			WAIT FOR 20 NS;
			
			-- Loop de Proceso
			WHILE NOT ENDFILE (inputFile) LOOP
				READLINE (inputFile, inputLine);
				
				HREAD(inputLine, var_pcData);
				pcData <= var_pcData(10 downto 0); --Tomamos los 11 bits
				READ(inputLine, var_UP);
				UP <= var_UP;
				READ(inputLine, var_DW);
				DW <= var_DW;
				READ(inputLine, var_L);
				L <= var_L;
				READ(inputLine, var_clr);
				clr <= var_clr;
				
				WAIT UNTIL RISING_EDGE ( clk );
				
				var_clk				:= clk;				
				var_instrucc		:= instrucc;
				var_stackPointer	:= stackPointer;
				
				-- Linea de Escritura
				WRITE(outputLine, var_clk, RIGHT, 5);
				WRITE(outputLine, var_clr, RIGHT, 5);
				HWRITE(outputLine, var_pcData, RIGHT, 5);
				WRITE(outputLine, var_L, RIGHT, 5);
				WRITE(outputLine, var_UP, RIGHT, 5);
				WRITE(outputLine, var_DW, RIGHT, 5);
				WRITE(outputLine, var_stackPointer, RIGHT, 5);
				WRITE(outputLine, var_instrucc, RIGHT, var_instrucc'LENGTH+2);
				
				WRITELINE(outputFile, outputLine);
			END LOOP;
			
			-- Cierro Archivos
			FILE_CLOSE(inputFile);
			FILE_CLOSE(outputFile);
			
			WAIT;
	end process;

END;