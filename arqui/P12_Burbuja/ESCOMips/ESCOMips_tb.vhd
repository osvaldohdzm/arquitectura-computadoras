LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY ESCOMips_tb IS
END ESCOMips_tb;
 
ARCHITECTURE behavior OF ESCOMips_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ESCOMips
    PORT(
         osc_clk : IN  std_logic;
         RCLR : IN  std_logic;
         PC_out : OUT  std_logic_vector(7 downto 0);
      	Data_out: out std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal osc_clk : std_logic := '0';
   signal RCLR : std_logic := '0';

 	--Outputs
   --signal MicroInstruccion_out : std_logic_vector(19 downto 0);
   signal PC_out : std_logic_vector(7 downto 0);
   --signal Instruccion_out : std_logic_vector(24 downto 0);
   --signal ReadData1_out : std_logic_vector(15 downto 0);
   --signal ReadData2_out : std_logic_vector(15 downto 0);
   --signal ResultadoALU_out : std_logic_vector(15 downto 0);
	signal Data_out: std_logic_vector(7 downto 0);
   -- Clock period definitions
   constant osc_clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ESCOMips PORT MAP (
          osc_clk => osc_clk,
          RCLR => RCLR,
          PC_out => PC_out,
          Data_out=>data_out
        );

   -- Clock process definitions
   osc_clk_process :process
   begin
		osc_clk <= '0';
		wait for osc_clk_period/2;
		osc_clk <= '1';
		wait for osc_clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
		rclr<='1';
		wait for 12 ns;
		rclr<='0';
      wait;
   end process;

END;
