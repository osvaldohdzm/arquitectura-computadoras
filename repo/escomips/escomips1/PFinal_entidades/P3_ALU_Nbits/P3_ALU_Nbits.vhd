library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity P3_ALU_Nbits is
	Generic ( N: integer := 16 );
   Port ( a,b : in  STD_LOGIC_VECTOR (N-1 downto 0);
          a_sel, b_sel : in  STD_LOGIC;
          op : in  STD_LOGIC_VECTOR (1 downto 0);
          s : inout  STD_LOGIC_VECTOR (N-1 downto 0);
          flags : out  STD_LOGIC_VECTOR (3 downto 0));
end P3_ALU_Nbits;

architecture Behavioral of P3_ALU_Nbits is
	component P3_ALU_1bit is
		 Port ( a,b,sel_a,sel_b,cin : in  STD_LOGIC;
				  op: in STD_LOGIC_VECTOR(1 downto 0);
				  s,cout : out  STD_LOGIC);
	end component;
	
	signal c: STD_LOGIC_VECTOR(N downto 0);
	
	begin
	c(0) <= b_sel;
	
	-- CICLO PARA CREAR ALU N BITS
	componentes: for i in 0 to N-1 generate
	begin
		comp: P3_ALU_1bit port map(
			a => a(i),
			b => b(i),
			sel_a => a_sel,
			sel_b => b_sel,
			cin => c(i),
			op => op,
			s => s(i),
			cout => c(i+1)
		);
	end generate;
	
	-- BANDERAS
	-- C - flags(0)
	-- Z - flags(1)
	-- N - flags(2)
	-- OV - flags(3)
	process(c,s,op)
	variable z_aux: STD_LOGIC;
	begin
		-- Z
		z_aux := '0';
		zero: for i in 0 to N-1 loop
			z_aux := z_aux OR s(i);
		end loop;
		flags(1) <= not(z_aux);
		
		-- N
		flags(2) <= s(N-1);
		
		if(op="11") then
			
			-- C
			flags(0) <= c(N);
			-- OV
			flags(3) <= c(N) XOR c(N-1);
		else
			-- C
			flags(0) <= '0';
			-- OV
			flags(3) <= '0';
		end if;
	end process;

end Behavioral;



