library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity practica6 is
	generic
	(
		TamanoPalabra		 : integer := 25;	-- Tama�o Palabra
		TamanoBus			 : integer := 11;	-- Tama�o Bus Direcciones
		TamanoSP	: integer := 3					-- Tama�o Bus Stack Pointer
	);
	port --S�lo los puertos que entran o salen de la entidad
	(
		pcData				: in std_logic_vector(TamanoBus-1 downto 0);
		L						: in std_logic;
		UP						: in std_logic;
		DW						: in std_logic;
		clk					: in std_logic;
		clr					: in std_logic;
		instrucc				: out	std_logic_vector(TamanoPalabra-1 downto 0);
		stackPointer		: out std_logic_vector(TamanoSP-1 downto 0)
	);

end practica6;

architecture A_practica6 of practica6 is

--Conecta ambos bloques
signal contadorPrograma : std_logic_vector(TamanoBus-1 downto 0);

--Encabezados de VHDLS
component P6_MemProg is
	port
	(
		dir		: in 	std_logic_vector(TamanoBus-1 downto 0);
		data		: out	std_logic_vector(TamanoPalabra-1 downto 0)
	);
end component;

component P6_Pila is
	port
	(
		pcData				: in std_logic_vector(TamanoBus-1 downto 0);
		contadorPrograma	: out std_logic_vector(TamanoBus-1 downto 0);
		stackPointer		: out std_logic_vector(TamanoSP-1 downto 0);
		L						: in std_logic;
		UP						: in std_logic;
		DW						: in std_logic;
		clk					: in std_logic;
		clr					: in std_logic
	);
end component;

begin -- Manda llamar las instancias creadas, para mapeo de los puertos o se�ales entre Practicas
	memoriaPrograma : P6_MemProg
		port map
		(
			dir	=> contadorPrograma,
			data	=> instrucc
		);
	--Lado IZQ: Se�ales de la Clase o entidad
	--Lado DER: Se�ales de la entidad Completa
	pila : P6_Pila
		port map
		(
			pcData				=> pcData,
			contadorPrograma	=> contadorPrograma,
			stackPointer		=> stackPointer,
			L						=> L,
			UP						=> UP,
			DW						=> DW,
			clk					=> clk,
			clr					=> clr
		);
	
end A_practica6;

