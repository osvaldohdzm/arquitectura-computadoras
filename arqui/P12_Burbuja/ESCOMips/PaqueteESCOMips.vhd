 
library IEEE;

use IEEE.STD_LOGIC_1164.all;

package PaqueteESCOMips is
	
	component ALU is
		generic
		(
			constant num: integer:=16
		);
		port
		(
			--Entradas
			A			:	in std_logic_vector(num-1 downto 0);	--Operando A
			B			:	in std_logic_vector(num-1 downto 0);	--Operando B
			ALUOP		:	in std_logic_vector(3 downto 0);			--Bus de selecci�n(sel_a,sel_b,op1,op0)
			
			--Salidas
			resultado:  out std_logic_vector(num-1 downto 0);  --Bits de Resultado
			ov			:	out std_logic;								   --Bandera de Overflow
			N			:	out std_logic;								   --Bandera de Signo
			Z			:	out std_logic;								   --Bandera Cero
			C			:	out std_logic								   --Bandera de Acarreo
		);
	end component;
	
	component ArchivoDeRegistros is
		generic
		(
			constant tamWord	: INTEGER:=16; 	--Tama�o de palabra
			constant tamDir	: INTEGER:=4 		--Longitud del bus de direcciones
		);
		port
		(
			--Entradas
			
			--Bus de Entrada de Datos
			WriteData: in std_logic_vector(tamWord-1 downto 0);
			
			--Direcci�n del Registro donde se almacenan los datos
			WriteRegister: in std_logic_vector(tamDir-1 downto 0);
			
			--Direcci�n del Primer Registro donde se leen los datos
			ReadRegister1: in std_logic_vector(tamDir-1 downto 0);	
			
			--Direcci�n del Segundo Registro donde se leen los datos
			ReadRegister2: in std_logic_vector(tamDir-1 downto 0);
			
			--Cantidad de bits a desplazar en las instrucciones de corrimiento
			--Toma los valores en el intervalo [0,log2(tamWord)-1]
			SHAMT: in std_logic_vector(tamDir-1 downto 0);
			
			--Habilitador de Escritura
			WR: in std_logic;
			
			--Habilitador de Corrimiento
			SHE: in std_logic;
			
			--Indicador para la direcci�n del corrimiento
			DIR: in std_logic;
			
			--Pulso de Reloj
			clk: in std_logic;
			
			--Se�al de Reset
			clr: in std_logic;
			
			--Salidas
			
			--Primer Bus de Datos de Salida correspondiente a la direcci�n indicada en ReadRegister1
			ReadData1: out std_logic_vector(tamWord-1 downto 0);
			
			--Segundo Bus de Datos de Salida correspondiente a la direcci�n indicada en ReadRegister2
			ReadData2: out std_logic_vector(tamWord-1 downto 0)
		);
	end component;
		
	component ExtensorDeDireccion is
		port
		(
			DataIn	: in std_logic_vector(11 downto 0);
			DataOut	: out std_logic_vector(15 downto 0)
		);
	end component;
	
	component ExtensorDeSigno is
		port
		(
			DataIn	: in std_logic_vector(11 downto 0);
			DataOut	: out std_logic_vector(15 downto 0)
		);
	end component;
	
	component MemoriaDeDatos is
		generic
		(
			--Tama�o del Bus de Datos
			constant tamWord	: integer:=16;
			
			--Tama�o del Bus de Direcciones
			constant tamBusDir: integer:=11
		);
		Port 
		( 
			--Entradas
			
			BusDir  : in  STD_LOGIC_VECTOR (tamBusDir-1  downto 0); 	--Bus de Direcciones
			DataIn  : in  STD_LOGIC_VECTOR (tamWord-1 downto 0); 		--Bus de Datos de Entrada
			WD 	  : in  STD_LOGIC;  											--Se�al para habilitar escritura
			clk 	  : in  STD_LOGIC;											--Se�al de Reloj
			
			--Salidas
			
			DataOut : out  STD_LOGIC_VECTOR (tamWord-1 downto 0) 		--Bus de Datos de Salida
		);
	end component;
	
	component MemoriaDePrograma is
		generic
		(
			constant tamInstruccion: integer:=25; --Tama�o del Bus de Instrucciones
			constant tamBusDir	  : integer:=9  --Tama�o del Bus de Direcciones
		);
		port
		(
			BusDir		: in std_logic_vector(tamBusDir-1 downto 0); 	  --Bus de Direcciones
			Instruccion	: out std_logic_vector(tamInstruccion-1 downto 0) --Bus de Instrucciones
		);
	end component;
	
	component PILA is
		generic
		(
			constant tamSP	: integer:=3; --Tama�o de StackPointer
			constant tamDir: integer:=16 --Tama�o de los buses de direcciones de los buses de instrucci�n
		);
		port
		(
			--Entradas
			DataInPila	: in std_logic_vector(tamDir-1 downto 0);
			Load			: in std_logic;
			UP				: in std_logic;
			Down			: in std_logic;
			clk			: in std_logic;
			clr			: in std_logic;
			
			--Salidas
			DataOutPila: out std_logic_vector(tamDir-1 downto 0)
			--salidaSP: out std_logic_vector(tamSP-1 downto 0)
		); 
	end component;
	
	component UnidadDeControl is
		generic
		(
			constant tam_Microinstruccion	:integer:=20;
			constant tam_OPCODE				: integer:=5;
			constant tam_FUNCIONCODE		: integer:=4
		);
		port
		(
			--Banderas de la ALU
			OV	: in std_logic;
			N	: in std_logic;
			Z	: in std_logic;
			C	: in std_logic;
			LF	: in std_logic;
			clk: in std_logic;
			clr: in std_logic;
			
			OP_CODE	: 	in std_logic_vector(tam_OPCODE-1 downto 0);
			FUN_CODE	: 	in std_logic_vector(tam_FUNCIONCODE-1 downto 0);
			
			--NA_out				: out std_logic;
			MicroInstruccion	: out std_logic_vector(tam_Microinstruccion-1 downto 0)
		);
	end component;
	
	component DivisorDeFrecuencia is
		port
		(
			osc_clk: in std_logic;
			clr: in std_logic;
			clk: out std_logic
		);
	end component;
	
end PaqueteESCOMips;

package body PaqueteESCOMips is 
end PaqueteESCOMips;