--Arch de Reg -3CV2 Manuel Arreola

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

entity ArchReg is
	 Generic (
					n: integer := 16 --Para generico
	 );
    Port ( clk : in  STD_LOGIC;
           clr : in  STD_LOGIC;
           dir : in  STD_LOGIC;
           wr : in  STD_LOGIC;
			  she : in  STD_LOGIC;
           write_reg : in  STD_LOGIC_VECTOR (3 downto 0);
           write_data : in  STD_LOGIC_VECTOR (n-1 downto 0);
           read_reg1 : in  STD_LOGIC_VECTOR (3 downto 0);
           read_reg2 : in  STD_LOGIC_VECTOR (3 downto 0);
           shamt : in  STD_LOGIC_VECTOR (3 downto 0);
           read_data1 : out  STD_LOGIC_VECTOR (n-1 downto 0);
           read_data2 : out  STD_LOGIC_VECTOR (n-1 downto 0)
           );
end ArchReg;

architecture Behavioral of ArchReg is

type banco is array (0 to n-1) of std_logic_vector(n-1 downto 0);	--Arreglo
signal archivo : banco;

begin

	process (clr,clk)
		variable aux: std_logic_vector(n-1 downto 0);
		begin
			if (clr = '1' ) then
				archivo <= (others =>"0000000000000000"); --clear
			elsif(rising_edge(clk)) then
				if((wr = '1')and(she='0')and(dir='0')) then
				
					--Lectura y escritura del registro
					archivo(conv_integer(write_reg)) <= write_data;
				end if;
				if((wr = '1')and(she='1')and(dir='0')) then --Toma lo del Barrel
					aux := archivo(conv_integer(read_reg1));
					
					--Corrimiento derecha
					for i in 0 to 3 loop
						for j in 0 to n-1 loop
							if ( shamt(i)='0' ) then
								aux := aux;
							else 
									if ((j+(2**i))>n-1) then
										aux(j):= '0';
									else 
										aux (j):= aux(j+(2**i));
									end if;
							end if;
						end loop;
					end loop;
				archivo (conv_integer(write_reg)) <= aux;
				end if;
		
				if((wr = '1')and(she='1')and(dir='1')) then
					aux := archivo(conv_integer(read_reg1));
					
					--Corrimiento izquierda
					for i in 0 to 3 loop
						for j in n-1 downto 0 loop
							if ( shamt(i)='0' ) then
								aux := aux;
							else
								if( (j-2**i)<0 )then
									aux(j) := '0';
								else
									aux(j) := aux(j-2**i);
								end if;
							end if;
						end loop;
					end loop;		
					archivo (conv_integer(write_reg)) <= aux;
				end if;
			end if;
		end process;
		
	--ReadRegister1: lee el archivo en el registro de la entrada solicitada:
	read_data1 <= archivo(conv_integer(read_reg1)); 
	--ReadRegister2
	read_data2 <= archivo(conv_integer(read_reg2));
	
end Behavioral;