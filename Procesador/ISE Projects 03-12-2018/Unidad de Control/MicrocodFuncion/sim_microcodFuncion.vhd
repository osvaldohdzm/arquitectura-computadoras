--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   00:41:21 11/25/2018
-- Design Name:   
-- Module Name:   /home/ise/Documents/Unidad de Control/MicrocodFuncion/sim_microcodFuncion.vhd
-- Project Name:  MicrocodFuncion
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: MicrocodFuncion
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY sim_microcodFuncion IS
END sim_microcodFuncion;
 
ARCHITECTURE behavior OF sim_microcodFuncion IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT MicrocodFuncion
    PORT(
         codFunc : IN  std_logic_vector(3 downto 0);
         sal : OUT  std_logic_vector(19 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal codFunc : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal sal : std_logic_vector(19 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: MicrocodFuncion PORT MAP (
          codFunc => codFunc,
          sal => sal
        );
		stim_proc: process
		begin

      -- insert stimulus here 
		codFunc <= "0000";
      wait for 20 ns;
		
		codFunc <= "0010";
      wait for 20 ns;
		
		codFunc <= "1100";
      wait;
   end process;

END;
