/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/_PracticasArqui/P4_ArchivoRegistros/P4_ArchivoRegistros.vhd";
extern char *IEEE_P_2592010699;
extern char *IEEE_P_3620187407;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
char *ieee_p_2592010699_sub_3293060193_503743352(char *, char *, char *, char *, unsigned char );
char *ieee_p_2592010699_sub_393209765_503743352(char *, char *, char *, char *);
int ieee_p_3620187407_sub_514432868_3965413181(char *, char *, char *);


static void work_a_1481163779_3212880686_p_0(char *t0)
{
    char t1[16];
    char t11[16];
    char *t2;
    char *t3;
    char *t4;
    int t5;
    int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    char *t10;
    char *t12;
    char *t13;
    int t14;
    unsigned int t15;
    char *t16;
    char *t17;
    char *t18;
    unsigned char t19;
    unsigned char t20;
    unsigned char t21;
    int t22;
    char *t23;
    char *t24;
    unsigned char t25;
    unsigned char t26;
    unsigned char t27;
    unsigned char t28;
    unsigned char t29;

LAB0:    xsi_set_current_line(24, ng0);
    t2 = (t0 + 2952U);
    t3 = *((char **)t2);
    t2 = (t0 + 1512U);
    t4 = *((char **)t2);
    t2 = (t0 + 8760U);
    t5 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t4, t2);
    t6 = (t5 - 0);
    t7 = (t6 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t5);
    t8 = (16U * t7);
    t9 = (0 + t8);
    t10 = (t3 + t9);
    t12 = (t11 + 0U);
    t13 = (t12 + 0U);
    *((int *)t13) = 15;
    t13 = (t12 + 4U);
    *((int *)t13) = 0;
    t13 = (t12 + 8U);
    *((int *)t13) = -1;
    t14 = (0 - 15);
    t15 = (t14 * -1);
    t15 = (t15 + 1);
    t13 = (t12 + 12U);
    *((unsigned int *)t13) = t15;
    t13 = ieee_p_2592010699_sub_3293060193_503743352(IEEE_P_2592010699, t1, t10, t11, (unsigned char)0);
    t16 = (t0 + 3368U);
    t17 = *((char **)t16);
    t16 = (t17 + 0);
    t18 = (t1 + 12U);
    t15 = *((unsigned int *)t18);
    t15 = (t15 * 1U);
    memcpy(t16, t13, t15);
    xsi_set_current_line(25, ng0);
    t2 = (t0 + 1672U);
    t3 = *((char **)t2);
    t2 = (t0 + 8776U);
    t5 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t3, t2);
    t4 = (t0 + 3488U);
    t10 = *((char **)t4);
    t4 = (t10 + 0);
    *((int *)t4) = t5;
    xsi_set_current_line(26, ng0);
    t2 = (t0 + 1992U);
    t3 = *((char **)t2);
    t19 = *((unsigned char *)t3);
    t20 = (t19 == (unsigned char)3);
    if (t20 != 0)
        goto LAB2;

LAB4:    t2 = (t0 + 1792U);
    t19 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t2, 0U, 0U);
    if (t19 != 0)
        goto LAB12;

LAB13:
LAB3:    t2 = (t0 + 5288);
    *((int *)t2) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(27, ng0);
    t2 = (t0 + 9212);
    *((int *)t2) = 0;
    t4 = (t0 + 9216);
    *((int *)t4) = 15;
    t5 = 0;
    t6 = 15;

LAB5:    if (t5 <= t6)
        goto LAB6;

LAB8:    goto LAB3;

LAB6:    xsi_set_current_line(28, ng0);
    t10 = (t0 + 9220);
    t21 = (16U != 16U);
    if (t21 == 1)
        goto LAB9;

LAB10:    t13 = (t0 + 9212);
    t14 = *((int *)t13);
    t22 = (t14 - 0);
    t7 = (t22 * 1);
    t8 = (16U * t7);
    t9 = (0U + t8);
    t16 = (t0 + 5400);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    t23 = (t18 + 56U);
    t24 = *((char **)t23);
    memcpy(t24, t10, 16U);
    xsi_driver_first_trans_delta(t16, t9, 16U, 0LL);

LAB7:    t2 = (t0 + 9212);
    t5 = *((int *)t2);
    t3 = (t0 + 9216);
    t6 = *((int *)t3);
    if (t5 == t6)
        goto LAB8;

LAB11:    t14 = (t5 + 1);
    t5 = t14;
    t4 = (t0 + 9212);
    *((int *)t4) = t5;
    goto LAB5;

LAB9:    xsi_size_not_matching(16U, 16U, 0);
    goto LAB10;

LAB12:    xsi_set_current_line(32, ng0);
    t3 = (t0 + 2152U);
    t4 = *((char **)t3);
    t21 = *((unsigned char *)t4);
    t25 = (t21 == (unsigned char)3);
    if (t25 == 1)
        goto LAB17;

LAB18:    t20 = (unsigned char)0;

LAB19:    if (t20 != 0)
        goto LAB14;

LAB16:
LAB15:    xsi_set_current_line(36, ng0);
    t2 = (t0 + 2152U);
    t3 = *((char **)t2);
    t21 = *((unsigned char *)t3);
    t25 = (t21 == (unsigned char)3);
    if (t25 == 1)
        goto LAB26;

LAB27:    t20 = (unsigned char)0;

LAB28:    if (t20 == 1)
        goto LAB23;

LAB24:    t19 = (unsigned char)0;

LAB25:    if (t19 != 0)
        goto LAB20;

LAB22:
LAB21:    xsi_set_current_line(41, ng0);
    t2 = (t0 + 2152U);
    t3 = *((char **)t2);
    t21 = *((unsigned char *)t3);
    t25 = (t21 == (unsigned char)3);
    if (t25 == 1)
        goto LAB37;

LAB38:    t20 = (unsigned char)0;

LAB39:    if (t20 == 1)
        goto LAB34;

LAB35:    t19 = (unsigned char)0;

LAB36:    if (t19 != 0)
        goto LAB31;

LAB33:
LAB32:    goto LAB3;

LAB14:    xsi_set_current_line(33, ng0);
    t3 = (t0 + 1032U);
    t12 = *((char **)t3);
    t3 = (t0 + 1192U);
    t13 = *((char **)t3);
    t3 = (t0 + 8728U);
    t5 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t13, t3);
    t6 = (t5 - 0);
    t7 = (t6 * 1);
    t8 = (16U * t7);
    t9 = (0U + t8);
    t16 = (t0 + 5400);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    t23 = (t18 + 56U);
    t24 = *((char **)t23);
    memcpy(t24, t12, 16U);
    xsi_driver_first_trans_delta(t16, t9, 16U, 0LL);
    goto LAB15;

LAB17:    t3 = (t0 + 2312U);
    t10 = *((char **)t3);
    t26 = *((unsigned char *)t10);
    t27 = (t26 == (unsigned char)2);
    t20 = t27;
    goto LAB19;

LAB20:    xsi_set_current_line(37, ng0);
    t2 = (t0 + 3368U);
    t12 = *((char **)t2);
    t2 = (t0 + 3488U);
    t13 = *((char **)t2);
    t5 = *((int *)t13);
    t2 = xsi_vhdl_bitvec_srl(t2, t12, 16U, t5);
    t16 = (t0 + 3248U);
    t17 = *((char **)t16);
    t16 = (t17 + 0);
    memcpy(t16, t2, 16U);
    xsi_set_current_line(38, ng0);
    t2 = (t0 + 3248U);
    t3 = *((char **)t2);
    t2 = (t0 + 8856U);
    t4 = ieee_p_2592010699_sub_393209765_503743352(IEEE_P_2592010699, t1, t3, t2);
    t10 = (t1 + 12U);
    t7 = *((unsigned int *)t10);
    t7 = (t7 * 1U);
    t19 = (16U != t7);
    if (t19 == 1)
        goto LAB29;

LAB30:    t12 = (t0 + 1192U);
    t13 = *((char **)t12);
    t12 = (t0 + 8728U);
    t5 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t13, t12);
    t6 = (t5 - 0);
    t8 = (t6 * 1);
    t9 = (16U * t8);
    t15 = (0U + t9);
    t16 = (t0 + 5400);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    t23 = (t18 + 56U);
    t24 = *((char **)t23);
    memcpy(t24, t4, 16U);
    xsi_driver_first_trans_delta(t16, t15, 16U, 0LL);
    goto LAB21;

LAB23:    t2 = (t0 + 2472U);
    t10 = *((char **)t2);
    t28 = *((unsigned char *)t10);
    t29 = (t28 == (unsigned char)2);
    t19 = t29;
    goto LAB25;

LAB26:    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t26 = *((unsigned char *)t4);
    t27 = (t26 == (unsigned char)3);
    t20 = t27;
    goto LAB28;

LAB29:    xsi_size_not_matching(16U, t7, 0);
    goto LAB30;

LAB31:    xsi_set_current_line(42, ng0);
    t2 = (t0 + 3368U);
    t12 = *((char **)t2);
    t2 = (t0 + 3488U);
    t13 = *((char **)t2);
    t5 = *((int *)t13);
    t2 = xsi_vhdl_bitvec_sll(t2, t12, 16U, t5);
    t16 = (t0 + 3248U);
    t17 = *((char **)t16);
    t16 = (t17 + 0);
    memcpy(t16, t2, 16U);
    xsi_set_current_line(43, ng0);
    t2 = (t0 + 3248U);
    t3 = *((char **)t2);
    t2 = (t0 + 8856U);
    t4 = ieee_p_2592010699_sub_393209765_503743352(IEEE_P_2592010699, t1, t3, t2);
    t10 = (t1 + 12U);
    t7 = *((unsigned int *)t10);
    t7 = (t7 * 1U);
    t19 = (16U != t7);
    if (t19 == 1)
        goto LAB40;

LAB41:    t12 = (t0 + 1192U);
    t13 = *((char **)t12);
    t12 = (t0 + 8728U);
    t5 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t13, t12);
    t6 = (t5 - 0);
    t8 = (t6 * 1);
    t9 = (16U * t8);
    t15 = (0U + t9);
    t16 = (t0 + 5400);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    t23 = (t18 + 56U);
    t24 = *((char **)t23);
    memcpy(t24, t4, 16U);
    xsi_driver_first_trans_delta(t16, t15, 16U, 0LL);
    goto LAB32;

LAB34:    t2 = (t0 + 2472U);
    t10 = *((char **)t2);
    t28 = *((unsigned char *)t10);
    t29 = (t28 == (unsigned char)3);
    t19 = t29;
    goto LAB36;

LAB37:    t2 = (t0 + 2312U);
    t4 = *((char **)t2);
    t26 = *((unsigned char *)t4);
    t27 = (t26 == (unsigned char)3);
    t20 = t27;
    goto LAB39;

LAB40:    xsi_size_not_matching(16U, t7, 0);
    goto LAB41;

}

static void work_a_1481163779_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    xsi_set_current_line(48, ng0);

LAB3:    t1 = (t0 + 2952U);
    t2 = *((char **)t1);
    t1 = (t0 + 1352U);
    t3 = *((char **)t1);
    t1 = (t0 + 8744U);
    t4 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t3, t1);
    t5 = (t4 - 0);
    t6 = (t5 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t4);
    t7 = (16U * t6);
    t8 = (0 + t7);
    t9 = (t2 + t8);
    t10 = (t0 + 5464);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t9, 16U);
    xsi_driver_first_trans_fast_port(t10);

LAB2:    t15 = (t0 + 5304);
    *((int *)t15) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_1481163779_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    xsi_set_current_line(49, ng0);

LAB3:    t1 = (t0 + 2952U);
    t2 = *((char **)t1);
    t1 = (t0 + 1512U);
    t3 = *((char **)t1);
    t1 = (t0 + 8760U);
    t4 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t3, t1);
    t5 = (t4 - 0);
    t6 = (t5 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t4);
    t7 = (16U * t6);
    t8 = (0 + t7);
    t9 = (t2 + t8);
    t10 = (t0 + 5528);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t9, 16U);
    xsi_driver_first_trans_fast_port(t10);

LAB2:    t15 = (t0 + 5320);
    *((int *)t15) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}


extern void work_a_1481163779_3212880686_init()
{
	static char *pe[] = {(void *)work_a_1481163779_3212880686_p_0,(void *)work_a_1481163779_3212880686_p_1,(void *)work_a_1481163779_3212880686_p_2};
	xsi_register_didat("work_a_1481163779_3212880686", "isim/P4_ArchivoRegistros_TestBench_isim_beh.exe.sim/work/a_1481163779_3212880686.didat");
	xsi_register_executes(pe);
}
