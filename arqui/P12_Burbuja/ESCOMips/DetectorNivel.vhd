 
library IEEE;

use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity DetectorNivel is
	port
	(
		clk: in std_logic;
		clr: in std_logic;
		--pclk: out std_logic;
		--nclk: out std_logic;
		NA: out std_logic
	);
end DetectorNivel;

architecture A_DetectorNivel of DetectorNivel is
	signal pclk1,nclk1: std_logic;
begin
	process(clk,clr)
	begin
		if (clr='1') then
			pclk1<='0';
		elsif (rising_edge(clk)) then
			pclk1<=(not pclk1);
		end if;
	end process;
	
	process(clk,clr)
	begin
		if (clr='1') then
			nclk1<='0';
		elsif (falling_edge(clk)) then
			nclk1<=(not nclk1);
		end if;
	end process;
	--nclk<=nclk1;
	--pclk<=pclk1;
	NA <= (pclk1 xor nclk1);
	
end A_DetectorNivel;