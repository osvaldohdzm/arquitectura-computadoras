
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity ALU is
    Port ( a,b,sel_a,sel_b,cin : in  STD_LOGIC;
           op : in  STD_LOGIC_VECTOR (1 downto 0);
           res,cout : out  STD_LOGIC);
end ALU;

architecture Behavioral of ALU is
component sumbit is
    Port ( a,b,cin : in  STD_LOGIC;
           cout,s : out  STD_LOGIC);
end component;
signal res_a,res_b,res_or,res_and,res_xor,res_sum,c : std_logic;
begin
	res_a <= a xor sel_a;
	res_b <= b xor sel_b;
	res_or <= res_a or res_b;
	res_and <= res_a and res_b;
	res_xor <= res_a xor res_b;
	suma: sumbit port map(res_a,res_b,cin,c,res_sum);
--	with op select res <=
--		res_and when "00",
--		res_or when "01",
--		res_xor when "10",
--		res_sum when others;
	mux: process(op,res_and,res_or,res_xor,res_sum,c)
	begin
		cout <= '0';
		case op is
			when "00" =>
				res<=res_and;
			when "01" =>
				res<=res_or;
			when "10" =>
				res<=res_xor;
			when others =>
				res<=res_sum;
				cout <= c;
		end case;
	end process mux;
end Behavioral;

