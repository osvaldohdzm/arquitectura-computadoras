--------------------------------------------------------------------------------
-- Test con Archivos
--------------------------------------------------------------------------------
LIBRARY IEEE;
LIBRARY STD;
USE STD.TEXTIO.ALL;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
 
ENTITY ArchReg_TB IS
GENERIC (
			n : integer := 16
);
END ArchReg_TB;
 
ARCHITECTURE behavior OF ArchReg_TB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ArchReg
    PORT(
         clk : IN  std_logic;
         clr : IN  std_logic;
         dir : IN  std_logic;
         wr : IN  std_logic;
         she : IN  std_logic;
         write_reg : IN  std_logic_vector(3 downto 0);
         write_data : IN  std_logic_vector(n-1 downto 0);
         read_reg1 : IN  std_logic_vector(3 downto 0);
         read_reg2 : IN  std_logic_vector(3 downto 0);
         shamt : IN  std_logic_vector(3 downto 0);
         read_data1 : OUT  std_logic_vector(n-1 downto 0);
         read_data2 : OUT  std_logic_vector(n-1 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';
   signal dir : std_logic := '0';
   signal wr : std_logic := '0';
   signal she : std_logic := '0';
   signal write_reg : std_logic_vector(3 downto 0) := (others => '0');
   signal write_data : std_logic_vector(15 downto 0) := (others => '0');
   signal read_reg1 : std_logic_vector(3 downto 0) := (others => '0');
   signal read_reg2 : std_logic_vector(3 downto 0) := (others => '0');
   signal shamt : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal read_data1 : std_logic_vector(n-1 downto 0);
   signal read_data2 : std_logic_vector(n-1 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ArchReg PORT MAP (
          clk => clk,
          clr => clr,
          dir => dir,
          wr => wr,
          she => she,
          write_reg => write_reg,
          write_data => write_data,
          read_reg1 => read_reg1,
          read_reg2 => read_reg2,
          shamt => shamt,
          read_data1 => read_data1,
          read_data2 => read_data2
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
  ----------------------
  file ARCH_RES : TEXT;  --Archivo de respuesta																					
	variable LINEA_RES : line;
	VARIABLE VAR_DATA1 : STD_LOGIC_VECTOR(n-1 DOWNTO 0);
	VARIABLE VAR_DATA2 : STD_LOGIC_VECTOR(n-1 DOWNTO 0);
	
	file ARCH_LEC : TEXT;  --Archivo de lectura
	variable LINEA_VEC : line;
	
	VARIABLE VAR_REDREG1 : STD_LOGIC_VECTOR(3 DOWNTO 0);
	VARIABLE VAR_REDREG2 : STD_LOGIC_VECTOR(3 DOWNTO 0);
	VARIABLE VAR_SHAMT : STD_LOGIC_VECTOR(3 DOWNTO 0);
	VARIABLE VAR_WRITEREG : STD_LOGIC_VECTOR(3 DOWNTO 0);
	VARIABLE VAR_WRITEDATA : STD_LOGIC_VECTOR(n-1 DOWNTO 0);
	VARIABLE VAR_WR : STD_LOGIC;
	VARIABLE VAR_SHE : STD_LOGIC;
	VARIABLE VAR_DIR : STD_LOGIC;
	VARIABLE VAR_CLR : STD_LOGIC;
	
	VARIABLE CADENA : STRING(1 TO 5);
	---------------------------------
   begin		
	
	file_open(ARCH_LEC, "C:\Users\Caos\Desktop\ESCOM\2017-2\Materias\Arqui\Practicas\Practica4\Lab4\Practica4\entradas.txt", READ_MODE); 	
	file_open(ARCH_RES, "C:\Users\Caos\Desktop\ESCOM\2017-2\Materias\Arqui\Practicas\Practica4\Lab4\Practica4\salidas.txt", WRITE_MODE); 
--   
	CADENA := " RR1 ";
	write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
	CADENA := " RR2 ";
	write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
	CADENA := "SHAMT";
	write(LINEA_RES, CADENA, right, CADENA'LENGTH+3);	
	CADENA := " WREG";
	write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
	CADENA := "WD   ";
	write(LINEA_RES, CADENA, right, CADENA'LENGTH+4);	
	CADENA := " WR  ";
	write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
	CADENA := " SHE ";
	write(LINEA_RES, CADENA, right, CADENA'LENGTH+2);	
	CADENA := " DIR ";
	write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
	CADENA := " RD1 ";
	write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
	CADENA := " RD2 ";
	write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
	
	
	
	writeline(ARCH_RES,LINEA_RES);							
	
	wait for 100ns;
	
	 FOR I IN 0 TO 9 LOOP   
			readline(ARCH_LEC,LINEA_VEC); 
			
			Hread(LINEA_VEC, VAR_REDREG1);		
			read_reg1 <= VAR_REDREG1;
			
			Hread(LINEA_VEC, VAR_REDREG2);		
			read_reg2 <= VAR_REDREG2;							
			
			Hread(LINEA_VEC, VAR_SHAMT);		
			shamt <= VAR_SHAMT;
			
			Hread(LINEA_VEC, VAR_WRITEREG);		
			write_reg <= VAR_WRITEREG;
			
			Hread(LINEA_VEC, VAR_WRITEDATA);		
			write_data <= VAR_WRITEDATA;
			
			read(LINEA_VEC, VAR_WR);  
			wr <= VAR_WR;
			
			read(LINEA_VEC, VAR_SHE);  
			she <= VAR_SHE;
			
			read(LINEA_VEC, VAR_DIR);  
			dir <= VAR_DIR;
			
			read(LINEA_VEC, VAR_CLR);  
			clr <= VAR_CLR;

			WAIT UNTIL RISING_EDGE(CLK);
			
			--Docs:		
			VAR_DATA1 := read_data1;
			VAR_DATA2 := read_data2;
			
			Hwrite(LINEA_RES, VAR_REDREG1, right, 4);	
			Hwrite(LINEA_RES, VAR_REDREG2, right, 7);	
			Hwrite(LINEA_RES, VAR_SHAMT, right, 7);	
			Hwrite(LINEA_RES, VAR_WRITEREG, right, 7);	
			Hwrite(LINEA_RES, VAR_WRITEDATA, right, 7);	
			write(LINEA_RES, VAR_WR, right, 7);
			write(LINEA_RES, VAR_SHE, right, 7);
			write(LINEA_RES, VAR_DIR, right, 7);
			Hwrite(LINEA_RES, VAR_DATA1, right, 7);
			Hwrite(LINEA_RES, VAR_DATA2, right, 7);

			writeline(ARCH_RES,LINEA_RES);
		end loop;
		file_close(ARCH_LEC);  
		file_close(ARCH_RES);  

      wait;
   end process;

END;
