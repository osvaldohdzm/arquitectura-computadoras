
# PlanAhead Launch Script for Post-Synthesis floorplanning, created by Project Navigator

create_project -name PFinal_Escomips -dir "C:/_PracticasArqui/ESCOMIPS/PFinal_Escomips/planAhead_run_4" -part xc7a100tcsg324-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "C:/_PracticasArqui/ESCOMIPS/PFinal_Escomips/PFinal_Escomips_Main.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/_PracticasArqui/ESCOMIPS/PFinal_Escomips} }
set_property target_constrs_file "PFinal_Escomips_Main.ucf" [current_fileset -constrset]
add_files [list {PFinal_Escomips_Main.ucf}] -fileset [get_property constrset [current_run]]
link_design
