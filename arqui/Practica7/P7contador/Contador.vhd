library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Contador is
    Port ( LB : in  STD_LOGIC;
           EB : in  STD_LOGIC;
           clk: in STD_LOGIC;
           clr: in STD_LOGIC;
           B : out  STD_LOGIC_VECTOR (3 downto 0));
end Contador;

architecture Behavioral of Contador is
signal sel: STD_LOGIC_VECTOR (1 downto 0);
begin
   sel <= LB&EB;
   process(clk,clr)
	variable cont: STD_LOGIC_VECTOR (3 downto 0);
      begin
         if (clr = '1') then
            cont := "0000"; --reset
         elsif(clk'event and clk = '1') then
            if(sel = "00") then
				   cont := "0000"; --reset?, no se considero esa condición, tons?
				elsif(sel = "01") then
				   cont := cont + 1 ; --incemrent
				elsif (sel = "10") then
				   cont := cont; --carga
			   end if;
			end if;
			B <= cont;
	end process;
end Behavioral;

