library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity F_CLR is
    Port ( CLR, CLK : in  STD_LOGIC;
           RCLR : out  STD_LOGIC);
end F_CLR;

architecture Behavioral of F_CLR is

begin
process(clk)
	BEGIN
		IF (FALLING_EDGE(CLK))THEN
			RCLR <= CLR;
			
		END IF;
end process;
end Behavioral;

