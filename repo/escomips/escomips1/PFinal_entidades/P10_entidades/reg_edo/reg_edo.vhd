library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity reg_edo is
    Port ( clk, clr, lf : in  STD_LOGIC;
           banderas : in  STD_LOGIC_VECTOR (3 downto 0);
           b_out : out  STD_LOGIC_VECTOR (3 downto 0));
end reg_edo;

architecture Behavioral of reg_edo is

begin
	process(clr, clk, lf)
	begin
		if (clr = '1') then
			b_out <= "0000";
		elsif (falling_edge(clk) and lf = '1') then
			b_out <= banderas;
		end if;
	end process;
end Behavioral;

