/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;

char *IEEE_P_3499444699;
char *IEEE_P_3620187407;
char *WORK_P_0751954218;
char *WORK_P_3903357176;
char *STD_STANDARD;
char *IEEE_P_2592010699;


int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    ieee_p_2592010699_init();
    work_p_3903357176_init();
    ieee_p_3499444699_init();
    ieee_p_3620187407_init();
    work_p_0751954218_init();
    work_a_3175194644_1609485942_init();
    work_a_2140513534_1756004201_init();
    work_a_1116757993_1458686526_init();
    work_a_0058504388_2251313869_init();
    work_a_3316347415_1483833065_init();
    work_a_3455128199_2134047297_init();
    work_a_3483163917_1886798510_init();
    work_a_3867927571_2233461185_init();
    work_a_3531332027_1282892900_init();
    work_a_1061504304_3277980275_init();
    work_a_2131384204_1394971497_init();
    work_a_2178422333_0619812440_init();
    work_a_3389147487_3658193169_init();
    work_a_0283168587_1513418833_init();
    work_a_2446174517_1389181138_init();
    work_a_0991361627_1008471584_init();
    work_a_4028457842_2372691052_init();


    xsi_register_tops("work_a_4028457842_2372691052");

    IEEE_P_3499444699 = xsi_get_engine_memory("ieee_p_3499444699");
    IEEE_P_3620187407 = xsi_get_engine_memory("ieee_p_3620187407");
    WORK_P_0751954218 = xsi_get_engine_memory("work_p_0751954218");
    WORK_P_3903357176 = xsi_get_engine_memory("work_p_3903357176");
    STD_STANDARD = xsi_get_engine_memory("std_standard");
    IEEE_P_2592010699 = xsi_get_engine_memory("ieee_p_2592010699");
    xsi_register_ieee_std_logic_1164(IEEE_P_2592010699);

    return xsi_run_simulation(argc, argv);

}
