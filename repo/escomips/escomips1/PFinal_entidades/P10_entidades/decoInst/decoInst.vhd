library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decoInst is
    Port ( op_code : in  STD_LOGIC_VECTOR (4 downto 0);
           tipoR, BEQ, BNEQ, BLT, BLE, BGT,  BGET : out  STD_LOGIC);
end decoInst;

architecture Behavioral of decoInst is
begin
	process(op_code)
	begin
		case op_code is
			when "00000" =>
				tipoR <= '1';
				BEQ <= '0';
				BNEQ <= '0';
				BLT <= '0';
				BLE <= '0';
				BGT <= '0';
				BGET <= '0';
			when "01101" =>
				tipoR <= '0';
				BEQ <= '1';
				BNEQ <= '0';
				BLT <= '0';
				BLE <= '0';
				BGT <= '0';
				BGET <= '0';
			when "01110" =>
				tipoR <= '0';
				BEQ <= '0';
				BNEQ <= '1';
				BLT <= '0';
				BLE <= '0';
				BGT <= '0';
				BGET <= '0';
			when "01111" =>
				tipoR <= '0';
				BEQ <= '0';
				BNEQ <= '0';
				BLT <= '1';
				BLE <= '0';
				BGT <= '0';
				BGET <= '0';
			when "10000" =>
				tipoR <= '0';
				BEQ <= '0';
				BNEQ <= '0';
				BLT <= '0';
				BLE <= '1';
				BGT <= '0';
				BGET <= '0';
			when "10001" =>
				tipoR <= '0';
				BEQ <= '0';
				BNEQ <= '0';
				BLT <= '0';
				BLE <= '0';
				BGT <= '1';
				BGET <= '0';
			when "10010" =>
				tipoR <= '0';
				BEQ <= '0';
				BNEQ <= '0';
				BLT <= '0';
				BLE <= '0';
				BGT <= '0';
				BGET <= '1';
			when others =>
				tipoR <= '0';
				BEQ <= '0';
				BNEQ <= '0';
				BLT <= '0';
				BLE <= '0';
				BGT <= '0';
				BGET <= '0';
		end case;
	end process;
end Behavioral;

