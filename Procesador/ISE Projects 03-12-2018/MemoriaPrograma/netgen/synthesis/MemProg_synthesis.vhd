--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: MemProg_synthesis.vhd
-- /___/   /\     Timestamp: Sat Oct 27 20:49:12 2018
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm MemProg -w -dir netgen/synthesis -ofmt vhdl -sim MemProg.ngc MemProg_synthesis.vhd 
-- Device	: xc7a100t-2-csg324
-- Input file	: MemProg.ngc
-- Output file	: C:\Users\Emilio\Documents\ISE Projects\MemoriaPrograma\netgen\synthesis\MemProg_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: MemProg
-- Xilinx	: C:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity MemProg is
  port (
    pc : in STD_LOGIC_VECTOR ( 11 downto 0 ); 
    inst : out STD_LOGIC_VECTOR ( 24 downto 0 ) 
  );
end MemProg;

architecture Structure of MemProg is
  signal pc_11_IBUF_0 : STD_LOGIC; 
  signal pc_10_IBUF_1 : STD_LOGIC; 
  signal pc_9_IBUF_2 : STD_LOGIC; 
  signal pc_8_IBUF_3 : STD_LOGIC; 
  signal pc_7_IBUF_4 : STD_LOGIC; 
  signal pc_6_IBUF_5 : STD_LOGIC; 
  signal pc_5_IBUF_6 : STD_LOGIC; 
  signal pc_4_IBUF_7 : STD_LOGIC; 
  signal pc_3_IBUF_8 : STD_LOGIC; 
  signal pc_2_IBUF_9 : STD_LOGIC; 
  signal pc_1_IBUF_10 : STD_LOGIC; 
  signal pc_0_IBUF_11 : STD_LOGIC; 
  signal inst_24_OBUF_12 : STD_LOGIC; 
  signal inst_21_OBUF_13 : STD_LOGIC; 
  signal inst_20_OBUF_14 : STD_LOGIC; 
  signal inst_16_OBUF_15 : STD_LOGIC; 
  signal inst_12_OBUF_16 : STD_LOGIC; 
  signal inst_2_OBUF_17 : STD_LOGIC; 
  signal inst_1_OBUF_18 : STD_LOGIC; 
  signal inst_0_OBUF_19 : STD_LOGIC; 
  signal N2 : STD_LOGIC; 
  signal Mram_inst2611 : STD_LOGIC; 
  signal Mram_inst28112_59 : STD_LOGIC; 
  signal Mram_inst28111 : STD_LOGIC; 
  signal Mram_inst2811 : STD_LOGIC; 
  signal Mram_inst21111 : STD_LOGIC; 
  signal Mram_inst16111_63 : STD_LOGIC; 
  signal Mram_inst1611 : STD_LOGIC; 
  signal N0 : STD_LOGIC; 
  signal Mram_inst31112_66 : STD_LOGIC; 
  signal Mram_inst1212_67 : STD_LOGIC; 
  signal Mram_inst1211_68 : STD_LOGIC; 
  signal Mram_inst121 : STD_LOGIC; 
  signal inst_23_OBUF_70 : STD_LOGIC; 
begin
  pc_11_IBUF : IBUF
    port map (
      I => pc(11),
      O => pc_11_IBUF_0
    );
  pc_10_IBUF : IBUF
    port map (
      I => pc(10),
      O => pc_10_IBUF_1
    );
  pc_9_IBUF : IBUF
    port map (
      I => pc(9),
      O => pc_9_IBUF_2
    );
  pc_8_IBUF : IBUF
    port map (
      I => pc(8),
      O => pc_8_IBUF_3
    );
  pc_7_IBUF : IBUF
    port map (
      I => pc(7),
      O => pc_7_IBUF_4
    );
  pc_6_IBUF : IBUF
    port map (
      I => pc(6),
      O => pc_6_IBUF_5
    );
  pc_5_IBUF : IBUF
    port map (
      I => pc(5),
      O => pc_5_IBUF_6
    );
  pc_4_IBUF : IBUF
    port map (
      I => pc(4),
      O => pc_4_IBUF_7
    );
  pc_3_IBUF : IBUF
    port map (
      I => pc(3),
      O => pc_3_IBUF_8
    );
  pc_2_IBUF : IBUF
    port map (
      I => pc(2),
      O => pc_2_IBUF_9
    );
  pc_1_IBUF : IBUF
    port map (
      I => pc(1),
      O => pc_1_IBUF_10
    );
  pc_0_IBUF : IBUF
    port map (
      I => pc(0),
      O => pc_0_IBUF_11
    );
  inst_24_OBUF : OBUF
    port map (
      I => inst_24_OBUF_12,
      O => inst(24)
    );
  inst_23_OBUF : OBUF
    port map (
      I => inst_23_OBUF_70,
      O => inst(23)
    );
  inst_22_OBUF : OBUF
    port map (
      I => inst_23_OBUF_70,
      O => inst(22)
    );
  inst_21_OBUF : OBUF
    port map (
      I => inst_21_OBUF_13,
      O => inst(21)
    );
  inst_20_OBUF : OBUF
    port map (
      I => inst_20_OBUF_14,
      O => inst(20)
    );
  inst_19_OBUF : OBUF
    port map (
      I => inst_23_OBUF_70,
      O => inst(19)
    );
  inst_18_OBUF : OBUF
    port map (
      I => inst_23_OBUF_70,
      O => inst(18)
    );
  inst_17_OBUF : OBUF
    port map (
      I => inst_23_OBUF_70,
      O => inst(17)
    );
  inst_16_OBUF : OBUF
    port map (
      I => inst_16_OBUF_15,
      O => inst(16)
    );
  inst_15_OBUF : OBUF
    port map (
      I => inst_23_OBUF_70,
      O => inst(15)
    );
  inst_14_OBUF : OBUF
    port map (
      I => inst_23_OBUF_70,
      O => inst(14)
    );
  inst_13_OBUF : OBUF
    port map (
      I => inst_23_OBUF_70,
      O => inst(13)
    );
  inst_12_OBUF : OBUF
    port map (
      I => inst_12_OBUF_16,
      O => inst(12)
    );
  inst_11_OBUF : OBUF
    port map (
      I => inst_23_OBUF_70,
      O => inst(11)
    );
  inst_10_OBUF : OBUF
    port map (
      I => inst_23_OBUF_70,
      O => inst(10)
    );
  inst_9_OBUF : OBUF
    port map (
      I => inst_23_OBUF_70,
      O => inst(9)
    );
  inst_8_OBUF : OBUF
    port map (
      I => inst_23_OBUF_70,
      O => inst(8)
    );
  inst_7_OBUF : OBUF
    port map (
      I => inst_23_OBUF_70,
      O => inst(7)
    );
  inst_6_OBUF : OBUF
    port map (
      I => inst_23_OBUF_70,
      O => inst(6)
    );
  inst_5_OBUF : OBUF
    port map (
      I => inst_23_OBUF_70,
      O => inst(5)
    );
  inst_4_OBUF : OBUF
    port map (
      I => inst_23_OBUF_70,
      O => inst(4)
    );
  inst_3_OBUF : OBUF
    port map (
      I => inst_23_OBUF_70,
      O => inst(3)
    );
  inst_2_OBUF : OBUF
    port map (
      I => inst_2_OBUF_17,
      O => inst(2)
    );
  inst_1_OBUF : OBUF
    port map (
      I => inst_1_OBUF_18,
      O => inst(1)
    );
  inst_0_OBUF : OBUF
    port map (
      I => inst_0_OBUF_19,
      O => inst(0)
    );
  Mram_inst26114 : LUT6
    generic map(
      INIT => X"0000000000000001"
    )
    port map (
      I0 => pc_10_IBUF_1,
      I1 => pc_5_IBUF_6,
      I2 => pc_7_IBUF_4,
      I3 => pc_8_IBUF_3,
      I4 => pc_9_IBUF_2,
      I5 => N2,
      O => inst_20_OBUF_14
    );
  Mram_inst26114_SW0 : LUT3
    generic map(
      INIT => X"8F"
    )
    port map (
      I0 => pc_2_IBUF_9,
      I1 => pc_0_IBUF_11,
      I2 => Mram_inst2611,
      O => N2
    );
  Mram_inst32113 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => Mram_inst28112_59,
      I1 => Mram_inst16111_63,
      O => inst_24_OBUF_12
    );
  Mram_inst26111 : LUT5
    generic map(
      INIT => X"00000001"
    )
    port map (
      I0 => pc_1_IBUF_10,
      I1 => pc_6_IBUF_5,
      I2 => pc_3_IBUF_8,
      I3 => pc_4_IBUF_7,
      I4 => pc_11_IBUF_0,
      O => Mram_inst2611
    );
  Mram_inst28114 : LUT6
    generic map(
      INIT => X"0100010001000000"
    )
    port map (
      I0 => pc_9_IBUF_2,
      I1 => pc_6_IBUF_5,
      I2 => pc_7_IBUF_4,
      I3 => Mram_inst2811,
      I4 => Mram_inst28111,
      I5 => Mram_inst28112_59,
      O => inst_21_OBUF_13
    );
  Mram_inst28112 : LUT6
    generic map(
      INIT => X"0001000000000000"
    )
    port map (
      I0 => pc_5_IBUF_6,
      I1 => pc_4_IBUF_7,
      I2 => pc_3_IBUF_8,
      I3 => pc_2_IBUF_9,
      I4 => pc_0_IBUF_11,
      I5 => pc_1_IBUF_10,
      O => Mram_inst28111
    );
  Mram_inst21114 : LUT3
    generic map(
      INIT => X"A8"
    )
    port map (
      I0 => Mram_inst121,
      I1 => Mram_inst21111,
      I2 => Mram_inst1211_68,
      O => inst_16_OBUF_15
    );
  Mram_inst21112 : LUT6
    generic map(
      INIT => X"0000000000010000"
    )
    port map (
      I0 => pc_5_IBUF_6,
      I1 => pc_10_IBUF_1,
      I2 => pc_9_IBUF_2,
      I3 => pc_8_IBUF_3,
      I4 => pc_1_IBUF_10,
      I5 => pc_7_IBUF_4,
      O => Mram_inst21111
    );
  Mram_inst16113 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => Mram_inst1611,
      I1 => Mram_inst16111_63,
      O => inst_12_OBUF_16
    );
  Mram_inst16112 : LUT6
    generic map(
      INIT => X"0000000000000001"
    )
    port map (
      I0 => pc_7_IBUF_4,
      I1 => pc_6_IBUF_5,
      I2 => pc_8_IBUF_3,
      I3 => pc_9_IBUF_2,
      I4 => pc_10_IBUF_1,
      I5 => pc_11_IBUF_0,
      O => Mram_inst16111_63
    );
  Mram_inst16111 : LUT6
    generic map(
      INIT => X"0000000000010000"
    )
    port map (
      I0 => pc_5_IBUF_6,
      I1 => pc_4_IBUF_7,
      I2 => pc_3_IBUF_8,
      I3 => pc_2_IBUF_9,
      I4 => pc_1_IBUF_10,
      I5 => pc_0_IBUF_11,
      O => Mram_inst1611
    );
  Mram_inst511 : LUT6
    generic map(
      INIT => X"0000000000000001"
    )
    port map (
      I0 => pc_9_IBUF_2,
      I1 => pc_8_IBUF_3,
      I2 => pc_7_IBUF_4,
      I3 => pc_6_IBUF_5,
      I4 => pc_5_IBUF_6,
      I5 => N0,
      O => inst_2_OBUF_17
    );
  Mram_inst511_SW0 : LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFFFF"
    )
    port map (
      I0 => pc_10_IBUF_1,
      I1 => pc_3_IBUF_8,
      I2 => pc_2_IBUF_9,
      I3 => pc_11_IBUF_0,
      I4 => pc_0_IBUF_11,
      I5 => pc_4_IBUF_7,
      O => N0
    );
  Mram_inst31114 : LUT6
    generic map(
      INIT => X"0100010001000000"
    )
    port map (
      I0 => pc_9_IBUF_2,
      I1 => pc_6_IBUF_5,
      I2 => pc_7_IBUF_4,
      I3 => Mram_inst2811,
      I4 => Mram_inst28112_59,
      I5 => Mram_inst31112_66,
      O => inst_1_OBUF_18
    );
  Mram_inst31113 : LUT6
    generic map(
      INIT => X"0000000000010000"
    )
    port map (
      I0 => pc_5_IBUF_6,
      I1 => pc_4_IBUF_7,
      I2 => pc_3_IBUF_8,
      I3 => pc_2_IBUF_9,
      I4 => pc_0_IBUF_11,
      I5 => pc_1_IBUF_10,
      O => Mram_inst31112_66
    );
  Mram_inst31112 : LUT6
    generic map(
      INIT => X"0000000000010000"
    )
    port map (
      I0 => pc_5_IBUF_6,
      I1 => pc_4_IBUF_7,
      I2 => pc_3_IBUF_8,
      I3 => pc_1_IBUF_10,
      I4 => pc_2_IBUF_9,
      I5 => pc_0_IBUF_11,
      O => Mram_inst28112_59
    );
  Mram_inst31111 : LUT3
    generic map(
      INIT => X"01"
    )
    port map (
      I0 => pc_8_IBUF_3,
      I1 => pc_10_IBUF_1,
      I2 => pc_11_IBUF_0,
      O => Mram_inst2811
    );
  Mram_inst1214 : LUT3
    generic map(
      INIT => X"A8"
    )
    port map (
      I0 => Mram_inst121,
      I1 => Mram_inst1211_68,
      I2 => Mram_inst1212_67,
      O => inst_0_OBUF_19
    );
  Mram_inst1213 : LUT6
    generic map(
      INIT => X"0000000000000001"
    )
    port map (
      I0 => pc_5_IBUF_6,
      I1 => pc_1_IBUF_10,
      I2 => pc_10_IBUF_1,
      I3 => pc_9_IBUF_2,
      I4 => pc_8_IBUF_3,
      I5 => pc_7_IBUF_4,
      O => Mram_inst1212_67
    );
  Mram_inst1212 : LUT6
    generic map(
      INIT => X"0000000000010000"
    )
    port map (
      I0 => pc_5_IBUF_6,
      I1 => pc_10_IBUF_1,
      I2 => pc_9_IBUF_2,
      I3 => pc_8_IBUF_3,
      I4 => pc_0_IBUF_11,
      I5 => pc_7_IBUF_4,
      O => Mram_inst1211_68
    );
  Mram_inst1211 : LUT5
    generic map(
      INIT => X"00000001"
    )
    port map (
      I0 => pc_2_IBUF_9,
      I1 => pc_6_IBUF_5,
      I2 => pc_3_IBUF_8,
      I3 => pc_4_IBUF_7,
      I4 => pc_11_IBUF_0,
      O => Mram_inst121
    );
  XST_GND1 : GND
    port map (
      G => inst_23_OBUF_70
    );

end Structure;

