library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity Deco is
	port(
		ec : in std_logic;
		b : in std_logic_vector(3 downto 0);
		sal_7_seg : out std_logic_vector(6 downto 0)
	);
end Deco;

architecture Behavioral of Deco is
begin
	process(ec, b)
	begin
		if (ec = '0') then --codigo para guion
			sal_7_seg <= "1000000";
		else
			case b is
				when "0000" =>
					sal_7_seg <= "0111111"; -- 0
				when "0001" =>
					sal_7_seg <= "0000110"; -- 1
				when "0010" =>
					sal_7_seg <= "1011011"; -- 2
				when "0011" =>
					sal_7_seg <= "1001111"; -- 3
				when "0100" =>
					sal_7_seg <= "1100110"; -- 4
				when "0101" =>
					sal_7_seg <= "0101101"; -- 5
				when "0110" =>
					sal_7_seg <= "1111101"; -- 6
				when "0111" =>
					sal_7_seg <= "0100111"; -- 7
				when "1000" =>
					sal_7_seg <= "1111111"; -- 8
				when "1001" =>
					sal_7_seg <= "1101111"; -- 9
				when others =>
					sal_7_seg <= "1000000";
			end case;
		end if;
	end process;
end Behavioral;