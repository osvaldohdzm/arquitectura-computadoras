--CORONA AGUILAR JANIS ADRIANA
--PRACTUCA 6A

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

LIBRARY STD;
USE STD.TEXTIO.ALL;
USE ieee.std_logic_TEXTIO.ALL;

 
ENTITY P6_MemProg_TB_Archivos IS
END P6_MemProg_TB_Archivos;
 
ARCHITECTURE behavior OF P6_MemProg_TB_Archivos IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT P6_MemProg
    PORT(
         dir : IN  std_logic_vector(10 downto 0);
         data : OUT  std_logic_vector(24 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal dir : std_logic_vector(10 downto 0) := (others => '0');

 	--Outputs
   signal data : std_logic_vector(24 downto 0);
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: P6_MemProg PORT MAP (
          dir => dir,
          data => data
        );
		  
	-- Stimulus process
   stim_proc: process
   
		-- Archivo entradas.txt
		FILE			inputFile	: TEXT;
		VARIABLE		inputLine	: LINE;
		
		-- Variables proceso
		VARIABLE var_dir			: STD_LOGIC_VECTOR (15 DOWNTO 0);
		VARIABLE var_data			: STD_LOGIC_VECTOR (24 DOWNTO 0);
		
		-- Archivo salida.txt
		FILE			outputFile	: TEXT;
		VARIABLE		outputLine	: LINE;

		VARIABLE line				: STRING (1 TO 5);
		
		begin
			-- Archivos
			FILE_OPEN (inputFile, "entradas.txt", READ_MODE);
			FILE_OPEN (outputFile, "salida.txt", WRITE_MODE);
			
			line := "  A  ";
			WRITE(outputLine, line, RIGHT, line'LENGTH+1);
			line := "24.20";
			WRITE(outputLine, line, RIGHT, line'LENGTH+1);
			line := "19.16";
			WRITE(outputLine, line, RIGHT, line'LENGTH+1);
			line := "15.12";
			WRITE(outputLine, line, RIGHT, line'LENGTH+1);
			line := "11..8";
			WRITE(outputLine, line, RIGHT, line'LENGTH+1);
			line := "7...4";
			WRITE(outputLine, line, RIGHT, line'LENGTH+1);
			line := "3...0";
			WRITE(outputLine, line, RIGHT, line'LENGTH+1);
			
			WRITELINE(outputFile, outputLine);
			
			WAIT FOR 20 NS;
			
			-- Loop de Proceso
			WHILE NOT ENDFILE (inputFile) LOOP
				READLINE (inputFile, inputLine);
				
				HREAD(inputLine, var_dir);
				dir <= var_dir(10 downto 0); --Tomo solamente los 11 bits
				
				WAIT FOR 20 NS;
				
				var_data := data;
				
				-- Linea escritura en archivo
				HWRITE(outputLine, var_dir, RIGHT, 5);
				WRITE(outputLine, var_data(24 downto 20), RIGHT, 6);
				WRITE(outputLine, var_data(19 downto 16), RIGHT, 6);
				WRITE(outputLine, var_data(15 downto 12), RIGHT, 6);
				WRITE(outputLine, var_data(11 downto 8), RIGHT, 6);
				WRITE(outputLine, var_data(7 downto 4), RIGHT, 6);
				WRITE(outputLine, var_data(3 downto 0), RIGHT, 6);
				
				WRITELINE(outputFile, outputLine);
			END LOOP;
			
			-- Cierre de Archivos
			FILE_CLOSE(inputFile);
			FILE_CLOSE(outputFile);
			
			WAIT;
   end process;

END;