library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



entity DISPLAY is
    Port ( PC : in  STD_LOGIC_VECTOR (3 downto 0);
           CODIGO : out  STD_LOGIC_VECTOR (6 downto 0));
end DISPLAY;

architecture Behavioral of DISPLAY is
constant d0: std_logic_vector(6 downto 0) := "0000001";
	constant d1: std_logic_vector(6 downto 0) := "1001111";
	constant d2: std_logic_vector(6 downto 0)  := "0010010";
	constant d3: std_logic_vector(6 downto 0)  := "0000110";
	constant d4: std_logic_vector(6 downto 0)  := "1001100";
	constant d5: std_logic_vector(6 downto 0)  := "0100100";
	constant d6: std_logic_vector(6 downto 0)  := "0100000";
	constant d7: std_logic_vector(6 downto 0) := "0001111";
	constant d8: std_logic_vector(6 downto 0)  := "0000000";
	constant d9: std_logic_vector(6 downto 0)  := "0001100";
	constant dA: std_logic_vector(6 downto 0)  := "0001000";
	constant dB: std_logic_vector(6 downto 0)  := "1100000";
	constant dC: std_logic_vector(6 downto 0)  := "0110001";
	constant dD: std_logic_vector(6 downto 0)  := "1000010";
	constant dE: std_logic_vector(6 downto 0)  := "0110001";
	CONSTANT dF: std_logic_vector(6 downto 0)  := "0111001";


begin
PDISPLAY: PROCESS(PC)
BEGIN 
	case PC is
			when "0000" =>
				CODIGO <= d0; -- muestra 0
			when "0001" =>
				CODIGO <= d1; -- muestra 1
			when "0010" =>
				CODIGO <= d2; -- muestra 2
			when "0011" =>
				CODIGO <= d3; --muestra 3
			when "0100" =>
				CODIGO <= d4; --muestra 4
			when "0101" =>
				CODIGO <= d5; --muestra 5
			when "0110" =>
				CODIGO <= d6; --muestra 6
			when "0111" =>
				CODIGO <= d7; --muestra 7
			when "1000" =>
				CODIGO <= d8; --muestra 8
			when "1001" =>
				CODIGO <= d9; -- muestra 9
			when "1010" =>
				CODIGO <= dA;
			when "1011" =>
				CODIGO <= dB;
			when "1100" =>
				CODIGO <= dC;
			when "1101" =>
				CODIGO <= dD;
			when "1110" =>
				CODIGO <= dE;
			when others =>
				CODIGO <= dF;
		end case;
END PROCESS PDISPLAY;

end Behavioral;

