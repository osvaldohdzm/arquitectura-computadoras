LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
use ieee.numeric_std.all;
USE IEEE.std_logic_unsigned.ALL;

LIBRARY STD;
USE STD.TEXTIO.ALL;
USE ieee.std_logic_TEXTIO.ALL;
 
ENTITY P7_Pila_TestBench IS
END P7_Pila_TestBench;
 
ARCHITECTURE behavior OF P7_Pila_TestBench IS
 
    COMPONENT P7_Pila
    PORT(
         d_in : IN  std_logic_vector(15 downto 0);
         dw : IN  std_logic;
         wpc : IN  std_logic;
         up : IN  std_logic;
         clk : IN  std_logic;
         clr : IN  std_logic;
         d_out : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal d_in : std_logic_vector(15 downto 0) := (others => '0');
   signal dw : std_logic := '0';
   signal wpc : std_logic := '0';
   signal up : std_logic := '0';
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';

 	--Outputs
   signal d_out : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: P7_Pila PORT MAP (
          d_in => d_in,
          dw => dw,
          wpc => wpc,
          up => up,
          clk => clk,
          clr => clr,
          d_out => d_out
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
	file ARCH_RES : TEXT;
	variable LINEA_RES : line;
	VARIABLE VAR_d_out : STD_LOGIC_VECTOR(15 DOWNTO 0);
	
	file ARCH_VEC : TEXT;
	variable LINEA_VEC : line;
	VARIABLE VAR_d_in : STD_LOGIC_VECTOR(15 DOWNTO 0);
	VARIABLE VAR_up : STD_LOGIC;
	VARIABLE VAR_dw : STD_LOGIC;
	VARIABLE VAR_wpc : STD_LOGIC;
	VARIABLE VAR_clr : STD_LOGIC;
	VARIABLE VAR_sp : INTEGER := 0;
	VARIABLE CADENA : STRING(1 TO 4);
	begin
		file_open(ARCH_VEC, "vectores.txt", READ_MODE);
		file_open(ARCH_RES, "resultados.txt", WRITE_MODE);
		
		--ESCRIBE LA CABECERA
		CADENA := "   D";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE
		CADENA := "  UP";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE
		CADENA := "  Dw";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE
		CADENA := " WPC";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE
		CADENA := " CLR";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE
		CADENA := "  SP";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE
		CADENA := "   Q";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE
		writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo
		
		wait for 100 ns;
		FOR I IN 0 TO 20 LOOP
			readline(ARCH_VEC, LINEA_VEC);
			
			Hread(LINEA_VEC, VAR_d_in);
			d_in <= VAR_d_in;
			
			read(LINEA_VEC, VAR_up);
			up <= VAR_up;
			
			read(LINEA_VEC, VAR_dw);
			dw <= VAR_dw;
			
			read(LINEA_VEC, VAR_wpc);
			wpc <= VAR_wpc;
							
			read(LINEA_VEC, VAR_clr);
			clr <= VAR_clr;
				
			if(up = '1' OR wpc = '1') then
				VAR_sp := VAR_sp + 1;
			elsif(dw = '1') then
				VAR_sp := VAR_sp - 1;
			end if;
			
			WAIT UNTIL RISING_EDGE(CLK);
			
			VAR_d_out := d_out;
			
			Hwrite(LINEA_RES, VAR_d_in, right, 5);
			write(LINEA_RES, VAR_up, right, 5);
			write(LINEA_RES, VAR_dw, right, 5);
			write(LINEA_RES, VAR_wpc, right, 5);
			write(LINEA_RES, VAR_clr, right, 5);
			write(LINEA_RES, VAR_sp, right, 5);
			Hwrite(LINEA_RES, VAR_d_out, right, 5);
			
			writeline(ARCH_RES, LINEA_RES); --Escribe la linea en el archivo de resultados
		end loop;
		file_close(ARCH_VEC);
		file_close(ARCH_RES);
      wait;
   end process;

END;
