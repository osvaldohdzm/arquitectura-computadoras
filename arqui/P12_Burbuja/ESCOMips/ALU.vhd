library ieee;

use ieee.std_logic_1164.all;

entity ALU is
	generic
	(
		constant num: integer:=16
	);
	port
	(
		--Entradas
		A			:	in std_logic_vector(num-1 downto 0);	--Operando A
		B			:	in std_logic_vector(num-1 downto 0);	--Operando B
		ALUOP		:	in std_logic_vector(3 downto 0);			--Bus de selecci�n(sel_a,sel_b,op1,op0)
		
		--Salidas
		resultado:  out std_logic_vector(num-1 downto 0);  --Bits de Resultado
		ov			:	out std_logic;								   --Bandera de Overflow
		N			:	out std_logic;								   --Bandera de Signo
		Z			:	out std_logic;								   --Bandera Cero
		C			:	out std_logic								   --Bandera de Acarreo
	);
end ALU;

architecture A_ALU of ALU is
	
	signal Amux,Bmux: std_logic_vector(num-1 downto 0);
	signal res_and,res_or,res_xor,res_sum,res_parcial: std_logic_vector(num-1 downto 0);
	signal Acarreo: std_logic_vector(num downto 0);
	
	--Funci�n para calcular el valor de la bandera Z
	function getValueZ(signal res_parcial: std_logic_vector)
		return std_logic is
		variable auxZ: std_logic:='0';
	begin
		for i in 0 to res_parcial'length-1 loop
			auxZ:=(auxZ or res_parcial(i));
		end loop;
		auxZ:=not auxZ;
		return auxZ;
	end function;
	
begin 
	  Acarreo(0)<=ALUOP(2);
		
	  ciclo: for i in 0 to num-1 generate	
				Amux(i)<= A(i) xor ALUOP(3);
				Bmux(i)<= B(i) xor ALUOP(2);
				
				res_and(i)<= Amux(i) and Bmux(i);
				res_or(i) <= Amux(i) or  Bmux(i);
				res_xor(i)<= Amux(i) xor Bmux(i);
				
				res_sum(i)<= A(i) xor Bmux(i) xor  Acarreo(i);
				Acarreo(i+1)<=
								(
									(Bmux(i) and A(i)) or
									(A(i) and Acarreo(i)) or
									(Bmux(i) and Acarreo(i))
								);
				
				res_parcial(i)<= 	res_and(i) when ALUOP(1 downto 0) = "00" else
										res_or(i)  when aluop(1 downto 0) = "01" else
										res_xor(i) when aluop(1 downto 0) = "10" else
										res_sum(i);	
	   end generate;
 
	N<=res_parcial(num-1);
	 
	Z<=getValueZ(res_parcial);
		
	C<=(ALUOP(1) and ALUOP(0) and Acarreo(num));
	
	OV<= (ALUOP(1) and ALUOP(0) and (Acarreo(num) xor Acarreo(num-1)));
	
	resultado<=res_parcial;
	
end A_ALU;