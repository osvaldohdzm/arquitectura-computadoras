
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use std.textio.all;
use ieee.Numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_textio.all;
 
ENTITY t_union IS
END t_union;
 
ARCHITECTURE behavior OF t_union IS 
 
    COMPONENT Union
    PORT(
         LFU : IN  std_logic;
         clk : IN  std_logic;
         clr : IN  std_logic;
         banderasU : IN  std_logic_vector(3 downto 0);
         CP_CODEU : IN  std_logic_vector(4 downto 0);
         IU : IN  std_logic_vector(3 downto 0);
			NIVEL_UC : OUT  std_logic;
         SalidaU : OUT  std_logic_vector(19 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal LFU : std_logic := '0';
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';
   signal banderasU : std_logic_vector(3 downto 0) := (others => '0');
   signal CP_CODEU : std_logic_vector(4 downto 0) := (others => '0');
   signal IU : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal SalidaU : std_logic_vector(19 downto 0);
	signal NIVEL_UC : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Union PORT MAP (
          LFU => LFU,
          clk => clk,
          clr => clr,
          banderasU => banderasU,
          CP_CODEU => CP_CODEU,
          IU => IU,
			 NIVEL_UC => NIVEL_UC,
          SalidaU => SalidaU
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
	 stim_proc: process
   
   FILE archivoEntrada : TEXT;
	FILE archivoSalida : TEXT;
	VARIABLE L_VECTOR : LINE;
	VARIABLE L_SALIDA : LINE;
	VARIABLE VAR_NIVELSAL : STD_LOGIC;
	VARIABLE VAR_S : STD_LOGIC_VECTOR(19 downto 0);

	VARIABLE VAR_CLR : STD_LOGIC;
	VARIABLE VAR_OPCODE : STD_LOGIC_VECTOR (4 DOWNTO 0);
	VARIABLE VAR_FUNCODE : STD_LOGIC_VECTOR (3 DOWNTO 0);
	VARIABLE VAR_BANDERAS : STD_LOGIC_VECTOR (3 DOWNTO 0);
	VARIABLE VAR_LF : STD_LOGIC;

	VARIABLE VAR_UP : STD_LOGIC;
	VARIABLE VAR_DW : STD_LOGIC;
	VARIABLE VAR_WPC : STD_LOGIC;
	VARIABLE VAR_SDMP : STD_LOGIC;
	VARIABLE VAR_SR2 : STD_LOGIC;
	VARIABLE VAR_SWD : STD_LOGIC;
	VARIABLE VAR_SEXT : STD_LOGIC;
	VARIABLE VAR_SHE : STD_LOGIC;
	VARIABLE VAR_DIR : STD_LOGIC;
	VARIABLE VAR_WR : STD_LOGIC;
	VARIABLE VAR_SOP1 : STD_LOGIC;
	VARIABLE VAR_SOP2 : STD_LOGIC;
	VARIABLE VAR_SDMD : STD_LOGIC;
	VARIABLE VAR_WD : STD_LOGIC;
	VARIABLE VAR_SR : STD_LOGIC;
	VARIABLE VAR_ALUOP : STD_LOGIC_VECTOR(3 DOWNTO 0);
	VARIABLE CADENA : STRING (1 TO 6);

   	BEGIN

		FILE_OPEN (archivoEntrada, "in.txt", READ_MODE);
		FILE_OPEN (archivoSalida, "out.txt", WRITE_MODE);

		CADENA := " O_COD";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := " F_COD";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := " FLAGS";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := "   CLR";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := "    LF";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := "    UP";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := "    DW";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := "   WPC";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := "  SDMP";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := "   SR2";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := "   SWD";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := "  SEXT";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := "   SHE";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := "   DIR";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := "    WR";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := "  SOP1";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := "  SOP2";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := "  SDMD";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := "    WD";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := "    SR";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := " ALUOP";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		CADENA := " NIVEL";
		WRITE(L_SALIDA, CADENA, RIGHT, CADENA'LENGTH + 1);
		WRITELINE(archivoSalida, L_SALIDA);

		WAIT FOR 10 NS;

		-- wait for clk_period*10;

		WHILE NOT ENDFILE (archivoEntrada) LOOP

			READLINE(archivoEntrada, L_VECTOR);
			READ(L_VECTOR, VAR_OPCODE);
			CP_CODEU <= VAR_OPCODE;
			READ(L_VECTOR, VAR_FUNCODE);
			IU <= VAR_FUNCODE;
			READ(L_VECTOR, VAR_BANDERAS);
			BanderasU <= VAR_BANDERAS;
			READ(L_VECTOR, VAR_CLR);
			clr <= VAR_CLR;
			READ(L_VECTOR, VAR_LF);
			LFU <= VAR_LF;

			WAIT UNTIL RISING_EDGE(clk);
			wait for 5 ns;
			
			VAR_S := SalidaU;
			VAR_NIVELSAL := NIVEL_UC;

			VAR_UP := SalidaU(18);
			VAR_DW := SalidaU(17);
			VAR_WPC := SalidaU(16);
			VAR_SDMP := SalidaU(19);
			VAR_SR2 := SalidaU(15);
			VAR_SWD := SalidaU(14);
			VAR_SEXT := SalidaU(13);
			VAR_SHE := SalidaU(12);
			VAR_DIR := SalidaU(11);
			VAR_WR := SalidaU(10);
			VAR_SOP1 := SalidaU(9);
			VAR_SOP2 := SalidaU(8);
			VAR_SDMD := SalidaU(3);
			VAR_WD := SalidaU(2);
			VAR_SR := SalidaU(1);
			VAR_ALUOP := SalidaU(7 downto 4);

			WRITE(L_SALIDA, VAR_OPCODE, RIGHT, 7);
			WRITE(L_SALIDA, VAR_FUNCODE, RIGHT, 7);
			WRITE(L_SALIDA, VAR_BANDERAS, RIGHT, 7);
			WRITE(L_SALIDA, VAR_CLR, RIGHT, 7);
			WRITE(L_SALIDA, VAR_LF, RIGHT, 7);
			WRITE(L_SALIDA, VAR_UP, RIGHT, 7);
			WRITE(L_SALIDA, VAR_DW, RIGHT, 7);
			WRITE(L_SALIDA, VAR_WPC, RIGHT, 7);
			WRITE(L_SALIDA, VAR_SDMP, RIGHT, 7);
			WRITE(L_SALIDA, VAR_SR2, RIGHT, 7);
			WRITE(L_SALIDA, VAR_SWD, RIGHT, 7);
			WRITE(L_SALIDA, VAR_SEXT, RIGHT, 7);
			WRITE(L_SALIDA, VAR_SHE, RIGHT, 7);
			WRITE(L_SALIDA, VAR_DIR, RIGHT, 7);
			WRITE(L_SALIDA, VAR_WR, RIGHT, 7);
			WRITE(L_SALIDA, VAR_SOP1, RIGHT, 7);
			WRITE(L_SALIDA, VAR_SOP2, RIGHT, 7);
			WRITE(L_SALIDA, VAR_SDMD, RIGHT, 7);
			WRITE(L_SALIDA, VAR_WD, RIGHT, 7);
			WRITE(L_SALIDA, VAR_SR, RIGHT, 7);
			WRITE(L_SALIDA, VAR_ALUOP, RIGHT, 7);
			if(VAR_NIVELSAL='1') then
				write(L_SALIDA, "ALTO", right, 7);
			else
				write(L_SALIDA, "BAJO", right, 7);
			end if;			
			WRITELINE(archivoSalida, L_SALIDA);


			WAIT UNTIL FALLING_EDGE(clk);
			wait for 5 ns;
			
			VAR_S := SalidaU;
			VAR_NIVELSAL := NIVEL_UC;
			
			VAR_UP := SalidaU(18);
			VAR_DW := SalidaU(17);
			VAR_WPC := SalidaU(16);
			VAR_SDMP := SalidaU(19);
			VAR_SR2 := SalidaU(15);
			VAR_SWD := SalidaU(14);
			VAR_SEXT := SalidaU(13);
			VAR_SHE := SalidaU(12);
			VAR_DIR := SalidaU(11);
			VAR_WR := SalidaU(10);
			VAR_SOP1 := SalidaU(9);
			VAR_SOP2 := SalidaU(8);
			VAR_SDMD := SalidaU(3);
			VAR_WD := SalidaU(2);
			VAR_SR := SalidaU(1);
			VAR_ALUOP := SalidaU(7 downto 4);

			WRITE(L_SALIDA, VAR_OPCODE, RIGHT, 7);
			WRITE(L_SALIDA, VAR_FUNCODE, RIGHT, 7);
			WRITE(L_SALIDA, VAR_BANDERAS, RIGHT, 7);
			WRITE(L_SALIDA, VAR_CLR, RIGHT, 7);
			WRITE(L_SALIDA, VAR_LF, RIGHT, 7);
			WRITE(L_SALIDA, VAR_UP, RIGHT, 7);
			WRITE(L_SALIDA, VAR_DW, RIGHT, 7);
			WRITE(L_SALIDA, VAR_WPC, RIGHT, 7);
			WRITE(L_SALIDA, VAR_SDMP, RIGHT, 7);
			WRITE(L_SALIDA, VAR_SR2, RIGHT, 7);
			WRITE(L_SALIDA, VAR_SWD, RIGHT, 7);
			WRITE(L_SALIDA, VAR_SEXT, RIGHT, 7);
			WRITE(L_SALIDA, VAR_SHE, RIGHT, 7);
			WRITE(L_SALIDA, VAR_DIR, RIGHT, 7);
			WRITE(L_SALIDA, VAR_WR, RIGHT, 7);
			WRITE(L_SALIDA, VAR_SOP1, RIGHT, 7);
			WRITE(L_SALIDA, VAR_SOP2, RIGHT, 7);
			WRITE(L_SALIDA, VAR_SDMD, RIGHT, 7);
			WRITE(L_SALIDA, VAR_WD, RIGHT, 7);
			WRITE(L_SALIDA, VAR_SR, RIGHT, 7);
			WRITE(L_SALIDA, VAR_ALUOP, RIGHT, 7);
			if(VAR_NIVELSAL='1') then
				write(L_SALIDA, "ALTO", right, 7);
			else
				write(L_SALIDA, "BAJO", right, 7);
			end if;				
			WRITELINE(archivoSalida, L_SALIDA);

		END LOOP;

		FILE_CLOSE(archivoEntrada);
		FILE_CLOSE(archivoSalida);

	WAIT;
   	END PROCESS;

END;
