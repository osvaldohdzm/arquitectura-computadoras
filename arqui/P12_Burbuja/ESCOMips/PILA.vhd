
library IEEE;

use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity PILA is
	generic
	(
		constant tamSP	: integer:=3; --Tama�o de StackPointer
		constant tamDir: integer:=16 --Tama�o de los buses de direcciones de los buses de instrucci�n
	);
	port
	(
		--Entradas
		DataInPila	: in std_logic_vector(tamDir-1 downto 0);
		Load			: in std_logic;
		UP				: in std_logic;
		Down			: in std_logic;
		clk			: in std_logic;
		clr			: in std_logic;
		
		--Salidas
		DataOutPila: out std_logic_vector(tamDir-1 downto 0)
		--salidaSP: out std_logic_vector(tamSP-1 downto 0)
	); 
end PILA;

architecture A_PILA of PILA is

	TYPE Localidades IS ARRAY (0 to ((2**tamSP)-1)) OF std_logic_vector(tamDir-1 downto 0);

begin
	
	process (clk,clr)
		variable SP		: integer range 0 to ((2**tamSP)-1);
		variable PILA	: Localidades;
	begin
		
		if (clr='1') then
			PILA:=(others=>(others=>'0'));
			SP:=0;
		elsif(rising_edge(clk)) then
				if (Load='0' and UP='0' and Down='0')	  then 	-- Incremento de PC[SP] y retenci�n de SP
					PILA(SP):=PILA(SP)+1;
				elsif (Load='1' and UP='0' and Down='0') then	--B (Salto incondicional)
					PILA(SP):=DataInPila;
				elsif (Load='1' and UP='1' and Down='0') then	--CALL (Llamada a Subrutina)
					SP:=SP+1;
					PILA(SP):=DataInPila;
				elsif (Load='0' and UP='0' and Down='1') then	--RET 
					SP:=SP-1;
					PILA(SP):=PILA(SP)+1;
			end if;
		end if;
		
		DataOutPila<=PILA(SP);
		
		--salidaSP<=conv_std_logic_vector(SP,tamSP);
	end process;
	
end A_PILA;