--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:58:01 06/08/2018
-- Design Name:   
-- Module Name:   C:/Users/Edgar Roa/Desktop/1_pe/ESCOMIPS/ESCOMIPS/TB.vhd
-- Project Name:  ESCOMIPS
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: ESCOMIPS
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TB IS
END TB;
 
ARCHITECTURE behavior OF TB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ESCOMIPS
    PORT(
         rclk : IN  std_logic;
         clr : IN  std_logic;
         lectura2 : OUT  std_logic_vector(7 downto 0);
         salidaContador : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal rclk : std_logic := '0';
   signal clr : std_logic := '0';

 	--Outputs
   signal lectura2 : std_logic_vector(7 downto 0);
   signal salidaContador : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant rclk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ESCOMIPS PORT MAP (
          rclk => rclk,
          clr => clr,
          lectura2 => lectura2,
          salidaContador => salidaContador
        );

   -- Clock process definitions
   rclk_process :process
   begin
		rclk <= '0';
		wait for rclk_period/2;
		rclk <= '1';
		wait for rclk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      clr <= '1';
      wait for 20 ns;
		clr <='0';
		wait;
   end process;

END;
