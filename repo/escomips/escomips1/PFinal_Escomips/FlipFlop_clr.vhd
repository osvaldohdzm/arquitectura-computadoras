library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FlipFlop_clr is
    Port ( rclr, clk : in  STD_LOGIC;
           clr : out  STD_LOGIC);
end FlipFlop_clr;

architecture Behavioral of FlipFlop_clr is

begin
	process(clk)
	begin
		if (falling_edge(clk)) then
			clr <= rclr;
		end if;
	end process;

end Behavioral;

