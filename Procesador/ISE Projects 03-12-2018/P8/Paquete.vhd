library IEEE;
use IEEE.STD_LOGIC_1164.all;

package Paquete is

component DivisorFreq is
   port(
 	 Clock: in std_logic;
 	 Reset: in std_logic;
 	 Output: out std_logic );
end component;

component CartaASM is
    Port ( clk, clr, ini, z, a0 : in  STD_LOGIC;
           ea, la, eb, lb, ec : out  STD_LOGIC);
end component;

component Contador is
	port(
		clk : in std_logic;
		clr : in std_logic;
		eb : in std_logic;
		lb : in std_logic;
		b : out std_logic_vector(3 downto 0)
	);
end component;

component Decodificador is
	port(
		b : in std_logic_vector(3 downto 0);
		sal_7_seg : out std_logic_vector(6 downto 0)
	);
end component;

component Demux2 is
    Port ( deco_sal : in  STD_LOGIC_VECTOR (6 downto 0);
           ec : in  STD_LOGIC;
           sal : out  STD_LOGIC_VECTOR (6 downto 0));
end component;

component Registro is
	port(
		clk : in std_logic;
		clr : in std_logic;
		ea : in std_logic;
		la : in std_logic;
		d : in std_logic_vector(8 downto 0);
		a : inout std_logic_vector(8 downto 0);
		a0 : out std_logic;
		z : out std_logic
	);
end component;

end Paquete;