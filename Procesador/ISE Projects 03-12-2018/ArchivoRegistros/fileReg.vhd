library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity fileReg is
    Port ( D : in  STD_LOGIC_VECTOR (15 downto 0);
			  sel : in STD_LOGIC_VECTOR (3 downto 0);
           clk,clr,l : in  STD_LOGIC;
           Q : out  STD_LOGIC_VECTOR (15 downto 0));
end fileReg;

architecture Behavioral of fileReg is

component registro is
    Port ( d : in  STD_LOGIC_VECTOR (15 downto 0);
           clk, clr, l : in  STD_LOGIC;
           q : out  STD_LOGIC_VECTOR (15 downto 0));
end component;

type banco is array(0 to 15) of std_logic_vector(15 downto 0);
signal arreglo : banco;

begin
	ciclo : for i in 0 to 15 generate
		aux : registro port map(
			d => D,
			clk => clk,
			clr => clr,
			l => l,
			q => arreglo(i)
		);
	end generate;
	
	q <= arreglo(conv_integer(sel));
	
end Behavioral;

