--
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package PAQUETITO is


component pila is
    Port ( UP,DW,WPC,CLK,CLR : in  STD_LOGIC;
           D : in  STD_LOGIC_VECTOR (15 downto 0);
           PC : out  STD_LOGIC_VECTOR (15 downto 0);
           SP_AUX : out  STD_LOGIC_VECTOR (2 downto 0));
end component;



cOMPONENT Memoria_programa is
GENERIC(N_PALABRAS: INTEGER:=10);
    Port ( PC : in  STD_LOGIC_VECTOR (N_PALABRAS-1 downto 0);
           Inst : out  STD_LOGIC_VECTOR (24 downto 0));
end component;


COMPONENT PRINCARCHREG is
    Port ( clk,clr,she,dir,write_reg : in  STD_LOGIC;
           SHAMT,WR,read_reg1,read_reg2 : in  STD_LOGIC_VECTOR (3 downto 0);
           write_data : in  STD_LOGIC_VECTOR (15 downto 0);
           read_data1,read_data2 : out  STD_LOGIC_VECTOR (15 downto 0));
end COMPONENT;


COMPONENT PRINCI_FSM is
    Port ( CLK,CLR,LF : in  STD_LOGIC;
           COD_FUNC,BANDERAS : in  STD_LOGIC_VECTOR (3 downto 0);
           S : out  STD_LOGIC_VECTOR (19 downto 0);
           COD_OP : in  STD_LOGIC_VECTOR (4 downto 0));
end COMPONENT;

COMPONENT divFrecuencia is
	Port (
				Osc_clk				: in STD_LOGIC;
				clr					: in STD_LOGIC;
				clk					: out STD_LOGIC
			);
end COMPONENT;

COMPONENT Ext_signo is
    Port ( din : in  STD_LOGIC_VECTOR (11 downto 0);
           dout : out  STD_LOGIC_VECTOR (15 downto 0));
end COMPONENT;
COMPONENT F_CLR is
    Port ( CLR, CLK : in  STD_LOGIC;
           RCLR : out  STD_LOGIC);
end COMPONENT;

COMPONENT EXT_DIR is
    Port ( DIN : in  STD_LOGIC_VECTOR (11 downto 0);
           DOUT : out  STD_LOGIC_VECTOR (15 downto 0));
end COMPONENT;

COMPONENT PrincipalALU is
    Port ( a,b : in  STD_LOGIC_VECTOR (15 downto 0);
			  ALUOP: IN STD_LOGIC_VECTOR (3 DOWNTO 0);
           S : out  STD_LOGIC_VECTOR (15 downto 0);
           C,Z,OV,N : out  STD_LOGIC
			 );
end COMPONENT;



COMPONENT Memoria_datos is
GENERIC(NUM_DIR: INTEGER:=16;
			TAM_PALABRA: INTEGER:=16);

    Port (
			  wd,clk: in std_logic;
			  d_in : in  STD_LOGIC_VECTOR (TAM_PALABRA-1 downto 0);
           dir : in  STD_LOGIC_VECTOR (NUM_DIR-1 downto 0);
           d_out : out  STD_LOGIC_VECTOR (TAM_PALABRA-1 downto 0));
end component;


COMPONENT DISPLAY is
    Port ( PC : in  STD_LOGIC_VECTOR (3 downto 0);
           CODIGO : out  STD_LOGIC_VECTOR (6 downto 0));
end COMPONENT;




end PAQUETITO;

package body PAQUETITO is

 
end PAQUETITO;
