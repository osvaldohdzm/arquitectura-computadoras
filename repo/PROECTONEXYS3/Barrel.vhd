
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Barrel is
    Port ( DIR : in  STD_LOGIC;
           SHAMT : in  STD_LOGIC_VECTOR (3 downto 0);
           READ_DATA2 : in  STD_LOGIC_VECTOR (15 downto 0);
           Q : out  STD_LOGIC_VECTOR (15 downto 0));
end Barrel;

architecture Behavioral of Barrel is
begin
shifter: process(DIR,SHAMT,READ_DATA2)
variable aux: std_logic_vector(15 downto 0);
	BEGIN
		aux := READ_DATA2;
		IF(dir = '0')then
			FOR i in 0 to 3 LOOP
				IF(SHAMT(i) = '1') then
					FOR j IN 15 DOWNTO 0 LOOP
						IF (j<2**i)then
							aux(j):='0';
						ELSE
							aux(j):=aux(j-(2**i));
						end if;
					END LOOP;
				END IF;
			END LOOP;
		elsif (dir ='1')then
				FOR i in 0 to 3 LOOP
					IF(SHAMT(i) = '1')then
						for j in 0 to 15 LOOP
								if(-j+15 < 2**i)then
									aux(j) := '0';
								else
									aux(j):=aux(j+2**i);
								end if;
						end loop;
					end if;
				END LOOP;
			
		end if;
	Q<=aux;
	END PROCESS shifter;
	
end Behavioral;





























