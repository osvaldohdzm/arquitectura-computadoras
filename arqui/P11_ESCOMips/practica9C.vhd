-- Pr�ctica 9 | Pr�ctica 9C - Detector de Nivel | Marco Antonio Rubio Cort�s

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity practica9C is
	port
	(
		CLK							: in std_logic;
		CLR							: in std_logic;
		
		NA								: out std_logic
	);
end practica9C;

architecture A_practica9C of practica9C is

signal PCLK : std_logic;
signal NCLK : std_logic;

begin

	pclkR1 : process (CLK, CLR)
	begin
		if (CLR = '1')	 then
			PCLK <= '0';
		elsif (rising_edge(CLK)) then
			PCLK <= not PCLK;
		end if;
	end process pclkR1;
	
	nclkR2 : process (CLK, CLR)
	begin
		IF (CLR = '1') then
			NCLK <= '0';
		elsif (falling_edge(CLK)) then
			NCLK <= not NCLK;
		end if;
	end process nclkR2;
	
	NA <= PCLK xor NCLK;

end A_practica9C;

