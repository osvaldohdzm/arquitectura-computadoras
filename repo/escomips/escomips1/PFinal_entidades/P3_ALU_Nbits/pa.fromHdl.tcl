
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name P3_ALU_Nbits -dir "C:/PracticasArqui/P3_ALU_Nbits/planAhead_run_1" -part xc7a100tcsg324-3
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "P3_ALU_Nbits.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {P3_ALU_1bit.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {P3_ALU_Nbits.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set_property top P3_ALU_Nbits $srcset
add_files [list {P3_ALU_Nbits.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc7a100tcsg324-3
