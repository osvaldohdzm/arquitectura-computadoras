library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ALU is
	GENERIC(M: INTEGER:= 4);
    Port ( 
      A,B      : in    STD_LOGIC_VECTOR (M-1 downto 0);
      O        : in    STD_LOGIC_VECTOR (3 downto 0);      
      C,N,OV,Z : out   STD_LOGIC;
	  R        : inout STD_LOGIC_VECTOR (M-1 downto 0)
	);
end ALU;

architecture A_ALU of ALU is
begin
	process(O,A,B)
		variable BIN: STD_LOGIC_VECTOR (M-1 downto 0); 
		variable P: STD_LOGIC_VECTOR (M-1 downto 0);
		variable G: STD_LOGIC_VECTOR (M-1 downto 0);
		variable Ci: STD_LOGIC_VECTOR (M downto 0);
		begin
			C<='0';
			OV<='0';		
			case O is
				when "0000" => 
					R <= A AND B;
				when "0001" => 
					R <= A OR B;
				when "0010"=> 
					R <= A XOR B;
				when "0110" =>
					R <= A XOR NOT(B);
				when "1010" =>
					R <= NOT(A) XOR B;
				when "1101" =>
					if A = B then
						R<= NOT(A);
					else 
						R <= NOT(A) OR NOT(B);
					end if;
				when "1100" =>
					if A = B then 
						R <= NOT(A);
					else
						R <= NOT(A) AND NOT(B);
					end if;
				when "0011" | "0111" =>
					Ci(0):= O(2);
					for i in 0 to M-1 loop
					  BIN(i):= B(i) XOR O(2);
					  P(i):= a(i) XOR BIN(i);
					  G(i):= a(i) AND BIN(i);
					  R(i)<= P(i) XOR Ci(i);
					  Ci(i+1):=(P(i)AND Ci(i))OR G(i);
					end loop;
					C <= Ci(M);
					OV<= Ci(M) XOR Ci(M-1);
				when others =>
					R<="0000";
					C<='0';
					OV<='0';
			end case;
		end process;
		process(R)
		begin
					Z<=NOT(R(3)OR R(2)OR R(1)OR R(0));
					N<=R(M-1);
		end process;		
end architecture A_ALU;

