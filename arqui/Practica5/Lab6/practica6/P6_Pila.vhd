library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity P6_Pila is
	generic
	(
		constant TamanoBus 	: integer := 11; -- Tam Bus
		constant TamanoSP		: integer := 3   -- Num Palabra (2^SP)
	);

	port
	(
		pcData				: in std_logic_vector(TamanoBus-1 downto 0);
		contadorPrograma	: out std_logic_vector(TamanoBus-1 downto 0);
		stackPointer		: out std_logic_vector(TamanoSP-1 downto 0);
		L						: in std_logic; --De practica seria WPC
		UP						: in std_logic;
		DW						: in std_logic;
		clk					: in std_logic;
		clr					: in std_logic
	);
end P6_Pila;

architecture A_P6_Pila of P6_Pila is

type pila is array (0 to (2**TamanoSP)-1) of std_logic_vector (TamanoBus-1 downto 0);
signal PC: pila; -- Contador de Programa

begin
	process(clk, clr, PC)
	variable SP: integer range 0 to (2**TamanoSP)-1;
	
	begin
		if(clr = '1') then
			PC <= (others => (others => '0'));
			SP := 0;
		elsif(rising_edge(clk)) then
			if(L = '1' and UP = '0' and DW = '0') then		-- Branch
				PC(SP) <= pcData;
			elsif(L = '1' and UP = '1' and DW = '0') then	-- Call
				SP := SP + 1;
				PC(SP) <= pcData;
			elsif(L = '0' and UP = '0' and DW = '1') then	-- Retenci�n
				SP := SP - 1;
				PC(SP) <= PC(SP) + 1;
			else --Todos en Cero (Modo Normal de Operaci�n)
				PC(SP) <= PC(SP) + 1;
			end if;
		end if;
		-- Formula de PC direccionado por SP (direcci�n)
		contadorPrograma <= PC(SP);
		stackPointer <= conv_std_logic_vector(SP, 3); -- De Profe
	end process;
	
end A_P6_Pila;

