-- ESCOMips | Pr�ctica 3 - ALU | Marco Antonio Rubio Cort�s

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity practica3 is
	generic
	(
		N: integer := 16
	);
	Port
	(
		a								: in  STD_LOGIC_VECTOR (N-1 downto 0);
		b								: in  STD_LOGIC_VECTOR (N-1 downto 0);
		aluop							: in  STD_LOGIC_VECTOR (3 downto 0);
		result						: out  STD_LOGIC_VECTOR (N-1 downto 0);
		flags							: out  STD_LOGIC_VECTOR (3 downto 0)
	);
end practica3;

architecture A_practica3 of practica3 is

signal sel_a					: STD_LOGIC;
signal sel_b					: STD_LOGIC;

signal a_Res, b_Res			: STD_LOGIC_VECTOR (N-1 downto 0);
signal and_Res, or_Res, xor_Res, sum_Res			: STD_LOGIC_VECTOR (N-1 downto 0);
signal res						: STD_LOGIC_VECTOR (N-1 downto 0);
signal c							: STD_LOGIC_VECTOR (N downto 0);

constant zeros: std_logic_vector (N - 1 downto 0) := (others => '0');

begin
	c(0) <= sel_b;

	sel_a <= aluop(3);
	sel_b <= aluop(2);
	
	alu: for i in 0 to N-1 generate
		a_Res(i) <= a(i) xor sel_a;
		b_Res(i) <= b(i) xor sel_b;
		
		and_Res(i) <= a_Res(i) and b_Res(i);
		or_Res(i) <= a_Res(i) or b_Res(i);
		xor_Res(i) <= a_Res(i) xor b_Res(i);
		
		sum_Res(i) <= a_Res(i) xor b_Res(i) xor c(i);
		c(i+1) <= (b_Res(i) and c(i)) or (a_Res(i) and c(i)) or (a_Res(i) and b_Res(i));
		
		res(i) <= and_Res(i) when aluop(1 downto 0) = "00" else
	   or_Res(i)  when aluop(1 downto 0) = "01" else
		xor_Res(i) when aluop(1 downto 0) = "10" else 
		sum_Res(i);
	end generate;
	
	-- OV
	flags(3) <= c(N) xor c(N-1) when aluop(1 downto 0) = "11" else '0';
	-- N
	flags(2) <= res(N-1);
	-- Z
	flags(1) <= '1' when res = zeros else '0';
	-- C
	flags(0) <= c(N) when aluop(1 downto 0) = "11" else '0';
	
	result <= res;
	
end A_practica3;