library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
LIBRARY work;
use work.PAQUETITO.all;
entity PRINCIPAL is
    Port ( ECLR,ECLK : in  STD_LOGIC;
				AN : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
           DOUT: out  STD_LOGIC_VECTOR (7 downto 0);
			  DISPL : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
end PRINCIPAL;

architecture Behavioral of PRINCIPAL is
signal data_sr2: std_logic_vector(3 DOWNTO 0) ;
SIGNAL D_SOP1: STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL D_SOP2: STD_LOGIC_VECTOR(15 DOWNTO 0 );
SIGNAL D_SEXT: STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL D_SDMD: STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL D_SR: STD_LOGIC_VECTOR (15 DOWNTO 0 );
SIGNAL D_SWD: STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL D_SDMP: STD_LOGIC_VECTOR (15 DOWNTO 0 );


SIGNAL ALUO: STD_LOGIC_VECTOR(3 DOWNTO 0);
--PILA
SIGNAL PCC : STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL SPA: STD_LOGIC_VECTOR(2 DOWNTO 0);
--END PILA

--MEMORIA PROGRAMA
SIGNAL INST: STD_LOGIC_VECTOR(24 DOWNTO 0);


--END MEM PROG
--ARCH REGISTRO

SIGNAL READ_DATA1,READ_DATA2: STD_LOGIC_VECTOR(15 DOWNTO 0);




--END ARCH REGISTRO

SIGNAL SIGN: STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL DIRR: STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL RBANDERAS: STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL DATA_OUT : STD_LOGIC_VECTOR(15 DOWNTO 0);
--SIGNAL SCLR,DCLK: STD_LOGIC; 
-- UNIDAD DE CONTROL

SIGNAL S: STD_LOGIC_VECTOR(19 DOWNTO 0);
SIGNAL S_ALU: STD_LOGIC_VECTOR(15 DOWNTO 0);
--S(19) : SDMP  	S(18) : UP  	S(17) : DW  S(16) : WPC  S(15) : SR2    S(14) :	SWD      S(13) :	SEXT   S(12) :	SHE	S(11) :	DIR
-- S(10): WR 	S(9): SOP1 S(8) :	SOP2	S(7): ALUOP4 S(6): ALUOP3 S(5): ALUOP2	S(4): ALUOP1   s(3): sdmd s(2): wd s(1): sr s(0): LF									
SIGNAL CLR,CLK: STD_LOGIC;
--END UNIDAD DE CONTROL
begin
data_sr2 <= inst(11 downto 8) when S(15) ='0' else	inst(19 downto 16);
D_SOP1 <= PCC WHEN S(9)='1' ELSE READ_DATA1;
FLPFLOPCLR: PROCESS (ECLK)
BEGIN
	IF(FALLING_EDGE(ECLK))THEN
		CLR <=ECLR;
	END IF;
END PROCESS FLPFLOPCLR;

D_SOP2 <= READ_DATA2 WHEN S(8) = '0' ELSE D_SEXT;

comp_pila: PILA PORT MAP(S(18),S(17),S(16),CLK,CLR,D_SDMP,PCC,SPA);
--CLEAR: F_CLR PORT MAP (CLR,CLK,SCLR);

AN <= "1110";
comp_memprog: Memoria_programa  PORT MAP(PCC(9 downto 0),INST);

--CLOCKSITO: divFrecuencia port map (clk,clr,dclk);
COMP_UC: PRINCI_FSM PORT MAP(CLK,CLR,s(0),INST(3 DOWNTO 0),RBANDERAS,S, INST(24 DOWNTO 20));


comp_arch_regis: PRINCARCHREG PORT MAP(CLK,CLR,S(12),S(11),S(10),INST(7 DOWNTO 4),INST(19 DOWNTO 16),INST(15 DOWNTO 12),data_sr2,D_SWD,READ_DATA1,READ_DATA2);
ALUO <= S(7)&S(6)&S(5)&S(4);
ALU : PrincipalALU PORT MAP(D_SOP1,D_SOP2,ALUO,S_ALU,RBANDERAS(0),RBANDERAS(1),RBANDERAS(2),RBANDERAS(3));

D_SDMD <= S_ALU WHEN S(3) = '0' ELSE INST(15 DOWNTO 0);

D_SDMP<= INST(15 DOWNTO 0) WHEN S(19) = '0' ELSE D_SR;




DATOS: Memoria_datos PORT MAP(S(2),CLK,READ_DATA2,D_SDMD,DATA_OUT);

D_SEXT <= SIGN WHEN S(13)='0' ELSE DIRR;



SIGNO: Ext_signo PORT MAP(INST(11 DOWNTO 0),SIGN);




DIR: Ext_dir PORT MAP(INST(11 DOWNTO 0), DIRR);



D_SR <= S_ALU WHEN S(1) = '1' ELSE DATA_OUT;


D_SWD<= D_SR WHEN S(14) = '1' ELSE INST(15 DOWNTO 0);


divisor: divFrecuencia PORT MAP (ECLK,CLR,CLK);


--PC <= PCC(7 DOWNTO 0);
DISP : DISPLAY PORT MAP(PCC(3 DOWNTO 0),DISPL);
DOUT <= S_ALU(7 DOWNTO 0);


























end Behavioral;

