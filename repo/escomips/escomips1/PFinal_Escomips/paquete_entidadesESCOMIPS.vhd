library IEEE;
use IEEE.STD_LOGIC_1164.all;

package paquete_entidadesESCOMIPS is
	component FlipFlop_clr is
		 Port ( rclr, clk : in  STD_LOGIC;
				  clr : out  STD_LOGIC);
	end component;
	
	component P3_ALU_Nbits is
		Generic ( N: integer := 16 );
		Port ( a,b : in  STD_LOGIC_VECTOR (N-1 downto 0);
				 a_sel, b_sel : in  STD_LOGIC;
				 op : in  STD_LOGIC_VECTOR (1 downto 0);
				 s : inout  STD_LOGIC_VECTOR (N-1 downto 0);
				 flags : out  STD_LOGIC_VECTOR (3 downto 0));
	end component;
	
	component P4_ArchivoRegistros is
		 Port ( write_data : in  STD_LOGIC_VECTOR (15 downto 0);
				  write_reg, read_reg1, read_reg2, shamt : in  STD_LOGIC_VECTOR (3 downto 0);
				  clk, clr, wr, she, dir : in  STD_LOGIC;
				  read_data1, read_data2 : out  STD_LOGIC_VECTOR (15 downto 0));
	end component;
	
	component P5_MemoriaDatos is
	generic(
		dir : integer := 11;
		datos : integer := 16
		);
		 Port ( dir_w : in  STD_LOGIC_VECTOR (dir-1 downto 0);
				  data_in : in STD_LOGIC_VECTOR (datos-1 downto 0);
				  clk, wd : in  STD_LOGIC;
				  data_out : out  STD_LOGIC_VECTOR (datos-1 downto 0));
	end component;
	
	component P6_MemoriaPrograma is
	generic(
		n_dir : integer := 11
	);
		 Port ( dir : in  STD_LOGIC_VECTOR (n_dir-1 downto 0);
				  dato : out  STD_LOGIC_VECTOR (24 downto 0));
	end component;
	
	component P7_Pila is
	generic(
		n : integer := 4
	);
		 Port ( d_in : in  STD_LOGIC_VECTOR (15 downto 0);
				  dw, wpc, up, clk, clr : in  STD_LOGIC;
				  d_out : out  STD_LOGIC_VECTOR (15 downto 0));
	end component;
	
	component extensorDireccion is
		 Port ( d_in : in  STD_LOGIC_VECTOR (11 downto 0);
				  d_out : out  STD_LOGIC_VECTOR (15 downto 0));
	end component;
	
	component extensorSigno is
		 Port ( d_in : in  STD_LOGIC_VECTOR (11 downto 0);
				  d_out : out  STD_LOGIC_VECTOR (15 downto 0));
	end component;
	
	component main is
		 Port ( clk, clr, lf : in  STD_LOGIC;
				  a_fun, banderas : in  STD_LOGIC_VECTOR (3 downto 0);
				  op_code : in  STD_LOGIC_VECTOR (4 downto 0);
				  s : out  STD_LOGIC_VECTOR (19 downto 0));
	end component;
end paquete_entidadesESCOMIPS;
