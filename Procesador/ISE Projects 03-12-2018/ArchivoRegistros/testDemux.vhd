
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
 
ENTITY testDemux IS
END testDemux;
 
ARCHITECTURE behavior OF testDemux IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT demultiplexor
    PORT(
         sel : IN  std_logic_vector(3 downto 0);
         entrada : IN  std_logic;
         salida : INOUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal sel : std_logic_vector(3 downto 0) := (others => '0');
   signal entrada : std_logic := '0';

	--BiDirs
   signal salida : std_logic_vector(15 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: demultiplexor PORT MAP (
          sel => sel,
          entrada => entrada,
          salida => salida
        );


   -- Stimulus process
   stim_proc: process
   begin		
		
		entrada <= '0';
		sel <= "0000";
      wait for 30 ns;
		
		entrada <= '1';
		sel <= "0010";
      wait for 30 ns;
		
		entrada <= '1';
		sel <= "0000";
      wait for 30 ns;
		
		entrada <= '1';
		sel <= "1111";
		wait;
   end process;

END;
