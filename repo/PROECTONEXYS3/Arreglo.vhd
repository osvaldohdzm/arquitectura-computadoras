
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


entity Arreglo is
    Port ( clk,clr : in  STD_LOGIC;
           l : in  STD_LOGIC_VECTOR (15 DOWNTO 0);
           d : in  STD_LOGIC_VECTOR(15 DOWNTO 0);
           read_reg1 : in  STD_LOGIC_VECTOR (3 downto 0);
           read_reg2 : in  STD_LOGIC_VECTOR (3 downto 0);
			  q2 : out  STD_LOGIC_VECTOR (15 downto 0);
           q1 : out  STD_LOGIC_VECTOR (15 downto 0));
end Arreglo;

architecture Behavioral of Arreglo is
component Registro is
    Port ( clk,clr : in  STD_LOGIC;
           l : in  STD_LOGIC;
           d : in  STD_LOGIC_VECTOR (15 downto 0);
           q : out  STD_LOGIC_VECTOR (15 downto 0));
end component;
type arre is array (15 downto 0) of std_logic_vector(15 downto 0);
signal qaux: arre;
begin
	cic: 
	for i in 0 to 15 generate
		regis: Registro port map (clk,clr,l(i),d,qaux(i));
	end generate;
	q1 <= qaux(conv_integer(read_reg1));
	q2 <= qaux(conv_integer(read_reg2));
	
	
	
end Behavioral;

