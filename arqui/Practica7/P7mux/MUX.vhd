library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity MUX is
    Port ( b : in  STD_LOGIC_VECTOR (6 downto 0);
           Salida : out  STD_LOGIC_VECTOR (6 downto 0);
           sel : in  STD_LOGIC);
end MUX;

architecture Behavioral of MUX is
signal guion : STD_LOGIC_VECTOR(6 downto 0);

begin
guion <= "0000001";
   process(sel,b,guion)
	   begin
		   if (sel = '1') then
	         Salida <= b;
		   else
			   Salida <= guion;
         end if;
	end process;
end Behavioral;