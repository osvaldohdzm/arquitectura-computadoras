
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY t_regEdo IS
END t_regEdo;
 
ARCHITECTURE behavior OF t_regEdo IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT regEdo2
    PORT(
         banderas : IN  std_logic_vector(3 downto 0);
         clr : IN  std_logic;
         clk : IN  std_logic;
         LF : IN  std_logic;
         salReg : INOUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal banderas : std_logic_vector(3 downto 0) := (others => '0');
   signal clr : std_logic := '0';
   signal clk : std_logic := '0';
   signal LF : std_logic := '0';

	--BiDirs
   signal salReg : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: regEdo2 PORT MAP (
          banderas => banderas,
          clr => clr,
          clk => clk,
          LF => LF,
          salReg => salReg
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      clr <= '1';
		
		wait for 20 ns;
		
		clr <= '0';
		banderas <= "0001";
		
		wait for 20 ns;
		
		lf <= '1';
      wait;
   end process;

END;
