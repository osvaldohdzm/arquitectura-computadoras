-- ESCOMips | Pr�ctica 9 - Unidad de Control | Marco Antonio Rubio Cort�s

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.P_practica9.all;

entity practica9 is
	port
	(
		op_function						: in std_logic_vector (3 downto 0);
		op_code							: in std_logic_vector (4 downto 0);
		flags								: in std_logic_vector (3 downto 0);
		LF									: in std_logic;
		
		CLR								: in std_logic;
		CLK								: in std_logic;
		
		microInstruccion				: out std_logic_vector (19 downto 0)
	);
end practica9;

architecture A_practica9 of practica9 is

signal OV		: std_logic;
signal N			: std_logic;
signal Z			: std_logic;
signal C			: std_logic;

signal EQ		: std_logic;
signal NEQ		: std_logic;
signal LT		: std_logic;
signal LE		: std_logic;
signal GT		: std_logic;
signal GET		: std_logic;

signal NA		: std_logic;

signal TIPOR	: std_logic;
signal BEQI		: std_logic;
signal BNEQI	: std_logic;
signal BLTI		: std_logic;
signal BLETI	: std_logic;
signal BGTI		: std_logic;
signal BGETI	: std_logic;

signal zero	: std_logic_vector (4 downto 0);
signal SDOPC	: std_logic;
signal SM		: std_logic;

signal selOPC	: std_logic_vector (4 downto 0);
signal dataOP	: std_logic_vector (19 downto 0);
signal dataFU	: std_logic_vector (19 downto 0);

begin

	zero <= "00000";
	
	banderas : practica9A														-- Registro de Banderas
		port map
		(
			flags => flags,
			LF => LF,
			
			CLK => CLK,
			CLR => CLR,
			
			OV => OV,
			N => N,
			Z => Z,
			C => C
		);
	
	condicion : practica9B														-- Verificador de Condici�n
		port map
		(
			OV => OV,
			N => N,
			Z => Z,
			C => C,
			
			EQ => EQ,
			NEQ => NEQ,
			LT => LT,
			LE => LE,
			GT => GT,
			GET => GET
		);
	
	nivel : practica9C															-- Detector de nivel
		port map
		(
			CLK => CLK,
			CLR => CLR,
			
			NA => NA
		);
	
	decoInstr : practica9D														-- Decodificador de Instruccci�n
		port map
		(
			op_code => op_code,
		
			-- Salidas | Decodificador de Instrucci�n
			TIPOR => TIPOR,
			BEQI => BEQI,
			BNEQI => BNEQI,
			BLTI => BLTI,
			BLETI => BLETI,
			BGTI => BGTI,
			BGETI => BGETI
		);
	
	UC : practica9E																-- Unidad de Control
		port map
		(
			-- Entradas | Decodificador de Instrucci�n
			TIPOR => TIPOR,
			BEQI => BEQI,
			BNEQI => BNEQI,
			BLTI => BLTI,
			BLETI => BLETI,
			BGTI => BGTI,
			BGETI => BGETI,
			
			-- Entradas | Nivel
			NA => NA,
			
			-- Entradas | Verificador de Condici�n
			EQ => EQ,
			NEQ => NEQ,
			LT => LT,
			LE => LE,
			GT => GT,
			GET => GET,
			
			-- Entradas | Se�ales de control
			CLK => CLK,
			CLR => CLR,
			
			-- Salidas | MUX
			SDOPC => SDOPC,
			SM => SM
		);
	
	MUXSDOPC : practica9F														-- MUX SDOPC
		port map
		(
			zeros => zero,
			operationCode => op_code,
			SDOPC => SDOPC,
		
			selOPC => selOPC
		);
	
	ROMoperacion : practica9G													-- ROM C�digos de Operaci�n
		port map
		(
			dir => selOPC,
			data => dataOP
		);
	
	ROMfuncion : practica9H														-- ROM C�digos de Funci�n
		port map
		(
			dir => op_function	,
			data => dataFU
		);
	
	MUXSM : practica9I															-- MUX SM
		port map
		(
			microCodFun => dataFU,
			microCodOpe => dataOP,
			SM => SM,
			
			microInstruccion => microInstruccion
		);
	
end A_practica9;

