
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name Lab1_Arqui -dir "C:/Users/Edgar Roa/Documents/8vo sem/Arquitectura/Practica1/Lab_02032017/Lab1_Arqui/planAhead_run_2" -part xc7a100tcsg324-3
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "cascada.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {casccada.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set_property top cascada $srcset
add_files [list {cascada.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc7a100tcsg324-3
