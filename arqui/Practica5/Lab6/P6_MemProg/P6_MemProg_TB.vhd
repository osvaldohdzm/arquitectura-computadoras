LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY P6_MemProg_TB IS
END P6_MemProg_TB;
 
ARCHITECTURE behavior OF P6_MemProg_TB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT P6_MemProg
    PORT(
         dir : IN  std_logic_vector(10 downto 0);
         data : OUT  std_logic_vector(24 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal dir : std_logic_vector(10 downto 0) := (others => '0');

 	--Outputs
   signal data : std_logic_vector(24 downto 0);
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: P6_MemProg PORT MAP (
          dir => dir,
          data => data
        );

	-- Stimulus process
   stim_proc: process
	variable aux: std_logic_vector(15 downto 0); --Auxiliar para los 4 Hexa
   begin
	
		aux := x"0000";
		dir <= aux(10 downto 0);
		wait for 20 ns;
		
		aux := x"0001";
		dir <= aux(10 downto 0);
		wait for 20 ns;
		
		aux := x"0002";
		dir <= aux(10 downto 0);
		wait for 20 ns;
		
		aux := x"0003";
		dir <= aux(10 downto 0);
		wait for 20 ns;
		
		aux := x"0004";
		dir <= aux(10 downto 0);
		wait for 20 ns;
		
		aux := x"0002";
		dir <= aux(10 downto 0);
		wait for 20 ns;
		
		aux := x"0003";
		dir <= aux(10 downto 0);
		
      wait;
   end process;

END;