
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY sim_control IS
END sim_control;
 
ARCHITECTURE behavior OF sim_control IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Control
    PORT(
         clk : IN  std_logic;
         clr : IN  std_logic;
         eq : IN  std_logic;
         neq : IN  std_logic;
         lt : IN  std_logic;
         le : IN  std_logic;
         gthan : IN  std_logic;
         get : IN  std_logic;
         tipor : IN  std_logic;
         beqi : IN  std_logic;
         bneqi : IN  std_logic;
         blti : IN  std_logic;
         bleti : IN  std_logic;
         bgti : IN  std_logic;
         bgeti : IN  std_logic;
         na : IN  std_logic;
         sdopc : OUT  std_logic;
         sm : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';
   signal eq : std_logic := '0';
   signal neq : std_logic := '0';
   signal lt : std_logic := '0';
   signal le : std_logic := '0';
   signal gthan : std_logic := '0';
   signal get : std_logic := '0';
   signal tipor : std_logic := '0';
   signal beqi : std_logic := '0';
   signal bneqi : std_logic := '0';
   signal blti : std_logic := '0';
   signal bleti : std_logic := '0';
   signal bgti : std_logic := '0';
   signal bgeti : std_logic := '0';
   signal na : std_logic := '0';

 	--Outputs
   signal sdopc : std_logic;
   signal sm : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Control PORT MAP (
          clk => clk,
          clr => clr,
          eq => eq,
          neq => neq,
          lt => lt,
          le => le,
          gthan => gthan,
          get => get,
          tipor => tipor,
          beqi => beqi,
          bneqi => bneqi,
          blti => blti,
          bleti => bleti,
          bgti => bgti,
          bgeti => bgeti,
          na => na,
          sdopc => sdopc,
          sm => sm
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
	
	na_process :process
   begin
		na <= '0';
		wait for clk_period/2;
		na <= '1';
		wait for clk_period/2;
   end process;

   -- Stimulus process
   stim_proc: process
   begin		
      -- insert stimulus here 
		tipor <= '1';
      wait for 60 ns;
		tipor <= '0';
		wait for 85 ns;
		beqi <= '1';
		wait for 10 ns;
		eq <= '1';
		wait for 10 ns;
   end process;

END;
