 LIBRARY ieee;
LIBRARY STD;
USE STD.TEXTIO.ALL;
USE ieee.std_logic_TEXTIO.ALL;	

USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_UNSIGNED.ALL;
USE ieee.std_logic_ARITH.ALL;

ENTITY TB_MEMPROGRAMA IS
END TB_MEMPROGRAMA;
 
ARCHITECTURE behavior OF TB_MEMPROGRAMA IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Memoria_programa
    PORT(
         PC : IN  std_logic_vector(11 downto 0);
         Inst : OUT  std_logic_vector(24 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal PC : std_logic_vector(11 downto 0) := (others => '0');

 	--Outputs
   signal Inst : std_logic_vector(24 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 

 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Memoria_programa PORT MAP (
          PC => PC,
          Inst => Inst
        );
 stim_proc: process
  file ARCH_RES : TEXT;																					
	variable LINEA_RES : line;
	
	VARIABLE VAR_INST : STD_LOGIC_VECTOR(24 DOWNTO 0);
	
	file ARCH_VEC : TEXT;
	variable LINEA_VEC : line;
	
	VARIABLE VAR_PC: STD_LOGIC_VECTOR(11 DOWNTO 0);
	
	VARIABLE CADENA : STRING(1 TO 10);
   begin		
		file_open(ARCH_VEC, "VECTORES.TXT", READ_MODE); 	
		file_open(ARCH_RES, "RESULTADO.TXT", WRITE_MODE); 	

		CADENA := "    A     ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := "   OPCODE ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := "19......16";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	
		CADENA := "15......12";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		CADENA := "11.......8";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		CADENA := "7........4";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		CADENA := "3........0";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);
		writeline(ARCH_RES,LINEA_RES);

		WAIT FOR 100 NS;
		FOR I IN 0 TO 6 LOOP
			readline(ARCH_VEC,LINEA_VEC); 

			hread(LINEA_VEC, VAR_PC);
			PC <= VAR_PC;
			
			wait for 10 ns;
			VAR_INST := INST;
			hwrite(LINEA_RES, VAR_PC, right, 7);	
		 	
			write(LINEA_RES, VAR_INST(24 DOWNTO 20),right,14);
			write(LINEA_RES, VAR_INST(19 DOWNTO 16),right,9);
			write(LINEA_RES, VAR_INST(15 DOWNTO 12),right,11);
			write(LINEA_RES, VAR_INST(11 DOWNTO 8),right,10);
			write(LINEA_RES, VAR_INST(7 DOWNTO 4),right,11);
			write(LINEA_RES, VAR_INST(3 DOWNTO 0),right,11);
		
			writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo
		end loop;
		file_close(ARCH_VEC);  -- cierra el archivo
		file_close(ARCH_RES);  -- cierra el archivo

      wait;
   end process;
END;
