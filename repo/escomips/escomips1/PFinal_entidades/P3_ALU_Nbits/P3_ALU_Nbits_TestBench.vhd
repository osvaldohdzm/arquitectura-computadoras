LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY P3_ALU_Nbits_TestBench IS
END P3_ALU_Nbits_TestBench;
 
ARCHITECTURE behavior OF P3_ALU_Nbits_TestBench IS
 
    COMPONENT P3_ALU_Nbits
    PORT(
         a : IN  std_logic_vector(3 downto 0);
         b : IN  std_logic_vector(3 downto 0);
         a_sel : IN  std_logic;
         b_sel : IN  std_logic;
         op : IN  std_logic_vector(1 downto 0);
         s : INOUT  std_logic_vector(3 downto 0);
         flags : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(3 downto 0) := (others => '0');
   signal b : std_logic_vector(3 downto 0) := (others => '0');
   signal a_sel : std_logic := '0';
   signal b_sel : std_logic := '0';
   signal op : std_logic_vector(1 downto 0) := (others => '0');

	--BiDirs
   signal s : std_logic_vector(3 downto 0);

 	--Outputs
   signal flags : std_logic_vector(3 downto 0);
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: P3_ALU_Nbits PORT MAP (
          a => a,
          b => b,
          a_sel => a_sel,
          b_sel => b_sel,
          op => op,
          s => s,
          flags => flags
        );

   -- Stimulus process
   stim_proc: process
   begin
      wait for 10 ns;
      --a=5, b=-2	
		a<="0101";
		b<="1110";
		--a+b
		a_sel<='0';
		b_sel<='0';
		op<="11";
		wait for 10 ns;
		--a-b
		a_sel<='0';
		b_sel<='1';
		op<="11";
		wait for 10 ns;
		--and
		a_sel<='0';
		b_sel<='0';
		op<="00";
		wait for 10 ns;
		--nand
		a_sel<='1';
		b_sel<='1';
		op<="01";
		wait for 10 ns;
		--or
		a_sel<='0';
		b_sel<='0';
		op<="01";
		wait for 10 ns;
		--nor
		a_sel<='1';
		b_sel<='1';
		op<="00";
		wait for 10 ns;
		--xor
		a_sel<='0';
		b_sel<='0';
		op<="10";
		wait for 10 ns;
		--xnor
		a_sel<='0';
		b_sel<='1';
		op<="10";
		wait for 10 ns;
		
		--a=5, b=7
		a<="0101";
		b<="0111";
		--a+b
		a_sel<='0';
		b_sel<='0';
		op<="11";
		wait for 10 ns;
		
		--a=5, b=5
		a<="0101";
		b<="0101";
		--a-b
		a_sel<='0';
		b_sel<='1';
		op<="11";
		wait for 10 ns;
		--nand
		a_sel<='1';
		b_sel<='1';
		op<="01";
		wait for 10 ns;
		--not
		a_sel<='1';
		b_sel<='1';
		op<="00";
      wait;
   end process;

END;
