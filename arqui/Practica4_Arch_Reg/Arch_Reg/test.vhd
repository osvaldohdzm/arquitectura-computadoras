--------------------------------------------------------------------------------
-- 
--------------------------------------------------------------------------------
LIBRARY ieee;
LIBRARY STD;
USE ieee.std_logic_1164.ALL;
USE STD.TEXTIO.ALL;
USE ieee.std_logic_TEXTIO.ALL;
USE ieee.std_logic_UNSIGNED.ALL;
USE ieee.std_logic_ARITH.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test IS
	GENERIC (
			n : integer := 16
	);
END test;
 
ARCHITECTURE behavior OF test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT p4
    PORT(
         clk : IN  std_logic;
         clr : IN  std_logic;
         writeData : IN  std_logic_vector(15 downto 0);
         writeReg : IN  std_logic_vector(3 downto 0);
         wr : IN  std_logic;
         readRegister1 : IN  std_logic_vector(3 downto 0);
         readRegister2 : IN  std_logic_vector(3 downto 0);
         she : IN  std_logic;
         dir : IN  std_logic;
         readData1 : INOUT  std_logic_vector(n-1 downto 0);
         readData2 : OUT  std_logic_vector(n-1 downto 0);
         shamt : IN  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';
   signal writeData : std_logic_vector(n-1 downto 0) := (others => '0');
   signal writeReg : std_logic_vector(3 downto 0) := (others => '0');
   signal wr : std_logic := '0';
   signal readRegister1 : std_logic_vector(3 downto 0) := (others => '0');
   signal readRegister2 : std_logic_vector(3 downto 0) := (others => '0');
   signal she : std_logic := '0';
   signal dir : std_logic := '0';
   signal shamt : std_logic_vector(3 downto 0) := (others => '0');

	--BiDirs
   signal readData1 : std_logic_vector(n-1 downto 0);

 	--Outputs
   signal readData2 : std_logic_vector(n-1 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: p4 PORT MAP (
          clk => clk,
          clr => clr,
          writeData => writeData,
          writeReg => writeReg,
          wr => wr,
          readRegister1 => readRegister1,
          readRegister2 => readRegister2,
          she => she,
          dir => dir,
          readData1 => readData1,
          readData2 => readData2,
          shamt => shamt
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   file ARCH_RES : TEXT;																					
	variable LINEA_RES : line;
	VARIABLE var_rd1: STD_LOGIC_VECTOR(n-1 DOWNTO 0);
	VARIABLE var_rd2: STD_LOGIC_VECTOR(n-1 DOWNTO 0);
	
	file ARCH_VEC : TEXT;
	variable LINEA_VEC : line;
	VARIABLE var_reg1 : STD_LOGIC_VECTOR(3 DOWNTO 0);
	VARIABLE var_reg2 : STD_LOGIC_VECTOR(3 DOWNTO 0);
	VARIABLE var_shamt : STD_LOGIC_VECTOR(3 DOWNTO 0);
	VARIABLE var_wreg : STD_LOGIC_VECTOR(3 DOWNTO 0);
	VARIABLE var_wdata : STD_LOGIC_VECTOR(n-1 DOWNTO 0);
	VARIABLE var_wr : STD_LOGIC;
	VARIABLE var_she : STD_LOGIC;
	VARIABLE var_dir : STD_LOGIC;
	VARIABLE var_clr : STD_LOGIC;
	VARIABLE CADENA : STRING(1 TO 5);
   begin		
		file_open(ARCH_VEC, "PRUEBA.TXT", READ_MODE); 	
		file_open(ARCH_RES, "RESULTADO.TXT", WRITE_MODE); 	
		--FORMATO DE ENTRADA/SALIDA (ARCHIVO PRUEBA.TXT, RESULTADO.TXT)
		CADENA := "RR1  ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA " RR1"
		CADENA := " RR2 ";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA " RR2"
		CADENA := "SHAMT";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA " SHAMT"
		CADENA := " WREG";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA " WRITE REGISTER"
		CADENA := "   WD";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);  --ESCRIBE LA CADENA " WRITE DATA"
		CADENA := "   WR";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);  --ESCRIBE LA CADENA " WR"
		CADENA := "  SHE";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);  --ESCRIBE LA CADENA " SHE"
		CADENA := "  DIR";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);  --ESCRIBE LA CADENA " DIR"
		CADENA := "  RD1";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);  --ESCRIBE LA CADENA " RD1"
		CADENA := "  RD2";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);  --ESCRIBE LA CADENA " RD2"
		writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo

		WAIT FOR 100 NS;
		FOR I IN 0 TO 10 LOOP
			readline(ARCH_VEC,LINEA_VEC); -- lee una linea completa

			Hread(LINEA_VEC, var_reg1);  --Lectura en Hexadecimal RReg1
			readRegister1 <= var_reg1;   --Asignación de la lectura a la variable
			Hread(LINEA_VEC, var_reg2);  --Lectura en Hexadecimal RReg2
			readRegister2 <= var_reg2;
			Hread(LINEA_VEC, var_shamt); --Lectura en Hexadecimal SHAMT
			shamt <= var_shamt;		
			Hread(LINEA_VEC, var_wreg);  --Lectura en Hexadecimal WREG
			writeReg <= var_wreg;
			Hread(LINEA_VEC, var_wdata); --Lectura en Hexadecimal WRITE DATA
			writeData <= var_wdata;
			read(LINEA_VEC, var_wr);     --Lectura WR
			wr <= var_wr;
			read(LINEA_VEC, var_she);    --Lectura SHE
			she <= var_she;
			read(LINEA_VEC, var_dir);    --Lectura DIR
			dir <= var_dir;
			read(LINEA_VEC, var_clr);    --Lectura CLR
			clr <= var_clr;
			
			WAIT UNTIL RISING_EDGE(CLK);	--ESPERO AL FLANCO DE SUBIDA 

				var_rd1:= readData1;
				var_rd2:= readData2;
				
			Hwrite(LINEA_RES, var_reg1, right, 4);	   --ESCRIBE EL CAMPO reg1 (En Hexa)
			Hwrite(LINEA_RES, var_reg2, right, 5);    --ESCRIBE EL CAMPO reg2 (En Hexa)
			Hwrite(LINEA_RES, var_shamt, 	right, 5);	--ESCRIBE EL CAMPO shamt
			Hwrite(LINEA_RES, var_wreg, 	right, 8);	--ESCRIBE EL CAMPO wreg
			Hwrite(LINEA_RES, var_wdata, 	right, 8);	--ESCRIBE EL CAMPO wdata
			write(LINEA_RES, var_wr, right, 5);
			write(LINEA_RES, var_she, right, 5);
			write(LINEA_RES, var_dir, right, 7);
			Hwrite(LINEA_RES, var_rd1, right, 8);
			Hwrite(LINEA_RES, var_rd2, right, 6);

			writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo
			
		end loop;
		file_close(ARCH_VEC);  -- cierra el archivo
		file_close(ARCH_RES);  -- cierra el archivo

      wait;
   end process;

--end process;
END;
