library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity condicion is
    Port ( banderas : in  STD_LOGIC_VECTOR (3 downto 0);
           eq, neq, lt, le, gt, get : out  STD_LOGIC);
end condicion;

architecture Behavioral of condicion is
signal c, z : STD_LOGIC;
begin
	c <= banderas(0);
	z <= banderas(1);
	
	eq <= z;
	neq <= not z;
	lt <= not c;
	le <= z or (not c);
	gt <= (not z) and c;
	get <= c;
	
end Behavioral;

