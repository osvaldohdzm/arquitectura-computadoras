
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity condicion is
    Port ( banderas : in  STD_LOGIC_VECTOR (3 downto 0);
           EQ, NE, L, LE, G, GE : out  STD_LOGIC);
end condicion;

architecture Behavioral of condicion is

signal flags : std_logic_vector (3 downto 0);

begin
    
	     flags <= banderas;

			EQ <= flags(0);
			NE <= not flags(0);
			L <= not flags(1);
			LE <= (flags(0)) or (not flags(1));
			G <= (not flags(0)) or flags(1);
			GE <= flags(1);


end Behavioral;

