library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity microcodigoFuncion is
    Port ( a : in  STD_LOGIC_VECTOR (3 downto 0);
           d : out  STD_LOGIC_VECTOR (19 downto 0));
end microcodigoFuncion;

architecture Behavioral of microcodigoFuncion is
type mem is array (0 to 15) of STD_LOGIC_VECTOR (19 downto 0);
signal aux_mem : mem := (
	0 => "00000100110000000001",	--ADD
	1 => "00000100110000100001",	--SUB
	2 => "00000100110000001001",	--AND
	3 => "00000100110000010001", --OR
	4 => "00000100110000011001", --XOR
	5 => "00000100110001110001", --NAND
	6 => "00000100110001101001", --NOR
	7 => "00000100110000111001", --XNOR
	8 => "00000100110001110001", --NOT
	9 => "00000011100000000000", --SLL
	10 => "00000010100000000000", --SRL
	others => (others => '0')
);
begin
	d <= aux_mem(conv_integer(a));
end Behavioral;

