library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library work;
use work.paquete_entidades.ALL;

entity main is
    Port ( clk, clr, lf : in  STD_LOGIC;
           a_fun, banderas : in  STD_LOGIC_VECTOR (3 downto 0);
           op_code : in  STD_LOGIC_VECTOR (4 downto 0);
           s : out  STD_LOGIC_VECTOR (19 downto 0));
end main;

architecture Behavioral of main is
signal d_fun, d_op_code : STD_LOGIC_VECTOR(19 downto 0);
signal a_op_code : STD_LOGIC_VECTOR(4 downto 0);
signal b_out : STD_LOGIC_VECTOR(3 downto 0);
signal SDOPC, SM, tipoR, BEQ, BNEQ, BLT, BLE, BGT, BGET, na, eq, neq, lt, le, gt, get : STD_LOGIC;
begin
	--entidad => main
	e_unidadControl : UnidadControl port map (
		clr => clr,
		clk => clk,
		tipoR => tipoR,
		BEQ => BEQ,
		BNEQ => BNEQ,
		BLT => BLT,
		BLE => BLE,
		BGT => BGT,
		BGET => BGET,
		na => na,
		eq => eq,
		neq => neq,
		lt => lt,
		le => le,
		gt => gt,
		get => get,
		SDOPC => SDOPC,
		SM => SM
	);
	
	e_condicion : condicion port map(
		banderas => banderas,
		eq => eq,
		neq => neq,
		lt => lt,
		le => le,
		gt => gt,
		get => get
	);
	
	e_decoInst : decoInst port map(
		op_code => op_code,
		tipoR => tipoR,
		BEQ => BEQ,
		BNEQ => BNEQ,
		BLT => BLT,
		BLE => BLE,
		BGT => BGT,
		BGET => BGET
	);
	
	e_microcodigoFuncion : microcodigoFuncion port map(
		a => a_fun,
		d => d_fun
	);
	
	e_microcodigoOpCode : microcodigoOpCode port map(
		a => a_op_code,
		d => d_op_code
	);
	
	e_mux_opCode : mux_opCode port map(
		op_code => op_code,
		SDOPC => SDOPC,
		a => a_op_code
	);
	
	e_nivel : nivel port map(
		clk => clk,
		clr => clr,
		na => na
	);
	
	e_reg_edo : reg_edo port map(
		clk => clk,
		clr => clr,
		lf => lf,
		banderas => banderas,
		b_out => b_out
	);
	
	e_mux_salida : mux_salida port map(
		d_fun => d_fun,
		d_op_code => d_op_code,
		SM => SM,
		s => s
	);

end Behavioral;

