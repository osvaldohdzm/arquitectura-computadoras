-- Proyecto - Paquete del ESCOMips | Marco Antonio Rubio Cort�s

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package P_ESCOMips is
	component practica7G is														-- Divisor de Frecuencias
		Port
		(
			Osc_clk				: in STD_LOGIC;
			clr					: in STD_LOGIC;
			clk					: out STD_LOGIC
		);
	end component;

	component practica3 is														-- ALU
		generic
		(
			N: integer := 16
		);
		Port
		(
			a								: in  STD_LOGIC_VECTOR (N-1 downto 0);
			b								: in  STD_LOGIC_VECTOR (N-1 downto 0);
			aluop							: in  STD_LOGIC_VECTOR (3 downto 0);
			result						: out  STD_LOGIC_VECTOR (N-1 downto 0);
			flags							: out  STD_LOGIC_VECTOR (3 downto 0)
		);
	end component;
	
	component practica4 is														-- Archivo de Registros
		generic
		(
			wordSize : integer := 16;												-- Tama�o de palabra
			busSize  : integer := 4													-- Tama�o del bus de direcciones
		);
		port
		(
			readRegister1	: in 	std_logic_vector(busSize-1 downto 0);
			readRegister2	: in 	std_logic_vector(busSize-1 downto 0);
			writeRegister	: in 	std_logic_vector(busSize-1 downto 0);
			SHAMT				: in 	std_logic_vector(busSize-1 downto 0);
			writeData		: in 	std_logic_vector(wordSize-1 downto 0);
			readData1		: out	std_logic_vector(wordSize-1 downto 0);
			readData2		: out	std_logic_vector(wordSize-1 downto 0);
			SHE				: in	std_logic;
			DIR				: in	std_logic;
			WR					: in	std_logic;
			clk				: in	std_logic;
			clr				: in	std_logic
		);
	end component;
	
	component practica5 is														-- Memoria de Datos
		generic
		(
			wordSize : integer := 16;											-- Tama�o de palabra
			busSize  : integer := 11												-- Tama�o del bus de direcciones
		);
		port
		(
			dir		: in 	std_logic_vector(busSize-1 downto 0);
			d_in		: in 	std_logic_vector(wordSize-1 downto 0);
			d_out		: out	std_logic_vector(wordSize-1 downto 0);
			
			WD					: in	std_logic;
			clk				: in	std_logic
		);	
	end component;
	
	component practica6B is														-- Pila
		generic
		(
			constant busSize 					: integer := 16;
			constant stackPointerSize		: integer := 3
		);
		port
		(
			pcData				: in std_logic_vector(busSize-1 downto 0);
			programCounter		: out std_logic_vector(busSize-1 downto 0);
			L						: in std_logic;
			UP						: in std_logic;
			DW						: in std_logic;
			clk					: in std_logic;
			clr					: in std_logic
		);
	end component;
	
	component practica6A is														-- Memoria de Programa
		generic
		(
			wordSize : integer := 25;										-- Tama�o de palabra
			busSize  : integer := 9											-- Tama�o del bus de direcciones
		);
		port
		(
			dir		: in 	std_logic_vector(busSize-1 downto 0);
			data		: out	std_logic_vector(wordSize-1 downto 0)
		);
	end component;
	
	component practica9 is														-- Unidad de Control
		port
		(
			op_function						: in std_logic_vector (3 downto 0);
			op_code							: in std_logic_vector (4 downto 0);
			flags								: in std_logic_vector (3 downto 0);
			LF									: in std_logic;

			CLR								: in std_logic;
			CLK								: in std_logic;

			microInstruccion				: out std_logic_vector (19 downto 0)
		);
	end component;
	
	component EXTSIG is															-- Extensor de Signo
		generic
		(
				N					: integer := 12;
				S					: integer := 16
		);
		port
		(
				entrada			: in std_logic_vector(N-1 downto 0);
				salida			: out std_logic_vector(S-1 downto 0)
		);
	end component;
	
	component EXTDIR is															-- Extensor de Direcci�n
		generic
		(
				N					: integer := 12;
				S					: integer := 16
		);
		port
		(
				entrada			: in std_logic_vector(N-1 downto 0);
				salida			: out std_logic_vector(S-1 downto 0)
		);
	end component;

end P_ESCOMips;

package body P_ESCOMips is


end P_ESCOMips;
