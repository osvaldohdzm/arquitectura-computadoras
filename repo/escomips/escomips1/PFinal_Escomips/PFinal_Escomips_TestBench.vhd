LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY PFinal_Escomips_TestBench IS
END PFinal_Escomips_TestBench;
 
ARCHITECTURE behavior OF PFinal_Escomips_TestBench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT PFinal_Escomips_Main
    PORT(
         rclr : IN  std_logic;
         clk : IN  std_logic;
         datos : OUT  std_logic_vector(7 downto 0);
         pc : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal rclr : std_logic := '0';
   signal clk : std_logic := '0';

 	--Outputs
   signal datos : std_logic_vector(7 downto 0);
   signal pc : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: PFinal_Escomips_Main PORT MAP (
          rclr => rclr,
          clk => clk,
          datos => datos,
          pc => pc
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		rclr <= '1';
      wait for 15 ns;
		rclr <= '0';
      wait;
   end process;

END;
