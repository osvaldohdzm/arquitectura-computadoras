 
library IEEE;

use IEEE.STD_LOGIC_1164.ALL;

entity VerificadorCondicion is
	port
	(
		--Banderas de la ALU
		OV: in std_logic;
		Z: in std_logic;
		N: in std_logic;
		C: in std_logic;
		
		EQ: out std_logic;
		NEQ: out std_logic;
		LT: out std_logic;
		LET: out std_logic;
		GT: out std_logic;
		GET: out std_logic
	);
end VerificadorCondicion;

architecture A_VerificadorCondicion of VerificadorCondicion is

begin

		--Condición de igualdad
		EQ<=Z;
		
		--Condición de desigualdad
		NEQ<=(not Z);
		
		--Condición menor que
		LT<=(not C);
		
		--Condición menor o igual
		LET<=(Z or (not C));
		
		--Condición mayor que
		GT<=(C and (not Z));
		
		--condición mayor o igual
		GET<=C;
end A_VerificadorCondicion;