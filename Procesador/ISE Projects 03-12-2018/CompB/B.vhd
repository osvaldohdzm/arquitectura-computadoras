library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity B is
	port(
		clk : in std_logic;
		clr : in std_logic;
		eb : in std_logic;
		lb : in std_logic;
		b : out std_logic_vector(3 downto 0)
	);
end B;

architecture Behavioral of B is
begin
	process(clk, clr, eb, lb)
	variable aux : std_logic_vector(3 downto 0);
	begin
		if (clr = '1') then --clear esta activado
			b <= "0000";
		elsif (rising_edge(clk)) then
			if (clr = '0' and eb = '0' and lb = '0') then
				--retencion
				aux := aux;
			elsif (clr = '0' and eb = '1' and lb = '0') then
				--B++
				aux := aux + 1;
			elsif (clr = '0' and eb = '0' and lb = '1') then
				--carga
				aux := "0000"; 
			end if;
			b <= aux;
		end if;
	end process;
end Behavioral;