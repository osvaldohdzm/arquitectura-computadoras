
LIBRARY ieee;
USE ieee.std_logic_1164.ALL; 
ENTITY sim_RegEstado IS
END sim_RegEstado;
 
ARCHITECTURE behavior OF sim_RegEstado IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT RegistroEstado
    PORT(
         clk : IN  std_logic;
         clr : IN  std_logic;
         lf : IN  std_logic;
         bandin : IN  std_logic_vector(3 downto 0);
         bandout : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';
   signal lf : std_logic := '0';
   signal bandin : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal bandout : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: RegistroEstado PORT MAP (
          clk => clk,
          clr => clr,
          lf => lf,
          bandin => bandin,
          bandout => bandout
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

      -- insert stimulus here 
		clr <= '1';
		lf <= '0';
		bandin <= "1010";
      wait for clk_period*10;
		
		clr <= '0';
		lf <= '0';
		bandin <= "1010";
      wait for clk_period*10;
		
		clr <= '0';
		lf <= '1';
		bandin <= "1010";
      wait for clk_period*10;
   end process;

END;
