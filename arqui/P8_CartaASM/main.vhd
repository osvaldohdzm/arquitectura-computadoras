--Author: My nuts
library IEEE;
library work;

use IEEE.STD_LOGIC_1164.ALL;
use work.paquete_entidades.ALL;

entity main is
    Port ( clr, clk : in  STD_LOGIC;
           ini : in  STD_LOGIC;
           dato : in  STD_LOGIC_VECTOR (8 downto 0);
           display : out  STD_LOGIC_VECTOR (6 downto 0));
end main;

architecture Behavioral of main is
signal lb, eb, la, ea, ec, a0, z : STD_LOGIC;
signal B : STD_LOGIC_VECTOR (3 downto 0);
signal aux : STD_LOGIC_VECTOR (6 downto 0);
begin

	e_unidadControl : unidadControl port map (
		clk => clk,
		clr => clr,
		ini => ini,
		a0 => a0,
		z => z,
		la => la,
		ea => ea,
		eb => eb,
		lb => lb,
		ec => ec
	);
	
	e_registro : registro port map (
		clk => clk,
		clr => clr,
		la => la,
		ea => ea,
		dato => dato,
		z => z,
		a0 => a0
	);
	
	e_contador : contador port map (
		clk => clk,
		clr => clr,
		lb => lb,
		eb => eb,
		B => B
	);
	
	e_deco : deco port map (
		B => B,
		aux => aux
	);
	
	e_mux : mux port map (
		aux => aux,
		ec => ec,
		display => display
	);

end Behavioral;

