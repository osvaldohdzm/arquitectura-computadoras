library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
--use ieee.numeric_std.all;

entity Registro is
	port(
		clk : in std_logic;
		clr : in std_logic;
		ea : in std_logic;
		la : in std_logic;
		d : in std_logic_vector(8 downto 0);
		a : inout std_logic_vector(8 downto 0);
		a0 : out std_logic;
		z : out std_logic
	);
end Registro;

architecture Behavioral of Registro is
begin
	process(clk, clr, ea, la, a)
	variable aux : bit_vector(8 downto 0);
	begin
		if (clr = '1') then --clear esta activado
			a <= "000000000";
		elsif (rising_edge(clk)) then
			if (clr = '0' and ea = '0' and la = '0') then
				--retencion
				a <= a;
			elsif (clr = '0' and ea = '0' and la = '1') then
				--carga
				a <= d;
			elsif (clr = '0' and ea = '1' and la = '0') then
				--corrimiento
				aux := to_bitvector(a);
				aux := aux srl 1;
				a <= to_stdlogicvector(aux);
			end if;
		end if;
		if (a = "000000000") then
			z <= '1';
		else
			z <= '0';
		end if;
		a0 <= a(0);
	end process;
end Behavioral;