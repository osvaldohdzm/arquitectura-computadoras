/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x1048c146 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/osvaldohm/Desktop/PRACTICA_5_ARQUITECTURA/PRINCIPAL/DEMUX.vhd";
extern char *IEEE_P_2592010699;



static void work_a_0200802635_3212880686_p_0(char *t0)
{
    char t5[16];
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    int t10;
    char *t11;
    int t13;
    char *t14;
    int t16;
    char *t17;
    int t19;
    char *t20;
    int t22;
    char *t23;
    int t25;
    char *t26;
    int t28;
    char *t29;
    int t31;
    char *t32;
    int t34;
    char *t35;
    int t37;
    char *t38;
    int t40;
    char *t41;
    int t43;
    char *t44;
    int t46;
    char *t47;
    int t49;
    char *t50;
    int t52;
    char *t53;
    int t55;
    char *t56;
    char *t58;
    char *t59;
    char *t60;
    char *t61;
    char *t62;

LAB0:    xsi_set_current_line(19, ng0);
    t1 = (t0 + 776U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 592U);
    t4 = *((char **)t1);
    t6 = ((IEEE_P_2592010699) + 2332);
    t7 = (t0 + 2976U);
    t1 = xsi_base_array_concat(t1, t5, t6, (char)99, t3, (char)97, t4, t7, (char)101);
    t8 = (t0 + 3021);
    t10 = xsi_mem_cmp(t8, t1, 5U);
    if (t10 == 1)
        goto LAB3;

LAB20:    t11 = (t0 + 3026);
    t13 = xsi_mem_cmp(t11, t1, 5U);
    if (t13 == 1)
        goto LAB4;

LAB21:    t14 = (t0 + 3031);
    t16 = xsi_mem_cmp(t14, t1, 5U);
    if (t16 == 1)
        goto LAB5;

LAB22:    t17 = (t0 + 3036);
    t19 = xsi_mem_cmp(t17, t1, 5U);
    if (t19 == 1)
        goto LAB6;

LAB23:    t20 = (t0 + 3041);
    t22 = xsi_mem_cmp(t20, t1, 5U);
    if (t22 == 1)
        goto LAB7;

LAB24:    t23 = (t0 + 3046);
    t25 = xsi_mem_cmp(t23, t1, 5U);
    if (t25 == 1)
        goto LAB8;

LAB25:    t26 = (t0 + 3051);
    t28 = xsi_mem_cmp(t26, t1, 5U);
    if (t28 == 1)
        goto LAB9;

LAB26:    t29 = (t0 + 3056);
    t31 = xsi_mem_cmp(t29, t1, 5U);
    if (t31 == 1)
        goto LAB10;

LAB27:    t32 = (t0 + 3061);
    t34 = xsi_mem_cmp(t32, t1, 5U);
    if (t34 == 1)
        goto LAB11;

LAB28:    t35 = (t0 + 3066);
    t37 = xsi_mem_cmp(t35, t1, 5U);
    if (t37 == 1)
        goto LAB12;

LAB29:    t38 = (t0 + 3071);
    t40 = xsi_mem_cmp(t38, t1, 5U);
    if (t40 == 1)
        goto LAB13;

LAB30:    t41 = (t0 + 3076);
    t43 = xsi_mem_cmp(t41, t1, 5U);
    if (t43 == 1)
        goto LAB14;

LAB31:    t44 = (t0 + 3081);
    t46 = xsi_mem_cmp(t44, t1, 5U);
    if (t46 == 1)
        goto LAB15;

LAB32:    t47 = (t0 + 3086);
    t49 = xsi_mem_cmp(t47, t1, 5U);
    if (t49 == 1)
        goto LAB16;

LAB33:    t50 = (t0 + 3091);
    t52 = xsi_mem_cmp(t50, t1, 5U);
    if (t52 == 1)
        goto LAB17;

LAB34:    t53 = (t0 + 3096);
    t55 = xsi_mem_cmp(t53, t1, 5U);
    if (t55 == 1)
        goto LAB18;

LAB35:
LAB19:    xsi_set_current_line(36, ng0);
    t1 = (t0 + 3357);
    t4 = (t0 + 1676);
    t6 = (t4 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t4);

LAB2:    t1 = (t0 + 1632);
    *((int *)t1) = 1;

LAB1:    return;
LAB3:    xsi_set_current_line(20, ng0);
    t56 = (t0 + 3101);
    t58 = (t0 + 1676);
    t59 = (t58 + 32U);
    t60 = *((char **)t59);
    t61 = (t60 + 40U);
    t62 = *((char **)t61);
    memcpy(t62, t56, 16U);
    xsi_driver_first_trans_fast_port(t58);
    goto LAB2;

LAB4:    xsi_set_current_line(21, ng0);
    t1 = (t0 + 3117);
    t4 = (t0 + 1676);
    t6 = (t4 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB5:    xsi_set_current_line(22, ng0);
    t1 = (t0 + 3133);
    t4 = (t0 + 1676);
    t6 = (t4 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB6:    xsi_set_current_line(23, ng0);
    t1 = (t0 + 3149);
    t4 = (t0 + 1676);
    t6 = (t4 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB7:    xsi_set_current_line(24, ng0);
    t1 = (t0 + 3165);
    t4 = (t0 + 1676);
    t6 = (t4 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB8:    xsi_set_current_line(25, ng0);
    t1 = (t0 + 3181);
    t4 = (t0 + 1676);
    t6 = (t4 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB9:    xsi_set_current_line(26, ng0);
    t1 = (t0 + 3197);
    t4 = (t0 + 1676);
    t6 = (t4 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB10:    xsi_set_current_line(27, ng0);
    t1 = (t0 + 3213);
    t4 = (t0 + 1676);
    t6 = (t4 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB11:    xsi_set_current_line(28, ng0);
    t1 = (t0 + 3229);
    t4 = (t0 + 1676);
    t6 = (t4 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB12:    xsi_set_current_line(29, ng0);
    t1 = (t0 + 3245);
    t4 = (t0 + 1676);
    t6 = (t4 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB13:    xsi_set_current_line(30, ng0);
    t1 = (t0 + 3261);
    t4 = (t0 + 1676);
    t6 = (t4 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB14:    xsi_set_current_line(31, ng0);
    t1 = (t0 + 3277);
    t4 = (t0 + 1676);
    t6 = (t4 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB15:    xsi_set_current_line(32, ng0);
    t1 = (t0 + 3293);
    t4 = (t0 + 1676);
    t6 = (t4 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB16:    xsi_set_current_line(33, ng0);
    t1 = (t0 + 3309);
    t4 = (t0 + 1676);
    t6 = (t4 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB17:    xsi_set_current_line(34, ng0);
    t1 = (t0 + 3325);
    t4 = (t0 + 1676);
    t6 = (t4 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB18:    xsi_set_current_line(35, ng0);
    t1 = (t0 + 3341);
    t4 = (t0 + 1676);
    t6 = (t4 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 40U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 16U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB36:;
}


extern void work_a_0200802635_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0200802635_3212880686_p_0};
	xsi_register_didat("work_a_0200802635_3212880686", "isim/TB_PRINCI_isim_beh.exe.sim/work/a_0200802635_3212880686.didat");
	xsi_register_executes(pe);
}
