library IEEE;

use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity DivisorDeFrecuencia is
	port
	(
		osc_clk: in std_logic;
		clr: in std_logic;
		clk: out std_logic
	);
end DivisorDeFrecuencia;

architecture A_DivisorDeFrecuencia of DivisorDeFrecuencia is
	constant n: integer:=25;
	signal cont1: std_logic_vector(n-1 downto 0);
begin
	
	process(osc_clk,clr)
	begin
		if(clr='1') then
			cont1<=(others=>'0');
		elsif (rising_edge(osc_clk)) then
			cont1<=cont1+1;
		end if;
	end process;
	
	clk<=cont1(n-1);
	
end A_DivisordEFrecuencia;