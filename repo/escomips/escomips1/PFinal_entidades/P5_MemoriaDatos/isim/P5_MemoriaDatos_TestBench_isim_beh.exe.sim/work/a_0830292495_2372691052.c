/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/_PracticasArqui/P5_MemoriaDatos/P5_MemoriaDatos_TestBench.vhd";
extern char *STD_TEXTIO;
extern char *IEEE_P_3564397177;
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
void ieee_p_3564397177_sub_1496949865_91900896(char *, char *, char *, unsigned char , unsigned char , int );
void ieee_p_3564397177_sub_2743816878_91900896(char *, char *, char *, char *);
void ieee_p_3564397177_sub_3205433008_91900896(char *, char *, char *, char *, char *, unsigned char , int );
void ieee_p_3564397177_sub_3988856810_91900896(char *, char *, char *, char *, char *);


static void work_a_0830292495_2372691052_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    int64 t7;
    int64 t8;

LAB0:    t1 = (t0 + 3928U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(54, ng0);
    t2 = (t0 + 4576);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(55, ng0);
    t2 = (t0 + 1968U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 3736);
    xsi_process_wait(t2, t8);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(56, ng0);
    t2 = (t0 + 4576);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(57, ng0);
    t2 = (t0 + 1968U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 3736);
    xsi_process_wait(t2, t8);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    goto LAB2;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

}

static void work_a_0830292495_2372691052_p_1(char *t0)
{
    char t5[16];
    char t10[8];
    char t11[8];
    char t12[8];
    char t13[8];
    char t19[16];
    char t24[16];
    char t32[16];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    int t8;
    unsigned int t9;
    int64 t14;
    int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    char *t20;
    char *t21;
    int t22;
    unsigned int t23;
    char *t25;
    int t26;
    unsigned char t27;
    char *t28;
    char *t29;
    char *t30;
    char *t31;

LAB0:    t1 = (t0 + 4176U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(74, ng0);
    t2 = (t0 + 2880U);
    t3 = (t0 + 8129);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 12;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (12 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)0);
    xsi_set_current_line(75, ng0);
    t2 = (t0 + 2776U);
    t3 = (t0 + 8141);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 14;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (14 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)1);
    xsi_set_current_line(78, ng0);
    t2 = (t0 + 8155);
    t4 = (t0 + 3344U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 4U);
    xsi_set_current_line(79, ng0);
    t2 = (t0 + 3984);
    t3 = (t0 + 3056U);
    t4 = (t0 + 3344U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t10, t7, 4U);
    t6 = (t0 + 7856U);
    t8 = (4U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t10, t6, (unsigned char)0, t8);
    xsi_set_current_line(80, ng0);
    t2 = (t0 + 8159);
    t4 = (t0 + 3344U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 4U);
    xsi_set_current_line(81, ng0);
    t2 = (t0 + 3984);
    t3 = (t0 + 3056U);
    t4 = (t0 + 3344U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t11, t7, 4U);
    t6 = (t0 + 7856U);
    t8 = (4U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t11, t6, (unsigned char)0, t8);
    xsi_set_current_line(82, ng0);
    t2 = (t0 + 8163);
    t4 = (t0 + 3344U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 4U);
    xsi_set_current_line(83, ng0);
    t2 = (t0 + 3984);
    t3 = (t0 + 3056U);
    t4 = (t0 + 3344U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t12, t7, 4U);
    t6 = (t0 + 7856U);
    t8 = (4U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t12, t6, (unsigned char)0, t8);
    xsi_set_current_line(84, ng0);
    t2 = (t0 + 8167);
    t4 = (t0 + 3344U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 4U);
    xsi_set_current_line(85, ng0);
    t2 = (t0 + 3984);
    t3 = (t0 + 3056U);
    t4 = (t0 + 3344U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t13, t7, 4U);
    t6 = (t0 + 7856U);
    t8 = (4U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t13, t6, (unsigned char)0, t8);
    xsi_set_current_line(86, ng0);
    t2 = (t0 + 3984);
    t3 = (t0 + 2776U);
    t4 = (t0 + 3056U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    xsi_set_current_line(88, ng0);
    t14 = (100 * 1000LL);
    t2 = (t0 + 3984);
    xsi_process_wait(t2, t14);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(89, ng0);
    t2 = (t0 + 8171);
    *((int *)t2) = 0;
    t3 = (t0 + 8175);
    *((int *)t3) = 11;
    t8 = 0;
    t15 = 11;

LAB8:    if (t8 <= t15)
        goto LAB9;

LAB11:    xsi_set_current_line(112, ng0);
    t2 = (t0 + 2880U);
    std_textio_file_close(t2);
    xsi_set_current_line(113, ng0);
    t2 = (t0 + 2776U);
    std_textio_file_close(t2);
    xsi_set_current_line(114, ng0);

LAB21:    *((char **)t1) = &&LAB22;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB9:    xsi_set_current_line(90, ng0);
    t4 = (t0 + 3984);
    t6 = (t0 + 2880U);
    t7 = (t0 + 3128U);
    std_textio_readline(STD_TEXTIO, t4, t6, t7);
    xsi_set_current_line(92, ng0);
    t2 = (t0 + 3984);
    t3 = (t0 + 3128U);
    t4 = (t0 + 2208U);
    t6 = *((char **)t4);
    t4 = (t0 + 7824U);
    ieee_p_3564397177_sub_3988856810_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(93, ng0);
    t2 = (t0 + 2208U);
    t3 = *((char **)t2);
    t2 = (t0 + 4640);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t16 = *((char **)t7);
    memcpy(t16, t3, 16U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(95, ng0);
    t2 = (t0 + 3984);
    t3 = (t0 + 3128U);
    t4 = (t0 + 2328U);
    t6 = *((char **)t4);
    t4 = (t0 + 7840U);
    ieee_p_3564397177_sub_3988856810_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(96, ng0);
    t2 = (t0 + 8179);
    t4 = (t0 + 2328U);
    t6 = *((char **)t4);
    t9 = (11 - 10);
    t17 = (t9 * 1U);
    t18 = (0 + t17);
    t4 = (t6 + t18);
    t16 = ((IEEE_P_2592010699) + 4024);
    t20 = (t19 + 0U);
    t21 = (t20 + 0U);
    *((int *)t21) = 0;
    t21 = (t20 + 4U);
    *((int *)t21) = 0;
    t21 = (t20 + 8U);
    *((int *)t21) = 1;
    t22 = (0 - 0);
    t23 = (t22 * 1);
    t23 = (t23 + 1);
    t21 = (t20 + 12U);
    *((unsigned int *)t21) = t23;
    t21 = (t24 + 0U);
    t25 = (t21 + 0U);
    *((int *)t25) = 10;
    t25 = (t21 + 4U);
    *((int *)t25) = 0;
    t25 = (t21 + 8U);
    *((int *)t25) = -1;
    t26 = (0 - 10);
    t23 = (t26 * -1);
    t23 = (t23 + 1);
    t25 = (t21 + 12U);
    *((unsigned int *)t25) = t23;
    t7 = xsi_base_array_concat(t7, t5, t16, (char)97, t2, t19, (char)97, t4, t24, (char)101);
    t23 = (1U + 11U);
    t27 = (12U != t23);
    if (t27 == 1)
        goto LAB12;

LAB13:    t25 = (t0 + 4704);
    t28 = (t25 + 56U);
    t29 = *((char **)t28);
    t30 = (t29 + 56U);
    t31 = *((char **)t30);
    memcpy(t31, t7, 12U);
    xsi_driver_first_trans_fast(t25);
    xsi_set_current_line(98, ng0);
    t2 = (t0 + 3984);
    t3 = (t0 + 3128U);
    t4 = (t0 + 2448U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_2743816878_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(99, ng0);
    t2 = (t0 + 2448U);
    t3 = *((char **)t2);
    t27 = *((unsigned char *)t3);
    t2 = (t0 + 4768);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t16 = *((char **)t7);
    *((unsigned char *)t16) = t27;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(101, ng0);

LAB16:    t2 = (t0 + 4496);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB17;
    goto LAB1;

LAB10:    t2 = (t0 + 8171);
    t8 = *((int *)t2);
    t3 = (t0 + 8175);
    t15 = *((int *)t3);
    if (t8 == t15)
        goto LAB11;

LAB18:    t22 = (t8 + 1);
    t8 = t22;
    t4 = (t0 + 8171);
    *((int *)t4) = t8;
    goto LAB8;

LAB12:    xsi_size_not_matching(12U, t23, 0);
    goto LAB13;

LAB14:    t4 = (t0 + 4496);
    *((int *)t4) = 0;
    xsi_set_current_line(103, ng0);
    t2 = (t0 + 1672U);
    t3 = *((char **)t2);
    t2 = (t0 + 2088U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 16U);
    xsi_set_current_line(105, ng0);
    t2 = (t0 + 3984);
    t3 = (t0 + 3056U);
    t4 = (t0 + 2328U);
    t6 = *((char **)t4);
    memcpy(t32, t6, 12U);
    t4 = (t0 + 7840U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t32, t4, (unsigned char)0, 5);
    xsi_set_current_line(106, ng0);
    t2 = (t0 + 3984);
    t3 = (t0 + 3056U);
    t4 = (t0 + 2208U);
    t6 = *((char **)t4);
    memcpy(t5, t6, 16U);
    t4 = (t0 + 7824U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t5, t4, (unsigned char)0, 5);
    xsi_set_current_line(107, ng0);
    t2 = (t0 + 3984);
    t3 = (t0 + 3056U);
    t4 = (t0 + 2448U);
    t6 = *((char **)t4);
    t27 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t27, (unsigned char)0, 5);
    xsi_set_current_line(108, ng0);
    t2 = (t0 + 3984);
    t3 = (t0 + 3056U);
    t4 = (t0 + 2088U);
    t6 = *((char **)t4);
    memcpy(t19, t6, 16U);
    t4 = (t0 + 7808U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t19, t4, (unsigned char)0, 5);
    xsi_set_current_line(110, ng0);
    t2 = (t0 + 3984);
    t3 = (t0 + 2776U);
    t4 = (t0 + 3056U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    goto LAB10;

LAB15:    t3 = (t0 + 1312U);
    t27 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t3, 0U, 0U);
    if (t27 == 1)
        goto LAB14;
    else
        goto LAB16;

LAB17:    goto LAB15;

LAB19:    goto LAB2;

LAB20:    goto LAB19;

LAB22:    goto LAB20;

}


extern void work_a_0830292495_2372691052_init()
{
	static char *pe[] = {(void *)work_a_0830292495_2372691052_p_0,(void *)work_a_0830292495_2372691052_p_1};
	xsi_register_didat("work_a_0830292495_2372691052", "isim/P5_MemoriaDatos_TestBench_isim_beh.exe.sim/work/a_0830292495_2372691052.didat");
	xsi_register_executes(pe);
}
