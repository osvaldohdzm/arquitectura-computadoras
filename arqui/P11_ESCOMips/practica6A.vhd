-- ESCOMips | Pr�ctica 6A - Memoria de Programa | Marco Antonio Rubio Cort�s

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use iEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity practica6A is
	generic
	(
		wordSize : integer := 25;										-- Tama�o de palabra
		busSize  : integer := 9											-- Tama�o del bus de direcciones
	);
	port
	(
		dir		: in 	std_logic_vector(busSize-1 downto 0);
		data		: out	std_logic_vector(wordSize-1 downto 0)
	);
end practica6A;

architecture A_practica6A of practica6A is

-- Programa de Prueba --
--constant LI		: std_logic_vector(4 downto 0)	:= "00001";
--constant R0		: std_logic_vector(3 downto 0)	:= x"0";
--constant R1		: std_logic_vector(3 downto 0)	:= x"1";
--
--constant CALL	: std_logic_vector(4 downto 0)	:= "10100";
--constant SUMA	: std_logic_vector(15 downto 0)	:= x"0005";
--
--constant SWI	: std_logic_vector(4 downto 0)	:= "00011";
--
--constant B		: std_logic_vector(4 downto 0)	:= "10011";
--constant CICLO	: std_logic_vector(15 downto 0)	:= x"0002";
--
--constant ADD	: std_logic_vector(4 downto 0)	:= "00000";
--constant ADD_F	: std_logic_vector(3 downto 0)	:= x"0";
--
--constant RET	: std_logic_vector(4 downto 0)	:= "10101";
--
--constant SU		: std_logic_vector(3 downto 0)	:= x"0";
-- Programa de Prueba --

-- Programa de Prueba --
constant LI		: std_logic_vector(4 downto 0)	:= "00001";
constant R0		: std_logic_vector(3 downto 0)	:= x"0";
constant R1		: std_logic_vector(3 downto 0)	:= x"1";

constant ADD	: std_logic_vector(4 downto 0)	:= "00000";
constant ADD_F	: std_logic_vector(3 downto 0)	:= x"0";

constant SWI	: std_logic_vector(4 downto 0)	:= "00011";

constant B		: std_logic_vector(4 downto 0)	:= "10011";
constant SUMA	: std_logic_vector(15 downto 0)	:= x"0002";

constant SU		: std_logic_vector(3 downto 0)	:= x"0";
-- Programa de Prueba --


-- Archivo de la memoria de Programa
type memoria is array (0 to ((2**busSize)-1)) of std_logic_vector((wordSize-1) downto 0);
-- signal programMemory: memoria := ((others => (others => '0')));
-- signal programMemory: memoria :=	( LI&R0&x"0001",
--												LI&R1&x"0007",
--												CALL&SU&SUMA,
--												SWI&R1&x"0005",
--												B&SU&CICLO,
--												ADD&R1&R1&R0&SU&ADD_F,
--												RET&SU&SU&SU&SU&SU,
--												others => (others => '0') );
-- constant programMemory: memoria :=	( LI&R0&x"0001",
--												LI&R1&x"0007",
--												ADD&R1&R1&R0&SU&ADD_F,
--												SWI&R1&x"0005",
--												B&SU&SUMA,
constant programMemory: memoria :=	( "0000100000000000000000000",
												"0000100010000000000000001",
												"0000100100000000000000000",
												"0000100110000000000001100",
												"0000001000000000100000000",
												"0001101000000000001001000",
												"0010100000001000000000000",
												"0010100010100000000000000",
												"0010100100010000000000001",
												"0111000110010111111111011",
												"0000001000000000100000000",
												others => (others => '0') );

begin
	
	-- Lecturas as�ncronas
	data <= programMemory(conv_integer(dir));					-- Lee de la localidad dada por dir y lo almacena en data

end A_practica6A;