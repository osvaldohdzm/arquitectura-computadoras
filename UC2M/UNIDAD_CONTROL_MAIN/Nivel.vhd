
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Nivel is
    Port ( clk,clr : in  STD_LOGIC;
           na : out  STD_LOGIC);
end Nivel;

architecture Behavioral of Nivel is
signal nclk, pclk : std_logic;
begin


process (clk, clr)
begin
	if (clr = '1') then
		pclk <= '0';
	elsif(clk'event and clk ='1') then
		pclk <= not pclk;
	end if;
end process;



process (clk, clr)
begin
	if (clr = '1') then
		nclk<= '0';
	elsif(clk'event and clk ='0') then
		nclk <= not nclk;
	end if;
end process;



na <= pclk xor nclk;
end Behavioral;

