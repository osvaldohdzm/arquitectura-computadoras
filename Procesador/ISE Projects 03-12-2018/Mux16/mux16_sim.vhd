
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY mux16_sim IS
END mux16_sim;
 
ARCHITECTURE behavior OF mux16_sim IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Mux16
    PORT(
         in0 : IN  std_logic_vector(15 downto 0);
         in1 : IN  std_logic_vector(15 downto 0);
         salida : OUT  std_logic_vector(15 downto 0);
         sel : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal in0 : std_logic_vector(15 downto 0) := (others => '0');
   signal in1 : std_logic_vector(15 downto 0) := (others => '0');
   signal sel : std_logic := '0';

 	--Outputs
   signal salida : std_logic_vector(15 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Mux16 PORT MAP (
          in0 => in0,
          in1 => in1,
          salida => salida,
          sel => sel
        );

   -- Stimulus process
   stim_proc: process
   begin		      -- insert stimulus here 
		in0 <= "1111111111111111";
		in1 <= "0000000000000000";
		sel <= '0';
      wait for 30 ns;
		
		in0 <= "1111111111111111";
		in1 <= "0000000000000000";
		sel <= '1';
      wait for 30 ns;
		
		in0 <= "1100000100001111";
		in1 <= "1010101010100100";
		sel <= '0';
      wait for 30 ns;
		
		in0 <= "1111101010101011";
		in1 <= "0000011111111100";
		sel <= '0';
      wait for 30 ns;
   end process;

END;
