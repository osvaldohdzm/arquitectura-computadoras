library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use iEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity P6_MemProg is --MEMORIA ROM
	generic
	(
		TamanoPalabra : integer := 25;	-- Tama�o palabra
		TamanoBus  : integer := 11			-- Tama�o bus de direcciones
	);
	port
	(
		dir		: in 	std_logic_vector(TamanoBus-1 downto 0);
		data		: out	std_logic_vector(TamanoPalabra-1 downto 0)
	);
	
end P6_MemProg;

architecture A_P6_MemProg of P6_MemProg is

--Algoritmo de Practica usando constantes
constant LI		: std_logic_vector(4 downto 0)	:= "00001";
constant R0		: std_logic_vector(3 downto 0)	:= x"0"; --Ayuda usando Hexa
constant R1		: std_logic_vector(3 downto 0)	:= x"1";
constant ADD	: std_logic_vector(4 downto 0)	:= "00000";
constant ADD_F	: std_logic_vector(3 downto 0)	:= x"0";
constant SWI	: std_logic_vector(4 downto 0)	:= "00011";
constant B		: std_logic_vector(4 downto 0)	:= "10011";
constant SUMA	: std_logic_vector(15 downto 0)	:= x"0002"; -- Direccion 2 para llamar
constant SU		: std_logic_vector(3 downto 0)	:= x"0";

-- Archivo de la memoria de Programa
type memoria is array (0 to ((2**TamanoBus)-1)) of std_logic_vector((TamanoPalabra-1) downto 0);

-- Si ponemoss signal da error de bloque no conectado, por como se maneja la memoria
constant MemoriaPrograma: memoria :=	( LI&R0&x"0001", --Direccion 0
												LI&R1&x"0007", -- Direccion 1
												ADD&R1&R1&R0&SU&ADD_F, -- Direccion 2
												SWI&R1&x"0005", --Direccion 3
												B&SU&SUMA, -- Dirreccion 4
												others => (others => '0') ); -- Rellenar todo lo demas con 0 (11 ceros)

begin
	
	-- Lectura As�ncronas
	data <= MemoriaPrograma(conv_integer(dir)); -- Leer de localidad de memoria dada por dir

end A_P6_MemProg;