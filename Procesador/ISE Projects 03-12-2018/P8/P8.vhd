library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library work;
use work.Paquete.all;

entity P8 is
    Port ( clk, clr, ini : in  STD_LOGIC;
           d : in  STD_LOGIC_VECTOR (8 downto 0);
           a : inout  STD_LOGIC_VECTOR (8 downto 0);
           display : out  STD_LOGIC_VECTOR (6 downto 0);
			  disp_aux : out  STD_LOGIC_VECTOR (7 downto 0));
end P8;

architecture Behavioral of P8 is
	signal z :  STD_LOGIC;
	signal a0 :  STD_LOGIC;
	signal la :  STD_LOGIC;
	signal lb :  STD_LOGIC;
	signal ea :  STD_LOGIC;
	signal eb :  STD_LOGIC;
	signal ec :  STD_LOGIC;
	signal b : STD_LOGIC_vector(3 downto 0);
	signal sal_7_seg : STD_LOGIC_VECTOR (6 downto 0);
	signal clk_1hz : STD_LOGIC;
	
begin
		u1 : CartaASM port map ( clk_1hz, clr, ini, z, a0, ea, la, eb, lb, ec );
		u2 : Contador port map( clk_1hz, clr, eb, lb, b);
		u3 : Registro port map( clk_1hz, clr, ea, la, d, a, a0, z);
		u4 : Decodificador port map( b, sal_7_seg );
		u5 : Demux2 port map( sal_7_seg, ec, display );
		u6 : DivisorFreq port map ( clk, clr, clk_1hz );
		
		disp_aux <= "01111111";
	
end Behavioral;

