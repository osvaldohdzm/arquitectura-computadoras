
library IEEE;

use IEEE.STD_LOGIC_1164.ALL;

use IEEE.STD_LOGIC_arith.ALL;

use IEEE.STD_LOGIC_unsigned.ALL;
entity microOpCode is

generic (
		bus_dir : integer := 5;
	bus_datos : integer:= 20
);
    Port ( CP_CODE  : in  STD_LOGIC_VECTOR (4 downto 0);
           salOpCode : out  STD_LOGIC_VECTOR (19 downto 0));
end microOpCode;

architecture Behavioral of microOpCode is

constant LI : std_logic_vector (19 downto 0) := "00000000010000000000"; 
constant LWI : std_logic_vector (19 downto 0) := "00000100010100001000"; 
constant LW: std_logic_vector (19 downto 0):=  "00000110010100110000";
constant SWI: std_logic_vector (19 downto 0) := "00001000000000001100";
constant SW: std_logic_vector (19 downto 0) :=  "00001010000100110100";

constant ADDI: std_logic_vector (19 downto 0) :="00000100010100110010";
constant SUBI: std_logic_vector (19 downto 0) :="00000100010101110010";
constant ANDI: std_logic_vector (19 downto 0) :="00000100010100000010";
constant ORI: std_logic_vector (19 downto 0) := "00000100010100010010";
constant XORI: std_logic_vector (19 downto 0) := "00000100010111000010";
constant NANDI: std_logic_vector (19 downto 0) := "00000100010111010010";
constant NORI: std_logic_vector (19 downto 0) :=  "00000100010111000010";
constant XNORI: std_logic_vector (19 downto 0) := "00000100010110100010";
constant BEQI: std_logic_vector (19 downto 0) :=  "10010000001100110010";
constant BNEI: std_logic_vector (19 downto 0) :=  "10010000001100110010";
constant BLTI: std_logic_vector (19 downto 0) :=  "10010000001100110010";
constant BLETI: std_logic_vector (19 downto 0) :=  "10010000001100110010";
constant BGTI: std_logic_vector (19 downto 0) := "10010000001100110010";
constant BGETI: std_logic_vector (19 downto 0) :=  "10010000001100110010";

constant B: std_logic_vector (19 downto 0) :=    "00010000000000000000";
constant KALL: std_logic_vector (19 downto 0) := "01010000000000000000";
constant RET: std_logic_vector (19 downto 0) :=  "00100000000000000000";
constant NOP: std_logic_vector (19 downto 0) :=  "00000000000000000000";

constant salto : std_logic_vector (19 downto 0) := "00001000000001110001";

type ejemplo is array (0 to (2**bus_dir)-1) of std_logic_vector(bus_Datos-1 downto 0);
constant aux : ejemplo := (salto,LI,LWI,SWI,SW,ADDI,SUBI,ANDI,ORI,XORI,NANDI,NORI,XNORI,BEQI,BNEI,BLTI,BLETI,BGTI,BGETI,B,KALL,RET,NOP,others=>(others=> '0'));

begin

salOpCode <= aux(conv_integer(CP_CODE));


end Behavioral;

