/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/leonardo faydella/Desktop/ProyectoFinal/Pila/pila.vhd";
extern char *IEEE_P_2592010699;
extern char *IEEE_P_3620187407;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
char *ieee_p_3620187407_sub_436279890_3965413181(char *, char *, char *, char *, int );
char *ieee_p_3620187407_sub_436351764_3965413181(char *, char *, char *, char *, int );
int ieee_p_3620187407_sub_514432868_3965413181(char *, char *, char *);


static void work_a_2016291610_3212880686_p_0(char *t0)
{
    char t24[16];
    char t25[16];
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    unsigned int t6;
    char *t7;
    unsigned char t8;
    unsigned int t9;
    char *t10;
    char *t11;
    unsigned char t12;
    unsigned char t13;
    unsigned char t14;
    unsigned char t15;
    unsigned char t16;
    unsigned char t17;
    char *t18;
    char *t19;
    int t20;
    int t21;
    unsigned int t22;
    char *t23;
    int t26;
    unsigned int t27;
    int t28;
    int t29;
    unsigned int t30;
    unsigned int t31;
    char *t32;
    char *t33;
    unsigned int t34;
    unsigned int t35;
    char *t36;
    char *t37;
    char *t38;

LAB0:    xsi_set_current_line(26, ng0);
    t1 = (t0 + 1672U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    t1 = (t0 + 1472U);
    t3 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t3 != 0)
        goto LAB7;

LAB8:
LAB3:    xsi_set_current_line(43, ng0);
    t1 = (t0 + 2568U);
    t2 = *((char **)t1);
    t1 = (t0 + 2448U);
    t5 = *((char **)t1);
    t1 = (t0 + 6672U);
    t20 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t5, t1);
    t21 = (t20 - 0);
    t6 = (t21 * 1);
    xsi_vhdl_check_range_of_index(0, 7, 1, t20);
    t9 = (16U * t6);
    t22 = (0 + t9);
    t7 = (t2 + t22);
    t10 = (t0 + 3952);
    t11 = (t10 + 56U);
    t18 = *((char **)t11);
    t19 = (t18 + 56U);
    t23 = *((char **)t19);
    memcpy(t23, t7, 16U);
    xsi_driver_first_trans_fast_port(t10);
    xsi_set_current_line(44, ng0);
    t1 = (t0 + 2448U);
    t2 = *((char **)t1);
    t1 = (t0 + 4016);
    t5 = (t1 + 56U);
    t7 = *((char **)t5);
    t10 = (t7 + 56U);
    t11 = *((char **)t10);
    memcpy(t11, t2, 3U);
    xsi_driver_first_trans_fast_port(t1);
    t1 = (t0 + 3872);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(27, ng0);
    t1 = xsi_get_transient_memory(128U);
    memset(t1, 0, 128U);
    t5 = t1;
    t6 = (16U * 1U);
    t7 = t5;
    memset(t7, (unsigned char)2, t6);
    t8 = (t6 != 0);
    if (t8 == 1)
        goto LAB5;

LAB6:    t10 = (t0 + 2568U);
    t11 = *((char **)t10);
    t10 = (t11 + 0);
    memcpy(t10, t1, 128U);
    xsi_set_current_line(28, ng0);
    t1 = xsi_get_transient_memory(3U);
    memset(t1, 0, 3U);
    t2 = t1;
    memset(t2, (unsigned char)2, 3U);
    t5 = (t0 + 2448U);
    t7 = *((char **)t5);
    t5 = (t7 + 0);
    memcpy(t5, t1, 3U);
    goto LAB3;

LAB5:    t9 = (128U / t6);
    xsi_mem_set_data(t5, t5, t6, t9);
    goto LAB6;

LAB7:    xsi_set_current_line(30, ng0);
    t2 = (t0 + 1032U);
    t5 = *((char **)t2);
    t12 = *((unsigned char *)t5);
    t13 = (t12 == (unsigned char)2);
    if (t13 == 1)
        goto LAB15;

LAB16:    t8 = (unsigned char)0;

LAB17:    if (t8 == 1)
        goto LAB12;

LAB13:    t4 = (unsigned char)0;

LAB14:    if (t4 != 0)
        goto LAB9;

LAB11:    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t8 = *((unsigned char *)t2);
    t12 = (t8 == (unsigned char)3);
    if (t12 == 1)
        goto LAB23;

LAB24:    t4 = (unsigned char)0;

LAB25:    if (t4 == 1)
        goto LAB20;

LAB21:    t3 = (unsigned char)0;

LAB22:    if (t3 != 0)
        goto LAB18;

LAB19:    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t8 = *((unsigned char *)t2);
    t12 = (t8 == (unsigned char)2);
    if (t12 == 1)
        goto LAB31;

LAB32:    t4 = (unsigned char)0;

LAB33:    if (t4 == 1)
        goto LAB28;

LAB29:    t3 = (unsigned char)0;

LAB30:    if (t3 != 0)
        goto LAB26;

LAB27:    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t8 = *((unsigned char *)t2);
    t12 = (t8 == (unsigned char)2);
    if (t12 == 1)
        goto LAB39;

LAB40:    t4 = (unsigned char)0;

LAB41:    if (t4 == 1)
        goto LAB36;

LAB37:    t3 = (unsigned char)0;

LAB38:    if (t3 != 0)
        goto LAB34;

LAB35:
LAB10:    goto LAB3;

LAB9:    xsi_set_current_line(31, ng0);
    t2 = (t0 + 1832U);
    t11 = *((char **)t2);
    t2 = (t0 + 2568U);
    t18 = *((char **)t2);
    t2 = (t0 + 2448U);
    t19 = *((char **)t2);
    t2 = (t0 + 6672U);
    t20 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t19, t2);
    t21 = (t20 - 0);
    t6 = (t21 * 1);
    xsi_vhdl_check_range_of_index(0, 7, 1, t20);
    t9 = (16U * t6);
    t22 = (0 + t9);
    t23 = (t18 + t22);
    memcpy(t23, t11, 16U);
    goto LAB10;

LAB12:    t2 = (t0 + 1352U);
    t10 = *((char **)t2);
    t16 = *((unsigned char *)t10);
    t17 = (t16 == (unsigned char)3);
    t4 = t17;
    goto LAB14;

LAB15:    t2 = (t0 + 1192U);
    t7 = *((char **)t2);
    t14 = *((unsigned char *)t7);
    t15 = (t14 == (unsigned char)2);
    t8 = t15;
    goto LAB17;

LAB18:    xsi_set_current_line(33, ng0);
    t1 = (t0 + 2448U);
    t10 = *((char **)t1);
    t1 = (t0 + 6672U);
    t11 = ieee_p_3620187407_sub_436279890_3965413181(IEEE_P_3620187407, t24, t10, t1, 1);
    t18 = (t0 + 2448U);
    t19 = *((char **)t18);
    t18 = (t19 + 0);
    t23 = (t24 + 12U);
    t6 = *((unsigned int *)t23);
    t9 = (1U * t6);
    memcpy(t18, t11, t9);
    xsi_set_current_line(34, ng0);
    t1 = (t0 + 1832U);
    t2 = *((char **)t1);
    t1 = (t0 + 2568U);
    t5 = *((char **)t1);
    t1 = (t0 + 2448U);
    t7 = *((char **)t1);
    t1 = (t0 + 6672U);
    t20 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t7, t1);
    t21 = (t20 - 0);
    t6 = (t21 * 1);
    xsi_vhdl_check_range_of_index(0, 7, 1, t20);
    t9 = (16U * t6);
    t22 = (0 + t9);
    t10 = (t5 + t22);
    memcpy(t10, t2, 16U);
    goto LAB10;

LAB20:    t1 = (t0 + 1352U);
    t7 = *((char **)t1);
    t15 = *((unsigned char *)t7);
    t16 = (t15 == (unsigned char)3);
    t3 = t16;
    goto LAB22;

LAB23:    t1 = (t0 + 1192U);
    t5 = *((char **)t1);
    t13 = *((unsigned char *)t5);
    t14 = (t13 == (unsigned char)2);
    t4 = t14;
    goto LAB25;

LAB26:    xsi_set_current_line(36, ng0);
    t1 = (t0 + 2448U);
    t10 = *((char **)t1);
    t1 = (t0 + 6672U);
    t11 = ieee_p_3620187407_sub_436351764_3965413181(IEEE_P_3620187407, t24, t10, t1, 1);
    t18 = (t0 + 2448U);
    t19 = *((char **)t18);
    t18 = (t19 + 0);
    t23 = (t24 + 12U);
    t6 = *((unsigned int *)t23);
    t9 = (1U * t6);
    memcpy(t18, t11, t9);
    xsi_set_current_line(37, ng0);
    t1 = (t0 + 2568U);
    t2 = *((char **)t1);
    t1 = (t0 + 2448U);
    t5 = *((char **)t1);
    t1 = (t0 + 6672U);
    t20 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t5, t1);
    t21 = (t20 - 0);
    t6 = (t21 * 1);
    xsi_vhdl_check_range_of_index(0, 7, 1, t20);
    t9 = (16U * t6);
    t22 = (0 + t9);
    t7 = (t2 + t22);
    t10 = (t25 + 0U);
    t11 = (t10 + 0U);
    *((int *)t11) = 15;
    t11 = (t10 + 4U);
    *((int *)t11) = 0;
    t11 = (t10 + 8U);
    *((int *)t11) = -1;
    t26 = (0 - 15);
    t27 = (t26 * -1);
    t27 = (t27 + 1);
    t11 = (t10 + 12U);
    *((unsigned int *)t11) = t27;
    t11 = ieee_p_3620187407_sub_436279890_3965413181(IEEE_P_3620187407, t24, t7, t25, 1);
    t18 = (t0 + 2568U);
    t19 = *((char **)t18);
    t18 = (t0 + 2448U);
    t23 = *((char **)t18);
    t18 = (t0 + 6672U);
    t28 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t23, t18);
    t29 = (t28 - 0);
    t27 = (t29 * 1);
    xsi_vhdl_check_range_of_index(0, 7, 1, t28);
    t30 = (16U * t27);
    t31 = (0 + t30);
    t32 = (t19 + t31);
    t33 = (t24 + 12U);
    t34 = *((unsigned int *)t33);
    t35 = (1U * t34);
    memcpy(t32, t11, t35);
    goto LAB10;

LAB28:    t1 = (t0 + 1352U);
    t7 = *((char **)t1);
    t15 = *((unsigned char *)t7);
    t16 = (t15 == (unsigned char)2);
    t3 = t16;
    goto LAB30;

LAB31:    t1 = (t0 + 1192U);
    t5 = *((char **)t1);
    t13 = *((unsigned char *)t5);
    t14 = (t13 == (unsigned char)3);
    t4 = t14;
    goto LAB33;

LAB34:    xsi_set_current_line(39, ng0);
    t1 = (t0 + 2568U);
    t10 = *((char **)t1);
    t1 = (t0 + 2448U);
    t11 = *((char **)t1);
    t1 = (t0 + 6672U);
    t20 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t11, t1);
    t21 = (t20 - 0);
    t6 = (t21 * 1);
    xsi_vhdl_check_range_of_index(0, 7, 1, t20);
    t9 = (16U * t6);
    t22 = (0 + t9);
    t18 = (t10 + t22);
    t19 = (t25 + 0U);
    t23 = (t19 + 0U);
    *((int *)t23) = 15;
    t23 = (t19 + 4U);
    *((int *)t23) = 0;
    t23 = (t19 + 8U);
    *((int *)t23) = -1;
    t26 = (0 - 15);
    t27 = (t26 * -1);
    t27 = (t27 + 1);
    t23 = (t19 + 12U);
    *((unsigned int *)t23) = t27;
    t23 = ieee_p_3620187407_sub_436279890_3965413181(IEEE_P_3620187407, t24, t18, t25, 1);
    t32 = (t0 + 2568U);
    t33 = *((char **)t32);
    t32 = (t0 + 2448U);
    t36 = *((char **)t32);
    t32 = (t0 + 6672U);
    t28 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t36, t32);
    t29 = (t28 - 0);
    t27 = (t29 * 1);
    xsi_vhdl_check_range_of_index(0, 7, 1, t28);
    t30 = (16U * t27);
    t31 = (0 + t30);
    t37 = (t33 + t31);
    t38 = (t24 + 12U);
    t34 = *((unsigned int *)t38);
    t35 = (1U * t34);
    memcpy(t37, t23, t35);
    goto LAB10;

LAB36:    t1 = (t0 + 1352U);
    t7 = *((char **)t1);
    t15 = *((unsigned char *)t7);
    t16 = (t15 == (unsigned char)2);
    t3 = t16;
    goto LAB38;

LAB39:    t1 = (t0 + 1192U);
    t5 = *((char **)t1);
    t13 = *((unsigned char *)t5);
    t14 = (t13 == (unsigned char)2);
    t4 = t14;
    goto LAB41;

}


extern void work_a_2016291610_3212880686_init()
{
	static char *pe[] = {(void *)work_a_2016291610_3212880686_p_0};
	xsi_register_didat("work_a_2016291610_3212880686", "isim/TB_ARCHIVOPILA_isim_beh.exe.sim/work/a_2016291610_3212880686.didat");
	xsi_register_executes(pe);
}
