/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x1048c146 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/osvaldohm/Desktop/UC2M/UNIDAD_CONTROL_MAIN/TBN_UC.vhd";
extern char *STD_TEXTIO;
extern char *IEEE_P_3564397177;
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1258338084_503743352(char *, char *, unsigned int , unsigned int );
unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
void ieee_p_3564397177_sub_1281154728_91900896(char *, char *, char *, char *, char *, unsigned char , int );
void ieee_p_3564397177_sub_1496949865_91900896(char *, char *, char *, unsigned char , unsigned char , int );
void ieee_p_3564397177_sub_2743816878_91900896(char *, char *, char *, char *);
void ieee_p_3564397177_sub_2889341154_91900896(char *, char *, char *, char *, char *);


static void work_a_2683493387_2372691052_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    int64 t7;
    int64 t8;

LAB0:    t1 = (t0 + 2656U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(70, ng0);
    t2 = (t0 + 3048);
    t3 = (t2 + 32U);
    t4 = *((char **)t3);
    t5 = (t4 + 40U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(71, ng0);
    t2 = (t0 + 1316U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 2556);
    xsi_process_wait(t2, t8);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(72, ng0);
    t2 = (t0 + 3048);
    t3 = (t2 + 32U);
    t4 = *((char **)t3);
    t5 = (t4 + 40U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(73, ng0);
    t2 = (t0 + 1316U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 2556);
    xsi_process_wait(t2, t8);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    goto LAB2;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

}

static void work_a_2683493387_2372691052_p_1(char *t0)
{
    char t5[16];
    char t10[16];
    char t11[16];
    char t12[16];
    char t13[16];
    char t14[16];
    char t15[16];
    char t16[16];
    char t21[8];
    char t22[8];
    char t23[8];
    char t24[8];
    char t26[24];
    char t27[16];
    char t28[8];
    char t29[8];
    char t30[8];
    char t31[8];
    char t32[24];
    char t33[16];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    int t8;
    unsigned int t9;
    int64 t17;
    int t18;
    char *t19;
    unsigned char t20;
    int t25;

LAB0:    t1 = (t0 + 2800U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(90, ng0);
    t2 = (t0 + 1976U);
    t3 = (t0 + 6572);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 12;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (12 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)0);
    xsi_set_current_line(91, ng0);
    t2 = (t0 + 1912U);
    t3 = (t0 + 6584);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 13;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (13 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)1);
    xsi_set_current_line(93, ng0);
    t2 = (t0 + 6597);
    t4 = (t0 + 2244U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 10U);
    xsi_set_current_line(94, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 2244U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t10, t7, 10U);
    t6 = (t0 + 6356U);
    t8 = (10U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t10, t6, (unsigned char)0, t8);
    xsi_set_current_line(95, ng0);
    t2 = (t0 + 6607);
    t4 = (t0 + 2244U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 10U);
    xsi_set_current_line(96, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 2244U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t11, t7, 10U);
    t6 = (t0 + 6356U);
    t8 = (10U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t11, t6, (unsigned char)0, t8);
    xsi_set_current_line(97, ng0);
    t2 = (t0 + 6617);
    t4 = (t0 + 2244U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 10U);
    xsi_set_current_line(98, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 2244U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t12, t7, 10U);
    t6 = (t0 + 6356U);
    t8 = (10U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t12, t6, (unsigned char)0, t8);
    xsi_set_current_line(99, ng0);
    t2 = (t0 + 6627);
    t4 = (t0 + 2244U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 10U);
    xsi_set_current_line(100, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 2244U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t13, t7, 10U);
    t6 = (t0 + 6356U);
    t8 = (10U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t13, t6, (unsigned char)0, t8);
    xsi_set_current_line(101, ng0);
    t2 = (t0 + 6637);
    t4 = (t0 + 2244U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 10U);
    xsi_set_current_line(102, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 2244U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t14, t7, 10U);
    t6 = (t0 + 6356U);
    t8 = (10U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t14, t6, (unsigned char)0, t8);
    xsi_set_current_line(103, ng0);
    t2 = (t0 + 6647);
    t4 = (t0 + 2244U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 10U);
    xsi_set_current_line(104, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 2244U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t15, t7, 10U);
    t6 = (t0 + 6356U);
    t8 = (10U + 13);
    std_textio_write7(STD_TEXTIO, t2, t3, t15, t6, (unsigned char)0, t8);
    xsi_set_current_line(105, ng0);
    t2 = (t0 + 6657);
    t4 = (t0 + 2244U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 10U);
    xsi_set_current_line(106, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 2244U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t16, t7, 10U);
    t6 = (t0 + 6356U);
    t8 = (10U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t16, t6, (unsigned char)0, t8);
    xsi_set_current_line(107, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 1912U);
    t4 = (t0 + 2080U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    xsi_set_current_line(109, ng0);
    t17 = (105 * 1000LL);
    t2 = (t0 + 2700);
    xsi_process_wait(t2, t17);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(110, ng0);
    t2 = (t0 + 6667);
    *((int *)t2) = 0;
    t3 = (t0 + 6671);
    *((int *)t3) = 52;
    t8 = 0;
    t18 = 52;

LAB8:    if (t8 <= t18)
        goto LAB9;

LAB11:    xsi_set_current_line(160, ng0);
    t2 = (t0 + 1976U);
    std_textio_file_close(t2);
    xsi_set_current_line(161, ng0);
    t2 = (t0 + 1912U);
    std_textio_file_close(t2);
    xsi_set_current_line(162, ng0);

LAB23:    *((char **)t1) = &&LAB24;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB9:    xsi_set_current_line(111, ng0);
    t4 = (t0 + 2700);
    t6 = (t0 + 1976U);
    t7 = (t0 + 2120U);
    std_textio_readline(STD_TEXTIO, t4, t6, t7);
    xsi_set_current_line(112, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2120U);
    t4 = (t0 + 1588U);
    t6 = *((char **)t4);
    t4 = (t0 + 6340U);
    ieee_p_3564397177_sub_2889341154_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(113, ng0);
    t2 = (t0 + 1588U);
    t3 = *((char **)t2);
    t2 = (t0 + 3084);
    t4 = (t2 + 32U);
    t6 = *((char **)t4);
    t7 = (t6 + 40U);
    t19 = *((char **)t7);
    memcpy(t19, t3, 5U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(114, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2120U);
    t4 = (t0 + 1452U);
    t6 = *((char **)t4);
    t4 = (t0 + 6324U);
    ieee_p_3564397177_sub_2889341154_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(115, ng0);
    t2 = (t0 + 1452U);
    t3 = *((char **)t2);
    t2 = (t0 + 3120);
    t4 = (t2 + 32U);
    t6 = *((char **)t4);
    t7 = (t6 + 40U);
    t19 = *((char **)t7);
    memcpy(t19, t3, 4U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(116, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2120U);
    t4 = (t0 + 1520U);
    t6 = *((char **)t4);
    t4 = (t0 + 6324U);
    ieee_p_3564397177_sub_2889341154_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(117, ng0);
    t2 = (t0 + 1520U);
    t3 = *((char **)t2);
    t2 = (t0 + 3156);
    t4 = (t2 + 32U);
    t6 = *((char **)t4);
    t7 = (t6 + 40U);
    t19 = *((char **)t7);
    memcpy(t19, t3, 4U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(118, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2120U);
    t4 = (t0 + 1656U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_2743816878_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(119, ng0);
    t2 = (t0 + 1656U);
    t3 = *((char **)t2);
    t20 = *((unsigned char *)t3);
    t2 = (t0 + 3192);
    t4 = (t2 + 32U);
    t6 = *((char **)t4);
    t7 = (t6 + 40U);
    t19 = *((char **)t7);
    *((unsigned char *)t19) = t20;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(120, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2120U);
    t4 = (t0 + 1724U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_2743816878_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(121, ng0);

LAB14:    t2 = (t0 + 2996);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB15;
    goto LAB1;

LAB10:    t2 = (t0 + 6667);
    t8 = *((int *)t2);
    t3 = (t0 + 6671);
    t18 = *((int *)t3);
    if (t8 == t18)
        goto LAB11;

LAB20:    t25 = (t8 + 1);
    t8 = t25;
    t4 = (t0 + 6667);
    *((int *)t4) = t8;
    goto LAB8;

LAB12:    t4 = (t0 + 2996);
    *((int *)t4) = 0;
    xsi_set_current_line(122, ng0);
    t2 = (t0 + 1724U);
    t3 = *((char **)t2);
    t20 = *((unsigned char *)t3);
    t2 = (t0 + 3228);
    t4 = (t2 + 32U);
    t6 = *((char **)t4);
    t7 = (t6 + 40U);
    t19 = *((char **)t7);
    *((unsigned char *)t19) = t20;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(123, ng0);
    t2 = (t0 + 1052U);
    t3 = *((char **)t2);
    t2 = (t0 + 1588U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 5U);
    xsi_set_current_line(124, ng0);
    t2 = (t0 + 868U);
    t3 = *((char **)t2);
    t2 = (t0 + 1452U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 4U);
    xsi_set_current_line(125, ng0);
    t2 = (t0 + 960U);
    t3 = *((char **)t2);
    t2 = (t0 + 1520U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 4U);
    xsi_set_current_line(126, ng0);
    t2 = (t0 + 684U);
    t3 = *((char **)t2);
    t20 = *((unsigned char *)t3);
    t2 = (t0 + 1656U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    *((unsigned char *)t2) = t20;
    xsi_set_current_line(127, ng0);
    t2 = (t0 + 776U);
    t3 = *((char **)t2);
    t20 = *((unsigned char *)t3);
    t2 = (t0 + 1724U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    *((unsigned char *)t2) = t20;
    xsi_set_current_line(128, ng0);
    t2 = (t0 + 1144U);
    t3 = *((char **)t2);
    t2 = (t0 + 1384U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 20U);
    xsi_set_current_line(129, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 1588U);
    t6 = *((char **)t4);
    memcpy(t21, t6, 5U);
    t4 = (t0 + 6340U);
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t21, t4, (unsigned char)0, 10);
    xsi_set_current_line(130, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 1452U);
    t6 = *((char **)t4);
    memcpy(t22, t6, 4U);
    t4 = (t0 + 6324U);
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t22, t4, (unsigned char)0, 10);
    xsi_set_current_line(131, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 1520U);
    t6 = *((char **)t4);
    memcpy(t23, t6, 4U);
    t4 = (t0 + 6324U);
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t23, t4, (unsigned char)0, 10);
    xsi_set_current_line(132, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 1656U);
    t6 = *((char **)t4);
    t20 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t20, (unsigned char)0, 10);
    xsi_set_current_line(133, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 1724U);
    t6 = *((char **)t4);
    t20 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t20, (unsigned char)0, 9);
    xsi_set_current_line(134, ng0);
    t2 = (t0 + 6675);
    t4 = (t0 + 2328U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 3U);
    xsi_set_current_line(135, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 2328U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t24, t7, 3U);
    t6 = (t0 + 6372U);
    t25 = (10U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t24, t6, (unsigned char)0, t25);
    xsi_set_current_line(136, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 1384U);
    t6 = *((char **)t4);
    memcpy(t26, t6, 20U);
    t4 = (t0 + 6308U);
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t26, t4, (unsigned char)0, 7);
    xsi_set_current_line(137, ng0);
    t2 = (t0 + 6678);
    t4 = (t0 + 2244U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 10U);
    xsi_set_current_line(138, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 2244U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t27, t7, 10U);
    t6 = (t0 + 6356U);
    t25 = (3U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t27, t6, (unsigned char)0, t25);
    xsi_set_current_line(139, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 1912U);
    t4 = (t0 + 2080U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    xsi_set_current_line(140, ng0);

LAB18:    t2 = (t0 + 3004);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB19;
    goto LAB1;

LAB13:    t3 = (t0 + 568U);
    t20 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t3, 0U, 0U);
    if (t20 == 1)
        goto LAB12;
    else
        goto LAB14;

LAB15:    goto LAB13;

LAB16:    t4 = (t0 + 3004);
    *((int *)t4) = 0;
    xsi_set_current_line(141, ng0);
    t2 = (t0 + 1052U);
    t3 = *((char **)t2);
    t2 = (t0 + 1588U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 5U);
    xsi_set_current_line(142, ng0);
    t2 = (t0 + 868U);
    t3 = *((char **)t2);
    t2 = (t0 + 1452U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 4U);
    xsi_set_current_line(143, ng0);
    t2 = (t0 + 960U);
    t3 = *((char **)t2);
    t2 = (t0 + 1520U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 4U);
    xsi_set_current_line(144, ng0);
    t2 = (t0 + 684U);
    t3 = *((char **)t2);
    t20 = *((unsigned char *)t3);
    t2 = (t0 + 1656U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    *((unsigned char *)t2) = t20;
    xsi_set_current_line(145, ng0);
    t2 = (t0 + 776U);
    t3 = *((char **)t2);
    t20 = *((unsigned char *)t3);
    t2 = (t0 + 1724U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    *((unsigned char *)t2) = t20;
    xsi_set_current_line(146, ng0);
    t2 = (t0 + 1144U);
    t3 = *((char **)t2);
    t2 = (t0 + 1384U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 20U);
    xsi_set_current_line(147, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 1588U);
    t6 = *((char **)t4);
    memcpy(t28, t6, 5U);
    t4 = (t0 + 6340U);
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t28, t4, (unsigned char)0, 10);
    xsi_set_current_line(148, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 1452U);
    t6 = *((char **)t4);
    memcpy(t29, t6, 4U);
    t4 = (t0 + 6324U);
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t29, t4, (unsigned char)0, 10);
    xsi_set_current_line(149, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 1520U);
    t6 = *((char **)t4);
    memcpy(t30, t6, 4U);
    t4 = (t0 + 6324U);
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t30, t4, (unsigned char)0, 10);
    xsi_set_current_line(150, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 1656U);
    t6 = *((char **)t4);
    t20 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t20, (unsigned char)0, 10);
    xsi_set_current_line(151, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 1724U);
    t6 = *((char **)t4);
    t20 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t20, (unsigned char)0, 9);
    xsi_set_current_line(152, ng0);
    t2 = (t0 + 6688);
    t4 = (t0 + 2328U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 3U);
    xsi_set_current_line(153, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 2328U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t31, t7, 3U);
    t6 = (t0 + 6372U);
    t25 = (10U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t31, t6, (unsigned char)0, t25);
    xsi_set_current_line(154, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 1384U);
    t6 = *((char **)t4);
    memcpy(t32, t6, 20U);
    t4 = (t0 + 6308U);
    ieee_p_3564397177_sub_1281154728_91900896(IEEE_P_3564397177, t2, t3, t32, t4, (unsigned char)0, 8);
    xsi_set_current_line(155, ng0);
    t2 = (t0 + 6691);
    t4 = (t0 + 2244U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 10U);
    xsi_set_current_line(156, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 2080U);
    t4 = (t0 + 2244U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t33, t7, 10U);
    t6 = (t0 + 6356U);
    t25 = (10U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t33, t6, (unsigned char)0, t25);
    xsi_set_current_line(157, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 1912U);
    t4 = (t0 + 2080U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    xsi_set_current_line(158, ng0);
    t2 = (t0 + 2700);
    t3 = (t0 + 1912U);
    t4 = (t0 + 2080U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    goto LAB10;

LAB17:    t3 = (t0 + 568U);
    t20 = ieee_p_2592010699_sub_1258338084_503743352(IEEE_P_2592010699, t3, 0U, 0U);
    if (t20 == 1)
        goto LAB16;
    else
        goto LAB18;

LAB19:    goto LAB17;

LAB21:    goto LAB2;

LAB22:    goto LAB21;

LAB24:    goto LAB22;

}


extern void work_a_2683493387_2372691052_init()
{
	static char *pe[] = {(void *)work_a_2683493387_2372691052_p_0,(void *)work_a_2683493387_2372691052_p_1};
	xsi_register_didat("work_a_2683493387_2372691052", "isim/TBN_UC_isim_beh.exe.sim/work/a_2683493387_2372691052.didat");
	xsi_register_executes(pe);
}
