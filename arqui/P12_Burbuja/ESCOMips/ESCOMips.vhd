library IEEE;
library WORK;

use IEEE.STD_LOGIC_1164.ALL;
use WORK.PaqueteESCOMips.ALL;

entity ESCOMips is
	port
	(
		clk: 	in std_logic;
		RCLR: in std_logic;
		PC_out					: out std_logic_vector(7 downto 0);
		Data_out:	out	std_logic_vector(7 downto 0)
	);
end ESCOMips;

architecture A_ESCOMips of ESCOMips is
	--Microinstruccion
	--SDMP,UP,DW,WPC,SR2,SWD,SEXT,SHE,DIR,WR,SOP1,SOP2,ALUOP[3],ALUOP[2],ALUOP[1],ALUOP[0],SDMD,WD,SR,LF
	signal SDMP		: std_logic;
	signal UP		: std_logic;
	signal DW		: std_logic;
	signal WPC		: std_logic;
	signal SR2		: std_logic;
	signal SWD		: std_logic;
	signal SEXT		: std_logic;
	signal SHE		: std_logic;
	signal DIR		: std_logic;
	signal WR		: std_logic;
	signal SOP1		: std_logic;
	signal SOP2		: std_logic;
	signal ALUOP	: std_logic_vector (3 downto 0);
	signal SDMD		: std_logic;
	signal WD		: std_logic;
	signal SR		: std_logic;
	signal LF		: std_logic;
	
	signal clr		: std_logic;
	
	signal FLAGS		: std_logic_vector ( 3 downto 0);
	signal PC			: std_logic_vector (15 downto 0);
	signal DataInPila	: std_logic_vector (15 downto 0);
	signal Instruccion: std_logic_vector (24 downto 0);
	
	signal Microinstruccion	: std_logic_vector (19 downto 0);
	signal ReadData1			: std_logic_vector (15 downto 0);
	signal ReadData2			: std_logic_vector (15 downto 0);
	signal ReadRegister2		: std_logic_vector ( 3 downto 0);
	signal WriteData			: std_logic_vector (15 downto 0);
	signal OperandoA			: std_logic_vector (15 downto 0);
	signal OperandoB			: std_logic_vector (15 downto 0);
	signal ResultadoALU		: std_logic_vector (15 downto 0);
	signal BusDirMD			: std_logic_vector (15 downto 0);
	signal DataOutMD			: std_logic_vector (15 downto 0);
	signal DataOut_ExtSigno	: std_logic_vector (15 downto 0);
	signal DataOut_ExtDir	: std_logic_vector (15 downto 0);
	signal DatoExt				: std_logic_vector (15 downto 0);
	signal ResultadoSR		: std_logic_vector (15 downto 0);
	
	--signal clk: std_logic;
begin
	
	SDMP	<=MicroInstruccion(19);
	UP		<=MicroInstruccion(18);
	DW		<=MicroInstruccion(17);
	WPC	<=MicroInstruccion(16);
	SR2	<=MicroInstruccion(15);
	SWD	<=MicroInstruccion(14);
	SEXT	<=MicroInstruccion(13);
	SHE	<=MicroInstruccion(12);
	DIR	<=MicroInstruccion(11);
	WR		<=MicroInstruccion(10);
	SOP1	<=MicroInstruccion(9);
	SOP2	<=MicroInstruccion(8);
	ALUOP	<=MicroInstruccion(7 downto 4);
	SDMD	<=MicroInstruccion(3);
	WD		<=MicroInstruccion(2);
	SR		<=MicroInstruccion(1);
	LF		<=MicroInstruccion(0);
	
	--Multiplexores
	DataInPila <= ResultadoSR when SDMP = '1' else Instruccion(15 downto 0);
	
	ReadRegister2 <= Instruccion(19 downto 16) when SR2  = '1' else Instruccion(11 downto 8);
	
	WriteData <= ResultadoSR when SWD='1' else Instruccion(15 downto 0);
	
	DatoExt <= DataOut_ExtDir when SEXT='1' else DataOut_ExtSigno;
	
	OperandoA <= PC when SOP1='1' else ReadData1;
	
	OperandoB <= DatoExt when SOP2='1' else ReadData2;
	
	BusDirMD <= Instruccion(15 downto 0) when SDMD='1' else ResultadoALU;
	
	ResultadoSR <= ResultadoALU when SR='1' else DataOutMD;
	
	
	UnidadAritmeticaLogica: ALU 
		port map
		(
			A=>OperandoA,
			B=>OperandoB,
			ALUOP=>ALUOP,
			resultado=>ResultadoALU,
			OV=>FLAGS(3),
			N =>FLAGS(2),
			Z =>FLAGS(1),
			C =>FLAGS(0)	
		);	
	
	ArchivoRegistros: ArchivoDeRegistros 
		port map
		(
			WriteData=>WriteData,
			WriteRegister=>Instruccion(19 downto 16),
			ReadRegister1=>Instruccion(15 downto 12),
			ReadRegister2=>ReadRegister2,
			SHAMT=>Instruccion(7 downto 4),	
			WR=>WR,
			SHE=>SHE,
			DIR=>DIR,
			clk=>clk,
			clr=>clr,
			ReadData1=>ReadData1,
			ReadData2=>ReadData2
		);
		
	ExtensorDireccion: ExtensorDeDireccion 
		port map
		(
			DataIn=>Instruccion(11 downto 0),	
			DataOut=>DataOut_ExtDir	
		);
	
	ExtensorSigno: ExtensorDeSigno 
		port map
		(
			DataIn=>Instruccion(11 downto 0),
			DataOut=>DataOut_ExtSigno
		);
	
	MemoriaDatos: MemoriaDeDatos 
		port map 
		( 
			BusDir=>BusDirMD(10 downto 0),  
			DataIn=>ReadData2, 
			WD=>WD, 	  	
			clk=>clk, 	
			DataOut=>DataOutMD 
		);
	
	MemoriaPrograma: MemoriaDePrograma 
		port map
		(
			BusDir=>PC(8 downto 0),
			Instruccion=>Instruccion
		);
	
	STACK: PILA 
		port map
		(
			DataInPila=>DataInPila,
			Load=>WPC,
			UP=>UP,
			Down=>DW,
			clk=>clk,
			clr=>clr,
			DataOutPila=>PC
		); 
	
	UC: UnidadDeControl 
		port map
		(
			OV => FLAGS(3),
			N  => FLAGS(2),
			Z  => FLAGS(1),
			C  => FLAGS(0),
			LF =>LF,
			clk=>clk,
			clr=>clr,
			OP_CODE=>Instruccion(24 downto 20),
			FUN_CODE=>Instruccion(3 downto 0),
			MicroInstruccion=>Microinstruccion
		);
--	
	process(clk)
	begin
		if(falling_edge(clk)) then
			clr<=RCLR;
		end if;
	end process;
	
	PC_out<=PC(7 downto 0);
	Data_out<=DataOutMD(7 downto 0);
	
end A_ESCOMips;