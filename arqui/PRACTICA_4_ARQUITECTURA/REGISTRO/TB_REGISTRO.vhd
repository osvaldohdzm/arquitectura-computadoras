
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 

 
ENTITY TB_REGISTRO IS
END TB_REGISTRO;
 
ARCHITECTURE behavior OF TB_REGISTRO IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Registro
    PORT(
         clk : IN  std_logic;
         clr : IN  std_logic;
         l : IN  std_logic;
         d : IN  std_logic_vector(15 downto 0);
         q : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';
   signal l : std_logic := '0';
   signal d : std_logic_vector(15 downto 0) := (others => '0');

 	--Outputs
   signal q : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Registro PORT MAP (
          clk => clk,
          clr => clr,
          l => l,
          d => d,
          q => q
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      d <= "0000000000000001";
		l <='1';
		clr <= '1';
      wait for 20 ns;
		clr <='0';
		wait for 20 ns;
		l <= '0';
		d<= "0000000000000010";
		wait for 20 ns;
		l <= '1';
		d<= "0000000000000010";
		wait for 20 ns;
		l <= '1';
		d<= "0000000000000010";
		clr <= '1';
      wait;
   end process;

END;
