library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity P7_Pila is
generic(
	n : integer := 4
);
    Port ( d_in : in  STD_LOGIC_VECTOR (15 downto 0);
           dw, wpc, up, clk, clr : in  STD_LOGIC;
           d_out : out  STD_LOGIC_VECTOR (15 downto 0));
end P7_Pila;

architecture Behavioral of P7_Pila is
type arregloPila is array (0 to (2**n)-1) of STD_LOGIC_VECTOR (15 downto 0);
begin

	process(clk, clr)
	variable pila : arregloPila;
	variable sp : integer range 0 to n-1;
	begin
		if(clr = '1') then
			pila := (others => (others => '0'));
			sp := 0;
		elsif(rising_edge(clk)) then
			if((up = '0') AND (dw = '0') AND (wpc = '0')) then
				sp := sp;
				pila(sp) := pila(sp) + 1;
			elsif((up = '0') AND (dw = '0') AND (wpc = '1')) then
				sp := sp;
				pila(sp) := d_in;
			elsif((up = '1') AND (dw = '0') AND (wpc = '1')) then
				sp := sp + 1;
				pila(sp) := d_in;
			elsif((up = '0') AND (dw = '1') AND (wpc = '0')) then
				sp := sp - 1;
				pila(sp) := pila(sp) + 1;
			end if;
		end if;
		
		d_out <= pila(sp);
				
	end process;

end Behavioral;

