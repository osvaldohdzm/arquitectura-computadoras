
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY alu_test IS
END alu_test;
 
ARCHITECTURE behavior OF alu_test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ALU16
    PORT(
         A : IN  std_logic_vector(15 downto 0);
         B : IN  std_logic_vector(15 downto 0);
         AOP : IN  std_logic_vector(3 downto 0);
         FLAGS : OUT  std_logic_vector(3 downto 0);
         S : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal A : std_logic_vector(15 downto 0) := (others => '0');
   signal B : std_logic_vector(15 downto 0) := (others => '0');
   signal AOP : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal FLAGS : std_logic_vector(3 downto 0);
   signal S : std_logic_vector(15 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ALU16 PORT MAP (
          A => A,
          B => B,
          AOP => AOP,
          FLAGS => FLAGS,
          S => S
        );

   -- Stimulus process
   stim_proc: process
   begin		
      -- insert stimulus here 
		a <= "0000000000000011";
		b <= "0000000000000001";
		aop <= "0011";
      wait for 30 ns;
		
		a <= "0000000000000011";
		b <= "0000000000000001";
		aop <= "0111";
      wait for 30 ns;
		
		a <= "0000000000000011";
		b <= "0000000000000011";
		aop <= "0111";
      wait;
   end process;

END;
