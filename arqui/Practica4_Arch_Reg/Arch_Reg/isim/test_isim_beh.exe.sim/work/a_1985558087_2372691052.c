/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x1048c146 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/OHM/Code/arquitectura-computadoras/profesora nayeli/Practica4_Arch_Reg/Arch_Reg/test.vhd";
extern char *STD_TEXTIO;
extern char *IEEE_P_3564397177;
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
void ieee_p_3564397177_sub_1496949865_91900896(char *, char *, char *, unsigned char , unsigned char , int );
void ieee_p_3564397177_sub_2743816878_91900896(char *, char *, char *, char *);
void ieee_p_3564397177_sub_3205433008_91900896(char *, char *, char *, char *, char *, unsigned char , int );
void ieee_p_3564397177_sub_3988856810_91900896(char *, char *, char *, char *, char *);


static void work_a_1985558087_2372691052_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    int64 t7;
    int64 t8;

LAB0:    t1 = (t0 + 3440U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(86, ng0);
    t2 = (t0 + 3824);
    t3 = (t2 + 32U);
    t4 = *((char **)t3);
    t5 = (t4 + 40U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(87, ng0);
    t2 = (t0 + 1844U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 3340);
    xsi_process_wait(t2, t8);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(88, ng0);
    t2 = (t0 + 3824);
    t3 = (t2 + 32U);
    t4 = *((char **)t3);
    t5 = (t4 + 40U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(89, ng0);
    t2 = (t0 + 1844U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 3340);
    xsi_process_wait(t2, t8);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    goto LAB2;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

}

static void work_a_1985558087_2372691052_p_1(char *t0)
{
    char t5[16];
    char t10[8];
    char t11[8];
    char t12[8];
    char t13[8];
    char t14[8];
    char t15[8];
    char t16[8];
    char t17[8];
    char t18[8];
    char t19[8];
    char t24[8];
    char t25[8];
    char t26[8];
    char t27[8];
    char t28[16];
    char t29[16];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    int t8;
    unsigned int t9;
    int64 t20;
    int t21;
    char *t22;
    unsigned char t23;
    int t30;

LAB0:    t1 = (t0 + 3584U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(113, ng0);
    t2 = (t0 + 2844U);
    t3 = (t0 + 9365);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 10;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (10 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)0);
    xsi_set_current_line(114, ng0);
    t2 = (t0 + 2780U);
    t3 = (t0 + 9375);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 13;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (13 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)1);
    xsi_set_current_line(116, ng0);
    t2 = (t0 + 9388);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(117, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t10, t7, 5U);
    t6 = (t0 + 9100U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t10, t6, (unsigned char)0, t8);
    xsi_set_current_line(118, ng0);
    t2 = (t0 + 9393);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(119, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t11, t7, 5U);
    t6 = (t0 + 9100U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t11, t6, (unsigned char)0, t8);
    xsi_set_current_line(120, ng0);
    t2 = (t0 + 9398);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(121, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t12, t7, 5U);
    t6 = (t0 + 9100U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t12, t6, (unsigned char)0, t8);
    xsi_set_current_line(122, ng0);
    t2 = (t0 + 9403);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(123, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t13, t7, 5U);
    t6 = (t0 + 9100U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t13, t6, (unsigned char)0, t8);
    xsi_set_current_line(124, ng0);
    t2 = (t0 + 9408);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(125, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t14, t7, 5U);
    t6 = (t0 + 9100U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t14, t6, (unsigned char)0, t8);
    xsi_set_current_line(126, ng0);
    t2 = (t0 + 9413);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(127, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t15, t7, 5U);
    t6 = (t0 + 9100U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t15, t6, (unsigned char)0, t8);
    xsi_set_current_line(128, ng0);
    t2 = (t0 + 9418);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(129, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t16, t7, 5U);
    t6 = (t0 + 9100U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t16, t6, (unsigned char)0, t8);
    xsi_set_current_line(130, ng0);
    t2 = (t0 + 9423);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(131, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t17, t7, 5U);
    t6 = (t0 + 9100U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t17, t6, (unsigned char)0, t8);
    xsi_set_current_line(132, ng0);
    t2 = (t0 + 9428);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(133, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t18, t7, 5U);
    t6 = (t0 + 9100U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t18, t6, (unsigned char)0, t8);
    xsi_set_current_line(134, ng0);
    t2 = (t0 + 9433);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(135, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 3112U);
    t6 = (t4 + 36U);
    t7 = *((char **)t6);
    memcpy(t19, t7, 5U);
    t6 = (t0 + 9100U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t19, t6, (unsigned char)0, t8);
    xsi_set_current_line(136, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2780U);
    t4 = (t0 + 2948U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    xsi_set_current_line(138, ng0);
    t20 = (100 * 1000LL);
    t2 = (t0 + 3484);
    xsi_process_wait(t2, t20);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(139, ng0);
    t2 = (t0 + 9438);
    *((int *)t2) = 0;
    t3 = (t0 + 9442);
    *((int *)t3) = 10;
    t8 = 0;
    t21 = 10;

LAB8:    if (t8 <= t21)
        goto LAB9;

LAB11:    xsi_set_current_line(180, ng0);
    t2 = (t0 + 2844U);
    std_textio_file_close(t2);
    xsi_set_current_line(181, ng0);
    t2 = (t0 + 2780U);
    std_textio_file_close(t2);
    xsi_set_current_line(183, ng0);

LAB19:    *((char **)t1) = &&LAB20;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB9:    xsi_set_current_line(140, ng0);
    t4 = (t0 + 3484);
    t6 = (t0 + 2844U);
    t7 = (t0 + 2988U);
    std_textio_readline(STD_TEXTIO, t4, t6, t7);
    xsi_set_current_line(142, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2988U);
    t4 = (t0 + 2048U);
    t6 = *((char **)t4);
    t4 = (t0 + 9020U);
    ieee_p_3564397177_sub_3988856810_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(143, ng0);
    t2 = (t0 + 2048U);
    t3 = *((char **)t2);
    t2 = (t0 + 3860);
    t4 = (t2 + 32U);
    t6 = *((char **)t4);
    t7 = (t6 + 40U);
    t22 = *((char **)t7);
    memcpy(t22, t3, 4U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(144, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2988U);
    t4 = (t0 + 2116U);
    t6 = *((char **)t4);
    t4 = (t0 + 9036U);
    ieee_p_3564397177_sub_3988856810_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(145, ng0);
    t2 = (t0 + 2116U);
    t3 = *((char **)t2);
    t2 = (t0 + 3896);
    t4 = (t2 + 32U);
    t6 = *((char **)t4);
    t7 = (t6 + 40U);
    t22 = *((char **)t7);
    memcpy(t22, t3, 4U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(146, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2988U);
    t4 = (t0 + 2184U);
    t6 = *((char **)t4);
    t4 = (t0 + 9052U);
    ieee_p_3564397177_sub_3988856810_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(147, ng0);
    t2 = (t0 + 2184U);
    t3 = *((char **)t2);
    t2 = (t0 + 3932);
    t4 = (t2 + 32U);
    t6 = *((char **)t4);
    t7 = (t6 + 40U);
    t22 = *((char **)t7);
    memcpy(t22, t3, 4U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(148, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2988U);
    t4 = (t0 + 2252U);
    t6 = *((char **)t4);
    t4 = (t0 + 9068U);
    ieee_p_3564397177_sub_3988856810_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(149, ng0);
    t2 = (t0 + 2252U);
    t3 = *((char **)t2);
    t2 = (t0 + 3968);
    t4 = (t2 + 32U);
    t6 = *((char **)t4);
    t7 = (t6 + 40U);
    t22 = *((char **)t7);
    memcpy(t22, t3, 4U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(150, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2988U);
    t4 = (t0 + 2320U);
    t6 = *((char **)t4);
    t4 = (t0 + 9084U);
    ieee_p_3564397177_sub_3988856810_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(151, ng0);
    t2 = (t0 + 2320U);
    t3 = *((char **)t2);
    t2 = (t0 + 4004);
    t4 = (t2 + 32U);
    t6 = *((char **)t4);
    t7 = (t6 + 40U);
    t22 = *((char **)t7);
    memcpy(t22, t3, 16U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(152, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2988U);
    t4 = (t0 + 2388U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_2743816878_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(153, ng0);
    t2 = (t0 + 2388U);
    t3 = *((char **)t2);
    t23 = *((unsigned char *)t3);
    t2 = (t0 + 4040);
    t4 = (t2 + 32U);
    t6 = *((char **)t4);
    t7 = (t6 + 40U);
    t22 = *((char **)t7);
    *((unsigned char *)t22) = t23;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(154, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2988U);
    t4 = (t0 + 2456U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_2743816878_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(155, ng0);
    t2 = (t0 + 2456U);
    t3 = *((char **)t2);
    t23 = *((unsigned char *)t3);
    t2 = (t0 + 4076);
    t4 = (t2 + 32U);
    t6 = *((char **)t4);
    t7 = (t6 + 40U);
    t22 = *((char **)t7);
    *((unsigned char *)t22) = t23;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(156, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2988U);
    t4 = (t0 + 2524U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_2743816878_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(157, ng0);
    t2 = (t0 + 2524U);
    t3 = *((char **)t2);
    t23 = *((unsigned char *)t3);
    t2 = (t0 + 4112);
    t4 = (t2 + 32U);
    t6 = *((char **)t4);
    t7 = (t6 + 40U);
    t22 = *((char **)t7);
    *((unsigned char *)t22) = t23;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(158, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2988U);
    t4 = (t0 + 2592U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_2743816878_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(159, ng0);
    t2 = (t0 + 2592U);
    t3 = *((char **)t2);
    t23 = *((unsigned char *)t3);
    t2 = (t0 + 4148);
    t4 = (t2 + 32U);
    t6 = *((char **)t4);
    t7 = (t6 + 40U);
    t22 = *((char **)t7);
    *((unsigned char *)t22) = t23;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(161, ng0);

LAB14:    t2 = (t0 + 3780);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB15;
    goto LAB1;

LAB10:    t2 = (t0 + 9438);
    t8 = *((int *)t2);
    t3 = (t0 + 9442);
    t21 = *((int *)t3);
    if (t8 == t21)
        goto LAB11;

LAB16:    t30 = (t8 + 1);
    t8 = t30;
    t4 = (t0 + 9438);
    *((int *)t4) = t8;
    goto LAB8;

LAB12:    t4 = (t0 + 3780);
    *((int *)t4) = 0;
    xsi_set_current_line(163, ng0);
    t2 = (t0 + 1512U);
    t3 = *((char **)t2);
    t2 = (t0 + 1912U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 16U);
    xsi_set_current_line(164, ng0);
    t2 = (t0 + 1604U);
    t3 = *((char **)t2);
    t2 = (t0 + 1980U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 16U);
    xsi_set_current_line(166, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 2048U);
    t6 = *((char **)t4);
    memcpy(t24, t6, 4U);
    t4 = (t0 + 9020U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t24, t4, (unsigned char)0, 4);
    xsi_set_current_line(167, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 2116U);
    t6 = *((char **)t4);
    memcpy(t25, t6, 4U);
    t4 = (t0 + 9036U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t25, t4, (unsigned char)0, 5);
    xsi_set_current_line(168, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 2184U);
    t6 = *((char **)t4);
    memcpy(t26, t6, 4U);
    t4 = (t0 + 9052U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t26, t4, (unsigned char)0, 5);
    xsi_set_current_line(169, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 2252U);
    t6 = *((char **)t4);
    memcpy(t27, t6, 4U);
    t4 = (t0 + 9068U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t27, t4, (unsigned char)0, 8);
    xsi_set_current_line(170, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 2320U);
    t6 = *((char **)t4);
    memcpy(t5, t6, 16U);
    t4 = (t0 + 9084U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t5, t4, (unsigned char)0, 8);
    xsi_set_current_line(171, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 2388U);
    t6 = *((char **)t4);
    t23 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t23, (unsigned char)0, 5);
    xsi_set_current_line(172, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 2456U);
    t6 = *((char **)t4);
    t23 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t23, (unsigned char)0, 5);
    xsi_set_current_line(173, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 2524U);
    t6 = *((char **)t4);
    t23 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t23, (unsigned char)0, 7);
    xsi_set_current_line(174, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 1912U);
    t6 = *((char **)t4);
    memcpy(t28, t6, 16U);
    t4 = (t0 + 8988U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t28, t4, (unsigned char)0, 8);
    xsi_set_current_line(175, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2948U);
    t4 = (t0 + 1980U);
    t6 = *((char **)t4);
    memcpy(t29, t6, 16U);
    t4 = (t0 + 9004U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t29, t4, (unsigned char)0, 6);
    xsi_set_current_line(177, ng0);
    t2 = (t0 + 3484);
    t3 = (t0 + 2780U);
    t4 = (t0 + 2948U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    goto LAB10;

LAB13:    t3 = (t0 + 568U);
    t23 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t3, 0U, 0U);
    if (t23 == 1)
        goto LAB12;
    else
        goto LAB14;

LAB15:    goto LAB13;

LAB17:    goto LAB2;

LAB18:    goto LAB17;

LAB20:    goto LAB18;

}


extern void work_a_1985558087_2372691052_init()
{
	static char *pe[] = {(void *)work_a_1985558087_2372691052_p_0,(void *)work_a_1985558087_2372691052_p_1};
	xsi_register_didat("work_a_1985558087_2372691052", "isim/test_isim_beh.exe.sim/work/a_1985558087_2372691052.didat");
	xsi_register_executes(pe);
}
