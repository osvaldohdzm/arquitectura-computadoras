library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity P6_MemoriaPrograma is
generic(
	n_dir : integer := 11
);
    Port ( dir : in  STD_LOGIC_VECTOR (n_dir-1 downto 0);
           dato : out  STD_LOGIC_VECTOR (24 downto 0));
end P6_MemoriaPrograma;

architecture Behavioral of P6_MemoriaPrograma is
type memoriaP is array (0 to (2**n_dir)-1) of STD_LOGIC_VECTOR (24 downto 0);
signal aux_mem : memoriaP:=(0 => "0000100000000000000000101", 
									 1 => "0000100010000000000001010", 
									 2 => "0000000010001000000000000", 
									 3 => "0001100010000000000000101", 
									 4 => "1001100000000000000000010", 
									others=>(others=>'0'));
begin

	dato <= aux_mem(conv_integer(dir));

end Behavioral;

