
LIBRARY ieee;
LIBRARY std;
use std.textio.all;
USE ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_textio.all;


 
ENTITY testArchivoReg IS
END testArchivoReg;
 
ARCHITECTURE behavior OF testArchivoReg IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT archivoReg
    PORT(
         clk : IN  std_logic;
         clr : IN  std_logic;
         wr : IN  std_logic;
         she : IN  std_logic;
         dir : IN  std_logic;
         write_reg : IN  std_logic_vector(3 downto 0);
         read_reg1 : IN  std_logic_vector(3 downto 0);
         read_reg2 : IN  std_logic_vector(3 downto 0);
         shamt : IN  std_logic_vector(3 downto 0);
         write_data : IN  std_logic_vector(15 downto 0);
         read_data1 : INOUT  std_logic_vector(15 downto 0);
         read_data2 : INOUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';
   signal wr : std_logic := '0';
   signal she : std_logic := '0';
   signal dir : std_logic := '0';
   signal write_reg : std_logic_vector(3 downto 0) := (others => '0');
   signal read_reg1 : std_logic_vector(3 downto 0) := (others => '0');
   signal read_reg2 : std_logic_vector(3 downto 0) := (others => '0');
   signal shamt : std_logic_vector(3 downto 0) := (others => '0');
   signal write_data : std_logic_vector(15 downto 0) := (others => '0');

	--BiDirs
   signal read_data1 : std_logic_vector(15 downto 0);
   signal read_data2 : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: archivoReg PORT MAP (
          clk => clk,
          clr => clr,
          wr => wr,
          she => she,
          dir => dir,
          write_reg => write_reg,
          read_reg1 => read_reg1,
          read_reg2 => read_reg2,
          shamt => shamt,
          write_data => write_data,
          read_data1 => read_data1,
          read_data2 => read_data2
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   file arch_res : text;																					
	variable linea_res : line;
	variable var_rdu : std_logic_vector(15 downto 0);
	variable var_rdd : std_logic_vector(15 downto 0);
	
	file arch_vec : text;
	variable linea_vec : line;
	

	variable var_rr1    :  std_logic_vector(3 downto 0);
	variable var_rr2    :  std_logic_vector(3 downto 0);
	variable var_shamt  :  std_logic_vector(3 downto 0);
	variable var_wre    :  std_logic_vector(3 downto 0);
	variable var_wd     :  std_logic_vector(15 downto 0);
	variable var_wr     :  std_logic;
	variable var_she    :  std_logic;
	variable var_dir    :  std_logic;
	variable var_clr    :  std_logic;

	
	variable cadena : string(1 to 4);
	variable cadena_i : string(1 to 6);
	variable cadena_x : string(1 to 5);

   begin		
		file_open(arch_res, "./salida.txt", write_mode); 	
		file_open(arch_vec, "./entradas_archivo_de_registro.txt", read_mode); 	

		cadena_x := "  rr1";
		write(linea_res, cadena_x, right, cadena_x'length+1);	
		cadena := " rr2";
		write(linea_res, cadena, right, cadena'length+1);
		cadena_i := "shamt ";
		write(linea_res, cadena_i, right, cadena_i'length+1);	
		cadena := "wreg";
		write(linea_res, cadena, right, cadena'length+1);
		cadena := " wd ";
		write(linea_res, cadena, right, cadena'length+1);
		cadena := " wr ";
		write(linea_res, cadena, right, cadena'length+1);
		cadena := " she";
		write(linea_res, cadena, right, cadena'length+1);
		cadena := " dir";
		write(linea_res, cadena, right, cadena'length+1);
		cadena := " rd1";
		write(linea_res, cadena, right, cadena'length+1);
		cadena := " rd2";
		write(linea_res, cadena, right, cadena'length+1);
		writeline(arch_res,linea_res);
      wait for 100 ns;	
 
		for i in 0 to 9 loop
			readline(arch_vec,linea_vec);
			
			read(linea_vec, var_rr1);
			read_reg1<=var_rr1;

			read(linea_vec, var_rr2);
			read_reg2<=var_rr2;

			read(linea_vec, var_shamt);
			shamt<=var_shamt;
			
			read(linea_vec, var_wre);
			write_reg<=var_wre;
			
			read(linea_vec, var_wd);
			write_data<=var_wd;
			
			read(linea_vec, var_wr);
			wr<=var_wr;

			read(linea_vec, var_she);
			she<=var_she;
			
			read(linea_vec, var_dir);
			dir<=var_dir;

			read(linea_vec, var_clr);
			clr<=var_clr;
			
			wait until rising_edge(clk);
			var_rdu := read_data1;	
			var_rdd := read_data2;	
	
			hwrite(linea_res, var_rr1, 	right, 5);
			hwrite(linea_res, var_rr2, 	right, 5);
			hwrite (linea_res, var_shamt, right, 5);	
			hwrite (linea_res, var_wre, 	right, 5);	
			hwrite (linea_res, var_wd, 	right, 5);	
			write(linea_res, var_wr, 	right, 5);
			write(linea_res, var_she, 	right, 5);
			write(linea_res, var_dir, 	right, 5);
			hwrite (linea_res, var_rdu, right, 5);	
			hwrite (linea_res, var_rdd, right, 5);	
			writeline(arch_res,linea_res);

		end loop;
		
		file_close(arch_vec);
		file_close(arch_res);  

      wait for clk_period*10;

      wait;
   end process;


END;
