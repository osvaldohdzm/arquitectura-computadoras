

library IEEE;
use IEEE.STD_LOGIC_1164.all;


package packet is

---- componente de la condicion

component condicion is
    Port ( banderas : in  STD_LOGIC_VECTOR (3 downto 0);
           EQ, NE, L, LE, G, GE : out  STD_LOGIC);
end component;

---- componente del decodificador

component decoInst is
  Port ( CP_CODE : in  STD_LOGIC_VECTOR (4 downto 0);
           tipoR,BEQ,BNEQ,BLT,BLE,BGT,BGET: OUT std_logic);
end component;

--- componente de la microFuncion

component microFuncion is
    Port ( i : in  STD_LOGIC_VECTOR (3 downto 0);
           funSal : out  STD_LOGIC_VECTOR (19 downto 0));
end component;

---- componente de microOpCode

component microOpCode is
    Port ( CP_CODE  : in  STD_LOGIC_VECTOR (4 downto 0);
           salOpCode : out  STD_LOGIC_VECTOR (19 downto 0));
end component;

--- componente de nivel 

component nivel is
    Port ( clk, clr : in  STD_LOGIC;
           na : out  STD_LOGIC);
end component;

--- componente de regEdo

component regEdo2 is
    Port ( banderas : in  STD_LOGIC_VECTOR (3 downto 0);
           clr, clk, LF : in  STD_LOGIC;
			  salReg : out std_logic_vector (3 downto 0)
			  );
end component;

--- componente de la unidadDeControlInterna
component unicontrol is
    Port ( tipor: in STD_LOGIC;
	        beq: IN STD_LOGIC;
			  bneq: IN STD_LOGIC;
			  blt: IN STD_LOGIC;
			  ble: IN STD_LOGIC;
			  bgt: IN STD_LOGIC;
			  bget: IN STD_LOGIC; 
			  clk: IN STD_LOGIC;
			  clr: IN STD_LOGIC; 
			  eq: IN STD_LOGIC;
			  neq: IN STD_LOGIC;
			  lt: IN STD_LOGIC;
			  le: IN STD_LOGIC;
			  gtt: IN STD_LOGIC;
			  get: IN STD_LOGIC;
			  na : in  STD_LOGIC;
           sdopc: OUT STD_LOGIC;
			  sm : out  STD_LOGIC
			  );
end component;

end packet;
