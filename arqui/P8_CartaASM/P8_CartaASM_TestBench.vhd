LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY P8_CartaASM_TestBench IS
END P8_CartaASM_TestBench;
 
ARCHITECTURE behavior OF P8_CartaASM_TestBench IS 
 
    COMPONENT main
    PORT(
         clr : IN  std_logic;
         clk : IN  std_logic;
         ini : IN  std_logic;
         dato : IN  std_logic_vector(7 downto 0);
         display : OUT  std_logic_vector(6 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clr : std_logic := '0';
   signal clk : std_logic := '0';
   signal ini : std_logic := '0';
   signal dato : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal display : std_logic_vector(6 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: main PORT MAP (
          clr => clr,
          clk => clk,
          ini => ini,
          dato => dato,
          display => display
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;
			clr<= '1';
			
		wait for 100 ns;
			dato<= "00001001";
			clr<= '0';
			wait for 10 ns;
			ini<= '1';
			
		wait;
   end process;

END;
