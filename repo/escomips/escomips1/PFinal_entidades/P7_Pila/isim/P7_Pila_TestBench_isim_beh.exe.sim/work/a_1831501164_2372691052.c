/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/_PracticasArqui/P7_Pila/P7_Pila_TestBench.vhd";
extern char *STD_TEXTIO;
extern char *IEEE_P_3564397177;
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
void ieee_p_3564397177_sub_1496949865_91900896(char *, char *, char *, unsigned char , unsigned char , int );
void ieee_p_3564397177_sub_2743816878_91900896(char *, char *, char *, char *);
void ieee_p_3564397177_sub_3205433008_91900896(char *, char *, char *, char *, char *, unsigned char , int );
void ieee_p_3564397177_sub_3988856810_91900896(char *, char *, char *, char *, char *);


static void work_a_1831501164_2372691052_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    int64 t7;
    int64 t8;

LAB0:    t1 = (t0 + 4608U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(59, ng0);
    t2 = (t0 + 5256);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(60, ng0);
    t2 = (t0 + 2288U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 4416);
    xsi_process_wait(t2, t8);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(61, ng0);
    t2 = (t0 + 5256);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(62, ng0);
    t2 = (t0 + 2288U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 4416);
    xsi_process_wait(t2, t8);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    goto LAB2;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

}

static void work_a_1831501164_2372691052_p_1(char *t0)
{
    char t5[16];
    char t10[8];
    char t11[8];
    char t12[8];
    char t13[8];
    char t14[8];
    char t15[8];
    char t16[8];
    char t27[16];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    int t8;
    unsigned int t9;
    int64 t17;
    int t18;
    char *t19;
    unsigned char t20;
    unsigned char t21;
    unsigned char t22;
    unsigned char t23;
    unsigned char t24;
    int t25;
    int t26;

LAB0:    t1 = (t0 + 4856U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(82, ng0);
    t2 = (t0 + 3560U);
    t3 = (t0 + 8904);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 12;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (12 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)0);
    xsi_set_current_line(83, ng0);
    t2 = (t0 + 3456U);
    t3 = (t0 + 8916);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 14;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (14 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)1);
    xsi_set_current_line(86, ng0);
    t2 = (t0 + 8930);
    t4 = (t0 + 4024U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 4U);
    xsi_set_current_line(87, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3736U);
    t4 = (t0 + 4024U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t10, t7, 4U);
    t6 = (t0 + 8644U);
    t8 = (4U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t10, t6, (unsigned char)0, t8);
    xsi_set_current_line(88, ng0);
    t2 = (t0 + 8934);
    t4 = (t0 + 4024U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 4U);
    xsi_set_current_line(89, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3736U);
    t4 = (t0 + 4024U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t11, t7, 4U);
    t6 = (t0 + 8644U);
    t8 = (4U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t11, t6, (unsigned char)0, t8);
    xsi_set_current_line(90, ng0);
    t2 = (t0 + 8938);
    t4 = (t0 + 4024U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 4U);
    xsi_set_current_line(91, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3736U);
    t4 = (t0 + 4024U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t12, t7, 4U);
    t6 = (t0 + 8644U);
    t8 = (4U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t12, t6, (unsigned char)0, t8);
    xsi_set_current_line(92, ng0);
    t2 = (t0 + 8942);
    t4 = (t0 + 4024U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 4U);
    xsi_set_current_line(93, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3736U);
    t4 = (t0 + 4024U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t13, t7, 4U);
    t6 = (t0 + 8644U);
    t8 = (4U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t13, t6, (unsigned char)0, t8);
    xsi_set_current_line(94, ng0);
    t2 = (t0 + 8946);
    t4 = (t0 + 4024U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 4U);
    xsi_set_current_line(95, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3736U);
    t4 = (t0 + 4024U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t14, t7, 4U);
    t6 = (t0 + 8644U);
    t8 = (4U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t14, t6, (unsigned char)0, t8);
    xsi_set_current_line(96, ng0);
    t2 = (t0 + 8950);
    t4 = (t0 + 4024U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 4U);
    xsi_set_current_line(97, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3736U);
    t4 = (t0 + 4024U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t15, t7, 4U);
    t6 = (t0 + 8644U);
    t8 = (4U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t15, t6, (unsigned char)0, t8);
    xsi_set_current_line(98, ng0);
    t2 = (t0 + 8954);
    t4 = (t0 + 4024U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 4U);
    xsi_set_current_line(99, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3736U);
    t4 = (t0 + 4024U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t16, t7, 4U);
    t6 = (t0 + 8644U);
    t8 = (4U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t16, t6, (unsigned char)0, t8);
    xsi_set_current_line(100, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3456U);
    t4 = (t0 + 3736U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    xsi_set_current_line(102, ng0);
    t17 = (100 * 1000LL);
    t2 = (t0 + 4664);
    xsi_process_wait(t2, t17);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(103, ng0);
    t2 = (t0 + 8958);
    *((int *)t2) = 0;
    t3 = (t0 + 8962);
    *((int *)t3) = 20;
    t8 = 0;
    t18 = 20;

LAB8:    if (t8 <= t18)
        goto LAB9;

LAB11:    xsi_set_current_line(141, ng0);
    t2 = (t0 + 3560U);
    std_textio_file_close(t2);
    xsi_set_current_line(142, ng0);
    t2 = (t0 + 3456U);
    std_textio_file_close(t2);
    xsi_set_current_line(143, ng0);

LAB27:    *((char **)t1) = &&LAB28;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB9:    xsi_set_current_line(104, ng0);
    t4 = (t0 + 4664);
    t6 = (t0 + 3560U);
    t7 = (t0 + 3808U);
    std_textio_readline(STD_TEXTIO, t4, t6, t7);
    xsi_set_current_line(106, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3808U);
    t4 = (t0 + 2528U);
    t6 = *((char **)t4);
    t4 = (t0 + 8628U);
    ieee_p_3564397177_sub_3988856810_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(107, ng0);
    t2 = (t0 + 2528U);
    t3 = *((char **)t2);
    t2 = (t0 + 5320);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t19 = *((char **)t7);
    memcpy(t19, t3, 16U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(109, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3808U);
    t4 = (t0 + 2648U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_2743816878_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(110, ng0);
    t2 = (t0 + 2648U);
    t3 = *((char **)t2);
    t20 = *((unsigned char *)t3);
    t2 = (t0 + 5384);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t19 = *((char **)t7);
    *((unsigned char *)t19) = t20;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(112, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3808U);
    t4 = (t0 + 2768U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_2743816878_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(113, ng0);
    t2 = (t0 + 2768U);
    t3 = *((char **)t2);
    t20 = *((unsigned char *)t3);
    t2 = (t0 + 5448);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t19 = *((char **)t7);
    *((unsigned char *)t19) = t20;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(115, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3808U);
    t4 = (t0 + 2888U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_2743816878_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(116, ng0);
    t2 = (t0 + 2888U);
    t3 = *((char **)t2);
    t20 = *((unsigned char *)t3);
    t2 = (t0 + 5512);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t19 = *((char **)t7);
    *((unsigned char *)t19) = t20;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(118, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3808U);
    t4 = (t0 + 3008U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_2743816878_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(119, ng0);
    t2 = (t0 + 3008U);
    t3 = *((char **)t2);
    t20 = *((unsigned char *)t3);
    t2 = (t0 + 5576);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t19 = *((char **)t7);
    *((unsigned char *)t19) = t20;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(121, ng0);
    t2 = (t0 + 1512U);
    t3 = *((char **)t2);
    t21 = *((unsigned char *)t3);
    t22 = (t21 == (unsigned char)3);
    if (t22 == 1)
        goto LAB15;

LAB16:    t2 = (t0 + 1352U);
    t4 = *((char **)t2);
    t23 = *((unsigned char *)t4);
    t24 = (t23 == (unsigned char)3);
    t20 = t24;

LAB17:    if (t20 != 0)
        goto LAB12;

LAB14:    t2 = (t0 + 1192U);
    t3 = *((char **)t2);
    t20 = *((unsigned char *)t3);
    t21 = (t20 == (unsigned char)3);
    if (t21 != 0)
        goto LAB18;

LAB19:
LAB13:    xsi_set_current_line(127, ng0);

LAB22:    t2 = (t0 + 5176);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB23;
    goto LAB1;

LAB10:    t2 = (t0 + 8958);
    t8 = *((int *)t2);
    t3 = (t0 + 8962);
    t18 = *((int *)t3);
    if (t8 == t18)
        goto LAB11;

LAB24:    t25 = (t8 + 1);
    t8 = t25;
    t4 = (t0 + 8958);
    *((int *)t4) = t8;
    goto LAB8;

LAB12:    xsi_set_current_line(122, ng0);
    t2 = (t0 + 3128U);
    t6 = *((char **)t2);
    t25 = *((int *)t6);
    t26 = (t25 + 1);
    t2 = (t0 + 3128U);
    t7 = *((char **)t2);
    t2 = (t7 + 0);
    *((int *)t2) = t26;
    goto LAB13;

LAB15:    t20 = (unsigned char)1;
    goto LAB17;

LAB18:    xsi_set_current_line(124, ng0);
    t2 = (t0 + 3128U);
    t4 = *((char **)t2);
    t25 = *((int *)t4);
    t26 = (t25 - 1);
    t2 = (t0 + 3128U);
    t6 = *((char **)t2);
    t2 = (t6 + 0);
    *((int *)t2) = t26;
    goto LAB13;

LAB20:    t4 = (t0 + 5176);
    *((int *)t4) = 0;
    xsi_set_current_line(129, ng0);
    t2 = (t0 + 1992U);
    t3 = *((char **)t2);
    t2 = (t0 + 2408U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 16U);
    xsi_set_current_line(131, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3736U);
    t4 = (t0 + 2528U);
    t6 = *((char **)t4);
    memcpy(t5, t6, 16U);
    t4 = (t0 + 8628U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t5, t4, (unsigned char)0, 5);
    xsi_set_current_line(132, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3736U);
    t4 = (t0 + 2648U);
    t6 = *((char **)t4);
    t20 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t20, (unsigned char)0, 5);
    xsi_set_current_line(133, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3736U);
    t4 = (t0 + 2768U);
    t6 = *((char **)t4);
    t20 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t20, (unsigned char)0, 5);
    xsi_set_current_line(134, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3736U);
    t4 = (t0 + 2888U);
    t6 = *((char **)t4);
    t20 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t20, (unsigned char)0, 5);
    xsi_set_current_line(135, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3736U);
    t4 = (t0 + 3008U);
    t6 = *((char **)t4);
    t20 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_1496949865_91900896(IEEE_P_3564397177, t2, t3, t20, (unsigned char)0, 5);
    xsi_set_current_line(136, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3736U);
    t4 = (t0 + 3128U);
    t6 = *((char **)t4);
    t25 = *((int *)t6);
    std_textio_write5(STD_TEXTIO, t2, t3, t25, (unsigned char)0, 5);
    xsi_set_current_line(137, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3736U);
    t4 = (t0 + 2408U);
    t6 = *((char **)t4);
    memcpy(t27, t6, 16U);
    t4 = (t0 + 8612U);
    ieee_p_3564397177_sub_3205433008_91900896(IEEE_P_3564397177, t2, t3, t27, t4, (unsigned char)0, 5);
    xsi_set_current_line(139, ng0);
    t2 = (t0 + 4664);
    t3 = (t0 + 3456U);
    t4 = (t0 + 3736U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    goto LAB10;

LAB21:    t3 = (t0 + 1632U);
    t20 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t3, 0U, 0U);
    if (t20 == 1)
        goto LAB20;
    else
        goto LAB22;

LAB23:    goto LAB21;

LAB25:    goto LAB2;

LAB26:    goto LAB25;

LAB28:    goto LAB26;

}


extern void work_a_1831501164_2372691052_init()
{
	static char *pe[] = {(void *)work_a_1831501164_2372691052_p_0,(void *)work_a_1831501164_2372691052_p_1};
	xsi_register_didat("work_a_1831501164_2372691052", "isim/P7_Pila_TestBench_isim_beh.exe.sim/work/a_1831501164_2372691052.didat");
	xsi_register_executes(pe);
}
