--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
-- Author : Leonardo Faydella Rojas
-- Create Date:   00:27:57 10/03/2018
-- Design Name:   
-- Module Name:   C:/Users/leonardo faydella/Desktop/ProyectoFinal/ArchivoRegistros/TB_ARREGLO.vhd
-- Project Name:  ArchivoRegistros
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Arreglo
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TB_ARREGLO IS
END TB_ARREGLO;
 
ARCHITECTURE behavior OF TB_ARREGLO IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Arreglo
    PORT(
         clk : IN  std_logic;
         clr : IN  std_logic;
         l : IN  std_logic_vector(15 downto 0);
         d : IN  std_logic_vector(15 downto 0);
         read_reg1 : IN  std_logic_vector(3 downto 0);
         read_reg2 : IN  std_logic_vector(3 downto 0);
         q2 : OUT  std_logic_vector(15 downto 0);
         q1 : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';
   signal l : std_logic_vector(15 downto 0) := (others => '0');
   signal d : std_logic_vector(15 downto 0) := (others => '0');
   signal read_reg1 : std_logic_vector(3 downto 0) := (others => '0');
   signal read_reg2 : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal q2 : std_logic_vector(15 downto 0);
   signal q1 : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Arreglo PORT MAP (
          clk => clk,
          clr => clr,
          l => l,
          d => d,
          read_reg1 => read_reg1,
          read_reg2 => read_reg2,
          q2 => q2,
          q1 => q1
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      wait for 100 ns;	
		clr <= '1';
		l <= "0000000000000001";
		d <= "1010001100000001";
		read_reg1 <= "0000";
		read_reg2 <= "0001";
		wait for 100 ns;
		clr <= '0';
		wait for 100 ns;
		l <= "0000000000000010";
		d <= "1111111100000001";
		read_reg1 <= "0000";
		read_reg2 <= "0001";
		wait for 100 ns;
		read_reg1 <= "0001";
		read_reg2 <= "0000";
		l <= "0100000000000000";
		read_reg1 <= "1110";
		wait for 100 ns;
		read_reg2 <= "0101";
      wait;
   end process;

END;
