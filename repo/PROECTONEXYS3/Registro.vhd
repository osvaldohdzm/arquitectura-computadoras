
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Registro is
    Port ( clk,clr : in  STD_LOGIC;
           l : in  STD_LOGIC;
           d : in  STD_LOGIC_VECTOR (15 downto 0);
           q : out  STD_LOGIC_VECTOR (15 downto 0));
end Registro;

architecture Behavioral of Registro is

begin
regis: process(clk,clr)
begin
	if(clr = '1') then
		q<=(others=>'0');
	elsif(rising_edge(clk))then
		if (l = '1') then
			q<=d;
		end if;
	end if;
end process regis;

end Behavioral;

