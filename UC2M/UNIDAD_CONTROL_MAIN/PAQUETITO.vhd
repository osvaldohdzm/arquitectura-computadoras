
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package PAQUETITO is

COMPONENT REGISTRO_ESTADO is
    Port ( LF : in  STD_LOGIC;
           CLK,CLR : in  STD_LOGIC;
           RBANDERAS : out  STD_LOGIC_VECTOR (3 downto 0);
           BANDERAS : in  STD_LOGIC_VECTOR (3 downto 0));
end COMPONENT;


COMPONENT Nivel is
    Port ( clk,clr : in  STD_LOGIC;
           na : out  STD_LOGIC);
end COMPONENT;

COMPONENT Mem_Func is
    Port ( A : in  STD_LOGIC_VECTOR (3 downto 0);
           D : out  STD_LOGIC_VECTOR (19 downto 0));
end COMPONENT;



COMPONENT MEMORIA_CODIGO is
    Port ( A : in  STD_LOGIC_VECTOR (4 downto 0);
           D : out  STD_LOGIC_VECTOR (19 downto 0));
end COMPONENT;


COMPONENT FSMCONTROL is
    Port ( SM : out  STD_LOGIC;
           SDOPC : out  STD_LOGIC;
           TIPOR : in  STD_LOGIC;
           BEQI : in  STD_LOGIC;
           BNEQI : in  STD_LOGIC;
           BLTI : in  STD_LOGIC;
           BLETI : in  STD_LOGIC;
           BGTI : in  STD_LOGIC;
           BGETI : in  STD_LOGIC;
           EQ : in  STD_LOGIC;
           NEQ : in  STD_LOGIC;
           LT : in  STD_LOGIC;
           LE : in  STD_LOGIC;
           GTI : in  STD_LOGIC;
           GET : in  STD_LOGIC;
           CLK : in  STD_LOGIC;
           CLR : in  STD_LOGIC;
           NA : in  STD_LOGIC);
end COMPONENT;



COMPONENT DECODIFICADOR_DE_INST is
    Port ( OPCODE : in  STD_LOGIC_VECTOR (4 downto 0);
           TIPO_R : out  STD_LOGIC;
           BEQI : out  STD_LOGIC;
           BNEQI : out  STD_LOGIC;
           BLTI : out  STD_LOGIC;
           BLETI : out  STD_LOGIC;
           BGTI : out  STD_LOGIC;
           BGETI : out  STD_LOGIC);
end COMPONENT;

COMPONENT CONDI is
    Port ( RBANDERAS : in  STD_LOGIC_VECTOR (3 downto 0);
           EQ : out  STD_LOGIC;
           NEQ : out  STD_LOGIC;
           LT : out  STD_LOGIC;
           LE : out  STD_LOGIC;
           GTI : out  STD_LOGIC;
           GET : out  STD_LOGIC);
end COMPONENT;


end PAQUETITO;

package body PAQUETITO is


 
end PAQUETITO;
