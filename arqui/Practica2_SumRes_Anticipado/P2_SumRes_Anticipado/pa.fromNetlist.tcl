
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name P2_SumRes_Anticipado -dir "C:/Users/chris/Desktop/P2_SumRes_Anticipado/planAhead_run_1" -part xc7a100tcsg324-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "C:/Users/chris/Desktop/P2_SumRes_Anticipado/P2_SumRes_Anticipado.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/Users/chris/Desktop/P2_SumRes_Anticipado} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "P2_SumRes_Anticipado.ucf" [current_fileset -constrset]
add_files [list {P2_SumRes_Anticipado.ucf}] -fileset [get_property constrset [current_run]]
link_design
