library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library work;
use work.paquete_entidadesESCOMIPS.ALL;

entity PFinal_Escomips_Main is
    Port ( rclr, clk : in  STD_LOGIC;
           datos, pc : out  STD_LOGIC_VECTOR (7 downto 0));
end PFinal_Escomips_Main;

architecture Behavioral of PFinal_Escomips_Main is

signal clr: STD_LOGIC;

-- ALU
signal in_ALU_a, in_ALU_b, datos_ALU : STD_LOGIC_VECTOR (15 downto 0);
signal banderas_ALU : STD_LOGIC_VECTOR (3 downto 0);

--ARCHIVO REGISTROS
signal write_data : STD_LOGIC_VECTOR (15 downto 0);
signal read_reg2:  STD_LOGIC_VECTOR (3 downto 0);
signal read_data1, read_data2 : STD_LOGIC_VECTOR (15 downto 0);

-- MEMORIA DE DATOS
signal dir_w_MemDatos : STD_LOGIC_VECTOR (10 downto 0);
signal data_out_MemDatos : STD_LOGIC_VECTOR (15 downto 0);

-- MEMORIA DE PROGRAMA
signal dato_MemPrograma : STD_LOGIC_VECTOR (24 downto 0);

-- PILA
signal d_out_Pila : STD_LOGIC_VECTOR (15 downto 0);

-- EXTENSOR DE DIRECCION
signal d_out_extDir : STD_LOGIC_VECTOR (15 downto 0);

-- EXTENSOR DE SIGNO
signal d_out_extSig : STD_LOGIC_VECTOR (15 downto 0);

-- UNIDAD DE CONTROL
signal s_UC : STD_LOGIC_VECTOR (19 downto 0);
-- s_UC(0) = SR
-- s_UC(1) = WD
-- s_UC(2) = SDMD
-- s_UC(3) = ALUOP(0)
-- s_UC(4) = ALUOP(1)
-- s_UC(5) = ALUOP(2)
-- s_UC(6) = ALUOP(3)
-- s_UC(7) = SOP2
-- s_UC(8) = SOP1
-- s_UC(9) = SEXT
-- s_UC(10) = LF
-- s_UC(11) = WR
-- s_UC(12) = DIR
-- s_UC(13) = SHE
-- s_UC(14) = SWD
-- s_UC(15) = SR2
-- s_UC(16) = SDMP
-- s_UC(17) = WPC
-- s_UC(18) = DW
-- s_UC(19) = UP

-- MUX
signal d_SR, d_SDMP, d_SEXT : STD_LOGIC_VECTOR (15 downto 0);

begin
	
	--MUXES
	-- MUX SR
	d_SR <= data_out_MemDatos when s_UC(0) = '0'
		else datos_ALU;
	-- MUX SDMP
	d_SDMP <= dato_MemPrograma(15 downto 0) when s_UC(16) = '0' 
		else d_SR;
	-- MUX SR2
	read_reg2 <= dato_MemPrograma(11 downto 8) when s_UC(15) = '0'
		else dato_MemPrograma(19 downto 16);
	-- MUX SWD
	write_data <= dato_MemPrograma(15 downto 0) when s_UC(14) = '0'
		else d_SR;
	-- SEXT
	d_SEXT <= d_out_extSig when s_UC(9) = '0'
		else d_out_extDir;
	-- SOP1
	in_ALU_a <= read_data1 when s_UC(8) = '0'
		else d_out_Pila;
	-- SOP2
	in_ALU_b <= read_data2 when s_UC(7) = '0'
		else d_SEXT;
	-- SDMD
	dir_w_MemDatos <= datos_ALU(10 downto 0) when s_UC(2) = '0'
		else dato_MemPrograma(10 downto 0);
		
	e_FF_clr : FlipFlop_clr port map (
		rclr => rclr,
		clr => clr,
		clk => clk
	);
					
	e_ALU : P3_ALU_Nbits port map (
		a => in_ALU_a,
		b => in_ALU_b,
		a_sel => s_UC(6),--ALUOP(3)
		b_sel => s_UC(5), --ALUOP(2)
		op =>  s_UC(4 downto 3),--ALUOP(1-0)
		s => datos_ALU,
		flags => banderas_ALU
	);
	
	e_ArchivoRegistros : P4_ArchivoRegistros port map (
		write_data => write_data,
		write_reg => dato_MemPrograma(19 downto 16),
		read_reg1 => dato_MemPrograma(15 downto 12),
		read_reg2 => read_reg2,
		shamt => dato_MemPrograma(7 downto 4),
		clk => clk,
		clr => clr,
		wr => s_UC(11),
		she => s_UC(13),
		dir => s_UC(12),
		read_data1 => read_data1,
		read_data2 => read_data2
	);
	
	e_MemoriaDatos : P5_MemoriaDatos port map (
		dir_w => dir_w_MemDatos,
		data_in => read_data2,
		clk => clk,
		wd => s_UC(1),
		data_out => data_out_MemDatos
	);
	
	e_MemoriaPrograma : P6_MemoriaPrograma port map (
		dir => d_out_Pila(10 downto 0),
		dato => dato_MemPrograma
	);
	
	e_Pila : P7_Pila port map (
		d_in => d_SDMP,
		dw => s_UC(18),
		wpc => s_UC(17),
		up => s_UC(19),
		clk => clk,
		clr => clr,
		d_out => d_out_Pila
	);
	
	e_extensorDireccion : extensorDireccion port map (
		d_in => dato_MemPrograma(11 downto 0),
		d_out => d_out_extDir
	);
	
	e_extensorSigno : extensorSigno port map (
		d_in => dato_MemPrograma(11 downto 0),
		d_out => d_out_extSig
	);
	
	e_unidadControl : main port map (
		clk => clk,
		clr => clr,
		lf => s_UC(10),
		a_fun => dato_MemPrograma(3 downto 0),
		banderas => banderas_ALU,
		op_code => dato_MemPrograma(24 downto 20),
		s => s_UC
	);
	
	datos <= read_data2 (7 downto 0);
	pc <= d_out_Pila (7 downto 0);

end Behavioral;

