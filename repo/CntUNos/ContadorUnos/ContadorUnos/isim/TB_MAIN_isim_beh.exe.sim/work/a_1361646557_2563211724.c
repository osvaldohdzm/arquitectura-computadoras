/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/ise/Desktop/ContadorUnos/ContadorUnos/PRINCIPAL.vhd";
extern char *IEEE_P_2592010699;

char *ieee_p_2592010699_sub_12303121079769504865_503743352(char *, char *, char *, char *, unsigned char );
char *ieee_p_2592010699_sub_24166140421859237_503743352(char *, char *, char *, char *);
unsigned char ieee_p_2592010699_sub_2763492388968962707_503743352(char *, char *, unsigned int , unsigned int );


static void work_a_1361646557_2563211724_p_0(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t4;
    unsigned int t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;

LAB0:    xsi_set_current_line(45, ng0);
    t1 = (t0 + 1992U);
    t2 = *((char **)t1);
    t1 = (t0 + 8920);
    t4 = 1;
    if (8U == 8U)
        goto LAB5;

LAB6:    t4 = 0;

LAB7:    if (t4 != 0)
        goto LAB3;

LAB4:
LAB11:    t13 = (t0 + 5512);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    *((unsigned char *)t17) = (unsigned char)2;
    xsi_driver_first_trans_fast(t13);

LAB2:    t18 = (t0 + 5400);
    *((int *)t18) = 1;

LAB1:    return;
LAB3:    t8 = (t0 + 5512);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    *((unsigned char *)t12) = (unsigned char)3;
    xsi_driver_first_trans_fast(t8);
    goto LAB2;

LAB5:    t5 = 0;

LAB8:    if (t5 < 8U)
        goto LAB9;
    else
        goto LAB7;

LAB9:    t6 = (t2 + t5);
    t7 = (t1 + t5);
    if (*((unsigned char *)t6) != *((unsigned char *)t7))
        goto LAB6;

LAB10:    t5 = (t5 + 1);
    goto LAB8;

LAB12:    goto LAB2;

}

static void work_a_1361646557_2563211724_p_1(char *t0)
{
    char t11[16];
    char t12[16];
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    unsigned int t13;
    unsigned int t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;

LAB0:    xsi_set_current_line(49, ng0);
    t1 = (t0 + 1512U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    t1 = (t0 + 1312U);
    t3 = ieee_p_2592010699_sub_2763492388968962707_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t3 != 0)
        goto LAB5;

LAB6:
LAB3:    t1 = (t0 + 5416);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(50, ng0);
    t1 = (t0 + 8928);
    t6 = (t0 + 5576);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    memcpy(t10, t1, 4U);
    xsi_driver_first_trans_fast_port(t6);
    goto LAB3;

LAB5:    xsi_set_current_line(52, ng0);
    t2 = (t0 + 1672U);
    t5 = *((char **)t2);
    t2 = (t0 + 8784U);
    t6 = ieee_p_2592010699_sub_12303121079769504865_503743352(IEEE_P_2592010699, t12, t5, t2, (unsigned char)0);
    t7 = (t12 + 12U);
    t13 = *((unsigned int *)t7);
    t13 = (t13 * 1U);
    t8 = xsi_vhdl_bitvec_rol(t8, t6, t13, 1);
    t9 = ieee_p_2592010699_sub_24166140421859237_503743352(IEEE_P_2592010699, t11, t8, t12);
    t10 = (t11 + 12U);
    t14 = *((unsigned int *)t10);
    t14 = (t14 * 1U);
    t4 = (4U != t14);
    if (t4 == 1)
        goto LAB7;

LAB8:    t15 = (t0 + 5576);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    memcpy(t19, t9, 4U);
    xsi_driver_first_trans_fast_port(t15);
    goto LAB3;

LAB7:    xsi_size_not_matching(4U, t14, 0);
    goto LAB8;

}

static void work_a_1361646557_2563211724_p_2(char *t0)
{
    char t6[16];
    char t12[16];
    char t14[16];
    char *t1;
    char *t2;
    char *t3;
    unsigned char t4;
    char *t5;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t13;
    char *t15;
    char *t16;
    int t17;
    unsigned int t18;
    int t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;

LAB0:    t1 = (t0 + 5080U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(56, ng0);
    t2 = (t0 + 2952U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t2 = (t0 + 1672U);
    t5 = *((char **)t2);
    t7 = ((IEEE_P_2592010699) + 4000);
    t8 = (t0 + 8784U);
    t2 = xsi_base_array_concat(t2, t6, t7, (char)99, t4, (char)97, t5, t8, (char)101);
    t9 = (t0 + 8932);
    t13 = ((IEEE_P_2592010699) + 4000);
    t15 = (t14 + 0U);
    t16 = (t15 + 0U);
    *((int *)t16) = 0;
    t16 = (t15 + 4U);
    *((int *)t16) = 3;
    t16 = (t15 + 8U);
    *((int *)t16) = 1;
    t17 = (3 - 0);
    t18 = (t17 * 1);
    t18 = (t18 + 1);
    t16 = (t15 + 12U);
    *((unsigned int *)t16) = t18;
    t11 = xsi_base_array_concat(t11, t12, t13, (char)99, (unsigned char)3, (char)97, t9, t14, (char)101);
    t18 = (1U + 4U);
    t19 = xsi_mem_cmp(t11, t2, t18);
    if (t19 == 1)
        goto LAB5;

LAB7:
LAB6:    xsi_set_current_line(57, ng0);
    t2 = (t0 + 8936);
    t5 = (t0 + 5640);
    t7 = (t5 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    memcpy(t10, t2, 7U);
    xsi_driver_first_trans_fast_port(t5);

LAB4:    xsi_set_current_line(56, ng0);

LAB11:    t2 = (t0 + 5432);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB12;

LAB1:    return;
LAB5:    xsi_set_current_line(57, ng0);
    t16 = (t0 + 3272U);
    t20 = *((char **)t16);
    t16 = (t0 + 5640);
    t21 = (t16 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    memcpy(t24, t20, 7U);
    xsi_driver_first_trans_fast_port(t16);
    goto LAB4;

LAB8:;
LAB9:    t3 = (t0 + 5432);
    *((int *)t3) = 0;
    goto LAB2;

LAB10:    goto LAB9;

LAB12:    goto LAB10;

}


extern void work_a_1361646557_2563211724_init()
{
	static char *pe[] = {(void *)work_a_1361646557_2563211724_p_0,(void *)work_a_1361646557_2563211724_p_1,(void *)work_a_1361646557_2563211724_p_2};
	xsi_register_didat("work_a_1361646557_2563211724", "isim/TB_MAIN_isim_beh.exe.sim/work/a_1361646557_2563211724.didat");
	xsi_register_executes(pe);
}
