LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

LIBRARY STD;
USE STD.TEXTIO.ALL;
USE ieee.std_logic_TEXTIO.ALL;

ENTITY P6_Pila_TB_Archivos IS
END P6_Pila_TB_Archivos;
 
ARCHITECTURE behavior OF P6_Pila_TB_Archivos IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT P6_Pila
    PORT(
         pcData : IN  std_logic_vector(10 downto 0);
         contadorPrograma : OUT  std_logic_vector(10 downto 0);
         stackPointer : OUT  std_logic_vector(2 downto 0);
         L : IN  std_logic;
         UP : IN  std_logic;
         DW : IN  std_logic;
         clk : IN  std_logic;
         clr : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal pcData : std_logic_vector(10 downto 0) := (others => '0');
   signal L : std_logic := '0';
   signal UP : std_logic := '0';
   signal DW : std_logic := '0';
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';

 	--Outputs
   signal contadorPrograma : std_logic_vector(10 downto 0);
   signal stackPointer : std_logic_vector(2 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: P6_Pila PORT MAP (
          pcData => pcData,
          contadorPrograma => contadorPrograma,
          stackPointer => stackPointer,
          L => L,
          UP => UP,
          DW => DW,
          clk => clk,
          clr => clr
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

	-- Stimulus process
	stim_proc: process

		-- Archivo entradas.txt
		FILE			inputFile	: TEXT;
		VARIABLE		inputLine	: LINE;
		
		-- Variables a procesar
		VARIABLE var_pcData						: STD_LOGIC_VECTOR (15 DOWNTO 0);
		VARIABLE var_L								: STD_LOGIC;
		VARIABLE var_UP							: STD_LOGIC;
		VARIABLE var_DW							: STD_LOGIC;
		VARIABLE var_clr							: STD_LOGIC;
		
		VARIABLE var_contadorPrograma			: STD_LOGIC_VECTOR (11 DOWNTO 0);
		VARIABLE var_stackPointer				: STD_LOGIC_VECTOR (2 DOWNTO 0);
		
		-- Archivo salidas.txt
		FILE			outputFile	: TEXT;
		VARIABLE		outputLine	: LINE;

		VARIABLE line				: STRING (1 TO 5);
		
		begin
			-- Se abren los archivos a usar
			FILE_OPEN (inputFile, "entradas.txt", READ_MODE);
			FILE_OPEN (outputFile, "salida.txt", WRITE_MODE);
			
			line := "  D  ";
			WRITE(outputLine, line, RIGHT, line'LENGTH);
			
			line := "  UP ";
			WRITE(outputLine, line, RIGHT, line'LENGTH);
			
			line := "  DW ";
			WRITE(outputLine, line, RIGHT, line'LENGTH);
			
			line := "  L  ";
			WRITE(outputLine, line, RIGHT, line'LENGTH);
			
			line := " CLR ";
			WRITE(outputLine, line, RIGHT, line'LENGTH);
			
			line := "  SP ";
			WRITE(outputLine, line, RIGHT, line'LENGTH);
			
			line := "  PC ";
			WRITE(outputLine, line, RIGHT, line'LENGTH);
			
			WRITELINE(outputFile, outputLine);
			
			WAIT FOR 20 NS;
			
			-- Loop de Lectura
			WHILE NOT ENDFILE (inputFile) LOOP
				READLINE (inputFile, inputLine);
				
				HREAD(inputLine, var_pcData);
				pcData <= var_pcData(10 downto 0);
				READ(inputLine, var_UP);
				UP <= var_UP;
				READ(inputLine, var_DW);
				DW <= var_DW;
				READ(inputLine, var_L);
				L <= var_L;
				READ(inputLine, var_clr);
				clr <= var_clr;
				
				WAIT UNTIL RISING_EDGE ( clk );
				
				var_contadorPrograma := '0'&contadorPrograma; --Agregamos el 0 para que sea multiplo de 4
				var_stackPointer := stackPointer;
				
				-- Linea de Escritura en archivo
				HWRITE(outputLine, var_pcData, RIGHT, 5);
				WRITE(outputLine, var_UP, RIGHT, 5);
				WRITE(outputLine, var_DW, RIGHT, 5);
				WRITE(outputLine, var_L, RIGHT, 5);
				WRITE(outputLine, var_clr, RIGHT, 5);
				WRITE(outputLine, var_stackPointer, RIGHT, 5);
				HWRITE(outputLine, var_contadorPrograma, RIGHT, 5);
				--Escritura al archivo
				WRITELINE(outputFile, outputLine);
			END LOOP;
		
			-- Cerrar Archivos
			FILE_CLOSE(inputFile);
			FILE_CLOSE(outputFile);
			
			WAIT;
	end process;
END;