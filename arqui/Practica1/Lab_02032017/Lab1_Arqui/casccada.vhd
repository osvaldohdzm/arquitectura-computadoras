library ieee;

use IEEE.STD_LOGIC_1164.ALL;

entity cascada is
    
	Port ( cin : in  STD_LOGIC; 
	--cin dice si es suma0 o resta1       
		a : in  STD_LOGIC_VECTOR (3 downto 0);     
		b : in  STD_LOGIC_VECTOR (3 downto 0);   
		s: out  STD_LOGIC_VECTOR (3 downto 0);   
		cout : out  STD_LOGIC);

end cascada;

architecture Behavioral of cascada is
	signal Eb:STD_LOGIC_VECTOR (3 downto 0);
	signal c : STD_LOGIC_VECTOR (4 downto 0);
begin
	c(0) <= cin;
	sumaResta: for i in 0 to 3 generate	
		Eb(i)<= B(i)xor cin;			
		s(i) <= a(i)xor Eb(i) xor c(i);
		c(i+1)<= (Eb(i)and c(i)) or (a(i) and c(i)) or (a(i) and Eb(i));
	end generate sumaResta;
cout <= c(4);

end Behavioral;