library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ALU_1bit is
    Port ( a, b, sel_a, sel_b, cin: in  STD_LOGIC;
           op : in  STD_LOGIC_VECTOR (1 downto 0);
           res, cout : out  STD_LOGIC);
end ALU_1bit;

architecture Behavioral of ALU_1bit is

	component sumador is Port(
		a, b, cin : in STD_LOGIC;
		s, cout : out STD_LOGIC);
	end component;

	signal res_a, res_b, res_and, res_or, res_xor, res_sum : STD_LOGIC;

begin
	res_a <= a xor sel_a;
	res_b <= b xor sel_b;
	
	res_and <= res_a and res_b;
	res_or <= res_a or res_b;
	res_xor <= res_a xor res_b;
	
	sum : sumador port map
	(
		a => res_a,
		b => res_b,
		cin => cin,
		s => res_sum,
		cout => cout
	);
	
	process(res_and, res_or, res_xor, res_sum, op)
	begin
		if(op = "00") then
			res <= res_and;
		elsif(op = "01") then
			res <= res_or;
		elsif(op = "10") then
			res <= res_xor;
		else
			res <= res_sum;
		end if;
	end process;
	
end Behavioral;

