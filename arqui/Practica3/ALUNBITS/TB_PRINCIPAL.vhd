--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   23:34:05 09/12/2018
-- Design Name:   
-- Module Name:   C:/Users/leonardo faydella/Desktop/ProyectoFinal/ALUNBITS/TB_PRINCIPAL.vhd
-- Project Name:  ALUNBITS
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Principal
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TB_PRINCIPAL IS
END TB_PRINCIPAL;
 
ARCHITECTURE behavior OF TB_PRINCIPAL IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Principal
    PORT(
         a : IN  std_logic_vector(3 downto 0);
         b : IN  std_logic_vector(3 downto 0);
         ALUOP : IN  std_logic_vector(3 downto 0);
         S : OUT  std_logic_vector(3 downto 0);
         C : OUT  std_logic;
         Z : OUT  std_logic;
         OV : OUT  std_logic;
         N : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(3 downto 0) := (others => '0');
   signal b : std_logic_vector(3 downto 0) := (others => '0');
   signal ALUOP : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal S : std_logic_vector(3 downto 0);
   signal C : std_logic;
   signal Z : std_logic;
   signal OV : std_logic;
   signal N : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 

 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Principal PORT MAP (
          a => a,
          b => b,
          ALUOP => ALUOP,
          S => S,
          C => C,
          Z => Z,
          OV => OV,
          N => N
        );

 

   -- Stimulus process
   stim_proc: process
   begin		
		ALUOP <= "0011"; --suma 5+(-2)
		a <= "0101";
		b <= "1110";
      wait for 100 ns;	
		ALUOP <= "0111"; --resta 5-(-2)
      wait for 100 ns;
		ALUOP <= "0000";-- and 5 & -2
      wait for 100 ns;
		ALUOP <= "0001";--or 5 or -2
      wait for 100 ns;
		ALUOP <= "0010";--xor 5 xor -2
      wait for 100 ns;
		
		ALUOP <= "0011"; --suma 5+(7)
		a <= "0101";
		b <= "0111";
		
		wait for 100 ns;
		
		ALUOP <= "1101"; --suma 5-(7)
		a <= "0101";
		b <= "0101";
		wait for 100 ns;
		ALUOP <= "0111"; --suma 5-(7)
		

 

      wait;
   end process;

END;
