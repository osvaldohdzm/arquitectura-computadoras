library IEEE;
use IEEE.STD_LOGIC_1164.all;

package paquete_entidades is
	component UnidadControl is
		 Port ( clr, clk, tipoR, BEQ, BNEQ, BLT, BLE, BGT, BGET, na, eq, neq, lt, le, gt, get : in  STD_LOGIC;
				  SDOPC, SM : out  STD_LOGIC);
	end component;
	
	component condicion is
		 Port ( banderas : in  STD_LOGIC_VECTOR (3 downto 0);
				  eq, neq, lt, le, gt, get : out  STD_LOGIC);
	end component;
	
	component decoInst is
		 Port ( op_code : in  STD_LOGIC_VECTOR (4 downto 0);
				  tipoR, BEQ, BNEQ, BLT, BLE, BGT,  BGET : out  STD_LOGIC);
	end component;
	
	component microcodigoFuncion is
		 Port ( a : in  STD_LOGIC_VECTOR (3 downto 0);
				  d : out  STD_LOGIC_VECTOR (19 downto 0));
	end component;
	
	component microcodigoOpCode is
		 Port ( a : in  STD_LOGIC_VECTOR (4 downto 0);
				  d : out  STD_LOGIC_VECTOR (19 downto 0));
	end component;
	
	component mux_opCode is
		 Port ( op_code : in  STD_LOGIC_VECTOR (4 downto 0);
				  SDOPC : in  STD_LOGIC;
				  a : out  STD_LOGIC_VECTOR (4 downto 0));
	end component;
	
	component mux_salida is
		 Port ( d_fun, d_op_code : in  STD_LOGIC_VECTOR (19 downto 0);
				  SM : in  STD_LOGIC;
				  s : out  STD_LOGIC_VECTOR (19 downto 0));
	end component;
	
	component nivel is
		 Port ( clk, clr : in  STD_LOGIC;
				  na : out  STD_LOGIC);
	end component;
	
	component reg_edo is
		 Port ( clk, clr, lf : in  STD_LOGIC;
				  banderas : in  STD_LOGIC_VECTOR (3 downto 0);
				  b_out : out  STD_LOGIC_VECTOR (3 downto 0));
	end component;
end paquete_entidades;
