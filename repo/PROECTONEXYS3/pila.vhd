
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;


entity pila is
    Port ( UP,DW,WPC,CLK,CLR : in  STD_LOGIC;
           D : in  STD_LOGIC_VECTOR (15 downto 0);
           PC : out  STD_LOGIC_VECTOR (15 downto 0);
           SP_AUX : out  STD_LOGIC_VECTOR (2 downto 0));
end pila;

architecture Behavioral of pila is

TYPE ARREGLO IS ARRAY  (0 to 7) OF STD_LOGIC_VECTOR(15 DOWNTO 0); 
begin


	STACK : PROCESS(CLK, CLR)
	VARIABLE SP : std_logic_vector(2 downto 0);
	VARIABLE PPC : ARREGLO; 
	BEGIN 
		IF(CLR = '1')THEN
			PPC := (OTHERS=>(OTHERS=>'0'));
			SP := (others=>'0');
		ELSIF(RISING_EDGE(CLK)) THEN
			IF(UP = '0' AND DW = '0' AND WPC = '1')THEN   -- SALTOS B, BGETI,BLGTI,BGTI, ETC,ETC
				PPC(conv_integer(SP)):=D;
			ELSIF(UP = '1' AND DW = '0' AND WPC = '1')THEN -- CALL
				SP := SP+1;
				PPC(conv_integer(SP)):=D;
			ELSIF (UP = '0' AND DW ='1' AND WPC = '0') THEN --RET
				SP:= SP-1;
				PPC(conv_integer(SP)):=PPC(conv_integer(SP))+1;
			ELSIF (UP = '0' AND DW='0' AND WPC='0') THEN -- INCREMENTOS
				PPC(conv_integer(SP)):=PPC(conv_integer(SP))+1;	
			END IF;
		END IF;
		
		PC <= PPC(conv_integer(SP));
		SP_AUX <= SP;

	END PROCESS STACK;

end Behavioral;

